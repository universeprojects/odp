package com.universeprojects.log.shared;

import com.universeprojects.common.shared.log.LogProcessor;


public abstract class BaseLogProcessor extends LogProcessor {

    protected UPLoggerConfigManager loggerConfigManager = createLoggerConfigManager();

    protected UPLoggerConfigManager createLoggerConfigManager() {
        return new UPLoggerConfigManager();
    }

    @Override
    public UPLoggerConfigManager getLoggerConfigManager() {
        return loggerConfigManager;
    }
}
