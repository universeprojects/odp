package org.slf4j.impl;

import com.universeprojects.common.shared.log.MarkerFactory;
import org.slf4j.IMarkerFactory;
import org.slf4j.spi.MarkerFactoryBinder;


@SuppressWarnings("unused")
public class StaticMarkerBinder implements MarkerFactoryBinder {

    public static StaticMarkerBinder SINGLETON = new StaticMarkerBinder();

    @Override
    public IMarkerFactory getMarkerFactory() {
        return MarkerFactory.getInstance();
    }

    @Override
    public String getMarkerFactoryClassStr() {
        return MarkerFactory.class.getCanonicalName();
    }
}
