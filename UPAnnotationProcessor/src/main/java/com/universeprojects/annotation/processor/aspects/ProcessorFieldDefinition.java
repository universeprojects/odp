package com.universeprojects.annotation.processor.aspects;

import java.util.Map;

public class ProcessorFieldDefinition {

    final String name;
    final String description;
    final Map<String, String> editorConfig;
    final String fieldTypeClass;
    final FieldType fieldType;
    final String param1;
    final String param2;

    public ProcessorFieldDefinition(String name, String description, Map<String, String> editorConfig, String fieldTypeClass, FieldType fieldType, String param1, String param2) {
        this.name = name;
        this.description = description;
        this.editorConfig = editorConfig;
        this.fieldTypeClass = fieldTypeClass;
        this.fieldType = fieldType;
        this.param1 = param1;
        this.param2 = param2;
    }

    public boolean isList() {
        return fieldType == FieldType.LIST;
    }

    public boolean isMap() {
        return fieldType == FieldType.MAP;
    }

    public boolean isLink() {
        return fieldType == FieldType.LINK;
    }

    public String getName() {
        return name;
    }

    public String getFieldTypeClass() {
        return fieldTypeClass;
    }

    public String getParam1() {
        return param1;
    }

    public String getParam2() {
        return param2;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, String> getEditorConfig() {
        return editorConfig;
    }
}
