package com.universeprojects.annotation.processor.metaClasses;

import com.universeprojects.common.shared.annotations.AutoSerializable;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedOptions;
import javax.annotation.processing.SupportedSourceVersion;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import java.util.Set;

@SupportedAnnotationTypes("com.universeprojects.common.shared.annotations.AutoSerializable")
@SupportedSourceVersion(SourceVersion.RELEASE_8)
@SupportedOptions({"autoSerializerInterface","autoSerializerClass","autoSerializerPackage"})
public class AutoSerializerProcessor extends AbstractProcessor {

    public static final String GENERATED_PACKAGE = "com.universeprojects.json.shared.serialization";
    public static final String GENERATED_NAME = "AutoSerializerGenerated";

    @Override
    public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv) {
        return MetaClassGenerator.INSTANCE.generateProcessor(annotations, roundEnv, processingEnv,
                GENERATED_PACKAGE, GENERATED_NAME, "com.universeprojects.json.shared.serialization.AutoSerializer",
                "autoSerializer", AutoSerializable.class, "AutoSerializerTemplate.vm");
    }


}
