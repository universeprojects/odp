package com.universeprojects.annotation.processor;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import javax.annotation.processing.ProcessingEnvironment;
import javax.tools.JavaFileObject;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import java.util.Properties;

public class TemplateGenerator {
    public static final TemplateGenerator INSTANCE = new TemplateGenerator();

    public boolean generateTemplateProcessor(ProcessingEnvironment processingEnv ,
                                                    String defaultPackageName, String defaultClassName, String defaultInterfaceName, String optionPrefix, String templateName, Map<String, Object> params) {
        String packageName = processingEnv.getOptions().get(optionPrefix+"Package");
        String className = processingEnv.getOptions().get(optionPrefix+"Class");
        String interfaceName = processingEnv.getOptions().get(optionPrefix+"Interface");
        String extensionClassName = processingEnv.getOptions().get(optionPrefix+"ExtensionClass");
        if(packageName == null) {
            packageName = defaultPackageName;
        }
        if(className == null) {
            className = defaultClassName;
        }
        if(interfaceName == null) {
            interfaceName = defaultInterfaceName;
        }
        String fullClassName = packageName+"."+className;
        JavaFileObject fileObject;
        Writer writer;
        try {
            fileObject = processingEnv.getFiler().createSourceFile(fullClassName);
            writer = fileObject.openWriter();
        } catch (IOException e) {
            return true;
            //throw new RuntimeException(e);
        }

        Properties velocityProperties = new Properties();
        velocityProperties.setProperty("resource.loader","class");
        velocityProperties.setProperty("class.resource.loader.description","Velocity Classpath Resource Loader");
        velocityProperties.setProperty("class.resource.loader.class","org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        Velocity.init(velocityProperties);
        VelocityContext velocityContext = new VelocityContext();
        for(Map.Entry<String,Object> param : params.entrySet()) {
            velocityContext.put(param.getKey(), param.getValue());
        }

        velocityContext.put("packageName", packageName);
        velocityContext.put("className", className);
        velocityContext.put("interfaceName", interfaceName);
        velocityContext.put("extensionClassName", extensionClassName);
        Template template = Velocity.getTemplate(templateName);
        template.merge(velocityContext, writer);
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return true;
    }
}
