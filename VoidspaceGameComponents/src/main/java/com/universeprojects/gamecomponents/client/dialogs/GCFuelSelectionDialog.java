package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.dialogs.station.store.*;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.vsdata.shared.CategorizedStoreItemData;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

import java.util.List;
import java.util.ArrayList;

public class GCFuelSelectionDialog extends GCSimpleWindow {

    private final static int WINDOW_W = 300;
    private final static int WINDOW_H = 500;
    private final H5EScrollablePane scrollablePane;
    private H5ELayer layer;
    private GCListItemActionHandler<GCStoreListItem> handler;
    private GCList<GCStoreListItem> itemList;

    public GCFuelSelectionDialog(H5ELayer layer, Callable1Args<GCStoreListItemData> onSelect) {
        super(layer, "fuelSelection", "Select fuel", WINDOW_W, WINDOW_H, false);
        this.layer = layer;
        positionProportionally(0.65f, 0.65f);
        setFullscreenOnMobile(true);
        setModal(true);
        scrollablePane = new H5EScrollablePane(layer);
        add(scrollablePane).grow();
        itemList = new GCList(layer, 1, 5, 1, true);
        scrollablePane.add(itemList).grow();
        scrollablePane.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        handler = new GCListItemActionHandler<GCStoreListItem>() {
            @Override
            protected void onSelectionUpdate(GCStoreListItem lastSelectedItem) {
                GCStoreListItem item = this.getSelectedItem();
                if (item != null) {
                    onSelect.call(item.getEntryData());
                    close();
                }
            }
        };
        enableNavigation();
    }

    public void setData(List<GCStoreListItemData> items) {
        itemList.clear();
        for (GCStoreListItemData itemData : items) {
            itemList.addItem(new GCStoreListItem(layer, handler, itemData));
        }
    }

}