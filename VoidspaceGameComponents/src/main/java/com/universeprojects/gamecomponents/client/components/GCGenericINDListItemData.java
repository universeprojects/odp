package com.universeprojects.gamecomponents.client.components;

public class GCGenericINDListItemData<T> {
    public T data;
    public String name;
    public String description;
    public String iconKey;
    public int quantity;

    public GCGenericINDListItemData(String name, String iconKey, String description, T data) {
        this(name, iconKey, description, 1, data);
    }

    public GCGenericINDListItemData(String name, String iconKey, String description, int quantity, T data) {
        this.name = name;
        this.iconKey = iconKey;
        this.description = description;
        this.quantity = quantity;
        this.data = data;
    }
}
