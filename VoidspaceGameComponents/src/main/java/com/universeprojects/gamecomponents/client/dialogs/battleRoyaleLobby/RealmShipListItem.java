package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.inputs.H5EButtonInvalidateListener;

public class RealmShipListItem extends Button {

    private final RealmShipData dataItem;
    private final H5EIcon shipIcon;

    public RealmShipListItem(H5ELayer layer, RealmShipData data) {
        super(layer.getEngine().getSkin(), "gc-list-item");
        addListener(new H5EButtonInvalidateListener(this));
        this.dataItem = data;

        // Ship Icon
        H5EContainer shipContainer = add(new H5EContainer(layer)).size(64).pad(2).getActor();
        shipContainer.setBackground("purchase-dialog-subpanel");

        if (data.mainIconURL == null || data.mainIconURL.length() == 0) {
            data.mainIconURL = "voidspace"; // just need something here so it doesn't crash
        }
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(data.mainIconURL);
        shipIcon = new H5EIcon(layer,spriteType);
        shipIcon.setScaling(Scaling.fit);
        shipContainer.add(shipIcon).grow();
        spriteType.subscribeToLoadStateChange((state)-> shipIcon.setDrawable(new TextureRegionDrawable(spriteType.getGraphicData())));

        if (!data.owned) {
            shipContainer.setColor(1.0f,0.25f,0.25f,1);
            shipIcon.setColor(1.0f,0.25f,0.25f,1);
        }
    }

    public RealmShipData getDataItem() {
        return dataItem;
    }
}