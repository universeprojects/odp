package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarButton;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarController;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndTarget;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.List;

public class GamepadDragAndDrop extends DragAndDrop {

    private final List<Target> targets = new ArrayList<>();
    private final Vector2 currentPosVec = new Vector2();
    private final Vector2 nearestPosVec = new Vector2();
    private Target currentTarget;
    private Source currentSource;
    private Payload currentPayload;

    public void move(UINavigation.Direction direction) {
        float minDistance = Float.MAX_VALUE;
        currentTarget.getActor().localToStageCoordinates(currentPosVec.set(currentTarget.getActor().getWidth() / 2, currentTarget.getActor().getHeight() / 2));
        Target nearest = currentTarget;
        for (Target target : targets) {
            target.getActor().localToStageCoordinates(nearestPosVec.set(target.getActor().getWidth() / 2, target.getActor().getHeight() / 2));
            boolean isNear = false;
            switch (direction) {
                case UP:
                    isNear = nearestPosVec.y > currentPosVec.y;
                    break;
                case DOWN:
                    isNear = nearestPosVec.y < currentPosVec.y;
                    break;
                case LEFT:
                    isNear = nearestPosVec.x < currentPosVec.x;
                    break;
                case RIGHT:
                    isNear = nearestPosVec.x > currentPosVec.x;
                    break;
            }
            if (isNear) {
                if (currentTarget != target && isVisible(target.getActor()) && currentPosVec.dst(nearestPosVec) < minDistance) {
                    if (target.drag(currentSource, currentPayload, 0, 0, 0)) {
                        minDistance = currentPosVec.dst(nearestPosVec);
                        nearest = target;
                    } else {
                        target.reset(currentSource, currentPayload);
                    }
                }
            }
        }
        currentTarget = nearest;
        moved();
    }

    public void moved() {
        if (currentPayload != null && currentTarget != null) {
            currentTarget.getActor().localToStageCoordinates(currentPosVec.set(0, currentTarget.getActor().getHeight()));
            currentPayload.getValidDragActor().setPosition(currentPosVec.x, currentPosVec.y, Align.center);
        }
    }

    @Override
    public void addTarget(Target target) {
        super.addTarget(target);
        targets.add(target);
    }

    @Override
    public void removeTarget(Target target) {
        super.removeTarget(target);
        targets.remove(target);
    }

    public void notifyTargetsDrag(Source source, Payload payload) {
        if (payload != null) {
            for (int i = 0; i < targets.size(); i++) {
                if (targets.get(i) instanceof GCActionBarDndTarget) {
                    ((GCActionBarDndTarget) targets.get(i)).notifyDrag(source, payload);
                }
            }
        }
    }

    public void notifyTargetsDrop() {
        for (Target target : targets) {
            if (target instanceof GCActionBarDndTarget) {
                ((GCActionBarDndTarget) target).notifyDrop();
            }
        }
    }

    public void startDrag(Source source, H5ELayer layer) {
        currentSource = source;
        currentPayload = source.dragStart(new InputEvent(), 0, 0, 0);
        layer.addActorToTop(currentPayload.getValidDragActor());
        for (Target target : targets) {
            if (target.getActor() == source.getActor()) {
                currentTarget = target;
                break;
            } else if (target.getActor().isVisible() && target.drag(currentSource, currentPayload, 0, 0, 0)) {
                currentTarget = target;
            }
        }
        moved();
    }

    public void drop() {
        if (isDragging()) {
            currentPayload.getValidDragActor().setVisible(false);
            currentPayload.getValidDragActor().remove();
            currentSource.dragStop(new InputEvent(), 0, 0, 0, currentPayload, currentTarget);
            currentTarget.drop(currentSource, currentPayload, 0, 0, 0);
            currentTarget = null;
            currentPayload = null;
            currentSource = null;
        }
    }

    @Override
    public boolean isDragging() {
        return currentPayload != null;
    }

    @Override
    public void clear() {
        super.clear();
        targets.clear();
    }

    private boolean isVisible(Actor actor) {
        if (actor.getParent() != null)
            return actor.isVisible() && isVisible(actor.getParent());
        else {
            return actor.isVisible();
        }
    }

    public Target getCurrentTarget() {
        return currentTarget;
    }

}
