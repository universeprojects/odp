package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.shared.UPUtils;

public class NavigationShortcut<T extends Actor> {
    public enum Trigger {
        MoveLeft, MoveRight, MoveUp, MoveDown, RightTrigger, LeftTrigger, DoubleEnter, Enter, Exit, Delete
    }

    public enum Action {
        Focus, Call, ReturnActor
    }

    private Trigger trigger;
    private Action action = Action.Focus;
    private Callable0Args callback;
    private Callable1Args<T> callbackWithActor;
    protected Class<T> sourceActorType;
    protected Actor target;
    protected T source;

    public static <T extends Actor> NavigationShortcut<T> create(Class<T> type, Actor target, Trigger trigger) {
        NavigationShortcut<T> shortcut = new NavigationShortcut<T>();
        shortcut.sourceActorType = type;
        shortcut.target = target;
        shortcut.trigger = trigger;
        return shortcut;
    }

    public static <T extends Actor> NavigationShortcut<T> create(T source, Actor target, Trigger trigger) {
        NavigationShortcut<T> shortcut = new NavigationShortcut<T>();
        shortcut.source = source;
        shortcut.target = target;
        shortcut.trigger = trigger;
        return shortcut;
    }

    public boolean move(T actor, UINavigation navigation, Trigger trig) {
        if (sourceActorType != null && actor != null) {
            if (trigger == trig && UPUtils.isSubclassOf(actor.getClass(), sourceActorType)) {
                if (action == Action.Focus) {
                    focus(navigation);
                } else if (action == Action.Call) {
                    callback.call();
                } else {
                    callbackWithActor.call(actor);
                }
                return true;
            }

        } else if (source != null) {
            if (trigger == trig && source == actor) {
                if (action == Action.Focus) {
                    focus(navigation);
                } else {
                    if(callback!=null)
                    callback.call();
                }
                return true;
            }
        } else {
            if (trigger == trig) {
                if (action == Action.Focus) {
                    focus(navigation);
                } else {
                    if(callback!=null)
                    callback.call();
                }
                return true;
            }
        }
        return false;
    }

    public void focus(UINavigation navigation) {
        navigation.focusOn(target);
    }

    public NavigationShortcut<T> setAction(Callable0Args action) {
        this.action = Action.Call;
        this.callback = action;
        return this;
    }

    public NavigationShortcut<T> setAction(Callable1Args<T> action) {
        this.action = Action.ReturnActor;
        this.callbackWithActor = action;
        return this;
    }


}
