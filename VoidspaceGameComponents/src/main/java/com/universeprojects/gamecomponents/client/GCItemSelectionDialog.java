package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.components.GCGenericINDList;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItem;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;

import java.util.List;

public class GCItemSelectionDialog extends GCSimpleWindow {

    private List<GCInventoryItem> inventoryItems;
    private final GCGenericINDList<GCInventoryItem> list;
    private final H5EInputBox inputBox;
    private GCListItemActionHandler<GCGenericINDListItem<GCInventoryItem>> handler;
    public GCItemSelectionDialog(H5ELayer layer){
        super(layer, "item-selection", "Select item", 450, 700, false);
        inputBox = new H5EInputBox(layer);
        inputBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                filter(inputBox.getText());
            }
        });
        handler = new GCListItemActionHandler<GCGenericINDListItem<GCInventoryItem>>() {
            @Override
            protected void onSelectionUpdate(GCGenericINDListItem<GCInventoryItem> newItemSelection) {
                close();
                onSelect(newItemSelection.getData().data);
            }
        };
        add(inputBox).growX();
        final H5EScrollablePane pane = new H5EScrollablePane(layer);
        list = new GCGenericINDList<>(layer,1, true);
        pane.add(list).grow();
        row();
        add(pane).grow();
    }

    public void filter(String filter){
        list.clear();
        for(GCInventoryItem item:inventoryItems){
            if(item.getName().contains(filter))
                list.addItem(new GCGenericINDListItemData<>(item.getName(), item.getIconSpriteKey(), item.getDescription(), item),handler);
        }
    }
    public void loadItems(List<GCInventoryItem> items){
        this.inventoryItems = items;
        filter("");
    }

    public void onSelect(GCInventoryItem item){ }

}
