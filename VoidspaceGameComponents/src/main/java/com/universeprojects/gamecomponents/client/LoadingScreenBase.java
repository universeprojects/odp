package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

public abstract class LoadingScreenBase {
    protected SpriteBatch spriteBatch;
    protected Texture loadingBarBackground;
    protected Texture loadingBar;
    protected Texture loginBackground;
    protected Texture voidspaceLogo;

    protected Texture loginBorders;
    protected NinePatchDrawable borderPatchDrawable;

    protected int initialX;
    protected int initialY;
    protected NinePatchDrawable loadingBarDrawable;
    protected final float barWidthLimit;

    public LoadingScreenBase(){
        loadingBarBackground = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar-background-borderless.png"));
        loginBackground = new Texture(Gdx.files.internal("images/GUI/ui2/preload/startup-background1.png"));
        voidspaceLogo = new Texture(Gdx.files.internal("images/GUI/ui2/preload/startup-logo1.png"));

        loginBorders = new Texture(Gdx.files.internal("images/GUI/ui2/preload/startup-lightborders1.png"));
        NinePatch borderPatch = new NinePatch(loginBorders, 0, 0, 72, 72);
        borderPatchDrawable = new NinePatchDrawable(borderPatch);

        loadingBar = new Texture(Gdx.files.internal("images/GUI/ui2/preload/loading-bar-borderless.png"));
        NinePatch patch = new NinePatch(loadingBar, 15, 15, 0, 0);
        loadingBarDrawable = new NinePatchDrawable(patch);

        spriteBatch = new SpriteBatch();

        initialX = Gdx.graphics.getWidth() / 2 - loadingBarBackground.getWidth() / 2;
        initialY = (int) ( Gdx.graphics.getHeight() * 0.15f + loadingBar.getHeight());

        barWidthLimit = loadingBar.getWidth()+21;
    }

    public abstract void render();

    protected abstract float calculateBarWidth(float elapsedTime);


}
