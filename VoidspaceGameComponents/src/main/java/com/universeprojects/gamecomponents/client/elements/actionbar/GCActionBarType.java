package com.universeprojects.gamecomponents.client.elements.actionbar;

public enum GCActionBarType {
    MAIN(10, 9),
    SIDE(2, 1);

    private final int buttonCount;
    private final int rowCount;

    GCActionBarType(int buttonCount, int rowCount) {
        this.buttonCount = buttonCount;
        this.rowCount = rowCount;
    }

    public int getButtonCount() {
        return buttonCount;
    }

    public int getRowCount() {
        return rowCount;
    }
}
