package com.universeprojects.gamecomponents.client.auth;

import java.util.HashMap;
import java.util.Map;

public class AuthCredentials {
    private final String provider;
    private final Map<String, Object> params;

    public AuthCredentials(String provider, Map<String, Object> params) {
        this.provider = provider;
        this.params = new HashMap<>(params);
    }

    public String getProvider() {
        return provider;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    @Override
    public String toString() {
        return "AuthCredentials{" +
            "provider='" + provider + '\'' +
            ", params=" + params +
            '}';
    }
}
