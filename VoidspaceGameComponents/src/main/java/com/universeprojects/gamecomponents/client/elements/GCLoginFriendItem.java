package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCLoginFriendItem extends GCListItem {

    private final String userName;
    private final String characterName;
    private final Long charId;

    public GCLoginFriendItem(H5ELayer layer, GCLoginFriendListHandler handler, String userName, String characterName, Long charId){
        super(layer, handler);
        if(userName == null) {
            userName = "";
        }
        if(characterName == null){
            characterName = "";
        }
        this.userName = userName;
        this.characterName = characterName;
        this.charId = charId;

        final GCLabel nameLabel = add(new GCLabel(layer)).left().growX().padLeft(5).height(50).getActor();
        nameLabel.setText(this.characterName + " ("+ this.userName + ")");
        nameLabel.setAlignment(Align.left);

    }

    public String getUserName(){
        return userName;
    }

    public String getCharacterName(){
        return characterName;
    }

    public Long getCharId() {
        return charId;
    }

    public static class GCLoginFriendListHandler extends GCListItemActionHandler<GCLoginFriendItem>{

        @Override
        protected void onSelectionUpdate(GCLoginFriendItem newItemSelection) {

        }
    }
}
