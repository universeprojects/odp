package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;

public class ComponentSource<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Source {

    private final C component;

    public ComponentSource(C component) {
        super(component);
        this.component = component;
    }

    @Override
    public C getActor() {
        return component;
    }

    @Override
    public ItemPayload<K, T, C> dragStart(InputEvent event, float x, float y, int pointer) {
        if (component.isEmpty() || component.getStage() == null) {
            return null;
        }
        boolean locked = component.checkDisconnectLocked();
        if(locked) {
            return null;
        }
        component.onDragStart();
        return new ItemPayload<>(component);
    }

    @Override
    public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
        component.onDragStop();
    }
}
