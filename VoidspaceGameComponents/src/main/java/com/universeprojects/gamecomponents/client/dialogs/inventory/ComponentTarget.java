package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.List;

class ComponentTarget<K, T extends GCInventoryItem, C extends GCInventorySlotBaseComponent<K, T, C>> extends DragAndDrop.Target {
    private final C component;

    public ComponentTarget(C component) {
        super(component);
        this.component = component;
    }

    @Override
    public C getActor() {
        return component;
    }

    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (!(payload instanceof ItemPayload)) return false;
        ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
        ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
        final GCInventoryBase<?, ?, ?> sourceInventory = componentSource.getActor().inventoryBase;
        T targetItem = component.inventoryBase.getItem(component.getKey());
        if (targetItem != null) {
            if (targetItem.getInternalInventory() != null) {
                // The 'sourceInventory.canCrossInventoryDropFromHere' might be a good idea here
                // but item inventories are usually of VoidspaceInventory type (the one that player
                // inventory uses too) and I don't seem to find an item that should not be able to be put
                // in player inventory
                if (canDropToInternalInventory(itemPayload, component.inventoryBase, component.getKey()))
                    return true;
            }

            if (canMoveFluidToInternalTank(itemPayload, component.inventoryBase, component.getKey()))
                return true;
        }
        if (component.inventoryBase == sourceInventory) {
            //noinspection unchecked
            ItemPayload<K, T, C> localPayload = (ItemPayload<K, T, C>) itemPayload;
            return component.inventoryBase.canSameInventoryDrop(localPayload.getSourceComponent().getKey(), localPayload.getObject(), component.getKey());
        } else {
            return canCrossInventoryDrop(componentSource, itemPayload, component.inventoryBase, component.getKey());
        }
    }

    @SuppressWarnings({"unchecked"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
    boolean canCrossInventoryDrop(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped,
                                  GCInventoryBase<K, T, C> targetInventory, K targetKey) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();
        boolean canDrop;
        canDrop = sourceInventory.canCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), targetInventory);
        canDrop = canDrop && targetInventory.canCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), targetKey);
        return canDrop;
    }

    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> boolean canMoveFluidToInternalTank(ItemPayload<?, ?, ?> itemPayloadUntyped,
                               GCInventoryBase<K, T, C> targetInventory, K inventoryHolderKey) {
        if(component.inventoryBase instanceof GCFluidTransfer) {
            return false;
        }
        //noinspection unchecked
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;

        IT payloadObject = itemPayload.getObject();
        if (payloadObject == null || payloadObject.getInternalTank() == null)
            return false;

        T item = targetInventory.getItem(inventoryHolderKey);
        if (item == null || item.getInternalTank() == null)
            return false;

        GCInternalTank sourceTank = payloadObject.getInternalTank();
        GCInternalTank targetTank = item.getInternalTank();

        if(sourceTank == targetTank) {
            return false;
        }

        String sourceFluidType = sourceTank.getFluidType();

        if (sourceFluidType == null) return false;
        if (targetTank.isFull()) return false;
        if (sourceTank.getCurrentVolume() == 0) return false;

        String targetFluidType = targetTank.getFluidType();
        FluidTransferRecipes recipes = FluidTransferRecipes.INSTANCE;
        List<String> totalIngredients = new ArrayList<>();
        if(targetFluidType != null) {
            totalIngredients.add(sourceFluidType);
            totalIngredients.add(targetFluidType);
        }
        String recipeResult = null;
        if(totalIngredients.size() > 1) {
            recipeResult = recipes.getResultFor(totalIngredients);
        }

        if (targetFluidType == null) {
            String targetForcedType = targetTank.getForcedType();
            if(targetForcedType != null) {
                return targetForcedType.equals(sourceFluidType) || recipes.getIngredientProportionsForRecipe(totalIngredients, targetForcedType) != null;
            }
            else {
                return true;
            }
        } else {
            return targetFluidType.equals(sourceFluidType) || recipeResult != null || recipes.getIngredientProportionsForRecipe(totalIngredients, targetFluidType) != null;
        }
    }

    @SuppressWarnings({"unchecked"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>>
    boolean canDropToInternalInventory(ItemPayload<?, ?, ?> itemPayloadUntyped,
                                       GCInventoryBase<K, T, C> targetInventory, K inventoryHolderKey) {

        if(component.inventoryBase instanceof GCFluidTransfer) {
            return false;
        }

        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;

        T item = targetInventory.getItem(inventoryHolderKey);
        return item != null && item.getInternalInventory().canAcceptItem(itemPayload.getObject());
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (!(payload instanceof ItemPayload)) return;
        ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
        ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;
        final GCInventoryBase<?, ?, ?> sourceInventory = componentSource.getActor().inventoryBase;
        T targetItem = component.inventoryBase.getItem(component.getKey());
        if (targetItem != null) {
            if (targetItem.getInternalInventory() != null && canDropToInternalInventory(itemPayload, component.inventoryBase, component.getKey())) {
                onDropToInternalInventory(componentSource, itemPayload);
                return;
            }
            if (targetItem.getInternalTank() != null && canMoveFluidToInternalTank(itemPayload, component.inventoryBase, component.getKey())) {
                onMoveFluidToInternalTank(componentSource, itemPayload);
                return;
            }
        }
        if (component.inventoryBase == sourceInventory) {
            //noinspection unchecked
            ItemPayload<K, T, C> localPayload = (ItemPayload<K, T, C>) itemPayload;
            final K payloadKey = localPayload.getSourceComponent().getKey();
            if (payloadKey.equals(component.getKey())) {
                return;
            }
            component.inventoryBase.onSameInventoryDrop(payloadKey, localPayload.getObject(), component.getKey());
        } else {
            onCrossInventoryDrop(componentSource, itemPayload);
        }
    }

    @SuppressWarnings({"unchecked"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> void onCrossInventoryDrop(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();
        sourceInventory.onCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), component.inventoryBase, true);
        component.inventoryBase.onCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), component.getKey());
    }

    @SuppressWarnings({"unchecked"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> void onDropToInternalInventory(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>) componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();
        GCInternalItemInventory targetInventory = component.inventoryBase.getItem(component.getKey()).getInternalInventory();
        sourceInventory.onCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), null, targetInventory.canAutoUnlink());
        targetInventory.moveItemToHere(sourceInventory, sourceKey, itemPayload.getObject());
    }

    @SuppressWarnings({"unchecked"})
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> void onMoveFluidToInternalTank(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>) itemPayloadUntyped;

        GCInternalTank sourceTank = itemPayload.getObject().getInternalTank();
        GCInternalTank targetTank = component.inventoryBase.getItem(component.getKey()).getInternalTank();

        sourceTank.openTransferDialogTo(targetTank);
    }

    private H5ELayer getDialogLayer() {
        H5EEngine engine = ((H5ELayer) getActor().getStage()).getEngine();
        return engine.getLayer("UI");
    }
}
