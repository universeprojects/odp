package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCKnowledgeData.GCKnowledgeDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

class GCInventionKnowledgeTab extends GCInventionTab {

    private final H5EScrollablePane listContainer;
    private GCList<GCKnowledgeListItem> knowledgeList;
    private final GCListItemActionHandler<GCKnowledgeListItem> listHandler;
    public static final int LIST_SPACING = 5;

    private final GCItemInternalTree leftTree;
    private GCKnowledgeData allKnowledge;
    private GCKnowledgeData currentData;

    GCInventionKnowledgeTab(GCInventionSystemDialog dialog) {
        super(dialog, "Knowledge");
        listContainer = rightContent.add(new H5EScrollablePane(layer)).grow().getActor();
        listContainer.getContent().top().left();
        listHandler = new GCListItemActionHandler<GCKnowledgeListItem>() {
            @Override
            protected void onSelectionUpdate(GCKnowledgeListItem lastSelectedItem) {
                // do nothing
            }
        };

        H5EScrollablePane treeContainer = leftContent.add(new H5EScrollablePane(layer)).padBottom(12).grow().getActor();
        treeContainer.getContent().top().left();
        leftTree = treeContainer.add(new GCItemInternalTree(layer)).top().left().getActor();
    }

    @Override
    protected void clearTab() {
        if (knowledgeList != null) {
            listHandler.reset();
            knowledgeList.clear();
            knowledgeList.remove();
            knowledgeList = null;
            listContainer.getContent().clear();
        }
    }

    void populateData(GCKnowledgeData knowledgeData) {
        clearTab();
        this.currentData=knowledgeData;
        knowledgeList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        knowledgeList.left().top();
        listContainer.add(knowledgeList).left().top().grow();

        for (int index = 0; index < knowledgeData.size(); index++) {
            GCKnowledgeDataItem dataItem = knowledgeData.get(index);
            if(dataItem.name.toLowerCase().contains(filterText.toLowerCase())) {
                GCKnowledgeListItem listItem = new GCKnowledgeListItem(layer, listHandler, dataItem);
                knowledgeList.addItem(listItem);
            }
        }
    }

    void populateCategories(GCKnowledgeData knowledgeData, GCKnowledgeDefData defData) {
        leftTree.clearTree();
        allKnowledge = GCKnowledgeData.createNew();
        GCItemTreeSubitem leafAll = leftTree.addItem("All",() -> {
            titleLabel.setText("Knowledge: All");
            populateData(allKnowledge);
        });
        for (GCKnowledgeDefData.GCKnowledgeDefDataItem item : defData.children) {
            extractCategoryData(item, leftTree, knowledgeData);
        }
        for (int index = knowledgeData.size()-1; index >= 0; index--) {
            GCKnowledgeDataItem dataItem = knowledgeData.get(index);
            allKnowledge.add(dataItem.name, dataItem.exp, dataItem.parentKey);
        }
        leafAll.click();
    }

    private boolean extractCategoryData(GCKnowledgeDefData.GCKnowledgeDefDataItem item, GCItemInternalTree currentTree, GCKnowledgeData knowledgeData) {
        boolean hasChildren = false;
        if (item.isParentCategory) {
            GCItemTreeItem category = currentTree.addCategory(item.name);
            boolean newHasChildren = false;
            for (GCKnowledgeDefData.GCKnowledgeDefDataItem subitem : item.children) {
                boolean extractionResult = extractCategoryData(subitem, category, knowledgeData);
                if (extractionResult) newHasChildren = true;
            }
            if (!newHasChildren) {
                currentTree.removeCategory(category);
            }
            hasChildren = newHasChildren;
        } else {
            // Category is a clickable leaf
            GCKnowledgeData leafData = GCKnowledgeData.createNew();
            for (int index = knowledgeData.size()-1; index >= 0; index--) {
                GCKnowledgeDataItem dataItem = knowledgeData.get(index);
                if (dataItem.parentKey!=null && item.name!=null) {
                    if (dataItem.parentKey.equals(item.name)) {
                        hasChildren = true;
                        leafData.add(dataItem.name, dataItem.exp, dataItem.parentKey);
                        allKnowledge.add(dataItem.name, dataItem.exp, dataItem.parentKey);
                        knowledgeData.remove(index);
                    }
                }
            }
            if (hasChildren) {
                currentTree.addItem(item.name, () -> {
                    titleLabel.setText("Knowledge: " + item.name);
                    populateData(leafData);
                });
            }
        }
        return hasChildren;
    }

    @Override
    protected void onFilter() {
        super.onFilter();
        populateData(currentData);
    }
}
