package com.universeprojects.gamecomponents.client.selection;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.common.GCEffects;
import com.universeprojects.gamecomponents.client.common.MatrixUINavigation;
import com.universeprojects.gamecomponents.client.common.NavigationShortcut;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.LinkedList;
import java.util.Objects;

public class GCSelectionMenu extends Window implements GraphicElement {

    private static final Logger log = Logger.getLogger(GCSelectionMenu.class);

    final static Color FONT_COLOR = Color.valueOf("#CCCCCC");
    public static final String MENU_HEADER_UNDERLINE_KEY = "images/GUI/selector/menu-header-underline.png";
    public final Hook<Callable0Args> HOOK_VisibilityChange = Hooks.createHook0Args();
    private boolean isInRange;
    private boolean isHidden;

    private final ImageButton btnClose;
    private final ImageButton btnUp;

    private final GCSelectionMenuNode rootNode;
    private GCSelectionMenuNode currentNode;

    private final LinkedList<GCSelectionMenuItem> menuItems;

    private GCSelectionMenuItem selectedItem;

    private final LinkedList<Breadcrumb> breadcrumbs = new LinkedList<>();

    private GCSelectionMenuActionHandler actionHandler;
    private final VerticalGroup content;
    private final H5EScrollablePane scrollPane;
    private MatrixUINavigation navigation;

    private final float directoryPriority;

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        final H5ELayer layer = getLayer();
        if(layer == null) {
            return null;
        }
        return layer.getEngine();
    }

    private final static class Breadcrumb {
        final GCSelectionMenuNode node;
        final float scrollY;
        final int selectionIndex;

        Breadcrumb(GCSelectionMenuNode node, float scrollY, int selectionIndex) {
            this.node = node;
            this.scrollY = scrollY;
            this.selectionIndex = selectionIndex;
        }
    }

    public GCSelectionMenu(H5ELayer layer, int width, int height) {
        this(layer, width, height, 0);
    }

    public GCSelectionMenu(H5ELayer layer, int width, int height, float directoryPriority) {
        super("Player", Dev.checkNotNull(layer).getEngine().getSkin(), "selection-menu-window");
        setTouchable(Touchable.childrenOnly);
        setStage(layer);
        this.directoryPriority = directoryPriority;
        final H5EEngine engine = layer.getEngine();

        setWidth(width);
        setHeight(height);

        final Label titleLabel = getTitleLabel();
        titleLabel.setAlignment(Align.left);
        titleLabel.setFontScale(1.5f);

        final H5EResourceManager rm = (H5EResourceManager) engine.getResourceManager();

        final Table titleTable = getTitleTable();
        final Cell<Label> labelCell = titleTable.getCell(titleLabel);
        labelCell.padBottom(5).padTop(10).padLeft(6).left();
        labelCell.fill(false, false);
        setClip(false);
        setTransform(true);
        setKeepWithinStage(false);

        final Stack stack = new Stack();
        final Cell<Stack> btnCell = titleTable.add(stack);
        btnCell.padRight(10).padTop(0).right();
        btnClose = new ImageButton(getSkin(), "selection-menu-close-btn");
        btnClose.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                onCloseBtnClicked();
                event.handle();
            }
        });
        stack.add(btnClose);

        btnUp = new ImageButton(getSkin(), "selection-menu-up-btn");
        btnUp.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                returnToParentMenu();
                event.handle();
            }
        });
        stack.add(btnUp);

        final H5ESpriteType underlineSpriteType = rm.getSpriteType(MENU_HEADER_UNDERLINE_KEY);

        final Image headerUnderline = new Image(underlineSpriteType.getGraphicData());

        getTitleTable().row();
        getTitleTable().add(headerUnderline).fillX().colspan(2).height(2).width(width);

        padTop(40);

        content = new VerticalGroup();
        content.grow().left().top();
        scrollPane = new H5EScrollablePane(layer);
        scrollPane.getContent().add(content).top().left().grow();
        scrollPane.setTouchable(Touchable.childrenOnly);
        scrollPane.setOverscroll(false, false);
        add(scrollPane).growX().left().top().padTop(4);

        row();
        add().growY();

        menuItems = new LinkedList<>();

        rootNode = new GCSelectionMenuNode("root");

        clear();

        setVisible(false);

        Tutorials.registerCheckerObject("window:select", this);

        navigation = new MatrixUINavigation(layer);
        navigation.setWindow(this, this);
        navigation.ignoreActors(btnClose, btnUp);
        navigation.addShortcut(NavigationShortcut.create(GCSelectionMenuClickable.class, null, NavigationShortcut.Trigger.Enter).setAction(GCSelectionMenuClickable::onClick));
        navigation.addShortcut(NavigationShortcut.create((Actor)null, null, NavigationShortcut.Trigger.Exit).setAction(()->{
            if(!isAtRootLevel()){
                returnToParentMenu();
            }else{
                onCloseBtnClicked();
            }
        }));

    }

    /**
     * Returns the menu to the state that it was after construction
     */
    public void clear() {
        setSelection(null);
        breadcrumbs.clear();

        rootNode.clearChildren();
        currentNode = rootNode;

        clearMenuItems();

        hideInstantly();
    }

    /**
     * (INTERNAL) Wipes the state of the menu items
     */
    private void clearMenuItems() {
        for (GCSelectionMenuItem item : menuItems) {
            item.remove();
            Tutorials.removeObject("selection-menu:item:" + item.getName());
        }
        menuItems.clear();
        content.clearChildren();
    }

    void setSelection(GCSelectionMenuItem menuItem) {
        if (selectedItem != null) {
            selectedItem.unselect();
        }
        if (menuItem != null) {
            menuItem.select();
        }
        selectedItem = menuItem;
    }

    /**
     * This is an overridable method
     */
    protected void onCloseBtnClicked() {
        hide();
    }

    void enter(GCSelectionMenuNode childNode) {
        if (currentNode != childNode.getParent()) {
            throw new IllegalStateException("Can only enter a child node of the current node");
        }
        breadcrumbs.push(new Breadcrumb(currentNode, scrollPane.getScrollY(), childNode.getIndex()));
        load(childNode, 0, 0);
    }

    private void load(Breadcrumb b) {
        Dev.checkNotNull(b);
        load(b.node, b.scrollY, b.selectionIndex);
    }

    private void load(GCSelectionMenuNode parentNode, float scrollY, int selectionIndex) {
        Dev.checkNotNull(parentNode);

        if (!parentNode.hasChildren()) {
            clearMenuItems();
        } else {
            // wipe the current visual state
            clearMenuItems();

            for (GCSelectionMenuNode node : parentNode.getOrderedChildren()) {
                GCSelectionMenuItem item = new GCSelectionMenuItem(this, node);
                menuItems.add(item);
                content.addActor(item);
                Tutorials.registerObject("selection-menu:item:" + item.getName(), item);

            }
            setSelection(menuItems.get(selectionIndex));
            scrollPane.setScrollY(scrollY);
        }

        final boolean isRoot = parentNode.getParent() == null;
        btnClose.setVisible(isRoot);
        btnUp.setVisible(!isRoot);

        this.currentNode = parentNode;
        if(navigation!=null) {
            navigation.clear();
        }
    }

    /*
     * =======================================================
     *                                                       |
     *          PUBLIC INTERFACE STARTS HERE                 |
     *                                                       |
     * =======================================================
     *
     *
     */

    public void show(boolean inRange) {
        if (!isHidden && isInRange == inRange) {
            return; // no change
        }

        if (isHidden) {
            isHidden = false;
            toFront();

            // reset to the root when the menu is coming out of hidden state
            load(rootNode, 0, 0);
        }

        isInRange = inRange;
        log.debug("showing menu " + (inRange ? "in range" : "out of range"));
        if(navigation!=null) {
            navigation.setActiveState(true);
            navigation.clear();
            WindowManager.getInstance(getEngine()).registerNavigation(navigation);
        }


        if (inRange) {
            GCEffects.show(this);
            final boolean isRoot = currentNode != rootNode;
            btnUp.setVisible(isRoot);
            btnClose.setVisible(!isRoot);
        } else {
            GCEffects.hide(this);
        }
    }

    public void hide() {
        if (isHidden) {
            return; // already hidden
        }
        isHidden = true;
        log.debug("hiding menu");

        GCEffects.hide(this);
        if(navigation!=null) {
            navigation.setActiveState(false);
            WindowManager.getInstance(getEngine()).unregisterNavigation(navigation);
        }
    }

    public void hideInstantly() {
        if (isHidden) {
            if(hasActions()) {
                clearActions();
                setVisible(false);
            }
            return; // already hidden
        }
        isHidden = true;
        log.debug("hiding menu instantly");
        if(navigation!=null) {
            navigation.setActiveState(false);
            WindowManager.getInstance(getEngine()).unregisterNavigation(navigation);
        }
        setVisible(false);
    }

    public boolean isAtRootLevel() {
        return currentNode == rootNode;
    }

    public void returnToParentMenu() {
        if (!breadcrumbs.isEmpty()) {
            load(breadcrumbs.pop());
        }
    }

    public void setTitle(String name) {
        getTitleLabel().setText(name);
    }

    public String getTitle(){
        return getTitleLabel().getText().toString();
    }

    public boolean isCurrentTitle(String name){
        return Objects.equals(name, getTitle());
    }

    public void addItem(String path) {
        addItem(path, 1);
    }

    public void addItem(String path, float priority) {
        if (Strings.isEmpty(path)) {
            throw new IllegalArgumentException("Menu item path can't be empty");
        }
        if (!path.startsWith("/")) {
            throw new IllegalArgumentException("Menu item path must begin with a '/'");
        }
        if (path.endsWith("/")) {
            throw new IllegalArgumentException("Menu item path must not end with a '/'");
        }
        String[] tokens = path.substring(1).split("/");

        GCSelectionMenuNode node = rootNode;
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (Strings.isEmpty(token)) {
                throw new IllegalArgumentException("Menu item path can't contain empty items");
            }
            final boolean lastToken = i == tokens.length - 1;
            float nodePriority = lastToken ? priority : directoryPriority;
            node = node.getChild(token, nodePriority, true);
        }
        if (breadcrumbs.isEmpty()) {
            load(rootNode, scrollPane.getScrollY(), currentNode.getIndex());
        } else {
            load(breadcrumbs.getLast());
        }
    }

    @Override
    public void setVisible(boolean visible) {
        if (visible == isVisible())
            return;

        super.setVisible(visible);
        Hooks.call(HOOK_VisibilityChange);
        H5ELayer.invalidateBufferForActor(this);
    }

    public void setActionHandler(GCSelectionMenuActionHandler handler) {
        this.actionHandler = Dev.checkNotNull(handler);
    }

    GCSelectionMenuActionHandler getActionHandler() {
        return this.actionHandler;
    }

    public H5EScrollablePane getScrollPane() {
        return scrollPane;
    }
}
