package com.universeprojects.gamecomponents.client.tutorial;

public class TutorialDialogData {
    public String dialogId;
    public String trigger;

    public TutorialDialogData(String dialogId, String trigger) {
        this.dialogId = dialogId;
        this.trigger = trigger;
    }

    public TutorialDialogData(String dialogId) {
        this.dialogId = dialogId;
    }

    public TutorialDialogData() {
    }
}
