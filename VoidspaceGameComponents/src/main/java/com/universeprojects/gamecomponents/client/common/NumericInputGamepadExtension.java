package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;

public class NumericInputGamepadExtension extends NavigationExtension{

    private TextField field;
    private int value;
    private int min;
    private int max;

    public NumericInputGamepadExtension(UINavigation navigation, int min, int max) {
        super(navigation);
        this.min = min;
        this.max = max;
    }

    @Override
    public void enter() {

    }

    @Override
    public void moveLeft() {
        super.moveLeft();
        value = Math.max(--value, min);
        field.setText(String.valueOf(value));
    }

    @Override
    public void moveRight() {
        super.moveRight();
        value = Math.min(++value, max);
        field.setText(String.valueOf(value));
    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof TextField && ((TextField)actor).getTextFieldFilter() != null && !((TextField)actor).getTextFieldFilter().acceptChar((TextField) actor,'a');
    }

    @Override
    public void init(Actor actor) {
        this.field = (TextField) actor;
        this.value = Integer.parseInt(field.getText());
        active = true;
    }
}
