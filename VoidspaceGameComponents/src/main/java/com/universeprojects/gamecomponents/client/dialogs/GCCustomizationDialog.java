package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.components.GCGenericINDList;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItem;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

/**
 * @author J. K. Rowling
 */
public class GCCustomizationDialog<D, T extends GCInventoryItem> extends GCWindow {
    private final T item;
    private final GCItemIcon<T> itemIcon;
    private final H5ELabel objectNameLabel;
    private final H5ELabel objectTypeLabel;
    private final H5ELabel objectRarityLabel;
    private final H5ELabel creditsLabel;

    private final GCGenericINDList<D> customizeOptionsList;

    private final GCLoadingIndicator loadingIndicator;

    public GCCustomizationDialog(H5ELayer layer, String dialogTitle, T item, Callable1Args<D> onAccept) {
        super(layer, 400, 600);
        this.item = item;
        left().top();
        defaults().left().top();
        getTitleLabel().setVisible(true);
        setTitle(dialogTitle);

        Table itemTable = add(new Table()).growX().getActor().top();
        itemTable.setBackground(StyleFactory.INSTANCE.panelStyleBlueCornersSemiTransparent);


        itemIcon = new GCItemIcon<>(layer);
        itemIcon.setItem(item);
        itemTable.add(itemIcon).size(64).pad(5);

        final Table nameTable = new Table();
        nameTable.setTouchable(Touchable.disabled);
        nameTable.left().top();
        nameTable.defaults().left().top();
        itemTable.add(nameTable).growX().padLeft(10);
        objectNameLabel = new H5ELabel(layer);
        objectNameLabel.setText(item.getName());
        //objectNameLabel.setFontScale(TITLE_FONT_SIZE);
        objectNameLabel.setAlignment(Align.left);
        objectNameLabel.setWrap(true);
        nameTable.add(objectNameLabel).padTop(-2).left().growX();

        nameTable.row();
        objectTypeLabel = new H5ELabel(layer);
        objectTypeLabel.setText(item.getItemClass());
        objectTypeLabel.setFontScale(0.8f);
        objectTypeLabel.setAlignment(Align.left);
        objectTypeLabel.setColor(Color.valueOf("#959595"));
        nameTable.add(objectTypeLabel).padTop(0).growX();

        nameTable.row();
        objectRarityLabel = new H5ELabel(layer);
        if (!"NONE".equals(item.getRarityText()))
            objectRarityLabel.setText(item.getRarityText());
        else
            objectRarityLabel.setText("");
        objectRarityLabel.setColor(item.getRarityColor());
        Label.LabelStyle labelStyle = getEngine().getSkin().get("label-laconic-small", Label.LabelStyle.class);
        objectRarityLabel.setStyle(labelStyle);
        objectRarityLabel.setAlignment(Align.left);
        nameTable.add(objectRarityLabel).padTop(4).growX().left();

        row();
        creditsLabel=new H5ELabel(layer);
        add(creditsLabel).right().padRight(5).padTop(2);
        row();
        customizeOptionsList = new GCGenericINDList<>(layer);
        add(customizeOptionsList).grow();
        customizeOptionsList.setOverscroll(false,true);
        customizeOptionsList.setScrollingDisabled(true,false);
        customizeOptionsList.setFadeScrollBars(true);
        row();

        H5EButton accept = add(new H5EButton("Accept", layer, StyleFactory.INSTANCE.buttonStyleDefault)).center().getActor();
        accept.addButtonListener(() -> {
            D selectedItem = customizeOptionsList.getSelectedItem();
            if (selectedItem == null) return;
            onAccept.call(selectedItem);
        });

        loadingIndicator = new GCLoadingIndicator(layer);
        enableNavigation();
        //debugAll();
    }

    public void showLoadingIndicator() {
        loadingIndicator.activate(customizeOptionsList.getContent());
    }

    public void hideLoadingIndicator() {
        loadingIndicator.deactivate();
    }

    public T getItem() {
        return item;
    }

    public void clearItemsList() {
        customizeOptionsList.clear();
    }

    public void addItem(String name, String iconKey, String description, int quantity, D data) {
        customizeOptionsList.addItem(new GCGenericINDListItemData<>(name, iconKey, description, quantity, data));
    }

    public void setCreditsCount(long credits){
        creditsLabel.setText("Available Credits: "+UPUtils.denomFormatString(credits + ""));
    }

    public void addAvailableItem(String name, String iconKey, String description, int quantity, String price, Callable0Args onClick, boolean available) {
        if(available) {
            addItem(name,iconKey, description,quantity,null);
        }else{
            GCGenericINDListItem item=customizeOptionsList.addAndReturn(new GCGenericINDListItemData<>(name, iconKey, description, quantity, null),null);
            item.row();
            item.add();
            H5ELabel priceLabel=new H5ELabel(price,getLayer());
            priceLabel.setStyle(StyleFactory.INSTANCE.labelBoldDefault);
            item.add(priceLabel).right().padRight(10);
            item.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    onClick.call();
                }
            });
            item.setDisabled(true);
        }
    }
}
