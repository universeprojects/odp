package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.universeprojects.gamecomponents.client.elements.GCSelectionIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings({"unchecked", "rawtypes"})
public abstract class UINavigation {

    public static final float MOVE_INTERVAL = 0.3f;

    public enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

    protected final H5ELayer layer;
    protected Cell currentCell;
    protected Actor currentActor;
    protected Group currentGroup;
    protected Group rootGroup;
    protected List<Actor> ignoredActors;
    protected List<NavigationShortcut> shortcuts;
    protected List<NavigationExtension> extensions;
    protected NavigationExtension currentExtension;
    protected GCSelectionIndicator indicator;
    protected Window window;
    protected H5EScrollablePane curScrollablePane;
    protected boolean isActive;
    protected Map<Direction, Boolean> heldButtonsMap;
    protected float dpadTime = 0;
    protected int selectionAccelerationIterations = 8;
    protected int continuousSelectionIterations = 0;

    public UINavigation(H5ELayer layer) {
        this.layer = layer;
        ignoredActors = new ArrayList<>();
        this.shortcuts = new ArrayList<>();
        this.extensions = new ArrayList<>();
        heldButtonsMap = new HashMap<>();
        heldButtonsMap.put(Direction.DOWN, false);
        heldButtonsMap.put(Direction.UP, false);
        heldButtonsMap.put(Direction.LEFT, false);
        heldButtonsMap.put(Direction.RIGHT, false);
        layer.getEngine().addInputProcessor(0, new InputAdapter() {
            @Override
            public boolean keyDown(int keycode) {
                boolean isKeyDown = super.keyDown(keycode);
                if (isActive && WindowManager.getInstance(layer.getEngine()).canUseKeyboardNavigation() && WindowManager.getInstance(layer.getEngine()).getActiveNavigation() == UINavigation.this) {
                    switch (keycode) {
                        case Input.Keys.ESCAPE:
                            exit();
                            break;
                        case Input.Keys.ENTER:
                            if (!isKeyDown)
                                enter();
                            break;
                        case Input.Keys.RIGHT:
                            move(Direction.RIGHT);
                            break;
                        case Input.Keys.LEFT:
                            move(Direction.LEFT);
                            break;
                        case Input.Keys.UP:
                            move(Direction.UP);
                            break;
                        case Input.Keys.DOWN:
                            move(Direction.DOWN);
                            break;
                        case Input.Keys.FORWARD_DEL:
                            delete();
                            break;
                    }
                }
                return isKeyDown;
            }
        });
    }

    protected boolean textAreaFocused() {
        return layer.getKeyboardFocus() instanceof TextField;
    }

    public boolean tryMove(Direction direction) {
        if (textAreaFocused()) {
            return false;
        }
        switch (direction) {
            case LEFT:
                return tryMoveLeft();
            case UP:
                return tryMoveUp();
            case RIGHT:
                return tryMoveRight();
            case DOWN:
                return tryMoveDown();
        }
        return true;
    }

    public void dpadPress(Direction direction, boolean isDown){
        if(isDown) {
            if(currentActor != null)
                move(direction);
            else update();
            dpadTime = MOVE_INTERVAL;
        }else{
            continuousSelectionIterations = 0;
        }
        if(heldButtonsMap.containsKey(direction)) {
            heldButtonsMap.put(direction, isDown);
        }
    }

    public boolean tryMoveLeft() {
        if (currentExtension != null) {
            currentExtension.moveLeft();
            return false;
        }
        if (currentActor != null) {
            if (isStaged(currentActor))
                unFocusAll();
            else exit();
        } else {
            findActor();
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.MoveLeft)) {
                return false;
            }
        }
        return true;
    }

    public boolean tryMoveUp() {
        if (currentExtension != null) {
            currentExtension.moveUp();
            return false;
        }
        if (currentActor != null) {
            if (isStaged(currentActor))
                unFocusAll();
            else exit();
        } else {
            findActor();
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.MoveUp)) {
                return false;
            }
        }
        return true;
    }

    public boolean tryMoveRight() {
        if (currentExtension != null) {
            currentExtension.moveRight();
            return false;
        }
        if (currentActor != null) {
            if (isStaged(currentActor))
                unFocusAll();
            else exit();
        } else {
            findActor();
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.MoveRight)) {
                return false;
            }
        }
        return true;
    }

    public boolean tryMoveDown() {
        if (currentExtension != null) {
            currentExtension.moveDown();
            return false;
        }
        if (currentActor != null) {
            if (isStaged(currentActor))
                unFocusAll();
            else exit();
        } else {
            findActor();
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.MoveDown)) {
                return false;
            }
        }
        return true;
    }

    public boolean tryExit() {
        if (textAreaFocused()) {
            exitTextArea();
            return false;
        }
        if (currentExtension != null) {
            currentExtension.exit();
            if (currentExtension.isActive()) {
                return false;
            }
            currentExtension = null;
            focusOnCurrent();
            return false;
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.Exit)) {
                return false;
            }
        }
        return true;
    }

    public boolean tryEnter() {
        if (textAreaFocused()) {
            return false;
        }
        if (currentExtension != null) {
            currentExtension.enter();
            return false;
        }
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.Enter)) {
                return false;
            }
        }
        if (currentActor != null) {
            for (NavigationExtension extension : extensions) {
                if (extension.isCompatible(currentActor)) {
                    extension.init(currentActor);
                    if(extension.isActive()) {
                        currentExtension = extension;
                    }
                    return false;
                }
            }
        }
        return true;
    }

    public <T extends NavigationExtension> T addExtension(T extension) {
        extensions.add(extension);
        return extension;
    }

    public void setActiveState(boolean activeState) {
        this.isActive = activeState;
        if (!activeState)
            onClose();
    }

    public boolean isStaged(Actor actor) {
        return actor.getStage() != null;
    }

    public void exitTextArea() {
        if (layer.getKeyboardFocus() instanceof H5EInputBox) {
            ((H5EInputBox) layer.getKeyboardFocus()).unfocus();
            layer.setKeyboardFocus(null);
        } else if (layer.getKeyboardFocus() instanceof H5ETextArea) {
            ((H5ETextArea) layer.getKeyboardFocus()).unfocus();
            layer.setKeyboardFocus(null);
        }
    }

    public void delete() {
        for (NavigationShortcut shortcut : shortcuts) {
            shortcut.move(currentActor, this, NavigationShortcut.Trigger.Delete);
        }
    }

    public void rightTriggerPress() {
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.RightTrigger)) {
                return;
            }
        }
    }

    public void leftTriggerPress() {
        for (NavigationShortcut shortcut : shortcuts) {
            if (shortcut.move(currentActor, this, NavigationShortcut.Trigger.LeftTrigger)) {
                return;
            }
        }
    }

    public void ignoreActors(Actor... actors) {
        ignoredActors.addAll(Arrays.asList(actors));
    }

    public void addShortcut(NavigationShortcut shortcut) {
        shortcuts.add(shortcut);
    }

    public void setWindow(Window window, Group rootGroup) {
        this.window = window;
        this.rootGroup = rootGroup;
        indicator = new GCSelectionIndicator(window, layer);

        if (window instanceof GCWindow) {
            ((GCWindow) window).onClose.registerHandler(this::onClose);
        }
    }

    public void onClose() {
        indicator.deactivate();
        for(Direction key:heldButtonsMap.keySet()){
            heldButtonsMap.put(key, false);
        }
    }

    public void act(float delta) {
        if (isActive() && WindowManager.getInstance(layer.getEngine()).getActiveNavigation() == this) {
            if (dpadTime <= 0) {
                dpadTime = MOVE_INTERVAL;
                for(Direction key:heldButtonsMap.keySet()){
                    if(heldButtonsMap.get(key)){
                        move(key);
                        continuousSelectionIterations += 1;
                    }
                }
            } else {
                dpadTime -= delta*(Math.pow((float)continuousSelectionIterations / selectionAccelerationIterations,2) + 1);
            }
        }
    }

    public void focusOn(Actor actor) {
        currentActor = actor;
        focusOnCurrent();
    }

    public boolean isActive() {
        return isActive;
    }

    protected boolean isVisible(Actor actor) {
        if (actor.getParent() != null)
            return actor.isVisible() && isVisible(actor.getParent());
        else {
            return actor.isVisible();
        }
    }

    public void update() {
    }

    public void clear() {
    }

    public abstract void exit();

    public abstract void focusOnCurrent();

    protected abstract void findActor();

    protected abstract boolean isFocusable(Actor actor);

    public abstract void enter();

    public abstract void move(Direction direction);

    public abstract void moveLeft();

    public abstract void moveUp();

    public abstract void moveRight();

    public abstract void moveDown();

    protected abstract void unFocusAll();

}
