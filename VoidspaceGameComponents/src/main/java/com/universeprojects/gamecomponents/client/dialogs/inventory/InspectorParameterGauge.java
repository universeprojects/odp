package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;

public class InspectorParameterGauge extends InspectorParameter {
    public final String textSummary;
    public final Float value;
    public final Float maxValue;

    public InspectorParameterGauge(Float value, Float maxValue, String textSummary) {
        super();
        this.value = value;
        this.maxValue = maxValue;
        this.textSummary = textSummary;
    }

    public InspectorParameterGauge(Float value, Float maxValue, String textSummary, Rarity rarity) {
        super(rarity);
        this.value = value;
        this.maxValue = maxValue;
        this.textSummary = textSummary;
    }

    public InspectorParameterGauge(Float value, Float maxValue, String textSummary, Color rarityColor) {
        super(rarityColor);
        this.value = value;
        this.maxValue = maxValue;
        this.textSummary = textSummary;
    }
}
