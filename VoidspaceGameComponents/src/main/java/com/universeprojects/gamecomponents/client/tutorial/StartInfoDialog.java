package com.universeprojects.gamecomponents.client.tutorial;

import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class StartInfoDialog extends TutorialInfoDialog {
    public static final String NAME = "StartInfo";

    public StartInfoDialog(H5ELayer layer, String closeTrigger) {
        super(layer, closeTrigger);
        H5ELabel label = new H5ELabel("TODO", layer);
        add(label);
    }
}
