package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Pools;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.GCUtils;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCLevelGauge;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GCItemInspector<K, T extends GCInventoryItem> extends GCWindow {

    public static final float CONTENT_FONT_SIZE = 0.8f;
    public static final float TITLE_FONT_SIZE = 1f;

    private final GCItemIcon<T> itemIcon;
    private final Cell<H5ELabel> objectNameCell;
    private final H5ELabel objectNameLabel;
    private final H5ELabel objectTypeLabel;
    private final H5ELabel objectRarityLabel;
    private final H5ELabel descriptionTextArea;
    private final Cell<?> descriptionTextCell;
    private final Table parameterTable;
    private final Table additionalComponentsTable;
    private final Table scrollTable;
    private GCInventory<?> inventory;
    private T item;

    private boolean positionNextToSlot = false;
    protected GCInventorySlotComponent<K, T, ?> currentComponent;

    private final H5EButton customizeBtn;

    public GCItemInspector(H5ELayer layer) {
        super(layer, 120, 200, "inspector-window");
        left().top();
        defaults().left().top();
        getTitleLabel().setVisible(false);
        setKeepWithinStage(true);

        Cell<?> oldCloseCell = getTitleTable().getCell(btnClose);
        Cell<?> newCloseCell = getTitleTable().add();
        customizeBtn = ButtonBuilder.inLayer(layer)
            .withStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow)
            .withText("Customize")
            .withButtonListener(this::onCustomizeBtnClick)
            .build();
        oldCloseCell.setActor(customizeBtn);
        newCloseCell.setActor(btnClose);
        newCloseCell.pad(oldCloseCell.getPadTop(), oldCloseCell.getPadLeft(), oldCloseCell.getPadBottom(), oldCloseCell.getPadRight());
        oldCloseCell.pad(oldCloseCell.getPadTop(), 0, 0, 15);
        customizeBtn.setVisible(false);
        H5EScrollablePane mainPanel = new H5EScrollablePane(layer);
        scrollTable = mainPanel.getContent();
        super.add(mainPanel).grow().padLeft(15).padRight(15).padTop(15);

        Table mainTable = scrollTable.add(new Table()).grow().getActor();
        scrollTable.row();

        itemIcon = new GCItemIcon<>(layer);
        mainTable.add(itemIcon).size(64);

        final Table nameTable = new Table();
        nameTable.setTouchable(Touchable.disabled);
        nameTable.left().top();
        nameTable.defaults().left().top();
        mainTable.add(nameTable).growX().padLeft(15);
        objectNameLabel = new H5ELabel(layer);
        objectNameLabel.setFontScale(TITLE_FONT_SIZE);
        objectNameLabel.setAlignment(Align.left);
        objectNameCell = nameTable.add(objectNameLabel).padTop(-2).left();

        nameTable.row();
        objectTypeLabel = new H5ELabel(layer);
        objectTypeLabel.setFontScale(CONTENT_FONT_SIZE);
        objectTypeLabel.setAlignment(Align.left);
        objectTypeLabel.setColor(Color.valueOf("#959595"));
        nameTable.add(objectTypeLabel).padTop(0).growX();

        nameTable.row();
        objectRarityLabel = new H5ELabel(layer);
        Label.LabelStyle labelStyle = getEngine().getSkin().get("label-laconic-small", Label.LabelStyle.class);
        objectRarityLabel.setStyle(labelStyle);
        objectRarityLabel.setAlignment(Align.left);
        nameTable.add(objectRarityLabel).padTop(4).growX().left();

        mainTable.row();



        parameterTable = new Table();
        parameterTable.left().top();
        mainTable.add(parameterTable).fill().colspan(2);

        mainTable.row();

        descriptionTextArea = new H5ELabel(layer);
        descriptionTextArea.setWrap(true);
        descriptionTextArea.setFontScale(CONTENT_FONT_SIZE);
        descriptionTextCell = mainTable.add(descriptionTextArea).colspan(2).fillX();

        mainTable.row();

        additionalComponentsTable = new Table();
        additionalComponentsTable.left().top();
        mainTable.add(additionalComponentsTable).fill().colspan(2).maxHeight(200);

        mainTable.row();

        layer.getEngine().onResize.registerHandler(()->Gdx.app.postRunnable(this::updateSize));

        GCUtils.preventSchematicWindowClosingFor(this);
    }

    protected void onCustomizeBtnClick() {

    }

    public boolean isPositionNextToSlot() {
        return positionNextToSlot;
    }

    public void setPositionNextToSlot(boolean positionNextToSlot) {
        this.positionNextToSlot = positionNextToSlot;
    }

    public H5EScrollablePane getScrollPanel() {
        return (H5EScrollablePane) scrollTable.getParent();
    }

    public void setSlotComponent(GCInventorySlotComponent<K, T, ?> component) {
        // place the icon on top of the invisible placeholder
        final T item = component.getItem();
        this.currentComponent = component;

        setItem(item);
        onSetSlotComponent(item, component);

        pack();
        if(isPositionNextToSlot()) {
            positionForSlot(component);
        } else {
            positionForBaseInventory(component);
        }
        toFront();
        activate();
    }

    /**
     * Resets the inspector's view of the item's fields and recreates the additional components
     */
    public void redrawInspector() {
        setItem(item);

        pack();
    }

    protected void onSetSlotComponent(T item, GCInventorySlotComponent<K, T, ?> component) {
    }

    public void setItem(T item) {
        this.item = item;
        itemIcon.setItem(item);

        objectNameLabel.setText(item.getName());
        if(objectNameLabel.getPrefWidth()>getEngine().getWidth()-160){
            objectNameLabel.setWrap(true);
            objectNameCell.width(getEngine().getWidth()-160);
        }else{
            objectNameLabel.setWrap(false);
            objectNameCell.width(objectNameLabel.getPrefWidth());
        }
        objectTypeLabel.setText(item.getItemClass());

        final String description = item.getDescription();
        int padding = 7;
        if (description.isEmpty()) {
            descriptionTextArea.remove();
        } else {
            descriptionTextCell.setActor(descriptionTextArea);
            padding = 14;
        }
        descriptionTextCell.padTop(padding);
        descriptionTextCell.padBottom(padding);
        descriptionTextArea.setVisible(!description.isEmpty());
        descriptionTextArea.setText(description);

        objectRarityLabel.setText(item.getRarityText());
        objectRarityLabel.setColor(item.getRarityColor());

        setParameters(item.getAdditionalProperties());
        List<GCInventory<?>> addedInventories = new ArrayList<>();
        item.createAdditionalInspectorComponents(getLayer(), additionalComponentsTable, addedInventories);
        if(addedInventories.size()>0) {
            setInventory(addedInventories.get(0));
        }
        customizeBtn.setVisible(item.canBeCustomized());
    }

    public T getItem() {
        return item;
    }

    public Table getAdditionalComponentsTable() {
        return additionalComponentsTable;
    }

    protected void positionForBaseInventory(GCInventorySlotComponent<?, T, ?> component) {
        Actor actor = component.inventoryBase;
        Vector2 vec = Pools.obtain(Vector2.class);
        vec.set(0, actor.getHeight());
        actor.localToStageCoordinates(vec);
        setPosition(vec.x, vec.y, Align.topRight);
        Pools.free(vec);
        capEdges();
    }

    protected void positionForSlot(GCInventorySlotComponent<K, T, ?> component) {
        Vector2 vec = Pools.obtain(Vector2.class);
        vec.set(0, 0);
        component.localToStageCoordinates(vec);
        setPosition(vec.x, vec.y, Align.bottomRight);
        Pools.free(vec);
        capEdges();
    }

    public void setParameters(Map<String, InspectorParameter> parameters) {
        parameterTable.clearChildren();
        parameterTable.setVisible(!parameters.isEmpty());

        for(Map.Entry<String, InspectorParameter> entry : parameters.entrySet()) {
            parameterTable.row().padBottom(2);
            final String keyText = entry.getKey();
            InspectorParameter parameterBase = entry.getValue();
            if (parameterBase instanceof InspectorParameterString) {
                InspectorParameterString parameter = (InspectorParameterString)parameterBase;
                final String valueText = parameter.value;
                final H5ELabel keyLabel = new H5ELabel(keyText, getLayer());
                final H5ELabel valueLabel = new H5ELabel(valueText, getLayer());
                keyLabel.setFontScale(CONTENT_FONT_SIZE);
                valueLabel.setFontScale(CONTENT_FONT_SIZE);
                keyLabel.setAlignment(Align.left);
                valueLabel.setAlignment(Align.left);

                Color color = parameter.rarityColor != null ? parameter.rarityColor : Rarity.COMMON.getColor();

                keyLabel.setColor(color);
                valueLabel.setColor(color);

                parameterTable.add(keyLabel).left();
                parameterTable.add(valueLabel).growX().padLeft(15).left();
            } else if (parameterBase instanceof InspectorParameterGauge) {
                InspectorParameterGauge parameter = (InspectorParameterGauge)parameterBase;
                final String summaryText = parameter.textSummary;
                final Float currentValue = parameter.value;
                final Float maxValue = parameter.maxValue;
                final H5ELabel keyLabel = new H5ELabel(keyText, getLayer());
                final GCLevelGauge valueProgressBar = new GCLevelGauge(getLayer());
                valueProgressBar.setLabel(summaryText);
                valueProgressBar.setValue(currentValue, maxValue);

                keyLabel.setFontScale(CONTENT_FONT_SIZE);
                keyLabel.setAlignment(Align.left);

                Color color = parameter.rarityColor != null ? parameter.rarityColor : Rarity.COMMON.getColor();

                keyLabel.setColor(color);

                parameterTable.add(keyLabel).left();
                parameterTable.add(valueProgressBar).grow().padLeft(15).left();
            }
        }
    }

    private void capEdges() {
        if (isFullscreen()) return;
        setX(UPMath.max(0, UPMath.min(getEngine().getWidth() - getWidth(), getX())));
        setY(UPMath.max(0, UPMath.min(getEngine().getHeight() - getHeight(), getY() - 30)));
    }

    public void updateSize(){
        if(getLayer() != null) {
            if (scrollTable.getPrefHeight() < getLayer().getHeight() - 200) {
                this.setHeight(scrollTable.getPrefHeight() + 50);
            } else {
                this.setHeight(getLayer().getHeight() - 200);
            }
            if (scrollTable.getPrefWidth() < getLayer().getWidth() - 50) {
                this.setWidth(scrollTable.getPrefWidth() + 45);
            } else {
                this.setWidth(getLayer().getWidth() - 20);
            }
        }
        if(getEngine()!=null && getEngine().isMobileMode())
            Gdx.app.postRunnable(()->positionProportionally(0.5f,0.5f));
    }

    public void setInventory(GCInventory<?> inventory){
        this.inventory = inventory;
    }

    public GCInventory<?> getInventory(){
        return inventory;
    }

    @Override
    public Cell add() {
        Cell cell = scrollTable.add();
        Gdx.app.postRunnable(this::updateSize);
        return cell;
    }

    @Override
    public <C extends Actor> Cell<C> add(C actor) {
        Cell<C> cell = scrollTable.add(actor);
        Gdx.app.postRunnable(this::updateSize);
        return cell;
    }

    @Override
    public Cell row() {
        Cell row = scrollTable.row();
        Gdx.app.postRunnable(this::updateSize);
        return row;
    }
}
