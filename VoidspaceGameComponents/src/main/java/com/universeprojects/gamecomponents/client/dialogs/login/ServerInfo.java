package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;

public interface ServerInfo {
    String getName();
    int getPopulation();
    Integer getMaxPopulation();
    String getStatusLabel();
    Color getStatusColor();
}
