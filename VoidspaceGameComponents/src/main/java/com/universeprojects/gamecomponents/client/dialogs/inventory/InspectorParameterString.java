package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;

public class InspectorParameterString extends InspectorParameter {
    public final String value;

    public InspectorParameterString(String value) {
        super();
        this.value = value;
    }

    public InspectorParameterString(String value, Rarity rarity) {
        super(rarity);
        this.value = value;
    }

    public InspectorParameterString(String value, Color rarityColor) {
        super(rarityColor);
        this.value = value;
    }
}
