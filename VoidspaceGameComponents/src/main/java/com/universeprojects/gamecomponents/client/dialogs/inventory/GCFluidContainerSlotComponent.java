package com.universeprojects.gamecomponents.client.dialogs.inventory;

public class GCFluidContainerSlotComponent<T extends GCInventoryItem> extends GCInventorySlotComponent<GCFluidTransfer.Slot, T, GCFluidContainerSlotComponent<T>> {
    public GCFluidContainerSlotComponent(GCInventoryBase<GCFluidTransfer.Slot, T, GCFluidContainerSlotComponent<T>> inventoryBase, GCFluidTransfer.Slot key, SlotConfig slotConfig) {
        super(inventoryBase, key, slotConfig);
    }

    @Override
    public String getEmptySpriteKey() {
        return "images/icons/base/junk_grade_2.png";
    }

    @Override
    public String getEmptyFrameIconStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public String getFrameStyle() {
        return "icon-equipment_item_frame";
    }

    @Override
    public int getFrameBaseIconSize() {
        return GCItemIcon.BASE_EMPTY_FRAME_ICON_SIZE;
    }
}
