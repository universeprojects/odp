package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

public class ScrollpaneNavigationExtension extends NavigationExtension{

    private float scrollDelta;
    private H5EScrollablePane scrollablePane;

    public ScrollpaneNavigationExtension(UINavigation navigation, float scrollDelta) {
        super(navigation);
        this.scrollDelta = scrollDelta;
    }

    @Override
    public void moveDown() {
        super.moveDown();
        scrollablePane.setScrollY(scrollablePane.getScrollY() + scrollDelta);
    }

    @Override
    public void moveUp() {
        super.moveUp();
        scrollablePane.setScrollY(scrollablePane.getScrollY() - scrollDelta);
    }

    @Override
    public void moveLeft() {
        super.moveLeft();
    }

    @Override
    public void moveRight() {
        super.moveRight();
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof H5EScrollablePane;
    }

    @Override
    public void init(Actor actor) {
        this.scrollablePane = (H5EScrollablePane) actor;
        active = true;
    }
}
