package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ERate;

public class RateNavigationExtension extends NavigationExtension{

    private H5ERate rateComponent;

    public RateNavigationExtension(UINavigation navigation) {
        super(navigation);
    }


    @Override
    public void enter() {

    }

    @Override
    public void moveRight() {
        super.moveRight();
        rateComponent.setValue(Math.min(5, rateComponent.getValue()+1f));
    }

    @Override
    public void moveLeft() {
        super.moveLeft();
        rateComponent.setValue(Math.max(1, rateComponent.getValue()-1f));
    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof H5ERate;
    }

    @Override
    public void init(Actor actor) {
        rateComponent = (H5ERate) actor;
        active = true;
    }
}
