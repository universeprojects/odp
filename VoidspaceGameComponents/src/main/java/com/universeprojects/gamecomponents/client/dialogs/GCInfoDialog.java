package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.GCUtils;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;


public abstract class GCInfoDialog implements DestroyableDialog {

    private final GCSimpleWindow window;

    private final H5EButton btnL;
    private final H5EButton btnR;

    /**
     * User setting
     */
    private final boolean destroyOnClose;

    public static GCInfoDialog openSimpleOKDialog(H5ELayer layer, String title, String[] caption, Callable0Args onClosed) {
        GCInfoDialog dialog = new GCInfoDialog(layer, true) {
            @Override
            protected String getTitleText() {
                return title;
            }

            @Override
            protected String[] getCaptionText() {
                return caption;
            }

            @Override
            protected String getButtonLeftText() {
                return null;
            }

            @Override
            protected String getButtonRightText() {
                return "OK";
            }

            @Override
            protected void onLeftButtonPressed() {
            }

            @Override
            protected void onRightButtonPressed() {
            }

            @Override
            protected void onDismiss() {
                if (onClosed != null) {
                    onClosed.call();
                }
            }
        };
        UPUtils.tieButtonToKey(dialog.window, Input.Keys.ENTER, dialog.btnR);
        UPUtils.tieButtonToKey(dialog.window, Input.Keys.ESCAPE, dialog.btnR);
        dialog.open();
        dialog.positionProportionally(0.5f, 0.5f);
        layer.setKeyboardFocus(dialog.window);
        return dialog;
    }

    /**
     * Creates a new info dialog.
     *
     * @param layer          The layer to create this dialog in.
     * @param destroyOnClose When this is set to TRUE, this dialog's UI elements will be destroyed when the dialog is closed.
     *                       This is useful for disposable dialogs, such as message pop-ups that are unrelated to each other.<br>
     *                       When this is set to FALSE, this dialog's UI elements will remain in memory after the dialog is closed,
     *                       effectively making it hidden. Only use that if the dialog is reusable over time.
     */
    public GCInfoDialog(H5ELayer layer, boolean destroyOnClose) {
        Dev.checkNotNull(layer);
        this.destroyOnClose = destroyOnClose;

        final int windowMinW = 400;
        final int windowMinH = 140;

        window = new GCSimpleWindow(layer, "info-dialog", getTitleText(), windowMinW, windowMinH, true, "popup-window");
        window.setMovable(false);
        window.setModal(true);
        window.setCloseButtonEnabled(false);
        window.defaults().left();
        window.enableNavigation();
        window.onClose.registerHandler(() -> {
            onDismiss();

            // in all cases, destroy the window if applicable:
            if (GCInfoDialog.this.destroyOnClose) {
                window.destroy();
            }
        });

        Table mainTable = new Table();
        window.add(mainTable).grow().colspan(2);

        if (getCaptionText() != null) {
            for (String lineText : getCaptionText()) {
                mainTable.row();
                final H5ELabel label = mainTable.add(new H5ELabel(lineText, layer)).center().padLeft(5).padRight(5).padTop(15).minWidth(340).growX().getActor();
                label.setWrap(true);
            }
        }



        // LEFT BUTTON
        String btnLText = Strings.nullToEmpty(getButtonLeftText());
        window.row();
        btnL = window.addMediumButton(btnLText).left().padBottom(5).padTop(15).getActor();
        btnL.setWidth(70);
        btnL.addButtonListener(() -> {
            onLeftButtonPressed();
            close(); // <-- this will fire an "onClose" event without user interaction
        });
        if (Strings.isEmpty(btnLText)) {
            btnL.setDisabled(true);
            btnL.setTouchable(Touchable.disabled);
        }

        // RIGHT BUTTON
        String btnRText = Strings.nullToEmpty(getButtonRightText());
        btnR = window.addMediumButton(btnRText).right().padBottom(15).padTop(15).getActor();
        btnR.setWidth(70);
        btnR.addButtonListener(() -> {
            onRightButtonPressed();
            close(); // <-- this will fire an "onClose" event without user interaction
        });
        if (Strings.isEmpty(btnRText)) {
            btnR.setDisabled(true);
            btnR.setTouchable(Touchable.disabled);
        }


        UPUtils.tieButtonToKey(window, Input.Keys.ENTER, btnL);
        UPUtils.tieButtonToKey(window, Input.Keys.ESCAPE, btnR);

    }

    public void activateFullscreenIfNecessary() {
        final H5EEngine engine = window.getLayer().getEngine();
        if(window.getWidth() > engine.getWidth() || window.getHeight() > engine.getHeight()) {
            window.setFullscreen(true);
        }
    }

    public void setX(float x) {
        window.setX(x);
    }

    public void setY(float y) {
        window.setY(y);
    }

    public void positionProportionally(Float propX, Float propY) {
        window.positionProportionally(propX, propY);
    }

    /**
     * Opens the dialog
     */
    public void open() {
        window.open();

        // make sure that disabled buttons are hidden from view
        btnL.setVisible(!btnL.isDisabled());
        btnR.setVisible(!btnR.isDisabled());
    }

    /**
     * Closes the dialog
     */
    public void close() {
        window.close();
    }

    /**
     * @return TRUE if the dialog is open, FALSE otherwise
     */
    public boolean isOpen() {
        return window.isOpen();
    }

    /**
     * When this dialog is no longer needed, this method MUST be called in order to detach it from the graphics engine.
     * "Closing" the window is not enough, and will cause a memory leak as hidden dialog instances pile up.
     * <p>
     * NOTE: this only needs to be done if the "destroy on close" setting is off.
     */
    @Override
    public void destroy() {
        window.destroy();
    }

    /**
     * Defines the text for the window title
     */
    protected abstract String getTitleText();

    /**
     * Defines the text for the main content
     *
     * @return The lines of the content, as a String array
     */
    protected abstract String[] getCaptionText();

    /**
     * Defines the label of the LEFT button
     * When this value is NULL, the button is unavailable
     */
    protected abstract String getButtonLeftText();

    /**
     * Defines the label of the RIGHT button
     * When this value is NULL, the button is unavailable
     */
    protected abstract String getButtonRightText();

    /**
     * Handles the scenario when the user presses the left button
     */
    protected abstract void onLeftButtonPressed();

    /**
     * Handles the scenario when the user presses the right button
     */
    protected abstract void onRightButtonPressed();

    /**
     * Handles the scenario when the user dismisses the dialog by hitting the [X] button
     */
    protected abstract void onDismiss();

    public void preventSchematicWindowClosing() {
        GCUtils.preventSchematicWindowClosingFor(window);
    }

}
