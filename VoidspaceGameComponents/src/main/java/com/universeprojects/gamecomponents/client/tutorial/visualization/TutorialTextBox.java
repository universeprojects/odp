package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.dialogs.Confirmable;
import com.universeprojects.gamecomponents.client.dialogs.GCConfirmationScreen;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.tutorial.TutorialSystemState;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarkerStyle;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Visual element that is currently being used in the
 * TutorialSystem to be displayed under the Tutorial Arrow
 * or as Tooltip in the middle of a screen.
 * <p>
 * Can hold and display following information:
 * <p>
 * - Description
 * - Icon (that will be displayed on the notch)
 * - "Okay" button (that triggers a specified objective)
 * - "Disable tutorials" (if button is present)
 */
public class TutorialTextBox implements Confirmable {
    private static final float NOTCH_Y_OFFSET = -1;
    private static final float NOTCH_X_OFFSET = 2;
    private static final float NOTCH_WIDTH = 46;
    private static final float NOTCH_HEIGHT = 32;

    private static final float CONTENT_LEFT_PADDING = 7f;
    private static final float BUTTON_WIDTH = 90;
    private static final float BUTTON_PADDING_SIDES = 15f;
    private static final float BUTTON_PADDING_TOP_BOTTOM = 5f;
    private static final float TABLE_PADDING_BOTTOM = 4f;
    private static final float TABLE_PADDING_TOP = 10f;
    private static final float CONTENT_FONT_SIZE = 0.8f;
    private static final float CHECKBOX_FONT_SIZE = 0.8f;
    private static final int WIDTH = 400;

    private final TutorialController tutorialController;
    private final H5ELayer layer;
    private final H5ELabel content;
    private final GCWindow window;
    private final H5EButton button;
    private final H5EButton checkBox;
    private final Cell<H5EButton> buttonCell;
    private final Cell<H5EButton> checkboxCell;
    private GCConfirmationScreen confirmDisableTutorial;
    private boolean open = false;
    private H5EIcon icon;
    private String triggerId;

    public TutorialTextBox(TutorialController tutorialController, H5ELayer layer, String windowId) {
        this.tutorialController = tutorialController;
        this.layer = layer;

        window = new TutorialTooltipWindow(layer);
        window.setId(windowId);

        content = new H5ELabel(layer);
        content.setWrap(true);
        content.setFontScale(CONTENT_FONT_SIZE);
        content.setText("");
        content.setTouchable(Touchable.disabled);

        button = ButtonBuilder
                .inLayer(layer)
                .withStyle("button2")
                .withText("okay")
                .build();

        button.getLabel().setFontScale(0.9f);
        button.setWidth(BUTTON_WIDTH);
        button.addButtonListener(() -> {
            Tutorials.trigger(triggerId);
            disableTutorialDialog();
        });

        button.getLabelCell()
                .padRight(BUTTON_PADDING_SIDES)
                .padLeft(BUTTON_PADDING_SIDES)
                .padTop(BUTTON_PADDING_TOP_BOTTOM)
                .padBottom(BUTTON_PADDING_TOP_BOTTOM);


        window.add(content)
                .colspan(2)
                .growY()
                .width(WIDTH)
                .padLeft(CONTENT_LEFT_PADDING);

        window.row();

        checkBox = ButtonBuilder.inLayer(layer).withStyle("default-checkable").build();
        checkBox.setRawText("Disable tutorials");
        checkBox.setChecked(false);
        checkBox.getLabel().setFontScale(CHECKBOX_FONT_SIZE);

        checkboxCell = window.add(checkBox)
                .center()
                .padBottom(TABLE_PADDING_BOTTOM)
                .padTop(TABLE_PADDING_TOP);

        buttonCell = window.add(button)
                .expand()
                .center()
                .padBottom(TABLE_PADDING_BOTTOM)
                .padTop(TABLE_PADDING_TOP);

        window.pack();
        reset();
    }

    public void disableTutorialDialog() {
        if (checkBox.isChecked()) {
            // Inhibit tutorial markers until user makes a decision
            tutorialController.setSystemState(TutorialSystemState.INHIBITED);

            confirmDisableTutorial = new GCConfirmationScreen(layer, this, "Do you really want to disable the tutorial system?");
            confirmDisableTutorial.setCallConfirmedOnExit(true);
            confirmDisableTutorial.open();
        }
    }

    public void close() {
        if (open)
            open = false;
        else
            return;

        window.close();
        reset();
    }

    /**
     * Reset current object to default state
     */
    private void reset() {
        content.setText("");
        triggerId = null;

        button.setVisible(false);
        checkBox.setVisible(false);

        buttonCell.setActor(null)
                .padBottom(0)
                .padTop(0);

        checkboxCell.setActor(null)
                .padBottom(0)
                .padTop(0);

        if (icon != null) {
            icon.remove();
            icon = null;
        }
    }

    /**
     * Apply the parameters from the passed tutorial marker
     * and show this component
     *
     * @param marker object that contains data for this text box
     */
    public void open(TutorialMarker marker) {
        if (open)
            close();

        checkBox.setChecked(false);
        open = true;
        content.setText("");

        if (marker.getTrigger() != null) {
            triggerId = marker.getTrigger();
            button.setVisible(true);
            buttonCell.setActor(button)
                    .padBottom(TABLE_PADDING_BOTTOM)
                    .padTop(TABLE_PADDING_TOP);
            checkBox.setVisible(true);
            checkboxCell.setActor(checkBox)
                    .padBottom(TABLE_PADDING_BOTTOM)
                    .padTop(TABLE_PADDING_TOP);

        }

        window.open();
    }

    public void setScale(float scale) {
        window.setScale(scale);
    }

    public void setPosition(float x, float y) {
        window.setPosition(x, y);

        if (icon != null) {
            icon.setPosition(
                    x + NOTCH_X_OFFSET,
                    y + window.getHeight() - icon.getHeight() + NOTCH_Y_OFFSET
            );
        }
    }

    public float getHeight() {
        return window.getHeight();
    }

    public float getWidth() {
        return window.getWidth();
    }

    // Will only be called for Tooltips
    public void onAnimationCompleted() {
        if (icon != null) {
            icon.setVisible(true);
        }
    }

    @Override
    public void confirmed() {
        TutorialSystemState state = confirmDisableTutorial.isConfirmed() ? TutorialSystemState.DISABLED : TutorialSystemState.ENABLED;
        tutorialController.setSystemState(state);
    }

}
