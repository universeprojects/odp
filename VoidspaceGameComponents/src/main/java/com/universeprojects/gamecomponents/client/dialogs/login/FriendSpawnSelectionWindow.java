package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCLoginFriendItem;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class FriendSpawnSelectionWindow extends SimpleSelectionWindow {

    private final Map<String, Map<String, Long>> friendMap = new HashMap<>();
    private final FriendLoader friendLoader;
    private final Callable1Args<Long> onSelected;

    @SuppressWarnings("rawtypes")
    public FriendSpawnSelectionWindow(H5ELayer layer, FriendLoader friendLoader, Callable1Args<Long> onSelected) {
        super(layer, "spawn-friend-select", "Select Friend", 500, 500);
        this.setSize(600, 500);
        this.friendLoader = friendLoader;
        this.onSelected = onSelected;
        handler = new GCLoginFriendItem.GCLoginFriendListHandler();

        H5EButton btnSpawn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Spawn").build();
        btnSpawn.addButtonListener(this::spawnButton);
        H5EButton btnRefresh = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Refresh").build();
        btnRefresh.addButtonListener(this::loadFriendData);
        H5EButton btnCancel = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Cancel").build();
        btnCancel.addButtonListener(this::cancelButton);
        btnTable.add(btnCancel).center().growX();
        btnTable.add(btnRefresh).center().growX();
        btnTable.add(btnSpawn).center().growX();
        enableNavigation();
//        friendMap = dialog.friendMap;
    }

    @Override
    protected void loadItemList() {
        itemList.clear();
        itemList.remove();
        itemList = null;
        itemList = new GCList<>(getLayer(), 1, 5, 5, true);
        itemList.top().left();
        mainPane.getContent().clear();
        mainPane.add(itemList).top().left().grow();

//        friendMap = dialog.friendMap;
        for (Map.Entry<String, Map<String, Long>> entry : friendMap.entrySet()) {
            String userName = entry.getKey();
            Map<String, Long> innerMap = entry.getValue();
            Map<String, Long> sortedMap = new TreeMap<>(innerMap);
            for (Map.Entry<String, Long> inEntry : sortedMap.entrySet()) {
                String characterName = inEntry.getKey();
                final Long charId = inEntry.getValue();
                GCLoginFriendItem item = new GCLoginFriendItem(getLayer(), (GCLoginFriendItem.GCLoginFriendListHandler) handler, userName, characterName, charId);
                itemList.addItem(item);
            }
        }
    }

    private void spawnButton() {
        GCLoginFriendItem selectedItem = (GCLoginFriendItem) handler.getSelectedItem();
        if (selectedItem != null) {
            close();
            onSelected.call(selectedItem.getCharId());
        } else {
            messageLabel.setText("*Please select a friend");
        }
    }

    private void cancelButton() {
        this.close();
    }

    protected void loadFriendData() {
        try {
            friendLoader.loadUserFriends((response) -> {
                friendMap.clear();
                friendMap.putAll(response);
                if (isOpen()) {
                    loadItemList();
                } else {
                    this.open();
                }
            }, (error) ->
                Log.error("Error while loading friends: " + error));
        } catch (Throwable ex) {
            Log.error("Error while loadng friend data:", ex);
        }


    }

    @Override
    public void open() {
        super.open();
        loadItemList();
        this.setSize(400, 500);
        handler.reset();
        positionProportionally(0.5f, 0.5f);
    }

    public interface FriendLoader {
        void loadUserFriends(Callable1Args<Map<String, Map<String, Long>>> successCallback, Callable1Args<String> errorCallback);
    }
}
