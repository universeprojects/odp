package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.math.BigDecimal;

public class GCLevelGauge extends H5EStack {

    private final ProgressBar level;
    private final H5ELabel nameLabel;
    private final H5ELabel percentLabel;
    private final NinePatch background;
    private final NinePatch knob;

    public GCLevelGauge(H5ELayer layer) {
        super();
        setTouchable(Touchable.disabled);

        final ProgressBar.ProgressBarStyle originalProgressBarStyle = layer.getEngine().getSkin().get("gauge-horizontal", ProgressBar.ProgressBarStyle.class);


        final NinePatchDrawable originalBackgroundDrawable = (NinePatchDrawable) originalProgressBarStyle.background;
        this.background = new NinePatch(originalBackgroundDrawable.getPatch());
        final NinePatchDrawable originalKnobDrawable = (NinePatchDrawable) originalProgressBarStyle.knobBefore;
        knob = new NinePatch(originalKnobDrawable.getPatch());
        knob.setColor(Color.valueOf("#00FF00"));

        final NinePatchDrawable backgroundDrawable = new NinePatchDrawable(this.background);
        backgroundDrawable.setMinWidth(originalBackgroundDrawable.getMinWidth());
        backgroundDrawable.setMinHeight(originalBackgroundDrawable.getMinHeight());
        final ProgressBar.ProgressBarStyle style = new ProgressBar.ProgressBarStyle(backgroundDrawable, null);
        final NinePatchDrawable knobDrawable = new NinePatchDrawable(knob);
        knobDrawable.setMinWidth(originalKnobDrawable.getMinWidth());
        knobDrawable.setMinHeight(originalKnobDrawable.getMinHeight());
        style.knobBefore = knobDrawable;

        level = new ProgressBar(0, 100, 1, false, style);
        add(level);
        level.setValue(50);

        final Table table = new Table();
        add(table);
        table.setFillParent(true);


        nameLabel = new H5ELabel(layer);
        table.add(nameLabel).growX().left().padLeft(this.background.getPadLeft()+4);
        nameLabel.setColor(Color.valueOf("#FFFFFF"));
        nameLabel.setAlignment(Align.left);

        percentLabel = new H5ELabel(layer);
        table.add(percentLabel).growX().right().padRight(this.background.getPadRight()+2);
        percentLabel.setColor(Color.valueOf("#FFFFFF"));
        percentLabel.setAlignment(Align.right);

        setLabel("Unknown");

        setFontScale(0.7f);

        setDimensions(250, 25);
    }

    public void show() {
        setVisible(true);
    }

    public void hide() {
        setVisible(false);
    }

    private void updateLabel() {
    }

    public void setDimensions(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("All inputs must be greater than 0");
        }

        setWidth(width);
        setHeight(height);
        updateLabel();
    }


    public void setEmptyColor(Color color) {
        background.setColor(color);
    }

    public void setFullColor(Color color) {
        knob.setColor(color);
    }

    public void setLabel(String text) {
        nameLabel.setText(Strings.nullToEmpty(text));
    }

    public void setFontScale(float fontSize) {
        nameLabel.setFontScale(fontSize);
        percentLabel.setFontScale(fontSize);

        nameLabel.setOrigin(0, nameLabel.getHeight() / 2);
        percentLabel.setOrigin(percentLabel.getWidth(), percentLabel.getHeight() / 2);
    }

    public void setFontColor(Color color) {
        nameLabel.setColor(color);
        percentLabel.setColor(color);
    }

    public void setRange(float max, float step) {
        level.setRange(0, max);
        level.setStepSize(step);
    }

    public void setValue(float value) {
        level.setValue(value);

        updateLabel();
    }

    public void setValue(float value, float max) {
        setRange(max, level.getStepSize());
        level.setValue(value);

        updateLabel();
    }
    public float getValue() {
        return level.getValue();
    }
    public ProgressBar getProgressBar(){
        return level;
    }
}
