package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

@SuppressWarnings("FieldCanBeLocal")
public class GCRecipeSlotListItem extends GCListItem {

    protected final GCRecipeSlotDataItem data;

    private final H5ELabel slotDescLabel;
    private final Button itemContainer;
    private final GCItemIcon<GCInventoryItem> itemIcon;
    private final H5ELabel itemNameLabel;
    private final ImageButton clearSlotBtn;
    private final H5EScrollablePane descPane;

    public static final int ICON_SIZE = 64;
    private final int DESC_LABEL_WIDTH = 305;

    private float descPrefWidth;
    private float descWidth;

    public GCRecipeSlotListItem(H5ELayer layer, GCRecipeSlotListHandler handler, GCRecipeSlotDataItem data) {
        super(layer, handler);

        if (data.index < 0) {
            throw new IllegalArgumentException("Index can't be negative");
        }
        this.data = data;

        String desc = data.getNameWithQuantity();
        if (data.required != null && !data.required) {
            desc += " (optional)";
        }
        layer.addListener(new InputListener(){
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if(handler.isItemSelected() && handler.getSelectedItem() == GCRecipeSlotListItem.this){
                    if(keycode == Input.Keys.FORWARD_DEL) {
                        handler.onDeselectOptionBtnPressed(data);
                    }
                }
                return super.keyDown(event, keycode);
            }
        });

        Table descTable = new Table();
        add(descTable).growX();
        descTable.defaults().left().top();

        descPane = new H5EScrollablePane(layer);
        descPane.getContent().left();
        descPane.setSmoothScrolling(false);
        //descTable.add(descPane).growX();

        slotDescLabel = descTable.add(new H5ELabel(layer)).left().top().growX().getActor();
        slotDescLabel.setText(desc);
        slotDescLabel.setColor(Color.valueOf("#CCCCCC"));
        slotDescLabel.setTouchable(Touchable.disabled);
        slotDescLabel.setWrap(true);

        descPrefWidth = slotDescLabel.getPrefWidth();
        descWidth = descPrefWidth;
//        if (descPrefWidth > DESC_LABEL_WIDTH) {
//            slotDescLabel.setText(data.getNameWithQuantity()+"            ");
//            descPrefWidth = slotDescLabel.getPrefWidth();
//            slotDescLabel.setText(data.getNameWithQuantity()+"            "+data.getNameWithQuantity());
//            descWidth = slotDescLabel.getPrefWidth();
//        }

        row();

        if (!data.isItemSelected()) {
            itemContainer = null;
            itemIcon = null;
            itemNameLabel = null;
            clearSlotBtn = null;
        } else {
            GCInventoryDataItem selectedOption = data.getSelectedItem();
            itemContainer = add(new Button(getEngine().getSkin(), "gc-list-item")).growX().getActor();
            itemContainer.left().top();
            itemContainer.defaults().left().top();

            H5EStack stack = new H5EStack();
            itemContainer.add(stack).size(ICON_SIZE).padRight(5).padLeft(20).size(ICON_SIZE);

            itemIcon = new GCItemIcon<>(layer);
            itemIcon.setItem(selectedOption.item);
            stack.add(itemIcon);
            itemIcon.addListener(new ClickListener() {
                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    handler.onInfoButtonClicked(GCRecipeSlotListItem.this, data);
                    event.handle();
                }
            });

            Table subTable = new Table().left().top();
            subTable.defaults().left().top();
            itemContainer.add(subTable).grow();

            H5EScrollablePane itemPane = new H5EScrollablePane(layer);
            itemPane.getContent().left();
            itemPane.setSmoothScrolling(false);
            subTable.add(itemPane).growX();

            itemNameLabel = itemPane.add(new GCLabel(layer)).getActor();
            itemNameLabel.setWrap(true);
            itemNameLabel.setText(selectedOption.item.getName());
            itemNameLabel.setColor(Color.valueOf("#FFFFFF"));
            itemNameLabel.setTouchable(Touchable.disabled);

            subTable.row();

            clearSlotBtn = descTable.add(new ImageButton(getSkin(),"btn-close-window")).left().top().getActor();
           // descPane.add().growX();
            clearSlotBtn.setColor(Color.valueOf("#CCCCCC"));
            clearSlotBtn.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    handler.onDeselectOptionBtnPressed(data);
                }
            });
        }

    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isChecked() || isOver()) {
            descPane.setSmoothScrolling(false);
            float pos = descPane.getScrollPercentX();
            float loopPos = descPrefWidth / (descWidth - DESC_LABEL_WIDTH);
            if (pos > loopPos) pos -= loopPos;
            pos += delta * 0.1f;
            descPane.setScrollPercentX(pos);
        } else {
            descPane.setSmoothScrolling(true);
            descPane.setScrollPercentX(0);
        }
    }

    static abstract class GCRecipeSlotListHandler extends GCListItemActionHandler<GCRecipeSlotListItem> {

        void updateSelection(GCRecipeSlotListItem item) {
            super.setSelection(item);
        }

        @Override
        protected final void onSelectionUpdate(GCRecipeSlotListItem lastSelectedItem) {
            GCRecipeSlotListItem selectedItem = getSelectedItem();
            if (selectedItem == null) {
                onSlotDeselected();
            } else {
                onSlotSelected(selectedItem.data);
            }
        }

        public abstract void onSlotDeselected();

        public abstract void onSlotSelected(GCRecipeSlotDataItem slot);

        public abstract void onDeselectOptionBtnPressed(GCRecipeSlotDataItem slot);

        public abstract void onInfoButtonClicked(Actor actor, GCRecipeSlotDataItem slot);
    }

}
