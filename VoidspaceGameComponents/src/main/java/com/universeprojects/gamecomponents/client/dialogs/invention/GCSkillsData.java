package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;

import java.util.ArrayList;
import java.util.List;

public class GCSkillsData {

    private final List<GCSkillDataItem> items = new ArrayList<>();

    private GCSkillsData() {
    }

    public static GCSkillsData createNew() {
        return new GCSkillsData();
    }

    public GCSkillsData add(GCSkillsData other) {
        for (GCSkillDataItem item : other.items) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item);
        }
        return this;
    }

    public void add(GCSkillDataItem item) {
        add(item.id, item.ideaId, item.name, item.description, item.iconKey, item.parentId, item.baseSeconds, item.rarityColor);
    }

    public GCSkillsData add(Long id, Long ideaId, String name, String description, String iconKey, Long parentId, long baseSeconds, Color rarityColor) {
        int index = items.size();
        items.add(new GCSkillDataItem(index, ideaId, id, name, description, iconKey, parentId, baseSeconds, rarityColor));
        return this;
    }

    public GCSkillDataItem get(int index) {
        return items.get(index);
    }

    public void remove(int index) {
        items.remove(index);
    }

    public void removeById(long skillId) {
        GCSkillDataItem itemToRemove = null;
        for (GCSkillDataItem item:items) {
            if (item.id == skillId) {
                itemToRemove = item;
                break;
            }
        }
        if (itemToRemove!=null)
            items.remove(itemToRemove);
    }

    public int size() {
        return items.size();
    }

    public static class GCSkillDataItem {
        final int index;

        public Long id;
        public Long ideaId;
        public String name;
        public final String description;
        public String iconKey;
        public Long parentId;
        public long baseSeconds;
        public Color rarityColor;

        GCSkillDataItem(int index, Long id, Long ideaId, String name, String description, String iconKey, Long parentId, long baseSeconds, Color rarityColor) {
            this.index = index;

            this.id = id;
            this.ideaId = ideaId;
            this.name = name;
            this.description = description;
            this.iconKey = iconKey;
            this.parentId = parentId;
            this.baseSeconds = baseSeconds;
            this.rarityColor = rarityColor;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                .append("index: ").append(index).append(", ")
                .append("id: ").append(id).append(", ")
                .append("name: ").append(name).append(", ")
                .append("description: ").append(description).append(", ")
                .append("iconKey: ").append(iconKey).append(", ")
                .append("parentId: ").append(parentId).append(", ")
                .toString();

        }
    }

}


