package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.elements.GCSwitch;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCSettingDialog {
    protected final GCSimpleWindow window;
    protected final VerticalGroup buttonPanel;
    protected Table settingPanel;

    protected final ButtonGroup<H5EButton> buttons;
    protected final H5ELayer layer;
    protected final Skin skin;

    public GCSettingDialog(H5ELayer layer) {
        window = new GCSimpleWindow(layer, "info-dialog", "Settings", 1000, 700, false);
        this.layer = layer;
        skin = window.getSkin();
        buttonPanel = new VerticalGroup();
        settingPanel = new Table();
        buttons = new ButtonGroup<>();
        buttons.setMaxCheckCount(1);
        buttons.setMinCheckCount(1);

        // add name of the buttons and event
        addSetting("Graphics", this::openGraphics);
        addSetting("Controls", this::openControls);
        addSetting("Sound", this::openSound);
        addSetting("Networking", this::openNetworking);

        window.add(buttonPanel).top().left();
        window.add(settingPanel).expand().top().left();
    }

    public void addSetting(String name, Callable0Args event) {
        H5EButton btn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText(name).build();
        btn.addCheckedButtonListener(event);
        buttons.add(btn);
        buttonPanel.addActor(btn);
        buttonPanel.grow();
    }

    private void display(Table setting) {
        settingPanel.clearChildren();
        settingPanel.add(setting);
    }

    private void openGraphics() {
        Table setting = new Table();
        H5ELabel title = new H5ELabel(layer);
        title.setFontScale(1);
        title.setText("Graphics");
        setting.add(title);
        setting.row();

        setting.add(new Label("Dustfield", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        setting.add(new Label("Effects", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        setting.add(new Label("Particles", skin));
        SelectBox<String> particles = new SelectBox<>(layer.getEngine().getSkin());
        particles.setItems("Low", "Medium", "High");
        particles.setSelected("Low");
        setting.add(particles).right();
        setting.row();

        setting.add(new Label("Resolution", skin));
        SelectBox<String> resolution = new SelectBox<>(layer.getEngine().getSkin());
        resolution.setItems("1024x768", "1280x800", "1366x768", "1600x1024", "1920x1080");
        resolution.setSelected("1024x768");
        setting.add(resolution).right();
        display(setting);
    }

    private void openControls() {
        Table setting = new Table();
        H5ELabel title = new H5ELabel(layer);
        title.setFontScale(1);
        title.setText("Controls");
        setting.add(title);
        setting.row();

        setting.add(new Label("Onscreen Controls", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        setting.add(new Label("User Interface", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        setting.add(new Label("Key Bindings", skin));
        SelectBox<String> binding = new SelectBox<>(layer.getEngine().getSkin());
        binding.setItems("A", "B");
        binding.setSelected("A");
        setting.add(binding).right();
        setting.row();

        H5EButton btnRotate = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("View Point Rotation").build();
        setting.add(btnRotate);
        setting.row();
        display(setting);
    }

    private void openSound() {
        Table setting = new Table();
        H5ELabel title = new H5ELabel(layer);
        title.setFontScale(1);
        title.setText("Sound");
        setting.add(title);
        setting.row();

        setting.add(new Label("Master Volume", skin));
        H5EInputBox masterField = new H5EInputBox(layer);
        masterField.setTypeNumber();

        GCSlider masterSlider = new GCSlider(layer);
        masterSlider.attachInputBox(masterField);
        masterSlider.setRange(0, 100, 1);
        masterSlider.setValue(100);

        setting.add(masterSlider);
        setting.add(masterField);
        setting.row();

        setting.add(new Label("Music", skin));
        H5EInputBox musicField = new H5EInputBox(layer);
        musicField.setTypeNumber();

        GCSlider musicSlider = new GCSlider(layer);
        musicSlider.attachInputBox(musicField);
        musicSlider.setRange(0, 100, 1);
        musicSlider.setValue(100);

        setting.add(musicSlider);
        setting.add(musicField);
        setting.row();

        setting.add(new Label("Sound Effect", skin));
        H5EInputBox effectField = new H5EInputBox(layer);
        effectField.setTypeNumber();

        GCSlider effectSlider = new GCSlider(layer);
        effectSlider.attachInputBox(effectField);
        effectSlider.setRange(0, 100, 1);
        effectSlider.setValue(100);

        setting.add(effectSlider);
        setting.add(effectField);
        setting.row();

        display(setting);
    }

    private void openNetworking() {
        Table setting = new Table();
        H5ELabel title = new H5ELabel(layer);
        title.setFontScale(1);
        title.setText("Networking");
        setting.add(title);
        setting.row();

        H5EButton btnPing = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Ping Server").build();
        Label ping = new Label("", skin);

        btnPing.addButtonListener(()-> ping.setText("123"));
        setting.add(btnPing);
        setting.add(ping);
        setting.row();

        setting.add(new Label("Toggle Fake Lag", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        setting.add(new Label("Toggle Sync-Only Mode", skin));
        setting.add(new GCSwitch(layer)).right();
        setting.row();

        H5EButton btnInfo = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Network Info").build();
        Label info = new Label("", skin);
        btnInfo.addButtonListener(()-> info.setText("Lorem ipsum dolor sit amet."));
        setting.add(btnInfo);
        setting.add(info);
        display(setting);
    }

    private void openSetting5() {
        Table setting = new Table();
        setting.add(new Label("5", skin));
        display(setting);
    }

    public void open() {
        window.open();
    }

    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }
}
