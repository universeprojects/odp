package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.TextTooltip;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.GCSelectionIndicator;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndSource;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndTarget;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCTextTooltip;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.inputs.H5EGamePad;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GCActionBar extends Window implements GraphicElement {
    /* Button row config */
    public static final String MISSING_ACTION_ICON = "images/icons/Advanced Processor.png";
    public static final float SELECTION_MOVE_TIME = 0.25f;
    protected static final int BUTTON_SPACING = 62;
    protected static final int FIRST_SLOT_POSITION = 178;
    protected static final int BUTTON_SLOT_Y = 26;
    /* Action bar image adjustment (all these variables are tentative) */
    private static final int IMAGE_WIDTH = 992;
    private static final int IMAGE_HEIGHT = 160;
    /* Button size */
    private static final int BUTTON_WIDTH = 48;
    private static final int BUTTON_HEIGHT = 50;
    private static final int LABEL_X_POS = 6;
    private static final int LABEL_Y_POS = 8;
    /* Side buttons config */
    private static final int SIDES_Y = 64;
    private static final int SIDES_RIGHT_X = 894;
    private static final int SIDES_LEFT_X = 48;
    private static final int SIDES_LABEL_X = 22;
    private static final int SIDES_LABEL_Y = -20;
    /* Gauge image sizes */
    private int gaugeWidth = 49;
    private int gaugeHeight = 113;
    /*  Gauge Positions  */
    private static final int ALL_GAUGE_Y = 35;
    private static final int OUTER_LEFT_GAUGE_X = 16;
    private static final int INNER_LEFT_GAUGE_X = 82;
    private static final int OUTER_RIGHT_GAUGE_X = 928;
    private static final int INNER_RIGHT_GAUGE_X = 861;

    protected final H5ELayer layer;
    private final GCActionBarController actionBarController;
    protected Map<String, ProgressBar> actionGauge = new LinkedHashMap<>();
    protected List<ProgressBar> actionGaugeList = new ArrayList<>();
    protected GCActionBarButton[] buttons = new GCActionBarButton[GCActionBarType.MAIN.getButtonCount()];
    protected GCActionBarButton specialButtonLeft;
    protected GCActionBarButton specialButtonRight;
    private H5EButton actionListButton;
    private int currentRow = 0;
    private int currentId = 0;
    private GCSelectionIndicator indicator;
    private float buttonTime = SELECTION_MOVE_TIME;

    private ActionBarMode mode = null;

    public GCActionBar(H5ELayer layer, GCActionBarController actionBarController, H5EButton actionListButton) {
        super("", layer.getEngine().getSkin(), "action-bar");
        this.actionListButton = actionListButton;
        this.layer = layer;
        this.actionBarController = actionBarController;

        setWidth(IMAGE_WIDTH);
        setHeight(IMAGE_HEIGHT);
        setY(0);

        setClip(false);
        setMovable(false);
        setKeepWithinStage(false);

        /* This lambda ensures the bar retains its position and size when the window changes*/
        getEngine().onResize.registerHandler(this::doResize);

        /* Adds actionGauges, from left to right. */
        addActionGauge(GaugeColor.YELLOW, "fuel", OUTER_LEFT_GAUGE_X, ALL_GAUGE_Y, false, false);
        addActionGauge(GaugeColor.RED, "health", INNER_LEFT_GAUGE_X, ALL_GAUGE_Y, true, false);
        addActionGauge(GaugeColor.GREEN, "energy-controlled", INNER_RIGHT_GAUGE_X, ALL_GAUGE_Y, false, false);
        addActionGauge(GaugeColor.GREY, "oxygen", OUTER_RIGHT_GAUGE_X, ALL_GAUGE_Y, true, false);


        /* Add the two side buttons */

        specialButtonLeft = addButton(GCActionBarType.SIDE, SIDES_LEFT_X, SIDES_Y, SIDES_LABEL_X, SIDES_LABEL_Y, "Q", 0);
        Tutorials.registerObject("action-prim", specialButtonLeft);
        specialButtonRight = addButton(GCActionBarType.SIDE, SIDES_RIGHT_X, SIDES_Y, SIDES_LABEL_X, SIDES_LABEL_Y, "E", 1);
        Tutorials.registerObject("action-sec", specialButtonRight);

        for (int i = 0; i < buttons.length; i++) {
            String numericalIcon = i == 9 ? "0" : String.valueOf(i + 1);

            buttons[i] = addButton(GCActionBarType.MAIN, FIRST_SLOT_POSITION + (i * BUTTON_SPACING), BUTTON_SLOT_Y, LABEL_X_POS, LABEL_Y_POS, numericalIcon, i);
            buttons[i].dndSource.setRow(getCurrentRow());
        }

        updateButtonRow();

        //hide();
        show();

        indicator = new GCSelectionIndicator(this, layer);
        indicator.setColor(0f,1,0.2f,1);
        doResize();
    }

    public void rightTrigger(){
        actionBarController.callAction(GCActionBarType.MAIN, 0, currentId);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if(!WindowManager.getInstance(getEngine()).isUINavigationActive()){
            Controller controller = Controllers.getControllers().isEmpty() ? null : Controllers.getControllers().get(0);
            if (controller == null) return;
            if(buttonTime>=SELECTION_MOVE_TIME) {
                buttonTime = 0;
                if (controller.getButton(H5EGamePad.RIGHT_BUTTON)) {
                    currentId++;
                    if(currentId>=buttons.length){
                        currentId = 0;
                    }
                    indicator.activate(buttons[currentId]);
                }else if(controller.getButton(H5EGamePad.LEFT_BUTTON)){
                    currentId--;
                    if(currentId<0){
                        currentId = buttons.length-1;
                    }
                    indicator.activate(buttons[currentId]);
                }else{
                    buttonTime = SELECTION_MOVE_TIME;
                }
            }else{
                buttonTime += delta;
            }
        }
    }

    void doResize() {
        int width = getEngine().getWidth();
        int height = getEngine().getHeight();
        setX(width/2f - getWidth()/2f);

        if (width < height || width < 1280) {
            setMode(ActionBarMode.MOBILE);
            specialButtonLeft.setIncreaseTouchAreaMobile(true);
            specialButtonRight.setIncreaseTouchAreaMobile(true);
        } else {
            setMode(ActionBarMode.NORMAL);
            specialButtonLeft.setIncreaseTouchAreaMobile(false);
            specialButtonRight.setIncreaseTouchAreaMobile(false);
        }
    }

    public void switchPage(boolean next) {
        int newPage = getCurrentRow() + (next ? 1 : -1);
        if (newPage >= 0 && newPage < GCActionBarType.MAIN.getRowCount())
            setCurrentRow(newPage);
    }

    /* Adds a label representing keyBinding and an image to the table, either can be left blank  */
    private GCActionBarButton addButton(GCActionBarType type, int buttonX, int buttonY, int labelX, int labelY, String keyBinding, int index) {
        DragAndDrop dragAndDrop = InventoryManager.getInstance(getEngine()).getDragAndDrop();

        GCActionBarButton actionButton = new GCActionBarButton(type, layer, keyBinding, this, actionBarController, index);

        addActor(actionButton);

        actionButton.dndSource = new GCActionBarDndSource(actionButton, null, layer, type, index);
        actionButton.dndTarget = new GCActionBarDndTarget(actionButton, actionBarController, type, getCurrentRow(), index);
        dragAndDrop.addTarget(actionButton.dndTarget);
        dragAndDrop.addSource(actionButton.dndSource);

        /* Adjust the button */
        actionButton.setPosition(buttonX, buttonY);
        actionButton.setSize(BUTTON_WIDTH, BUTTON_HEIGHT );
        actionButton.setLabelPosition(labelX, labelY);

        return actionButton;
    }

    public void updateButtonRow() {
        setButtonOperation(actionBarController.getAction(GCActionBarType.SIDE, 0, 0), specialButtonLeft);
        setButtonOperation(actionBarController.getAction(GCActionBarType.SIDE, 0, 1), specialButtonRight);

        for (int i = 0; i < buttons.length; i++) {
            setButtonOperation(actionBarController.getAction(GCActionBarType.MAIN, getCurrentRow(), i), buttons[i]);
        }
    }

    protected void setButtonOperation(GCOperation action, GCActionBarButton button) {
        button.setReactiveIcon(action == null ? null : action.getIcons());
        button.dndSource.setAction(action);
        if(action != null) {
            String actionHumanName = action.getActionHumanName();
            button.addListener(new TextTooltip(actionHumanName == null ? "Action" : actionHumanName, layer.getEngine().getSkin()));
        }
        button.dndSource.setRow(getCurrentRow());
        button.dndTarget.setRow(getCurrentRow());
    }

    /* Adds an actionGauge to the bar */
    private void addActionGauge(GaugeColor gaugeColor, String key, int gaugeX, int gaugeY, boolean rightOriented, boolean small) {

        String style; // Holds the style that is passed on to progress bar constructor

        /* insert direction to the style string to indicate which direction the bar should face */
        if (rightOriented) {
            style = "right";
        } else {
            style = "left";
        }

        style += "-gauge-" + gaugeColor.toString().toLowerCase();
        if (small) style += "-small";


        ProgressBar gaugeBar = new ProgressBar(0, 100, 1, true, layer.getEngine().getSkin(), style);

        addActor(gaugeBar);
        gaugeBar.setValue(100);

        /* Set positions */
        gaugeBar.setHeight(gaugeHeight);
        gaugeBar.getStyle().knobBefore.setMinWidth(gaugeWidth);
        gaugeBar.setWidth(gaugeWidth);
        gaugeBar.setPosition(gaugeX, gaugeY);
        actionGauge.put(key, gaugeBar);
        actionGaugeList.add(gaugeBar);
        gaugeBar.addListener(new GCTextTooltip(key.substring(0, 1).toUpperCase() + key.substring(1), layer.getEngine().getSkin()));
    }

    public int getCurrentRow() {
        return currentRow;
    }

    public void setCurrentRow(int row) {
        if (currentRow == row) return;
        System.out.println("Now viewing page number " + row);
        this.currentRow = row;
        updateButtonRow();
    }

    public void setGaugeColour(String key, GaugeColor colour) {
        ProgressBar gauge = actionGauge.get(key);
        for (int i = 0; i < actionGaugeList.size(); i++) {
            if (actionGaugeList.get(i) == gauge) {
                if (i == 0 || i == 2) {
                    String style = "left-gauge-" + colour.toString().toLowerCase();
                    gaugeStyle(key, style);
                } else if (i == 1 || i == 3) {
                    String style = "right-gauge-" + colour.toString().toLowerCase();
                    gaugeStyle(key, style);
                }
            }
        }

    }

    /* Changes the style for a actionGauge given an index*/
    private void gaugeStyle(String key, String style) {
        actionGauge.get(key).setStyle(layer.getEngine().getSkin().get(style, ProgressBar.ProgressBarStyle.class));
    }

    /**
     * Set level of the gauge value
     *
     * @param key   gauge name
     * @param value new gauge value
     */
    public void setGaugeValue(String key, float value) { //maybe not public
        ProgressBar gauge = actionGauge.get(key);
        if (gauge == null) {
            throw new IllegalArgumentException("Unable to find gauge: " + key);
        }
        if (gauge.getValue() == value) {
            return;
        }
        gauge.setValue(value);
        H5ELayer.invalidateBufferForActor(this);
    }

    public ProgressBar getActionGauge(String key) {
        return actionGauge.get(key);
    }

    public void changeActionGauge(String oldKey, String key) {
        ProgressBar obj = actionGauge.remove(oldKey);
        actionGauge.put(key, obj);
    }

    public void show() {
        align(Align.bottom);
        positionProportionally(.5f);
        setVisible(true);
        toFront();
    }

    public void hide() {
        setVisible(false);
    }

    public void dismiss() {
        clear();
        hide();
    }

    // Set to center
    public void positionProportionally(float propX) {
        setX(getEngine().getWidth() * propX - getOriginX() - getWidth() / 2f);
        setY(0);
    }

    public void positionProportionally(float propX, float propY) {
        setX(getEngine().getWidth() * propX - getOriginX() - getWidth() / 2f);
        setY(getEngine().getHeight() * propY - getOriginY() - getHeight() / 2f);
    }

    public void setCaption(String text) {
        getTitleLabel().setText(text);
    }

    @Override
    public void clear() {
        setCaption("");
    }

    @Override
    public H5ELayer getLayer() {
        //return (H5ELayer) getStage();
        return layer;
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public enum GaugeColor {
        BLUE,
        GREEN,
        YELLOW,
        ORANGE,
        PURPLE,
        PINK,
        GREY,
        RED
    }

    public enum ActionBarMode {
        NORMAL,
        MOBILE
    }

    public GCActionBar.ActionBarMode getMode() {
        return this.mode;
    }

    public void setMode(GCActionBar.ActionBarMode mode) {
        if (this.mode == mode) return;
        this.mode = mode;

        // Variables required to update layout
        String newStyle = "action-bar";
        int newFirstSlotPosition = FIRST_SLOT_POSITION;
        int newButtonSlotY = BUTTON_SLOT_Y;
        int buttonNumber = buttons.length;
        int newImageWidth = IMAGE_WIDTH;
        int newImageHeight = IMAGE_HEIGHT;
        int newSidesY = SIDES_Y;
        int newSidesRightX = SIDES_RIGHT_X;
        int newSidesLeftX = SIDES_LEFT_X;
        int newSidesLabelY = SIDES_LABEL_Y;
        gaugeWidth = 49;
        gaugeHeight = 113;
        boolean smallGauges = false;
        int newAllGaugeY = ALL_GAUGE_Y;
        int newOuterLeftGaugeX = OUTER_LEFT_GAUGE_X;
        int newInnerLeftGaugeX = INNER_LEFT_GAUGE_X;
        int newOuterRightGaugeX = OUTER_RIGHT_GAUGE_X;
        int newInnerRightGaugeX = INNER_RIGHT_GAUGE_X;
        float specialButtonHeight = BUTTON_HEIGHT;
        float specialButtonWidth = BUTTON_WIDTH;

        if (mode == ActionBarMode.MOBILE) {
            newStyle = "action-bar-mobile";
            newFirstSlotPosition = 37;
            newButtonSlotY = 4;
            buttonNumber = UPMath.min(buttons.length, 6);
            newImageWidth = 462;
            newImageHeight = 138;
            newSidesY = (int) (78 + BUTTON_WIDTH * 0.25F);
            newSidesRightX = (int) (390+ BUTTON_WIDTH * 0.25F);
            newSidesLeftX = (int) (30+ BUTTON_WIDTH * 0.25F);
            newSidesLabelY = -8;
            gaugeWidth = 27;
            gaugeHeight = 60;
            smallGauges = true;
            newAllGaugeY = 73;
            newOuterLeftGaugeX = 24;
            newInnerLeftGaugeX = 58;
            newOuterRightGaugeX = 419;
            newInnerRightGaugeX = 387;
            specialButtonHeight = BUTTON_HEIGHT / 2f;
            specialButtonWidth = BUTTON_WIDTH / 2f;
        }

        clearChildren();
        actionGauge.clear();
        actionGaugeList.clear();

        setStyle(layer.getEngine().getSkin().get(newStyle, Window.WindowStyle.class));
        setWidth(newImageWidth);
        setHeight(newImageHeight);

        /* Adds actionGauges, from left to right. */
        addActionGauge(GaugeColor.YELLOW, "fuel", newOuterLeftGaugeX, newAllGaugeY, false, smallGauges);
        addActionGauge(GaugeColor.RED, "health", newInnerLeftGaugeX, newAllGaugeY, true, smallGauges);
        addActionGauge(GaugeColor.GREEN, "energy", newInnerRightGaugeX, newAllGaugeY, false, smallGauges);
        addActionGauge(GaugeColor.GREY, "oxygen", newOuterRightGaugeX, newAllGaugeY, true, smallGauges);

        specialButtonLeft.setPosition(newSidesLeftX, newSidesY);
        specialButtonLeft.setLabelPosition(SIDES_LABEL_X, newSidesLabelY);
        specialButtonLeft.setSize(specialButtonWidth, specialButtonHeight);
        addActor(specialButtonLeft);

        specialButtonRight.setPosition(newSidesRightX, newSidesY);
        specialButtonRight.setLabelPosition(SIDES_LABEL_X, newSidesLabelY);
        specialButtonRight.setSize(specialButtonWidth, specialButtonHeight);
        addActor(specialButtonRight);

        if (actionListButton!=null) {
            addActor(actionListButton);
            actionListButton.setPosition(getWidth()*0.19f, getHeight() * 0.83f - actionListButton.getHeight());
        }


        for (int i = 0; i < buttonNumber; i++) {
            buttons[i].setPosition(newFirstSlotPosition + i*BUTTON_SPACING, newButtonSlotY);
            addActor(buttons[i]);
        }

        setX(getEngine().getWidth()/2f - getWidth()/2f);

    }
}
