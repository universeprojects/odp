package com.universeprojects.gamecomponents.client.selection;

import com.universeprojects.common.shared.util.Dev;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

class GCSelectionMenuNode implements Comparable<GCSelectionMenuNode> {

    private final int index;
    private final String name;
    private final GCSelectionMenuNode parent;
    private Map<String, GCSelectionMenuNode> childrenByName = null;
    private List<GCSelectionMenuNode> children = null;
    private List<GCSelectionMenuNode> children_view = null;
    private final float priority;

    GCSelectionMenuNode(String name) {
        this(0, name, null, 0);
    }

    private GCSelectionMenuNode(int index, String name, GCSelectionMenuNode parent, float priority) {
        this.index = index;
        this.name = Dev.checkNotEmpty(name);
        this.parent = parent;
        this.priority = priority;
    }

    @Override
    public int compareTo(GCSelectionMenuNode o) {
        final int priorityComparison = Float.compare(priority, o.priority);
        if(priorityComparison != 0) {
            return priorityComparison;
        }
        return name.compareTo(o.name);
    }

    int getIndex() {
        return index;
    }

    String getName() {
        return name;
    }

    GCSelectionMenuNode getParent() {
        return parent;
    }

    GCSelectionMenuNode getChild(String name, float nodePriority, boolean createIfNotFound) {
        Dev.checkNotEmpty(name);
        if (children == null) {
            childrenByName = new TreeMap<>();
            children = new ArrayList<>();
            children_view = Collections.unmodifiableList(children);
        }
        if (childrenByName.containsKey(name)) {
            return childrenByName.get(name);
        }

        if (createIfNotFound) {
            int nextIndex = children.size();
            GCSelectionMenuNode newChild = new GCSelectionMenuNode(nextIndex, name, this, nodePriority);

            children.add(newChild);
            childrenByName.put(name, newChild);

            return newChild;
        }
        throw new IllegalStateException("Child menu node not found for name: " + name);
    }

    boolean hasChildren() {
        return children != null && children.size() > 0;
    }

    void sortChildren() {
        if(children != null) {
            //noinspection Java8ListSort,Convert2Lambda
            Collections.sort(children, new Comparator<GCSelectionMenuNode>() {
                @Override
                public int compare(GCSelectionMenuNode o1, GCSelectionMenuNode o2) {
                    return o2.compareTo(o1);
                }
            });
        }
    }

    List<GCSelectionMenuNode> getOrderedChildren() {
        sortChildren();
        return children_view;
    }

    void clearChildren() {
        if (children != null) {
            children.clear();
        }
        if (childrenByName != null) {
            childrenByName.clear();
        }
    }

    public float getPriority() {
        return priority;
    }

    String getPath() {
        if (parent == null) {
            return "/"; // root
        }
        return parent.getPath() + name + (hasChildren() ? "/" : "");
    }

}
