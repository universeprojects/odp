package com.universeprojects.gamecomponents.client.dialogs.login;

import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public abstract class GCServerSelectorItem extends GCListItem {

    protected GCServerSelectorItem(H5ELayer layer, GCListItemActionHandler<?> actionHandlerIn) {
        super(layer, actionHandlerIn);
    }

    @Override
    public abstract String toString();

}
