package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class UILayerNavigation extends MatrixUINavigation{

    public UILayerNavigation(H5ELayer layer) {
        super(layer);
    }

    @Override
    public void setWindow(Window window, Group rootGroup) {
        super.setWindow(window, layer.getRoot());
    }

    @Override
    public void exit() {
        if (tryExit()) {
            if (currentGroup != rootGroup) {
                if (isInsidePanel(currentActor) != null) {
                    lastListItem = currentActor;
                }
                currentActor = currentGroup;
                currentGroup = rootGroup;
                update();
            } else {
                indicator.deactivate();
            }
        }
    }

    @Override
    public void tryToExtractActors(Actor actor) {
        if((!(actor instanceof Window) || actor.equals(window)) && actor.isVisible())
        super.tryToExtractActors(actor);
    }

    @Override
    protected boolean isFocusable(Actor actor) {
        if(!(actor instanceof Window) || actor.equals(window))
            return super.isFocusable(actor);
        else return false;
    }
}
