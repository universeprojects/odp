package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;

/**
 * user for scrolling by gamepad in list with labels
 */
public class LabelListNavigationExtension extends NavigationExtension{
    protected H5EScrollablePane panel;
    public LabelListNavigationExtension(UINavigation navigation, H5EScrollablePane panel) {
        super(navigation);
        this.panel = panel;
    }

    @Override
    public void enter() {

    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof Label && actor.isDescendantOf(panel.getContent());
    }

    @Override
    public void init(Actor actor) {
        active = false;
    }
}
