package com.universeprojects.gamecomponents.client.dialogs.invention;

import java.util.ArrayList;
import java.util.List;

public class GCIdeaCategoryData {

    public List<GCIdeaCategoryDataItem> children = new ArrayList<>();

    public GCIdeaCategoryData() {
    }

    public static GCIdeaCategoryData createNew() {
        return new GCIdeaCategoryData();
    }

    public GCIdeaCategoryData.GCIdeaCategoryDataItem add(long id, String name, boolean isParentCategory, String description) {
        int index = children.size();
        GCIdeaCategoryData.GCIdeaCategoryDataItem item = new GCIdeaCategoryData.GCIdeaCategoryDataItem(id, index, name, isParentCategory, description);
        children.add(item);
        return item;
    }

    public GCIdeaCategoryData.GCIdeaCategoryDataItem get(int index) {
        return children.get(index);
    }

    public int size() {
        return children.size();
    }

    public static class GCIdeaCategoryDataItem extends GCIdeaCategoryData {
        public int index;
        public long id;
        public String name;
        public boolean isParentCategory;
        public String description;

        public GCIdeaCategoryDataItem(long id, int index, String name, boolean isParentCategory, String description) {
            this.index = index;
            this.id = id;
            this.name = name;
            this.isParentCategory = isParentCategory;
            this.description = description;
        }
    }
}