package com.universeprojects.gamecomponents.client.tutorial;

import com.universeprojects.html5engine.client.framework.H5ELayer;

public class TutorialInfoDialogs {
    @SuppressWarnings("SwitchStatementWithTooFewBranches")
    public static TutorialInfoDialog generateDialog(H5ELayer layer, String dialogId, String triggerName) {
        switch (dialogId) {
            case StartInfoDialog.NAME:
                return new StartInfoDialog(layer, triggerName);
            default:
                return null;
        }
    }

    public static TutorialInfoDialog openDialog(H5ELayer layer, String dialogId, String trigger) {
        final TutorialInfoDialog dialog = generateDialog(layer, dialogId, trigger);
        if(dialog != null) {
            dialog.open();
        }
        return dialog;
    }
}
