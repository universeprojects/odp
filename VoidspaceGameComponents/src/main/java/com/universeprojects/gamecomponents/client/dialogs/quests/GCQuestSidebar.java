package com.universeprojects.gamecomponents.client.dialogs.quests;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.List;

/**
 * This class describes the quest quick-view sidebar
 */
public class GCQuestSidebar extends GCWindow {
    /**
     * The default width of the sidebar
     */
    private static final int DEFAULT_WIDTH = 350;

    /**
     * The default height of the sidebar
     */
    private static final int DEFAULT_HEIGHT = 300;

    /**
     * The default proportions of the screen for positioning the sidebar
     */
    private static final float DEFAULT_Y_PROPORTION = 1.0f;//0.55

    private final GCLabel title;

    /**
     * A list of the currently displayed quests
     */
    private final H5EScrollablePane displayedQuestsScrollPane;
    private final GCList<GCQuestSidebarListItem> displayedQuests;

    /**
     * A reference to the list of quests that can be displayed in the sidebar
     */
    private List<GCQuestListItem> questItemList = null;

    private boolean toggledOn = true;
    private boolean disabled = false;

    private int maxHeightFromTop = 280;

    final H5EButton buttonHelp;
    private String helperUrl = null;
    private String currentQuestId = null;

    final H5EButton buttonReplayTutorials;
    final H5EButton buttonSkipQuest;

    /**
     * Constructors for the sidebar
     */
    public GCQuestSidebar(H5ELayer layer) {
        this(layer, DEFAULT_WIDTH, DEFAULT_HEIGHT,  "clear-window");
    }

    private GCQuestSidebar(H5ELayer layer, Integer width, Integer height, String styleName) {
        super(layer, width, height, styleName);

        //Set the window's properties
        //padTop(25);
        setMovable(false);
        setPackOnOpen(true);
        setCloseButtonEnabled(false);
        setCloseable(false);
        setClickThrough(true);
        setTouchable(Touchable.childrenOnly);
        getTitleLabel().setTouchable(Touchable.disabled);

        //Add the list of quests to the sidebar
        row();
        title = new GCLabel("", layer);
        add(title);
        row();
        displayedQuests = new GCList<>(layer, 1, 10, 5, true);
        displayedQuestsScrollPane = new H5EScrollablePane(layer);
        displayedQuestsScrollPane.add(displayedQuests).grow().left().padLeft(0);
        add(displayedQuestsScrollPane).padTop(8).grow();

        row();
        Table buttonList = new Table();
        final H5ESpriteType helpButtonSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType("images/GUI/ui2/button-bar-buttons/help1.png");
        final ImageTextButton.ImageTextButtonStyle helpButtonStyle = StyleFactory.createButtonStyle(helpButtonSpriteType.getTextureRegion(), StyleFactory.INSTANCE.font);
        buttonHelp = new H5EButton(layer, helpButtonStyle);
        buttonHelp.setTooltip("Video demonstration of this quest");
        buttonHelp.addButtonListener(() -> onHelpButtonClick(helperUrl));
        buttonHelp.setVisible(false);
        buttonList.add(buttonHelp).padTop(4).padRight(4).align(Align.left);

        final H5ESpriteType skipQuestButtonSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType("images/GUI/ui2/button-skip-normal.png");
        final ImageTextButton.ImageTextButtonStyle skipQuestButtonStyle = StyleFactory.createButtonStyle(skipQuestButtonSpriteType.getTextureRegion(), StyleFactory.INSTANCE.font);
        buttonSkipQuest = new H5EButton(layer, skipQuestButtonStyle);
        buttonSkipQuest.setTooltip("Skip this quest");
        buttonSkipQuest.addButtonListener(() -> onSkipQuestButtonClick(currentQuestId));
        buttonList.add(buttonSkipQuest).padTop(4).padRight(4).align(Align.left);
        buttonSkipQuest.setVisible(false);

        final H5ESpriteType replayTutorialsButtonSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType("images/GUI/ui2/button-replay1-normal.png");
        final ImageTextButton.ImageTextButtonStyle replayTutorialsButtonStyle = StyleFactory.createButtonStyle(replayTutorialsButtonSpriteType.getTextureRegion(), StyleFactory.INSTANCE.font);
        buttonReplayTutorials = new H5EButton(layer, replayTutorialsButtonStyle);
        buttonReplayTutorials.setTooltip("Replay tutorial for this quest");
        buttonReplayTutorials.addButtonListener(this::onReplayTutorialsButtonClick);
        buttonList.add(buttonReplayTutorials).padTop(4).padRight(4).align(Align.left);
        buttonReplayTutorials.setVisible(false);

        add(buttonList).align(Align.left);

        //Start in the displayed state by default
        open();

        updatePosition(DEFAULT_WIDTH, DEFAULT_HEIGHT);

        // Note: the sidebar toggle button is in a separate class called "QuestSidebarButton"
    }

    protected void onHelpButtonClick(String helperUrl) {

    }

    protected void onSkipQuestButtonClick(String currentQuestId) {

    }

    protected void onReplayTutorialsButtonClick() {

    }


    /**
     * Set the sidebar's reference to the list of quests that can be displayed
     * @param quests The list of quests that can be displayed
     */
    public void link (List<GCQuestListItem> quests) {
        questItemList = quests;
    }

    /**
     * Refresh the quests displayed on the sidebar
     */
    public void refresh () {
        //Open the sidebar if it is not already
        if (!isOpen()) super.open();
        setVisible(toggledOn && !disabled);

        //Clear the currently displayed quests
        displayedQuests.clear();
        displayedQuests.getButtonGroup().clear();

        //Load quests to display from the reference list
        if (questItemList == null || questItemList.isEmpty()) {
            setTitle("No Quests");
            setBackground((Drawable) null);
            displayedQuestsScrollPane.setVisible(false);
            buttonHelp.setVisible(false);
            buttonReplayTutorials.setVisible(false);
            buttonSkipQuest.setVisible(false);
            return;
        }
        else {
            setBackground(StyleFactory.INSTANCE.panelStyleDarkBorderSemiTransparent);
            displayedQuestsScrollPane.setVisible(true);
            buttonReplayTutorials.setVisible(true);
            buttonSkipQuest.setVisible(true);
        }

        //Only display objectives for a single quest
        boolean foundQuest = false;
        for (GCQuestListItem questListItem : questItemList) {
            if (foundQuest) break;

            //Display all objectives
            setTitle("");
            title.setText(questListItem.name);
            for (GCQuestObjective objective : questListItem.objectiveMap.values()) {
                GCQuestSidebarListItem listItem = new GCQuestSidebarListItem(questListItem.layer, questListItem.actionHandler, objective);
                displayedQuests.addItem(listItem);
                foundQuest = true;
            }

            helperUrl = questListItem.getHelperUrl();
            buttonHelp.setVisible(helperUrl != null);
            currentQuestId = questListItem.id;
        }

        //updatePosition(getEngine().getWidth(), getEngine().getHeight());
        capLeft();
    }

    /**
     * Open the sidebar
     */
    @Override
    public void open () {
        refresh();
    }

    /**
     * Overriding the close method to make sure that the sidebar never closes
     */
    @Override
    public void close () {}

    public void updatePosition(int width, int height) {
        setWidth(UPMath.min(DEFAULT_WIDTH, width/3));
        float distanceFromTop = UPMath.max(height*(1-DEFAULT_Y_PROPORTION), maxHeightFromTop);
        setPosition(24, height - distanceFromTop - getHeight());
        capLeft();
    }

    public void toggle() {
        toggledOn = !toggledOn;
        setVisible(toggledOn);
    }

    public void disableBar(boolean disable) {
        disabled = disable;
        setVisible(toggledOn && !disable);
    }

    public void setMaxHeightFromTop(int maxHeightFromTop) {
        this.maxHeightFromTop = maxHeightFromTop;
    }

    public boolean isToggledOn() {
        return toggledOn;
    }

    public boolean isDisabled() {
        return disabled;
    }


}
