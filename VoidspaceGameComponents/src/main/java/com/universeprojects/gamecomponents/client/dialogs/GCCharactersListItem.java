package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.vsdata.shared.CharacterData;

@SuppressWarnings("FieldCanBeLocal")
public class GCCharactersListItem extends GCListItem {
    public final CharacterData data;
    private final Table nameTable;
    private final H5EScrollLabel nameLabel;
    private final H5ELabel descriptionLabel;

    public GCCharactersListItem(H5ELayer layer, GCListItemActionHandler<GCCharactersListItem> actionHandler, CharacterData data) {
        super(layer, actionHandler);
        this.data = data;
        nameTable = new Table();
        nameTable.left().top();
        nameTable.defaults().left().top();
        add(nameTable).growX().fillY();
        nameLabel = new H5EScrollLabel(data.characterName, layer);
        descriptionLabel = new H5ELabel(data.nickname, layer);
        descriptionLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        descriptionLabel.setColor(Color.valueOf("#AAAAAA"));
        descriptionLabel.setWrap(true);
        rebuildActor();
    }

    @Override
    public String getName() {
        return data.characterName;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        nameLabel.setAutoScroll(isChecked() || isOver());
    }

    public void rebuildActor() {
        nameTable.clear();
        nameTable.add(nameLabel).fillX().expandX().padLeft(5);
        nameTable.row();
        nameTable.add(descriptionLabel).fillX().expandX().padLeft(5);
        nameLabel.setupScrolling();
    }
}
