package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class NavigationExtension {

    protected UINavigation navigation;
    protected boolean active = false;

    public NavigationExtension(UINavigation navigation){
        this.navigation=navigation;
    }

    public void moveDown(){}

    public void moveUp(){}

    public void moveRight(){}

    public void moveLeft(){}

    public abstract void enter();

    public abstract void exit();

    public abstract boolean isCompatible(Actor actor);

    public abstract void init(Actor actor);

    public boolean isActive(){
        return active;
    }
}
