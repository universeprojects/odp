package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.ActivateDeactivateWrapper;
import com.universeprojects.common.shared.util.ReactiveValue;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndPayload;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndSource;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionBarDndTarget;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionDndSource;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.ActionIconData;

import java.util.ArrayList;
import java.util.List;

import static com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar.MISSING_ACTION_ICON;

public class GCActionBarButton extends Table {
    protected final H5ELayer layer;
    private final Logger log = Logger.getLogger(GCActionBarButton.class);
    protected final DragAndDrop.Target plusTarget;
    protected H5ELabel buttonLabel;
    protected H5EIcon multiplicityIcon;
    protected GCActionBarDndTarget dndTarget;
    protected GCActionBarDndSource dndSource;
    protected Cell iconCell;
    private List<ReactiveActorValueCompound<ActionIconData>> icons;
    private ActivateDeactivateWrapper<Table> primaryIcon;
    private ActivateDeactivateWrapper<H5EIcon> secondaryIcon;
    private boolean isGroup;
    private GCActionBarController controller;
    private GCActionBarType type;
    private GCActionBar actionBar;
    private int index = 0;
    private boolean increaseTouchAreaMobile;

    public GCActionBarButton(GCActionBarType type, H5ELayer layer, String keyBinding, GCActionBar actionBar, GCActionBarController controller, int index) {
        super();

        this.type = type;
        this.layer = layer;
        this.index = index;
        this.controller = controller;
        this.actionBar = actionBar;

        multiplicityIcon = H5EIcon.fromSpriteType(layer, "images/GUI/ui2/action-multiple-targets.png", Scaling.fit, Align.center);
        multiplicityIcon.setVisible(false);
        addActor(multiplicityIcon);

        buttonLabel = new H5ELabel(keyBinding, layer);
        buttonLabel.setPosition(6, 8);
        buttonLabel.setStyle(layer.getEngine().getSkin().get("label-small", Label.LabelStyle.class));
        addActor(buttonLabel);

        setTouchable(Touchable.enabled);

        addListener(new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (primaryIcon.get()!=null && event.getTarget().isDescendantOf(primaryIcon.get())
                        || event.getTarget() == secondaryIcon.get()) {
                    try {
                        controller.callAction(type, actionBar.getCurrentRow(), index);
                    } catch (Throwable ex) {
                        log.error("Unable to call action", ex);
                    }
                }
            }
        });

        iconCell = add();
        icons = new ArrayList<>();
//        icons = new ReactiveActorValueCompound<>(this);
//        icons.setCallback(iconData -> {
//            if (icons.hasReactiveValue()) {
//                applyIcon(iconData, false);
//            } else {
//                // No actions are currently assigned to this button
//                applyIcon(null, true);
//            }
//        });

        primaryIcon = new ActivateDeactivateWrapper<>(null, icon -> {
            removeActor(icon);
            iconCell.setActor(null);
        }, actor -> iconCell.setActor(actor).grow());

        secondaryIcon = new ActivateDeactivateWrapper<>(null, this::removeActor, actor -> {
            addActor(actor);
            if(icons.size()<4) {
                actor.setPosition(getWidth() - actor.getWidth(), 0);
            }else{
                actor.setPosition(getWidth()/2f, getHeight()/2f, Align.center);
            }
        });
        Table plusTable = new Table();
        plusTable.setVisible(false);
        layer.addActorToTop(plusTable);
        plusTable.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
        plusTable.setSize(getWidth(), getHeight());
        Vector2 position = new Vector2(0, getHeight());
        this.localToStageCoordinates(position);
        plusTable.setPosition(position.x, position.y, Align.bottomLeft);
        H5ELabel labelPlus = new H5ELabel("+", layer);
        labelPlus.setAlignment(Align.center);
        plusTable.add(labelPlus).center().grow();
        plusTarget = new DragAndDrop.Target(plusTable) {
            @Override
            public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
                return true;
            }

            @Override
            public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload2, float x, float y, int pointer) {
                boolean fromActionBar = source instanceof GCActionBarDndSource;
                GCActionBarDndPayload dndPayload = (GCActionBarDndPayload) payload2;
                if (fromActionBar) {
                    GCActionBarDndSource dndSource = (GCActionBarDndSource) source;
                    controller.setAction(dndSource.getType(), dndSource.getRow(), dndSource.getIndex(), null, false);
                }
                isGroup = true;
                GCOperation operation = fromActionBar ? dndPayload.getAction() : dndPayload.getAction().getActionBarCopy();
                controller.addActionToFiringGroup(type, 0, index, operation);
            }
        };
    }

    @Override
    protected void sizeChanged() {
        super.sizeChanged();

        multiplicityIcon.setSize(getWidth() * 0.39F, getHeight() * 0.34F);
        multiplicityIcon.setPosition(getWidth() - this.multiplicityIcon.getWidth(), getHeight() - this.multiplicityIcon.getHeight() - 2);
        if (secondaryIcon.get() != null) {
            H5EIcon icon = secondaryIcon.get();
            icon.setSize(getWidth() * 0.52F, getHeight() * 0.5F);
            icon.setPosition(getWidth() - icon.getWidth(), 0);
        }
        H5ELayer.invalidateBufferForActor(this);
    }

    private void applyIcon(List<ReactiveValue<ActionIconData>> iconDataList, boolean hide) {
        if (!hide) {
            List<Color> colors = new ArrayList<>();
            Color brightest = Color.BLACK;
            List<String> primaryImages = new ArrayList<>();
            String secondaryImage = null;
            boolean multipleTargets = false;
            for (ReactiveValue<ActionIconData> iconReactiveData : iconDataList) {
                ActionIconData iconData = iconReactiveData.getValue();
                Color color = iconData != null ? iconData.color : Color.WHITE;
                if (color.r > brightest.r || color.b > brightest.b || color.g > brightest.g) {
                    brightest = color;
                }
                multipleTargets |= iconData != null && iconData.multipleTargets;
                if (iconData == null || (iconData.actionIcon == null && iconData.itemIcon == null)) {
                    primaryImages.add(MISSING_ACTION_ICON);
                } else if (iconData.actionIcon != null && iconData.itemIcon != null) {
                    primaryImages.add(iconData.itemIcon);
                    if (secondaryImage == null)
                        secondaryImage = iconData.actionIcon;
                } else if (iconData.actionIcon != null) {
                    primaryImages.add(iconData.actionIcon);
                } else {
                    primaryImages.add(iconData.itemIcon);
                }
                colors.add(color);
            }
            changePrimaryImage(primaryImages, colors);
            changeSecondaryImage(secondaryImage, brightest);
            multiplicityIcon.setVisible(multipleTargets);
        } else {
            changePrimaryImage(null, null);
            changeSecondaryImage(null, null);
            multiplicityIcon.setVisible(false);
        }
        H5ELayer.invalidateBufferForActor(this);
    }

    public void setReactiveIcon(List<ReactiveValue<ActionIconData>> newIcons) {
        if (newIcons != null && !newIcons.isEmpty()) {
            isGroup = newIcons.size() > 1;
            icons.clear();
            for (ReactiveValue<ActionIconData> iconData : newIcons) {
                ReactiveActorValueCompound<ActionIconData> compound = new ReactiveActorValueCompound<>(this);
                icons.add(compound);
                compound.setCallback(this::iconsChanged);
                compound.setValue(iconData);
            }
        } else {
            icons.clear();
            primaryIcon.set(null);
            secondaryIcon.set(null);
            multiplicityIcon.setVisible(false);
        }
    }

    private void iconsChanged(ActionIconData iconData) {
        boolean isEmpty = true;
        List<ReactiveValue<ActionIconData>> reactiveValues = new ArrayList<>();
        for (ReactiveActorValueCompound<ActionIconData> actorValueCompound : icons) {
            isEmpty &= !actorValueCompound.hasReactiveValue();
            reactiveValues.add(actorValueCompound.getCurrentReactiveValue());
        }
        if (!isEmpty)
            applyIcon(reactiveValues, false);
        else applyIcon(null, true);
    }

    public void changePrimaryImage(List<String> imageList, List<Color> color) {
        if (imageList == null || imageList.isEmpty()) {
            primaryIcon.set(null);
        } else {
            if (!isGroup) {
                H5EIcon icon = H5EIcon.fromSpriteType(layer, imageList.get(0), Scaling.fit, Align.center);
                if (color != null && !color.isEmpty()) {
                    icon.setColor(color.get(0));
                }
                Table table = new Table();
                table.add(icon).grow();
                primaryIcon.set(table);
            } else {
                Table table = new Table();
                int size = Math.min(imageList.size(), 4);
                int rowSize = 0;
                for (int i = 0; i < size; i++) {
                    rowSize++;
                    if (rowSize > 2) {
                        table.row();
                        rowSize = 0;
                    }
                    H5EIcon icon = H5EIcon.fromSpriteType(layer, imageList.get(i), Scaling.fit, Align.center);
                    if (color != null && !color.isEmpty()) {
                        icon.setColor(color.get(i));
                    }
                    table.add(icon).size(getWidth() * 0.5F, getHeight() * 0.5F);
                }
                table.top();
                primaryIcon.set(table);
            }
        }
    }

    public void changeSecondaryImage(String image, Color color) {
        if (image == null) {
            secondaryIcon.set(null);
        } else {
            H5EIcon icon = H5EIcon.fromSpriteType(layer, image, Scaling.fit, Align.center);
            if (color != null) {
                icon.setColor(color);
            }
            icon.setWidth(getWidth() * 0.52F);
            icon.setHeight(getHeight() * 0.5F);
            secondaryIcon.set(icon);
        }
    }

    public void changeLabel(String keyBinding) {
        removeActor(buttonLabel);
        buttonLabel = new H5ELabel(keyBinding, layer);
        addActor(buttonLabel);
        buttonLabel.setPosition(6, 8);
        buttonLabel.setStyle(layer.getEngine().getSkin().get("label-small", Label.LabelStyle.class));
    }

    /* Changes the position of the label */
    public void setLabelPosition(int x, int y) {
        if (buttonLabel != null) {
            buttonLabel.setPosition(x, y);
        }
    }

    public void setIncreaseTouchAreaMobile(boolean increaseTouchAreaMobile) {
        this.increaseTouchAreaMobile = increaseTouchAreaMobile;
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        if (increaseTouchAreaMobile) {
            return x > -getWidth() && x < getWidth() * 2 && y > -getHeight() && y < getHeight() * 2 ? super.hit(0, 0, touchable) : null;
        } else {
            return super.hit(x, y, touchable);
        }
    }

    public void tryCreateFiringGroup(GCActionDndSource source, GCActionBarDndPayload payload) {
        // if (payload.getAction().getIcon().getValue().actionIcon.equals("images/aspect-action-icons/fire-weapon1.png")) {
        if (source.getActor()!=this && icons != null && !icons.isEmpty() && icons.get(0).getCurrentReactiveValue() != null
//                    && icon.getCurrentReactiveValue().getValue().actionIcon.equals("images/aspect-action-icons/fire-weapon1.png")
        ) {
            Vector2 position = new Vector2(0, getHeight());
            this.localToStageCoordinates(position);
            plusTarget.getActor().setPosition(position.x, position.y, Align.bottomLeft);
            plusTarget.getActor().setSize(getWidth(),getHeight());
            plusTarget.getActor().setVisible(true);
            plusTarget.getActor().toFront();
            InventoryManager.getInstance(layer.getEngine()).getDragAndDrop().addTarget(plusTarget);
        }
        // }
    }

    public void hideFiringGroupTarget() {
        plusTarget.getActor().setVisible(false);
    }
}

