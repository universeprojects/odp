package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.common.shared.util.DevException;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class GCSlotItemSelector {

    private final H5EScrollablePane slotListContainer;
    private GCList<GCRecipeSlotListItem> slotList;
    private final GCRecipeSlotListItem.GCRecipeSlotListHandler slotListHandler;

    private final H5EScrollablePane optionsListContainer;
    private GCList<GCExperimentsListItem> optionsList;
    private final GCListItemActionHandler<GCExperimentsListItem> optionsListHandler;

    private GCRecipeData data;
    private Integer selectedSlotIdx;
    public static final int LIST_SPACING = 5;
    private int slotListColumns=1;

    private final H5ELayer layer;

    public GCSlotItemSelector(H5ELayer layer, H5EScrollablePane slotListContainer, H5EScrollablePane optionsListContainer) {
        this.layer = layer;

        this.slotListContainer = slotListContainer;
        this.optionsListContainer = optionsListContainer;
        slotListHandler = new GCRecipeSlotListItem.GCRecipeSlotListHandler() {

            @Override
            public void onSlotDeselected() {
                selectedSlotIdx = null;
                clearSlotOptionsList();
            }

            @Override
            public void onSlotSelected(GCRecipeSlotDataItem slot) {
                selectedSlotIdx = slot.index;
                if (!slot.isItemSelected()) {
                    populateSlotOptionsData(slot.getItems());
                }else{
                    setDescription(slot);
                }
            }

            @Override
            public void onDeselectOptionBtnPressed(GCRecipeSlotDataItem slot) {
                GCInventoryDataItem itemToDeselect = slot.getSelectedItem();
                if (itemToDeselect == null) {
                    throw new DevException("Unexpected: de-select button is only available when an item is selected");
                }
                slot.clearSelectedItem();
                selectedSlotIdx = slot.index;

                boolean dataReload = onSlotItemDeselected(slot, itemToDeselect);
                if (!dataReload) {
                    populateSlotData(data, slot.index, true);
                }
            }

            @Override
            public void onInfoButtonClicked(Actor actor, GCRecipeSlotDataItem slot) {
                onSlotInfoClicked(actor, slot.getSelectedItem());
            }
        };

        optionsListHandler = new GCListItemActionHandler<GCExperimentsListItem>() {
            boolean infoClicked = false;

            @Override
            protected void onSelectionUpdate(GCExperimentsListItem lastSelectedItem) {
                if(infoClicked) {
                    GCExperimentsListItem selectedItem = getSelectedItem();
                    selectedItem.setProgrammaticChangeEvents(false);
                    selectedItem.setChecked(false);
                    selectedItem.setProgrammaticChangeEvents(true);
                    infoClicked = false; //To prevent the item from being picked for the slot when the info button is clicked
                    return;
                }
                GCRecipeSlotDataItem slot = data.getSlot(selectedSlotIdx);
                if (slot.isItemSelected()) {
                    throw new DevException("Unexpected: option selection is only available when the slot is empty");
                }

                GCExperimentsListItem selectedOption = this.getSelectedItem();
                if (selectedOption == null) {
                    throw new DevException("Unexpected: options list should only be visible for selection, not for de-selection");
                }

                slot.selectItem(selectedOption.data.item.getUid());
                GCInventoryDataItem selectedItem = slot.getSelectedItem();

                boolean dataReload = onSlotItemSelected(slot, selectedItem);
                if (!dataReload) {
                    populateSlotData(data, selectedSlotIdx, true);
                    clearSlotOptionsList();
                }
            }

            @Override
            public void onInfoButtonClicked(GCExperimentsListItem item) {
                onSlotInfoClicked(item, item.data);
                infoClicked = true;
            }
        };

        clear();
    }

    public GCRecipeData getDataDirect() {
        return data;
    }

    public Integer getSelectedSlotIdx() {
        return selectedSlotIdx;
    }

    public GCRecipeData getData() {
        return data.copy();
    }

    public void init(GCRecipeData data, boolean restoreScrollPosition, Integer slotIndex) {
        this.data = data.copy();

        preselectItems();
        clearSlotOptionsList();
        populateSlotData(this.data, slotIndex, restoreScrollPosition);
    }

    private void preselectItems() {
        Set<String> selectedItems = new HashSet<>();
        outer:
        for (GCRecipeSlotDataItem slot : data.getSlots()) {
            GCInventoryData items = slot.getItems();
            for (int i = 0; i < items.size(); i++) {
                GCInventoryDataItem selectedItem = items.get(0);
                String uid = selectedItem.item.getUid();
                boolean added = selectedItems.add(uid);
                if(added) {
                    slot.selectItem(uid);
                    onSlotItemSelected(slot, selectedItem);
                    continue outer;
                }
            }
        }
    }

    public void update(GCRecipeData data) {
        List<GCRecipeSlotDataItem> existingSlots = this.data.getSlots();
        List<GCRecipeSlotDataItem> incomingSlots = data.getSlots();
        if (existingSlots.size() != incomingSlots.size()) {
            throw new IllegalStateException("The number of slots is different");
        }
        for (int i = 0; i < existingSlots.size(); i++) {
            GCRecipeSlotDataItem incomingSlot = data.getSlot(i);
            GCRecipeSlotDataItem existingSlot = this.data.getSlot(i);
            existingSlot.replaceItems(incomingSlot.getItems());
        }

        clearSlotOptionsList();
        populateSlotData(this.data, selectedSlotIdx, true);
    }

    public void clear() {
        data = GCRecipeData.createNew(null, GCRecipeData.RecipeMode.ITEM_SKILL, -1L); // default empty
        clearSlotList();
        clearSlotOptionsList();
    }

    private void clearSlotList() {
        if (slotList != null) {
            slotListHandler.reset();
            slotList.clear();
            slotList.remove();
            slotList = null;
            slotListContainer.getContent().clear();
        }
    }

    private void clearSlotOptionsList() {
        if (optionsList != null) {
            optionsListHandler.reset();
            optionsList.clear();
            optionsList.remove();
            optionsList = null;
        }
        optionsListContainer.getContent().clear();
    }

    private void populateSlotData(GCRecipeData recipeData, Integer selectedSlotIdx, boolean restoreScrollPosition) {
        float scrollPosition = restoreScrollPosition ? slotListContainer.getScrollPosition() : 0;

        clearSlotList();
        if (recipeData == null) {
            return;
        }

        GCRecipeSlotListItem itemToSelect = null;
        slotList = new GCList<>(layer, slotListColumns, LIST_SPACING, LIST_SPACING, true);
        for (int index = 0; index < recipeData.size(); index++) {
            GCRecipeSlotDataItem dataItem = recipeData.getSlot(index);
            GCRecipeSlotListItem listItem = new GCRecipeSlotListItem(layer, slotListHandler, dataItem);
            slotList.addItem(listItem);
            if (selectedSlotIdx != null && selectedSlotIdx == index) {
                itemToSelect = listItem;
            }
        }

        slotListContainer.add(slotList).top().left().grow();
        if (itemToSelect != null) {
            slotListHandler.updateSelection(itemToSelect);
        }
        slotListContainer.scrollToPosition(scrollPosition);
    }

    private void setDescription(GCRecipeSlotDataItem data){
        optionsListContainer.getContent().clear();
        H5ELabel label=new H5ELabel(data.data.description,layer);
        label.setColor(Color.valueOf("#CCCCCC"));
        optionsListContainer.add(label).left();
        optionsListContainer.getContent().row();
    }

    private void populateSlotOptionsData(GCInventoryData slotOptionsData) {
        clearSlotOptionsList();
        if (slotOptionsData == null) {
            return;
        }
        optionsListContainer.add(new H5ELabel("Select the tools/materials to use", layer)).left();
        optionsListContainer.getContent().row();
        optionsList = new GCList<>(layer, 1, LIST_SPACING, LIST_SPACING, true);
        outer:
        for (int index = 0; index < slotOptionsData.size(); index++) {
            GCInventoryDataItem dataItem = slotOptionsData.get(index);
            for (GCRecipeSlotListItem item : slotList.getItems()) {
                if(item.data.getSelectedItem() != null && item.data.getSelectedItem().equals(dataItem)) {
                    continue outer;
                }
            }

            GCExperimentsListItem listItem = new GCExperimentsListItem(layer, optionsListHandler, dataItem, optionsListContainer);
            optionsList.addItem(listItem);
        }

        optionsListContainer.add(optionsList).top().left().grow();
    }

    public void setColumnsCount(int columnsCount){
        slotListColumns=columnsCount;
        if(slotList!=null) {
            slotList.setColumns(columnsCount);
        }
    }
    /**
     * @return TRUE if the control data will be re-loaded as a result of this operation
     */
    protected abstract boolean onSlotItemSelected(GCRecipeSlotDataItem slot, GCInventoryDataItem selectedItem);

    /**
     * @return TRUE if the control data will be re-loaded as a result of this operation
     */
    protected abstract boolean onSlotItemDeselected(GCRecipeSlotDataItem slot, GCInventoryDataItem deselectedItem);

    protected abstract void onSlotInfoClicked(Actor actor, GCInventoryDataItem item);


}
