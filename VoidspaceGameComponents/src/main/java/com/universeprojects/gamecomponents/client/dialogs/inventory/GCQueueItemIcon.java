package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCQueueItemIcon<T extends GCInventoryItem> extends GCItemIcon {

    private boolean highlighted;
    public GCQueueItemIcon(H5ELayer layer){
        super(layer);
    }

    public void setHighlightIcon(String spriteKey){
        highlightIcon= H5EIcon.fromSpriteType(getLayer(),spriteKey);
        addActor(highlightIcon);
        highlightIcon.setVisible(false);
    }

    public void setHighlighted(boolean highlighted){
        this.highlighted=highlighted;
        layout();
    }

    @Override
    public boolean isHighlighted(){
        return highlighted;
    }


}
