package com.universeprojects.gamecomponents.client.dialogs.station.store;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class GCCategoryStoreSplitComponent extends Table {

    private static final int TREE_WIDTH = 215;
    // UI
    private final GCItemTree categoryTree;
    private final GCItemInternalTree categoryInternalTree;
    private final GCListItemActionHandler<GCStoreListItem> listHandler;
    private GCList<GCStoreListItem> itemList;
    private final H5EScrollablePane storeScrollView;
    private final H5ELayer layer;
    private final H5ELabel noElementsLabel;
    private final H5EStack stack;
    private final H5EInputBox filter;
    private final Cell<H5EInputBox> filterCell;
    private final Table labelTable;
    private String filterText = "";
    private List<GCStoreListItemData> curProducts;
    private List<GCStoreListItemData> allProducts;
    private boolean makeAllCategory = false;

    public GCCategoryStoreSplitComponent(H5ELayer layer, boolean makeAllCategory, Callable1Args<GCStoreListItem> onSelected) {
        this(layer, onSelected);
        this.makeAllCategory = makeAllCategory;

    }

    public GCCategoryStoreSplitComponent(H5ELayer layer, Callable1Args<GCStoreListItem> onSelected) {
        this.layer = layer;
        left().top();
        defaults().left().top();
        listHandler = new GCListItemActionHandler<GCStoreListItem>() {
            @Override
            protected void onSelectionUpdate(GCStoreListItem lastSelectedItem) {
                GCStoreListItem item = this.getSelectedItem();
                if (item != null) {
                    onSelected.call(item);
                }
            }
        };
        filter = new H5EInputBox(layer, "Filter");
        filter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                filterText = filter.getText().toLowerCase();
                filterItems();
                event.handle();
            }
        });
        row();
        categoryTree = add(new GCItemTree(layer)).top().left().growY().width(TREE_WIDTH).getActor();
        categoryTree.setBackground(StyleFactory.INSTANCE.panelStyleSimpleSemiTransparent);
        categoryTree.top().left();
        filterCell = categoryTree.add(filter).left().growX();
        filterCell.clearActor();
        categoryTree.row();
        stack = add(new H5EStack()).grow().getActor();
        noElementsLabel = new H5ELabel("Nothing here", layer);
        noElementsLabel.setAlignment(Align.center);
        noElementsLabel.setColor(Color.valueOf("#CCCCCC"));
        labelTable = new Table();
        labelTable.add(noElementsLabel).left().top();
        itemList = new GCList<>(layer, 1, 5, 1, true);
        itemList.setMaxCheckCount(1);
        storeScrollView = new H5EScrollablePane(layer);
        storeScrollView.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
        storeScrollView.add(itemList).growX();
        storeScrollView.getContent().left().top();

        stack.add(storeScrollView);
        stack.add(labelTable);
        categoryInternalTree = new GCItemInternalTree(layer);
        categoryTree.setInternalTree(categoryInternalTree);
    }

    public void setPortraitMode(boolean mode){
        super.clear();
        left().top();
        defaults().left().top();
        if(mode) {
            add(categoryTree).top().left().grow();
            categoryTree.left();
            categoryTree.defaults().left().growX();
            categoryTree.getCells().get(0).left();
            categoryTree.getCells().get(1).left();
            row();
        }else{
            add(categoryTree).top().left().growY().width(TREE_WIDTH);
        }
        add(stack).grow();
    }

    public void setCategoryTreeStyle(Drawable background) {
        categoryTree.setBackground(background);
    }

    public void setShopListStyle(ScrollPane.ScrollPaneStyle scrollPaneStyle) {
        storeScrollView.setStyle(scrollPaneStyle);
    }

    public void clear() {
        clearView();
        clearFilter();
    }

    private void clearView() {
        categoryTree.clearTree();
        itemList.clear();
    }

    public void filterItems() {
        itemList.clear();
        populateRightList(curProducts);
    }

    public void clearFilter() {
        filter.setText("");
        filterText = "";
    }

    public void setData(Map<String, Object> data) {
        filterCell.setActor(filter);
        clearView();
        curProducts = new ArrayList<>();
        recursivelyPopulateCategories(categoryInternalTree, data);
    }

    private void addItem(GCStoreListItemData entryData) {
        GCStoreListItem entry = new GCStoreListItem(layer, listHandler, entryData);
        itemList.addItem(entry);
    }


    @SuppressWarnings("unchecked")
    private List<GCStoreListItemData> getAllItems(Map<String, Object> data) {
        List<GCStoreListItemData> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            Object val = entry.getValue();
            if (val instanceof Map) {
                list.addAll(getAllItems((Map<String, Object>) val));
            } else if (val instanceof List) {
                list.addAll((List<GCStoreListItemData>) val);
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private void recursivelyPopulateCategories(GCItemInternalTree categoryOrTree, Map<String, Object> data) {
        if (data == null) {
            return;
        }
        if (makeAllCategory) {
            int categoriesCount = data.size();
            if (categoriesCount > 1) {
                GCItemTreeSubitem all;
                if (allProducts == null) {
                    allProducts = getAllItems(data);
                    all = categoryOrTree.addItem("All", () -> populateRightList(allProducts));
                } else {
                    all = categoryOrTree.addItem("All", () -> populateRightList(getAllItems(data)));
                }
                //Data being passed, "all" is shown defaultly
                if (itemList.size() == 0)
                    all.click();
            }
        }
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            String name = entry.getKey();
            Object val = entry.getValue();
            if (val instanceof Map) {
                GCItemTreeItem category = categoryOrTree.addCategory(name);
                recursivelyPopulateCategories(category, (Map<String, Object>) val);
            } else if (val instanceof List && !name.equals("")) {
                categoryOrTree.addItem(name, () -> {
                    // This triggers when the category is selected (only the leaf category)
                    // We're expected to populate the right panel with this category now I guess
                    // Weird implementation!
                    populateRightList((List<GCStoreListItemData>) val);
                });

            }
        }
        categoryInternalTree.invalidateHierarchy();
    }

    private void populateRightList(List<GCStoreListItemData> products) {
        curProducts = new ArrayList<>(products);
        itemList.clear();
        for (GCStoreListItemData product : products) {
            if (product.getItem().getQuantity() > 0 && (product.getData().name.toLowerCase().contains(filterText) || product.getData().sellerNickname.toLowerCase().contains(filterText))) {
                addItem(product);
            }
        }
        if (itemList.size() > 0) {
            labelTable.reset();
        } else {
            labelTable.reset();
            labelTable.add(noElementsLabel).left().top();
        }
    }

    //if we pass new GCStoreListItemData with same ID, we replace it with a new data and item
    public void updateItem(GCStoreListItemData itemData) {
        for (GCStoreListItemData product : allProducts) {
            if (product.getData().id.equals(itemData.getData().id)) {
                product.setData(itemData.getData());
                product.setItem(itemData.getItem());
            }
        }
        populateRightList(curProducts);
    }

    public Table getLabelTable() {
        return labelTable;
    }

}
