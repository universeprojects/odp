package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.components.GCGenericINDList;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItem;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EDropDown;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class GCCategoryItemSplitComponent<T> extends Table {

    private static final int TREE_WIDTH = 215;
    // UI
    protected final GCItemTree categoryTree;
    protected final GCItemInternalTree categoryInternalTree;
    protected final GCGenericINDList<T> itemList;
    protected final H5ELayer layer;
    protected final H5ELabel label;
    protected final H5EStack stack;
    protected final H5EInputBox filter;
    protected final Table labelTabel;
    protected final Cell dropdownCell;
    private GCListItemActionHandler<GCGenericINDListItem<T>> handler;
    private String filterText = "";
    private List<GCGenericINDListItemData<T>> curProducts;
    private boolean makeAllCategory = false;

    public GCCategoryItemSplitComponent(H5ELayer layer, boolean makeAllCategory, Callable1Args<GCGenericINDListItemData<T>> onClick) {
        this(layer, makeAllCategory);
        handler = new GCListItemActionHandler<GCGenericINDListItem<T>>() {
            @Override
            protected void onSelectionUpdate(GCGenericINDListItem<T> newItemSelection) {
                if (isItemSelected())
                    onClick.call(getSelectedItem().getData());
            }
        };
    }

    public GCCategoryItemSplitComponent(H5ELayer layer, boolean makeAllCategory) {
        this(layer);
        this.makeAllCategory = makeAllCategory;
    }

    public GCCategoryItemSplitComponent(H5ELayer layer) {
        this.layer = layer;
        left().top();
        defaults().left().top();
        filter = new H5EInputBox(layer, "Filter");
        filter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                filterText = filter.getText().toLowerCase();
                filterItems();
                event.handle();
            }
        });
        row();
        categoryTree = add(new GCItemTree(layer)).top().left().growY().width(TREE_WIDTH).getActor();
        categoryTree.setBackground(StyleFactory.INSTANCE.panelStyleSimpleSemiTransparent);
        categoryTree.top().left();
        dropdownCell = categoryTree.add().left().growX();
        categoryTree.row();
        categoryTree.add(filter).left().growX();
        categoryTree.row();
        stack = add(new H5EStack()).grow().getActor();
        itemList = new GCGenericINDList<>(layer);
        label = new H5ELabel("Nothing here", layer);
        label.setColor(Color.valueOf("#CCCCCC"));
        labelTabel = new Table();
        labelTabel.add(label).left().top();
        stack.add(itemList);
        stack.add(labelTabel);
        categoryInternalTree = new GCItemInternalTree(layer);
        categoryTree.setInternalTree(categoryInternalTree);
    }

    public void setCategoryTreeStyle(Drawable background) {
        categoryTree.setBackground(background);
    }

    public void setItemListStyle(ScrollPane.ScrollPaneStyle scrollPaneStyle) {
        itemList.setStyle(scrollPaneStyle);
    }

    public void clear() {
        clearView();
    }

    private void clearView() {
        categoryTree.clearTree();
        itemList.clear();
    }

    public void filterItems() {
        itemList.clear();
        populateRightList(curProducts);
    }

    public void clearFilter() {
        filter.setText("");
        filterText = "";
    }

    public void setData(Map<String, Object> data) {
        clearView();
        curProducts = new ArrayList<>();
        recursivelyPopulateCategories(categoryInternalTree, data);
    }

    @SuppressWarnings("unchecked")
    private List<GCGenericINDListItemData<T>> getAllItems(Map<String, Object> data) {
        List<GCGenericINDListItemData<T>> list = new ArrayList<>();
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            Object val = entry.getValue();
            if (val instanceof Map) {
                list.addAll(getAllItems((Map<String, Object>) val));
            } else if (val instanceof List) {
                list.addAll((List<GCGenericINDListItemData<T>>) val);
            }
        }
        return list;
    }

    @SuppressWarnings("unchecked")
    private void recursivelyPopulateCategories(GCItemInternalTree categoryOrTree, Map<String, Object> data) {
        if (data == null) {
            return;
        }
        if (makeAllCategory) {
            int categoriesCount = data.size();
            if (categoriesCount > 1) {
                List<GCGenericINDListItemData<T>> list = getAllItems(data);
                GCItemTreeSubitem a = categoryOrTree.addItem("All", () -> populateRightList(list, handler));
                //Data being passed, "all" is shown by default
                if (itemList.size() == 0)
                    a.click();
            }
        }
        for (Map.Entry<String, Object> entry : data.entrySet()) {
            String name = entry.getKey();
            Object val = entry.getValue();
            if (val instanceof Map) {
                GCItemTreeItem category = categoryOrTree.addCategory(name);
                recursivelyPopulateCategories(category, (Map<String, Object>) val);
            } else if (val instanceof List && !name.equals("")) {
                categoryOrTree.addItem(name, () -> {
                    // This triggers when the category is selected (only the leaf category)
                    // We're expected to populate the right panel with this category now I guess
                    // Weird implementation!
                    populateRightList((List<GCGenericINDListItemData<T>>) val, handler);
                });

            }
        }
        categoryInternalTree.invalidateHierarchy();
    }

    public void populateRightList(List<GCGenericINDListItemData<T>> products) {
        populateRightList(products, null);
    }

    public void populateRightList(List<GCGenericINDListItemData<T>> products, GCListItemActionHandler<GCGenericINDListItem<T>> actionHandler) {
        curProducts = products;
        itemList.clear();

        if (products != null) {
            for (GCGenericINDListItemData<T> product : products) {
                if (product.name.toLowerCase().contains(filterText) || product.description.toLowerCase().contains(filterText)) {
                    itemList.addItem(product, actionHandler);
                }
            }
        }
        if (itemList.size() > 0) {
            labelTabel.reset();
        } else {
            labelTabel.add(label).left().top();
        }
    }

    public List<T> getSelectedItems() {
        return itemList.getSelectedItems();
    }

    public void setMaxCheckCount(int count) {
        itemList.setMaxCheckCount(count);
    }

    public void setScrollProperties() {
        itemList.setScrollingDisabled(true, false);
        itemList.setOverscroll(false, true);
    }

    public void setDropdown(H5EDropDown dropDown) {
        dropdownCell.setActor(dropDown);
    }
}
