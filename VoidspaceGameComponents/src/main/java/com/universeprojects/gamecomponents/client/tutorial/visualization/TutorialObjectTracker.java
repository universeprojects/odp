package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialStage;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * This object is responsible for tracking all the UI elements
 * that are being added to the screen in Windows.
 * <p>
 * It acts as preliminary filter to eliminate performance overhead
 * from attempting to check visability of non-relevant actors
 * <p>
 * The information about registered actors is being passed to
 * {@link TutorialMarkerController} for further screening and displaying
 * the tutorial marker
 */
public class TutorialObjectTracker {
    private final Map<String, List<Actor>> objects = new HashMap<>();
    private final TreeSet<TutorialStage> stages;
    private final Set<String> objectsOfInterest = new HashSet<>();
    private final TutorialController tutorialController;
    private Map<String, Object> checkers = new HashMap<>();
    private TutorialMarkerController component;

    public TutorialObjectTracker(TutorialController tutorialController) {
        this.tutorialController = tutorialController;
        this.stages = new TreeSet<>(Collections.reverseOrder());
    }

    public void initMarkerComponent(H5ELayer uiLayer) {
        component = new TutorialMarkerController(tutorialController, uiLayer);
        uiLayer.addActorToTop(component);
    }

    public boolean isReady() {
        return component != null;
    }

    public void registerObject(String objectId, Actor actor) {
        List<Actor> actors = objects.get(objectId);
        if(actors == null) {
            actors = new ArrayList<>();
            objects.put(objectId, actors);
        }
        actors.add(actor);

        if (objectsOfInterest.contains(objectId)) {
            updateMarker();
        }
    }

    public void registerObject(Actor actor) {
        Object tutorialId = actor.getUserObject();
        if (tutorialId == null)
            return;
        registerObject((String) tutorialId, actor);
    }

    public void registerCheckerObject(String id, Object obj){
        if(id == null || obj == null)return;
        checkers.put(id, obj);
    }

    public void removeObject(String objectId) {
        objects.remove(objectId);
        if (objectsOfInterest.contains(objectId)) {
            updateMarker();
        }
    }

    public void removeObject(String objectId, Actor actor) {
        final List<Actor> actors = objects.get(objectId);
        if(actors != null) {
            actors.remove(actor);
            if(actors.isEmpty()) {
                objects.remove(objectId);
            }
        }
        if (objectsOfInterest.contains(objectId)) {
            updateMarker();
        }
    }

    protected void updateMarker() {
        component.clearTargets();
        for (TutorialStage stage : stages) {
            if(stage.getMarkers() == null) {
                continue;
            }
            ListIterator<TutorialMarker> iterator = stage.getMarkers().listIterator(stage.getMarkers().size());
            while (iterator.hasPrevious()) {
                TutorialMarker marker = iterator.previous();
                final List<Actor> actors = objects.get(marker.getObjectId());
                if (actors != null) {
                    for (Actor actor : actors) {
                        component.addTarget(new ActorMarkerPair(actor, marker));
                    }
                }
            }
        }
    }

    public void refreshMarkerList(List<TutorialStage> stages) {
        this.stages.clear();
        this.objectsOfInterest.clear();

        for (TutorialStage stage : stages) {
            this.stages.add(stage);

            if( stage.getMarkers() == null) {
                continue;
            }

            for (TutorialMarker marker : stage.getMarkers()) {
                this.objectsOfInterest.add(marker.getObjectId());
            }
        }

        updateMarker();
    }

    public boolean hasObject(String objectId) {
        return objects.containsKey(objectId);
    }

    public Map<String, Object> getCheckers(){
        return checkers;
    }

    public Actor getObject(String actorId) {
        final List<Actor> actors = objects.get(actorId);
        if(actors != null && !actors.isEmpty()) {
            return actors.get(0);
        }
        return null;
    }
}
