package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class GCMobileWarningDialog {

    Preferences preferences = Gdx.app.getPreferences("client");
    private GCWindow mainWindow;
    private H5ELayer layer;
    private H5EIcon graphic;
    private H5EButton btnDismiss;
    private H5EButton btnGotoWebsite;

    public GCMobileWarningDialog(H5ELayer layer){
        this.layer = layer;

        mainWindow = new GCWindow(this.layer, 400, 450, StyleFactory.INSTANCE.windowStylePopup_NoTitle){
            @Override
            protected void onClose() {
                super.onClose();
            }
        };
        mainWindow.setModal(true);
        mainWindow.defaults().center();
        mainWindow.setFullscreenOnMobile(false);
        mainWindow.setCloseButtonEnabled(false);

        mainWindow.onClose.registerHandler(() -> {
            int count = preferences.getInteger("mobile-warning-dialog-popup-counter", 0);
            count++;
            preferences.putInteger("mobile-warning-dialog-popup-counter", count);
            preferences.flush();
        });


        layer.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (mainWindow.hit(x - mainWindow.getX(), y - mainWindow.getY(), true) == null) {
                    mainWindow.close();
                }

                return false;

            }
        });

        graphic = new H5EIcon(layer, "images/GUI/mobile-warning1.png", 400, 400);
        mainWindow.add(graphic).height(400).width(400).align(Align.center).colspan(2).pad(10);

        mainWindow.row();

        btnDismiss = new H5EButton("Dismiss", layer);
        mainWindow.add(btnDismiss).align(Align.left).pad(4).width(170);
        btnDismiss.addButtonListener(() -> mainWindow.close());


        btnGotoWebsite = new H5EButton("Visit Website", layer);
        mainWindow.add(btnGotoWebsite).align(Align.right).pad(4).width(170);
        btnGotoWebsite.addButtonListener(() -> {
            Gdx.net.openURI("https://www.voidspacegame.com/gamelist");
        });

    }

    public void openIfApplicable() {
        if (layer.getEngine().isMobileMode() == false) return;

        int count = preferences.getInteger("mobile-warning-dialog-popup-counter", 0);
        if (count>=2) return;

        mainWindow.open();
        mainWindow.pack();
        mainWindow.positionProportionally(0.5f,0.5f);
        mainWindow.toFront();
    }

}
