package com.universeprojects.gamecomponents.client.elements.actionbar.dnd;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarButton;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBarType;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCActionBarDndSource extends GCActionDndSource {
    private final GCActionBarType type;
    private final Integer index;
    private Integer row;
    private H5ELayer layer;

    public GCActionBarDndSource(Actor actor, GCOperation action, H5ELayer layer, GCActionBarType type, Integer index) {
        super(actor, action, layer);
        this.layer = layer;
        this.type = type;
        this.index = index;
    }

    @Override
    public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
        DragAndDrop.Payload payload = super.dragStart(event, x, y, pointer);
        InventoryManager.getInstance(layer.getEngine()).getDragAndDrop().notifyTargetsDrag(this, payload);
        return payload;
    }

    @Override
    public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
        InventoryManager.getInstance(layer.getEngine()).getDragAndDrop().notifyTargetsDrop();
        super.dragStop(event, x, y, pointer, payload, target);
    }

    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public Integer getIndex() {
        return index;
    }

    public GCActionBarType getType() {
        return type;
    }
}
