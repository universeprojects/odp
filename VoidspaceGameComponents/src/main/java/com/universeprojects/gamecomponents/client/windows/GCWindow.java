package com.universeprojects.gamecomponents.client.windows;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.common.MatrixUINavigation;
import com.universeprojects.gamecomponents.client.common.UINavigation;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EWindow;
import com.universeprojects.html5engine.shared.GenericEvent;

import java.util.HashSet;
import java.util.Set;

public class GCWindow extends Window implements H5EWindow {

    /**
     * The transparency value applied to active windows
     */
    static final float ACTIVE_ALPHA = 1f;
    /**
     * The transparency value applied to inactive windows
     */
    static final float INACTIVE_ALPHA = 1f;
    /**
     * The global minimum width
     */
    private static final int WINDOW_MIN_WIDTH = 200;
    /**
     * The global minimum height
     */
    private static final int WINDOW_MIN_HEIGHT = 125;
    /**
     * The height of the title bar
     */
    private static final int TITLE_BAR_HEIGHT = 40;
    /**
     * The total amount of X-axis padding to the left of the content pane
     */
    private static final int CONTENT_PADDING_LEFT = 16;
    /**
     * The total amount of X-axis padding to the right of the content pane
     */
    private static final int CONTENT_PADDING_RIGHT = 16;
    /**
     * The total amount of Y-axis padding from the top of the content pane
     */
    private static final int CONTENT_PADDING_TOP = 8;
    /**
     * The total amount of Y-axis padding from the bottom of the content pane
     */
    private static final int CONTENT_PADDING_BOTTOM = 16;
    private static final int CONTENT_PADDING_W = CONTENT_PADDING_LEFT + CONTENT_PADDING_RIGHT;
    private static final int CONTENT_PADDING_H = CONTENT_PADDING_TOP + CONTENT_PADDING_BOTTOM;
    public final GenericEvent.GenericEvent0Args onClose = new GenericEvent.GenericEvent0Args();
    public final GenericEvent.GenericEvent0Args onOpen = new GenericEvent.GenericEvent0Args();
    /**
     * This button is in the corner of the title bar; Clicking it closes the window
     */
    protected final ImageButton btnClose;
    protected final Cell<ImageButton> btnCell;
    private final Set<String> namedComponents = new HashSet<>();
    private boolean packOnOpen;
    private boolean clickThrough = false;
    private boolean closeable = true;

    private Vector2 lastAutoPositionScreenSize;
    private final GenericEvent.GenericEventHandler0Args resizeHandler = this::positionOnResize;
    private boolean autoPositioningActive = false;
    private boolean resizeHandlerWasSetup = false;

    /**
     * Fullscreen
     * */
    public final static int EDGE_BUFFER = 100;
    protected boolean fullscreen = false;
    protected boolean fullscreenOnMobile = false;
    protected boolean showTitleBarOnFullscreen = true;
    protected boolean disableOptionsButtonInFullscreen = true;
    protected final H5EContainer fullscreenTopBar;
    protected final H5EContainer fullScreenLeftCorner;
    protected final H5EContainer fullscreenRightCorner;
    protected final H5EContainer fullscreenTitleBacking;
    protected final Label fullscreenTitleLabel;
    protected Vector2 previousSize;
    protected Vector2 previousPosition;
    protected UINavigation navigation;

    private boolean keepWithinStage = false;

    /**
     * This keeps track of all the input boxes in the window (they take a bit of special care)
     */
    public GCWindow(H5ELayer layer, Integer width, Integer height) {
        this(layer, width, height, "default");
    }

    public GCWindow(H5ELayer layer, Integer width, Integer height, String styleName) {
        this(layer, width, height, (GCWindowStyle) layer.getEngine().getSkin().get(styleName, WindowStyle.class));
    }

    public GCWindow(H5ELayer layer, Integer width, Integer height, GCWindowStyle style) {
        super("", style);
        setSkin(layer.getEngine().getSkin());
        layer.addActorToTop(this);
        final Label titleLabel = getTitleLabel();
        titleLabel.setAlignment(Align.center);
        final Table titleTable = getTitleTable();
        if (style.titleBackground != null) {
            titleLabel.getStyle().background = style.titleBackground;
        }
        final Cell<Label> labelCell = titleTable.getCell(titleLabel);
        labelCell.padBottom(style.labelPadBottom).padLeft(style.labelPadLeft);
        labelCell.fill(false, false);
        setClip(false);
        setTransform(true);
        setKeepWithinStage(false);

        width = capMin(width, (int) getBackground().getMinWidth());
        height = capMin(height, (int) getBackground().getMinHeight());

        /*
      Stores the last seen screen width (for window resizing purposes)
     */

        addCaptureListener(new InputListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!getWindowManager().isActive(GCWindow.this)) {
                    getWindowManager().activateWindow(GCWindow.this);
//                    event.setBubbles(false);
//                    return true;
//                } else {
                }
                return false;
            }
        });
        addListener(new ClickListener() {
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                event.handle();
                return super.touchDown(event, x, y, pointer, button);
            }

            @Override
            public void touchDragged(InputEvent event, float x, float y, int pointer) {
                event.handle();
                super.touchDragged(event, x, y, pointer);
            }

            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                event.handle();
                super.touchUp(event, x, y, pointer, button);
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                event.handle();
                super.clicked(event, x, y);
            }

            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                event.handle();
                super.enter(event, x, y, pointer, fromActor);
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                event.handle();
                super.exit(event, x, y, pointer, toActor);
            }
        });

        setWidth(width);
        setHeight(height);

        btnClose = new ImageButton(getSkin(), "btn-close-window");
        btnClose.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                close(true);
                event.handle();
            }
        });
        btnCell = titleTable.add(btnClose);
        btnCell.padRight(style.closeButtonPadRight).padTop(style.closeButtonPadTop);

        // do this so that the boundaries check takes effect
        setX(getX());
        setY(getY());

        // start hidden
        setupInactiveState();
        setVisible(false);

        // Initialize fullscreen components
        fullscreenTopBar = new H5EContainer(layer);
        fullscreenTopBar.setBackground("top-graphic-button-bar-right");
        fullscreenTopBar.setHeight(37);
        fullscreenTopBar.setVisible(false);
        addActor(fullscreenTopBar);

        fullScreenLeftCorner = new H5EContainer(layer);
        fullScreenLeftCorner.setBackground("top-graphic-left-corner");
        fullScreenLeftCorner.setSize(52, 42);
        fullScreenLeftCorner.setVisible(false);
        addActor(fullScreenLeftCorner);

        fullscreenRightCorner = new H5EContainer(layer);
        fullscreenRightCorner.setBackground("top-graphic-right-corner");
        fullscreenRightCorner.setSize(52, 42);
        fullscreenRightCorner.setVisible(false);
        H5ESprite rightCornerSprite = new H5ESprite(layer, (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType("GUI/ui2/top_graphic1_right_corner_backing"));
        rightCornerSprite.addListener(new ClickListener() {
            @Override
            public void clicked (InputEvent event, float x, float y) {
                close(true);
                event.handle();
            }
        });
        fullscreenRightCorner.add(rightCornerSprite).size(52, 42);
        addActor(fullscreenRightCorner);

        fullscreenTitleBacking = new H5EContainer(layer);
        fullscreenTitleBacking.setBackground("title-bar-background");
        fullscreenTitleBacking.setVisible(false);
        addActor(fullscreenTitleBacking);

        fullscreenTitleLabel = new Label(getTitleLabel().getText().toString(), new Label.LabelStyle(style.titleFont, Color.valueOf("#e0b487")));
        fullscreenTitleBacking.add(fullscreenTitleLabel);

        previousSize = new Vector2(getWidth(), getHeight());
        previousPosition = new Vector2(getX(), getY());
    }

    private static int capMin(Integer value, int min) {
        return value == null ? min : UPMath.capMin(value, min);
    }

    public void centerOnScreen() {
        positionProportionally(0.5f, 0.5f);
    }

    public void enableNavigation(){
        navigation = new MatrixUINavigation(getLayer());
        navigation.setWindow(this,this);
        navigation.ignoreActors(btnClose);
    }

    public boolean isRemoved() {
        return getStage() == null;
    }

    public boolean isCloseButtonEnabled() {
        return btnClose.isVisible();
    }

    @Override
    public void positionProportionally(Float screenProportionX, Float screenProportionY) {
        if (lastAutoPositionScreenSize == null) {
            lastAutoPositionScreenSize = new Vector2();
        }
        lastAutoPositionScreenSize.set(getEngine().getWidth(), getEngine().getHeight());
        activateAutoPositioning();
        if (fullscreen) return;
        doPositionProportionally(screenProportionX, screenProportionY);
    }

    public void setStyle(GCWindowStyle style) {
        super.setStyle(style);

        final Label titleLabel = getTitleLabel();
        final Table titleTable = getTitleTable();
        if (style.titleBackground != null) {
            titleLabel.getStyle().background = style.titleBackground;
        }
        final Cell<Label> labelCell = titleTable.getCell(titleLabel);
        labelCell.padBottom(style.labelPadBottom).padLeft(style.labelPadLeft);

    }

    protected void positionOnResize() {
        if (!fullscreen) {
            if (lastAutoPositionScreenSize != null) {
                final float propX = UPMath.max(0, UPMath.min(1, getX(Align.center) / lastAutoPositionScreenSize.x));
                final float propY = UPMath.max(0, UPMath.min(1, getY(Align.center) / lastAutoPositionScreenSize.y));
                lastAutoPositionScreenSize.set(getEngine().getWidth(), getEngine().getHeight());
                doPositionProportionally(propX, propY);
            }
        } else {
            int screenWidth = getEngine().getWidth();
            int screenHeight = getEngine().getHeight();

            setSize(screenWidth + getPadRight() + getPadLeft(), screenHeight + getPadTop() + getPadBottom() - fullScreenLeftCorner.getHeight() - 2);
            setPosition(-getPadLeft(), -getPadBottom());

            // Reposition components
            float contentX = getPadLeft();
            float contentY = getPadBottom();
            fullscreenTopBar.setWidth(screenWidth);
            fullscreenTopBar.setPosition(contentX, contentY + screenHeight - fullscreenTopBar.getHeight());
            fullScreenLeftCorner.setPosition(contentX, contentY + screenHeight - fullScreenLeftCorner.getHeight());
            fullscreenRightCorner.setPosition(contentX + screenWidth - fullscreenRightCorner.getWidth(), contentY + screenHeight - fullscreenRightCorner.getHeight());
            fullscreenTitleBacking.setPosition(contentX + (screenWidth - fullscreenTitleBacking.getWidth()) / 2f, contentY + screenHeight - fullscreenTitleBacking.getHeight());

            btnClose.setPosition(contentX + screenWidth - btnClose.getWidth() - 7, contentY + screenHeight - btnClose.getPrefHeight() - 6);
        }
    }

    private void activateAutoPositioning() {
        if (lastAutoPositionScreenSize == null) {
            return; //not set up
        }
        if (!isOpen()) {
            return; // will be set up when the window opens
        }
        H5EEngine engine = getEngine();
        if (engine == null) {
            return;
        }

        if (!resizeHandlerWasSetup) {
            engine.onResize.registerHandler(resizeHandler);
            resizeHandlerWasSetup = true;
        }
        autoPositioningActive = true;
    }

    public void deactivateAutoPositioning() {
        if(lastAutoPositionScreenSize == null) {
            return; //not set up
        }
        if (!autoPositioningActive) return;
        autoPositioningActive = false;
        if (resizeHandlerWasSetup) {
            final H5EEngine engine = getEngine();
            if(engine != null) {
                engine.onResize.removeHandler(resizeHandler);
            }
            resizeHandlerWasSetup = false;
        }
    }

    @Override
    public void setCloseButtonEnabled(boolean newVal) {
        btnClose.setVisible(newVal);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        final H5ELayer layer = getLayer();
        if (layer == null) {
            return null;
        }
        return layer.getEngine();
    }

    @Override
    public void act(float delta) {
        if(navigation != null)
            navigation.act(delta);
        super.act(delta);
    }

    //        private final H5EEventHandler eventHandler = new H5EEventHandler() {
//        @Override
//        public void handleEvent(H5EEvent event) {
//            if (event == H5EButton.onButtonUnpressEvent) {
//                if (event.getSource() == btnClose) {
//                    close(true);
//                }
//            } else if (event == H5EGraphicElement.onPointerDownEvent) {
//                if (event.getSource() == inactiveOverlay || event.getSource() == titleBar) {
//                    GCWindowManager.getInstance().activateWindow(GCWindow.this);
//                }
//            } else if (event == H5EEngine.onResizeEvent) {
//                // maintain the proportion of window coordinates in relation to screen dimensions
//                float propX = getX() / screenWidth;
//                float propY = getY() / screenHeight;
//                int newScreenW = engine.getWidth();
//                int newScreenH = engine.getHeight();
//                setX(newScreenW * propX);
//                setY(newScreenH * propY);
//                screenWidth = newScreenW;
//                screenHeight = newScreenH;
//            }
//        }
//    };
    //    /**
//     * Internal helper method
//     */
//    private static void applyTransparencyRecursively(H5EGraphicElement element, float newVal) {
//        Dev.checkNotNull(element);
//        Dev.check(newVal == ACTIVE_ALPHA || newVal == INACTIVE_ALPHA);
//
//        // if this is a container, recursively apply to children first
//        if (element instanceof H5EContainer) {
//            H5EContainer container = (H5EContainer) element;
//            if (container.hasChildren()) {
//                for (H5EGraphicElement child : container.getChildren()) {
//                    applyTransparencyRecursively(child, newVal);
//                }
//            }
//        }
//
//        // finally, apply the value to this particular element
//
//        float curVal = element.getTransparency();
//        if (element.getTransparencyFactor() == 1.0) {
//            // Fallback to this legacy correction mechanism when a "transparency factor" setting is not defined!
//            //
//            // This will attempt to proportionally scale the element's custom transparency value... it is an unreliable approach.
//            if (newVal == ACTIVE_ALPHA && curVal != INACTIVE_ALPHA) {
//                newVal = curVal * (ACTIVE_ALPHA / INACTIVE_ALPHA);
//            } else if (newVal == INACTIVE_ALPHA && curVal != ACTIVE_ALPHA) {
//                newVal = curVal * (INACTIVE_ALPHA / ACTIVE_ALPHA);
//            }
//        }
//        element.setTransparency(newVal);
//    }
//
//    /**
//     * Internal framework method. DO NOT MAKE PUBLIC!
//     * <p>
//     * Updates the visual state of the window to reflect that it is "active"
//     */
    final void setupActiveState() {
        setColor(new Color(1f, 1f, 1f, ACTIVE_ALPHA));
        if(navigation!=null)
            navigation.setActiveState(true);
    }
//        if (getTransparency() == INACTIVE_ALPHA) {
//            return; // already setup
//        }
//
//        // visual effect
//        applyTransparencyRecursively(window, INACTIVE_ALPHA);
//
//        // all pointer events will be caught by the overlay
//        inactiveOverlay.setTouchable(Touchable.enabled);
//
//        // disable the input boxes, if there are any
//        if (inputBoxes != null) {
//            for (H5EInputBox inputBox : inputBoxes) {
//                inputBox.disable();
//            }
//        }
//    }
//
//    public void setContentWidth(int width) {
//        setWidth(width + CONTENT_PADDING_W);
//    }
//
//    public void setContentHeight(int height) {
//        setHeight(height + TITLE_BAR_HEIGHT + CONTENT_PADDING_H);
//    }

    //        if (getTransparency() == ACTIVE_ALPHA) {
//            return; // already setup
//        }
//
//        // visual effect
//        applyTransparencyRecursively(window, ACTIVE_ALPHA);
//
//        // make the window jump above all other windows
//        // TODO: this is not reliable, because the windows are not guaranteed to be in the same layer (need to enforce that)
//        layer.moveToForeground(window);
//
//        // the overlay becomes transparent to let pointer events through
//        inactiveOverlay.setTouchable(Touchable.disabled);
//
//        // enable the input boxes, if there are any
//        if (inputBoxes != null) {
//            for (H5EInputBox inputBox : inputBoxes) {
//                inputBox.enable();
//            }
//        }
//
//    }
//
//    /**
//     * Internal framework method. DO NOT MAKE PUBLIC!
//     * <p>
//     * Updates the visual state of the window to reflect that it is "inactive"
//     */
    final void setupInactiveState() {
        setColor(new Color(1f, 1f, 1f, INACTIVE_ALPHA));
    }

    @Override
    public void setWidth(int width) {
        width = UPMath.capMin(width, WINDOW_MIN_WIDTH);

        super.setWidth(width);
    }

    @Override
    public void setHeight(int height) {
        height = UPMath.capMin(height, WINDOW_MIN_HEIGHT);

        super.setHeight(height);
    }

    @Override
    public void setOriginCenter() {
        setOrigin(Math.round(getWidth() / 2f), Math.round(getHeight() / 2f));
    }

    @Override
    public String getId() {
        return getName();
    }

    @Override
    public void setId(String id) {
        setName(id);
    }

    @Override
    public void setTitle(String title) {
        if (Strings.nullToEmpty(title).isEmpty()) {
            getTitleLabel().setVisible(false);
            return;
        }
        getTitleLabel().setVisible(true);
        getTitleLabel().setText(Strings.nullToEmpty(title).toUpperCase());
        fullscreenTitleLabel.setText(Strings.nullToEmpty(title).toUpperCase());
        fullscreenTitleBacking.setSize(fullscreenTitleLabel.getPrefWidth()+60, fullscreenTitleLabel.getPrefHeight()+22);
    }

    private void doPositionProportionally(Float screenProportionX, Float screenProportionY) {
        setOriginCenter();
        /*
         *	Implementation note:
         *
         *  Though H5EGraphicElement supports proportional positioning, we do not use that mechanism
         *  as it introduces undesired side-effects on the rendering of sprites that assemble the
         *
         *  More specifically, due to round-off errors "seams" between the sprites become visible.
         *  This implementation makes sure to only use int precision, and simply overrides the existing
         *  coordinates.
         */

        if (screenProportionX != null && (screenProportionX < 0 || screenProportionX > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }
        if (screenProportionY != null && (screenProportionY < 0 || screenProportionY > 1)) {
            throw new IllegalArgumentException("Proportional position value must be a fraction between 0 and 1, or NULL");
        }

        int newX = screenProportionX == null ? 0 : (int) (screenProportionX * getEngine().getWidth());
        int newY = screenProportionY == null ? 0 : (int) (screenProportionY * getEngine().getHeight());

//        // Limit the top of the dialog box to the top of the screen
//        if (screenProportionY != null) {
//            float engineHeight = getEngine().getHeight();
//            newY = Math.min((int)(engineHeight - getHeight() / 2f), (int)(screenProportionY * engineHeight));
//        }

        // override the pre-existing coordinates with the new coordinates
        setX(Math.round(newX), Align.center);
        setY(Math.round(newY), Align.center);

        capTop();


        setSize(getWidth(), getHeight());
        validate();

        setY(Math.round(getY()));
        setX(Math.round(getX()));
    }

    /**
     * Limit the top of the dialog box to the top of the screen
     */
    public void capTop() {
        int newY = Math.min((int) (getEngine().getHeight() - getHeight() / 2f), (int) (getY() + getHeight() / 2f));
        setY(Math.round(newY), Align.center);
    }

    /**
     * Limit the bottom of the dialog box to the bottom of the screen
     */
    public void capBottom() {
        int newY = Math.max((int) (getHeight() / 2f), (int) (getY() + getHeight() / 2f));
        setY(Math.round(newY), Align.center);
    }

    /**
     * Limit the right side of the dialog box to the right side of the screen
     */
    public void capRight() {
        int newX = Math.min((int) (getEngine().getWidth() - getWidth() / 2f), (int) (getX() + getWidth() / 2f));
        setX(Math.round(newX), Align.center);
    }

    /**
     * Limit the left side of the dialog box to the left side of the screen
     */
    public void capLeft() {
        int newX = Math.max((int) (getWidth() / 2f), (int) (getX() + getWidth() / 2f));
        setX(Math.round(newX), Align.center);
    }

    /**
     * Tile windows next to each other in proportion to the screen
     *
     * @param screenProportionX The proportion of the screen to center the windows
     * @param windows           A list of windows to tile next to the current window
     */
    public void positionXProportionally(float screenProportionX, GCWindow...windows) {
        float totalWidth = this.getWidth();

        if (windows != null) {
            for (GCWindow window : windows) {
                if (window != null) {
                    totalWidth += window.getWidth();
                }
            }

            float centerX = screenProportionX * getEngine().getWidth();
            float leftX = centerX - totalWidth / 2f;
            float cumulativeWidth = this.getWidth();

            this.setX(UPMath.round(leftX + this.getWidth() / 2f), Align.center);

            for (GCWindow window : windows) {
                window.setX(UPMath.round(leftX + cumulativeWidth + window.getWidth() / 2f), Align.center);
                cumulativeWidth += window.getWidth();
            }
        } else {
            this.setX(UPMath.round(screenProportionX * getEngine().getWidth()), Align.center);
        }
    }

    /**
     * Tile windows on top of each other in proportion to the screen
     *
     * @param screenProportionY The proportion of the screen to center the windows
     * @param windows           A list of windows to tile on top of the current window
     */
    public void positionYProportionally(float screenProportionY, GCWindow... windows) {
        float totalHeight = this.getHeight();

        if (windows != null) {
            for (GCWindow window : windows) {
                if (window != null) {
                    totalHeight += window.getHeight();
                }
            }

            float centerY = screenProportionY * getEngine().getHeight();
            float leftY = centerY - totalHeight / 2f;
            float cumulativeHeight = this.getHeight();

            this.setY(UPMath.round(leftY + this.getHeight() / 2f), Align.center);

            for (GCWindow window : windows) {
                window.setY(UPMath.round(leftY + cumulativeHeight + window.getHeight() / 2f), Align.center);
                cumulativeHeight += window.getHeight();
            }
        } else {
            this.setY(UPMath.round(screenProportionY * getEngine().getHeight()), Align.center);
        }
    }

    @Override
    public void setPosition(float x, float y) {
        super.setPosition((int)x, (int)y);
    }

    @Override
    public void setPosition(float x, float y, int alignment) {
        super.setPosition((int)x, (int)y, alignment);
    }

    @Override
    public void keepWithinStage () {
        if (!fullscreen) super.setKeepWithinStage(keepWithinStage);
    }

    @Override
    public void setKeepWithinStage (boolean keepWithinStage) {
        super.setKeepWithinStage(keepWithinStage);
        this.keepWithinStage = keepWithinStage;
    }

    @Override
    protected void positionChanged () {
        if (keepWithinStage && isOpen() && !fullscreen) {
            // Keep within the bounds of the screen, with the top-left of the window taking highest priority
            setX(UPMath.max(0, UPMath.min(getEngine().getWidth() - getWidth(), getX())));
            setY(UPMath.min(getEngine().getHeight() - getHeight(), UPMath.max(0, getY())));
        }
    }
//
//    public void positionLike(GCWindow other) {
//        setOriginCenter();
//        setX(other.getX());
//        setY(other.getY());
//    }
//
//    public void positionBelow(GCWindow other) {
//        setOriginTopRight();
//        setRelativeToPositionOf(other.window, -other.getContentPaddingX(), 0);
//    }


    @Override
    public Actor hit(float x, float y, boolean touchable) {
        Actor actor = super.hit(x,y,touchable);
        if(clickThrough){
            if(actor == this){
                return null;
            }
            return actor;
        }
        return actor;
    }

    /**
     * Ub-binds this window element from the framework.
     * This is done when the window is no longer needed, to free up memory/resources.
     */
    @Override
    public void destroy() {
        if (getLayer() == null) {
            return;
        }
        getWindowManager().windowClosed(this);
        remove();
    }

    @SuppressWarnings("PointlessBooleanExpression")
    @Override
    public void open() {
        open(packOnOpen);

        //noinspection StatementWithEmptyBody
        if (getWindowManager().getMobileMode() == true && isFullscreenOnMobile()) {
            // Don't "pack with same size" if we're in mobile mode as a full screen window
        } else if (packOnOpen == false) {
            packWithSameSize();
        }
    }

    public void open(boolean pack) {
        if (isOpen()) {
            toFront();
            return;
        }
        if (pack) {
            pack();
        }
        if (fullscreen) {
            positionOnResize();
        }
        setVisible(true);
        getWindowManager().activateWindow(this);

        onOpen.fire();
        toFront();

        activateAutoPositioning();
        registerTutorialComponents(this);
        if(navigation!=null){
            navigation.clear();
        }
    }

    /**
     * Perform a recursive scan on this
     * Window's children. All the actors
     * that have a User Object defined as string
     * will be registered in Tutorial Manager with
     * this object as a Component ID.
     */
    protected void registerTutorialComponents(Group group) {
        for (Actor actor : group.getChildren()) {

            if (actor instanceof Group)
                registerTutorialComponents((Group) actor);

            if (actor.getUserObject() instanceof String) {
                String componentId = (String) actor.getUserObject();
                Tutorials.registerObject(componentId, actor);
                namedComponents.add(componentId);
            }
        }
    }

    protected void unregisterTutorialComponents() {
        for (String componentId : namedComponents) {
            Tutorials.removeObject(componentId);
        }
    }

    @Override
    public void close() {
        close(false);
    }

    /**
     * @param byUserInteraction pass TRUE to indicate that the window is being closed as a result of direct interaction by the user
     */
    public void close(boolean byUserInteraction) {
        if (!isOpen()) {
            return;
        }

        setVisible(false);
        getWindowManager().windowClosed(this);

        onClose();

        deactivateAutoPositioning();
        unregisterTutorialComponents();
    }

    protected void onClose() {
        onClose.fire();
    }

    @Override
    public boolean isOpen() {
        return isVisible();
    }

    /**
     * Makes this the active window, unless it is already active
     */
    @Override
    public void activate() {
        if (isOpen()) {
            getWindowManager().activateWindow(this);
            toFront();
        }
    }

    protected WindowManager getWindowManager() {
        return WindowManager.getInstance(getEngine());
    }

    @Override
    public int getContentPaddingX() {
        return CONTENT_PADDING_W;
    }

    @Override
    public int getContentPaddingY() {
        return CONTENT_PADDING_H;
    }

    @Override
    public int getTitleBarHeight() {
        return TITLE_BAR_HEIGHT;
    }

    public boolean isPackOnOpen() {
        return packOnOpen;
    }

    public void setPackOnOpen(boolean packOnOpen) {
        this.packOnOpen = packOnOpen;
    }

    public void setClickThrough(boolean value){
        clickThrough = value;
    }

    public boolean getClickThrough(){
        return clickThrough;
    }

    public void packWithSameSize() {
        float beforeChangeHeight = getHeight();
        float beforeChangeWidth = getWidth();

        super.pack();

        setWidth(Math.round(beforeChangeWidth)+1);
        setHeight(Math.round(beforeChangeHeight)+1);
    }

    @Override
    public void pack() {
        // Here we want to remember what the hight of the window was before packing so we can make force the window
        // to grow  downwards instead of growing upwards (which is a problem because the title bar was getting pushed up)
        float beforeChangeHeight = getHeight();

        super.pack();

        // Now move the window down based on it's resize
        float delta = getHeight() - beforeChangeHeight;
        setY(Math.round(getY() - delta));
        setX(Math.round(getX()));
    }

    @SuppressWarnings("unused")
    public static class GCWindowStyle extends WindowStyle {

        public Drawable titleBackground = null;
        public float closeButtonPadRight = -25;
        public float closeButtonPadTop = -5;
        public float labelPadLeft = 20;
        public float labelPadBottom = 6;
        public GCWindowStyle() {
        }
        public GCWindowStyle(BitmapFont titleFont, Color titleFontColor, Drawable background) {
            super(titleFont, titleFontColor, background);
        }
        public GCWindowStyle(GCWindowStyle style) {
            super(style);
            this.titleBackground = style.titleBackground;
            this.closeButtonPadRight = style.closeButtonPadRight;
            this.closeButtonPadTop = style.closeButtonPadTop;
            this.labelPadLeft = style.labelPadLeft;
            this.labelPadBottom = style.labelPadBottom;
        }

    }

    public boolean remove() {
        deactivateAutoPositioning();
        return super.remove();
    }

    public boolean isCloseable() {
        return closeable;
    }

    public void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    public boolean isFullscreen() {
        return fullscreen;
    }

    public boolean isShowTitleBarOnFullscreen() {
        return showTitleBarOnFullscreen;
    }

    public void setShowTitleBarOnFullscreen(boolean showTitleBarOnFullscreen) {
        this.showTitleBarOnFullscreen = showTitleBarOnFullscreen;
    }

    public void setFullscreen(boolean fullscreen) {
        if (fullscreen == this.fullscreen) return;
        this.fullscreen = fullscreen;
        fullscreenTopBar.setVisible(fullscreen && showTitleBarOnFullscreen);
        fullScreenLeftCorner.setVisible(fullscreen && showTitleBarOnFullscreen);
        fullscreenRightCorner.setVisible(fullscreen && showTitleBarOnFullscreen);
        fullscreenTitleBacking.setVisible(fullscreen && showTitleBarOnFullscreen);
        if (fullscreen) {
            setMovable(false);
            previousSize.x = getWidth();
            previousSize.y = getHeight();
            previousPosition.x = getX();
            previousPosition.y = getY();
            addActor(btnClose);
            pad(getPadTop()+EDGE_BUFFER+fullScreenLeftCorner.getHeight()+2,getPadLeft()+EDGE_BUFFER,getPadBottom()+EDGE_BUFFER,getPadRight()+EDGE_BUFFER);
            fullscreenTitleLabel.setText(getTitleLabel().getText());
            fullscreenTitleBacking.setSize(fullscreenTitleLabel.getPrefWidth()+60, fullscreenTitleLabel.getPrefHeight()+22);
        } else {
            setMovable(true);
            pad(getPadTop()-EDGE_BUFFER-fullScreenLeftCorner.getHeight()-2,getPadLeft()-EDGE_BUFFER,getPadBottom()-EDGE_BUFFER,getPadRight()-EDGE_BUFFER);
            btnCell.setActor(btnClose);
            setSize(previousSize.x, previousSize.y);
            setPosition(previousPosition.x, previousPosition.y);
        }

        positionOnResize();
        getWindowManager().disableOptionsButtonInFullscreen(disableOptionsButtonInFullscreen);
        getWindowManager().updateFullscreen();
    }

    public boolean isFullscreenOnMobile() {
        return fullscreenOnMobile;
    }

    public void setFullscreenOnMobile(boolean fullscreenOnMobile) {
        this.fullscreenOnMobile = fullscreenOnMobile;
    }

    public void disableOptionsButtonInFullscreen(boolean disableOptionsButtonInFullscreen) {
        this.disableOptionsButtonInFullscreen = disableOptionsButtonInFullscreen;
    }

    public boolean getDisableOptionsButtonInFullscreen() {
        return disableOptionsButtonInFullscreen;
    }

    public UINavigation getNavigation(){
        return navigation;
    }

    public void setNavigation(UINavigation navigation){
        this.navigation = navigation;
    }
}
