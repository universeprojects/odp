package com.universeprojects.gamecomponents.client.elements;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;

public abstract class GCListItemActionHandler<T extends GCListItem> {

    private T selectedItem;

    protected GCListItemActionHandler() {

    }

    public T getSelectedItem() {
        return selectedItem;
    }

    public boolean isItemSelected() {
        return selectedItem != null;
    }

    public void reset() {
        selectedItem = null;
    }

    public void clearSelection() {
        setSelection(null);
    }

    public void setSelection(T item) {
        this.selectedItem = item;
        onSelectionUpdate(item);
    }

    /**
     * To be implemented by child class.
     * Performs additional logic when the selection changes.
     */
    protected abstract void onSelectionUpdate(T newItemSelection);

    protected void onDoubleClick(T item) {
    }

    public void onInfoButtonClicked(T item) {
    }

    public static class SimpleHandler<T extends GCListItem> extends GCListItemActionHandler<T> {

        private final Callable1Args<T> selectionChangeCallable;
        private final Callable1Args<T> doubleClickCallable;


        public SimpleHandler(Callable1Args<T> selectionChangeCallable, Callable1Args<T> doubleClickCallable) {
            this.selectionChangeCallable = selectionChangeCallable;
            this.doubleClickCallable = doubleClickCallable;
        }

        public SimpleHandler(Callable0Args selectionChangeCallable) {
            this((item) -> selectionChangeCallable.call());
        }

        public SimpleHandler(Callable1Args<T> selectionChangeCallable) {
            this(selectionChangeCallable, null);
        }

        public SimpleHandler(Callable0Args selectionChangeCallable, Callable1Args<T> doubleClickCallable) {
            this((item) -> selectionChangeCallable.call(), doubleClickCallable);
        }

        @Override
        protected void onSelectionUpdate(T lastSelectedItem) {
            selectionChangeCallable.call(lastSelectedItem);
        }

        @Override
        protected void onDoubleClick(T item) {
            if(doubleClickCallable != null) {
                doubleClickCallable.call(item);
            }
        }
    }
}
