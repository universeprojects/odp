package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCUpdateErrorWindow extends GCGenericErrorWindow {

    public GCUpdateErrorWindow(H5ELayer layer, String message, Throwable error, Runnable continueCallback, Runnable backCallback) {
        super(layer);

        setWidth(400);
        addH1(message).colspan(2);
        addEmptyLine(10);
        addH2(error.getMessage()).colspan(2).getActor().setColor(Color.RED);
        addEmptyLine(10);
        addH2("You can try to join to this game using this version of the game,\r\nhowever it's not guaranteed to work.").align(Align.center).colspan(2).getActor().setAlignment(Align.center);
        addEmptyLine(30);

        addMediumButton("Join with current version").getActor().addButtonListener(() -> {
            close();
            continueCallback.run();
        });

        addMediumButton("Select different game").getActor().addButtonListener(() -> {
            close();
            backCallback.run();
        });
    }
}
