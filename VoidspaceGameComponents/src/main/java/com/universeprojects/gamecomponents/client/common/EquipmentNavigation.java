package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventorySlotBaseComponent;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionButton;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.inputs.H5EGamePad;


public class EquipmentNavigation extends MatrixUINavigation {

    private static final float LONG_PRESS = 0.4f;

    private GCWindow mainWindow;
    private GCWindow secondaryWindow;
    private GamepadDragAndDrop dragAndDrop;
    private float pressTime = 1;

    public EquipmentNavigation(H5ELayer layer, GCWindow mainWindow){
        this(layer, mainWindow, null);
        setWindow(mainWindow, mainWindow);
    }

    public EquipmentNavigation(H5ELayer layer, GCWindow mainWindow, GCWindow secondaryWindow) {
        super(layer);
        this.mainWindow = mainWindow;
        this.secondaryWindow = secondaryWindow;
        dragAndDrop = InventoryManager.getInstance(layer.getEngine()).getDragAndDrop();

        Controllers.addListener(new ControllerAdapter() {
            @Override
            public boolean buttonDown(Controller controller, int buttonIndex) {
                if (isActive && WindowManager.getInstance(layer.getEngine()).getActiveNavigation() == EquipmentNavigation.this) {
                    if (isActive && buttonIndex == H5EGamePad.FACE_BUTTON_A) {
                        pressTime = 0;
                    }
                }
                return super.buttonDown(controller, buttonIndex);
            }

            @Override
            public boolean buttonUp(Controller controller, int buttonIndex) {
                if (isActive && WindowManager.getInstance(layer.getEngine()).getActiveNavigation() == EquipmentNavigation.this) {
                    if (buttonIndex == H5EGamePad.FACE_BUTTON_A) {
                        if (pressTime < LONG_PRESS) {
                            if (currentActor instanceof GCInventorySlotBaseComponent) {
                                ((GCInventorySlotBaseComponent<?, ?, ?>) currentActor).click();
                            } else if (currentActor instanceof GCActionButton) {
                                for (EventListener listener : currentActor.getListeners()) {
                                    if (listener instanceof ClickListener) {
                                        ((ClickListener) listener).clicked(new InputEvent(), 0, 0);
                                    }
                                }
                            }
                        } else {
                            if (dragAndDrop.isDragging()) {
                                dragAndDrop.drop();
                                if (isVisible(currentActor)) {
                                    focusOnCurrent();
                                } else {
                                    clear();
                                    update();
                                }
                            }
                        }
                        pressTime = 1;
                    }
                }
                return super.buttonUp(controller, buttonIndex);
            }
        });
    }

    @Override
    public void exit() {
        if (tryExit()) {
            if (!indicator.isActive() && mainWindow.isCloseButtonEnabled()) {
                mainWindow.close();
            } else {
                indicator.deactivate();
            }
        }
    }

    @Override
    public void move(Direction direction) {
        if (dragAndDrop.isDragging()) {
            dragAndDrop.move(direction);
            Actor dndActor = dragAndDrop.getCurrentTarget().getActor();
            if (secondaryWindow != null) {
                if (dndActor.isDescendantOf(mainWindow)) {
                    window = mainWindow;
                } else {
                    window = secondaryWindow;
                }
                indicator.setWindow(window);
            }
            indicator.activate(dndActor);
            H5EScrollablePane pane = isInsidePanel(dndActor);
            moveScrollbar(dndActor, pane);
        } else {
            if (tryMove(direction)) {
                Callable0Args moveFunction = () -> {
                };
                switch (direction) {
                    case UP:
                        moveFunction = this::moveUp;
                        break;
                    case LEFT:
                        moveFunction = this::moveLeft;
                        break;
                    case DOWN:
                        moveFunction = this::moveDown;
                        break;
                    case RIGHT:
                        moveFunction = this::moveRight;
                        break;
                }
                update();
                nearest = currentActor;
                moveFunction.call();
                if (currentActor != nearest) {
                    if (secondaryWindow != null) {
                        if (nearest.isDescendantOf(mainWindow)) {
                            window = mainWindow;
                        } else {
                            window = secondaryWindow;
                        }
                        indicator.setWindow(window);
                    }
                    currentActor = nearest;
                    focusOnCurrent();
                }
            }
        }
    }

    private void startDrag() {
        if (!dragAndDrop.isDragging()) {
            if (currentActor instanceof GCInventorySlotBaseComponent) {
                if (!((GCInventorySlotBaseComponent<?, ?, ?>) currentActor).isEmpty()) {
                    dragAndDrop.startDrag(((GCInventorySlotBaseComponent<?, ?, ?>) currentActor).getDragAndDropSource(), layer);
                }
            }
            if (currentActor instanceof GCActionButton) {
                dragAndDrop.startDrag(((GCActionButton) currentActor).getDndSource(), layer);
            }
            if (dragAndDrop.getCurrentTarget() != null) {
                indicator.activate(dragAndDrop.getCurrentTarget().getActor());
            }
        }
    }

    @Override
    public void onClose() {
        super.onClose();
        pressTime = 1;
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isActive() && WindowManager.getInstance(layer.getEngine()).getActiveNavigation() == this) {
            if (pressTime < LONG_PRESS && pressTime < 1) {
                pressTime += delta;
            } else if (pressTime >= LONG_PRESS && pressTime < 1) {
                pressTime = 1;
                startDrag();
            }
        }
    }

    @Override
    protected boolean isFocusable(Actor actor) {
        if (actor != null && isVisible(actor) && !ignoredActors.contains(actor)) {
            if (actor instanceof Button) {
                return true;
            }
            if (actor instanceof TextField) {
                return true;
            }
            for (NavigationExtension extension : extensions) {
                if (extension.isCompatible(actor)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void setMainWindow(GCWindow mainWindow) {
        this.mainWindow = mainWindow;
    }

    public void setSecondaryWindow(GCWindow secondaryWindow){
        this.secondaryWindow = secondaryWindow;
    }

    @Override
    public void update() {
        actorsSnapshot.clear();
        if (mainWindow != null) {
            fetchActors(mainWindow);
            if (secondaryWindow != null) {
                fetchActors(secondaryWindow);
            }
            if (currentActor == null && actorsSnapshot.size() > 0)
                currentActor = getLeftTopCornerActor();
            focusOnCurrent();
        }
    }

}
