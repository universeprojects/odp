package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.DestroyableDialog;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCPopupNewDiscovery<K, T extends GCInventoryItem> extends GCWindow implements DestroyableDialog {

    public static final float TITLE_FONT_SIZE = 1f;

    private final static int WINDOW_WIDTH = 450;
    private final static int BODY_WIDTH = WINDOW_WIDTH - 158;

    private final H5EIcon icon;
    private final H5ELabel objectNameLabel;
    private final H5ELabel descriptionTextArea;

    protected GCInventorySlotComponent<K, T, ?> currentComponent;


    public GCPopupNewDiscovery(H5ELayer layer, String windowTitle, String spriteTypeKey, String name, String description) {
        super(layer, WINDOW_WIDTH, 200, "popup-window2");
        left().top();
        defaults().left().top();
        getTitleLabel().setVisible(true);
        setTitle(windowTitle);
        setKeepWithinStage(true);
        setCloseButtonEnabled(true);
        enableNavigation();


        Table mainTable = add(new Table()).width(WINDOW_WIDTH).height(200).padTop(20).growY().top().left().getActor();

        H5EIcon iconBacking = H5EIcon.fromSpriteType(layer, "images/GUI/new-discovery-icon-backing1.png", Scaling.contain, Align.center);
        iconBacking.setSize(128,128);

        icon = H5EIcon.fromSpriteType(layer, spriteTypeKey, Scaling.contain, Align.center);
        icon.setSize(64, 64);
        Table imageContainer = new Table();
        imageContainer.add(icon).width(64).height(64);

        H5EStack iconContainerStack = new H5EStack(iconBacking, imageContainer);
        iconContainerStack.setSize(128,128);
        mainTable.add(iconContainerStack).top().left().padLeft(-20).padTop(-20);

        final Table nameTable = new Table();
        nameTable.setTouchable(Touchable.disabled);
        nameTable.left().top();
        nameTable.defaults().left().top();
        objectNameLabel = new H5ELabel(layer);
        objectNameLabel.setStyle(StyleFactory.INSTANCE.labelDefault);
        objectNameLabel.setAlignment(Align.left);
        objectNameLabel.setText(name);
        if (objectNameLabel.getPrefWidth()>BODY_WIDTH) {
            objectNameLabel.setStyle(StyleFactory.INSTANCE.labelSmallElectrolize);
            objectNameLabel.setWrap(true);
        }
        nameTable.add(objectNameLabel).pad(2).padTop(-2).left().growX();

        nameTable.row();

        H5EIcon underline = new H5EIcon(layer, "images/GUI/selector/menu-header-underline.png", Scaling.none, Align.left, 200, 7);
        underline.setColor(0.7f, 0.7f, 0.7f, 0.4f);
        nameTable.add(underline).growX();

        nameTable.row();

        descriptionTextArea = new H5ELabel(layer);
        descriptionTextArea.setAlignment(Align.left);
        descriptionTextArea.setText(description);
        descriptionTextArea.setStyle(StyleFactory.INSTANCE.labelSmallElectrolize);
        descriptionTextArea.setWrap(true);
        descriptionTextArea.setColor(0.7f, 0.7f, 0.7f, 1f);
        nameTable.add(descriptionTextArea).left().width(BODY_WIDTH);

        mainTable.add(nameTable).width(BODY_WIDTH).growY().left().top();

    }

}
