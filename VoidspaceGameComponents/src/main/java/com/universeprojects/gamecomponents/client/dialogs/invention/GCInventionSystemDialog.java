package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.ItemTreeNavigation;
import com.universeprojects.gamecomponents.client.common.MatrixUINavigation;
import com.universeprojects.gamecomponents.client.common.NavigationShortcut;
import com.universeprojects.gamecomponents.client.common.NumericInputGamepadExtension;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData;
import com.universeprojects.gamecomponents.client.dialogs.inventory.BuildQueueEntryItemLike;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCQueueItemIcon;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.vsdata.shared.BuildQueueEntry;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("FieldCanBeLocal")
public abstract class GCInventionSystemDialog extends GCWindow {

    public static final int LEFT_AREA_WIDTH = 320;
    public static final int RIGHT_AREA_WIDTH = 464;
    final int TAB_BUTTON_WIDTH = 30;
    final int TAB_BUTTON_HEIGHT = 25;

    protected final H5ELayer layer;

    private final H5EButton btnKnowledge;
    private final H5EButton btnExperiments;
    private final H5EButton btnIdeas;
    private final H5EButton btnBuildItem;
    private final H5EButton btnRecipes;
    private final H5EButton btnIdeasUnread;
    private final H5EButton btnBuildItemUnread;

    private final ButtonGroup<H5EButton> buttons;

    private final H5EButton btnCancel;
    private final Table progressBarTable;
    private final H5EProgressBar embeddedProgressBar;

    public final Stack mainStack;
    public final Table mainTable;
    private final Table leftArea;
    private final Table rightArea;
    private final HorizontalGroup horizontalGroup;
    public final H5EStack lowerButtonRow;
    public final H5EStack leftContent;
    public final H5EStack rightContent;
    public final H5EStack rightTitleArea;
    public final H5EStack progressBarArea;
    public final H5EScrollablePane queuePane;
    public final Cell<H5EScrollablePane> queueTableCell;
    public List<GCQueueItemIcon<?>> queueIconList;

    protected final GCInventionKnowledgeTab knowledgeTab;
    protected final GCInventionExperimentsTab experimentsTab;
    protected final GCInventionIdeasTab ideasTab;
    protected final GCInventionItemsTab itemsTab;
    protected final GCInventionRecipeTab recipeTab;
    private final Map<GCInventionTab, H5EButton> tabsAndButtons = new LinkedHashMap<>();
    private boolean shouldShowSkillButtons = true;
    private GCInventionTab currentTab;
    private GCInventionTab lastMainTab;

    private final GCLoadingIndicator loadingIndicator;

    public enum WindowState {DESKTOP, LANDSCAPE, PORTRAIT}
    private WindowState lastState = WindowState.DESKTOP;

    public boolean omitClearingRecipeTabOnce = false;

    public GCInventionSystemDialog(H5ELayer layer) {
        super(layer, 800, 600, "ui2-invention-window");
        this.layer = Dev.checkNotNull(layer);

        setId("invention-system-control");
        setTitle("          ");
        defaults().left().top();
        setFullscreenOnMobile(true);
        loadingIndicator = new GCLoadingIndicator(layer);

        mainStack = new Stack();
        mainTable = new Table();
        mainStack.add(mainTable);
        add(mainStack).left().top().grow();
        leftArea = mainTable.add(new Table()).left().top().growY().width(LEFT_AREA_WIDTH).getActor();
        rightArea = mainTable.add(new Table()).left().top().growY().width(RIGHT_AREA_WIDTH).getActor();
        rightArea.top().left();
        queuePane = new H5EScrollablePane(layer);
        queuePane.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueCornersSemiTransparent);
        queueTableCell = rightArea.add(queuePane).left().top().growX().height(64).padLeft(5).padRight(5);
        H5ELabel emptyQueue=new H5ELabel("Empty job queue", layer);
        emptyQueue.setColor(Color.valueOf("#CCCCCC"));
        queuePane.getContent().stack(emptyQueue);
        rightArea.row();
        rightTitleArea = rightArea.add(new H5EStack()).left().top().height(32).fillX().getActor();
        rightArea.row();
        rightContent = rightArea.add(new H5EStack()).left().top().grow().getActor();

        horizontalGroup = leftArea.add(new HorizontalGroup()).
                padTop(24).padBottom(36).padLeft(23).left().getActor();
        horizontalGroup.space(0);

        btnKnowledge = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-knowledge.png")
                .withName("tab-btn-knowledge")
                .withTooltip("Knowledge")
                .build();


        btnExperiments = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-experiments.png")
                .withName("tab-btn-experiments")
                .withTooltip("Experiments")
                .withTutorialId("button:invention:tab:experiment")
                .build();

        btnIdeas = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-ideas.png")
                .withName("tab-btn-ideas")
                .withTooltip("Ideas")
                .withTutorialId("button:invention:tab:ideas")
                .build();

        btnIdeasUnread = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-ideas-unread.png")
                .withName("tab-btn-ideas")
                .withTooltip("Ideas")
                .withTutorialId("button:invention:tab:ideas!")
                .build();

        btnBuildItem = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-build-item.png")
                .withName("tab-btn-buildItem")
                .withTooltip("Build items")
                .withTutorialId("button:invention:tab:builditem")
                .build();

        btnBuildItemUnread = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-build-item-unread.png")
                .withName("tab-btn-buildItem")
                .withTooltip("Build items")
                .withTutorialId("button:invention:tab:builditem!")
                .build();

        btnRecipes = ButtonBuilder.inLayer(layer)
                .withStyle("button2-tab")
                .withForegroundSpriteType("images/invention/tab-recipe1.png")
                .withName("tab-btn-recipe")
                .withTooltip("Recipe")
                .withTutorialId("button:invention:tab:recipes")
                .build();


        final H5EButton[] buttonArray = {btnKnowledge, btnExperiments, btnIdeas, btnBuildItem, btnRecipes};

        for (H5EButton btn : buttonArray) {
            horizontalGroup.addActor(btn);
            btn.getImageCell()
                    .width(TAB_BUTTON_WIDTH)
                    .height(TAB_BUTTON_HEIGHT)
                    .align(Align.center);
        }
        btnIdeasUnread.getImageCell().width(TAB_BUTTON_WIDTH).height(TAB_BUTTON_HEIGHT).align(Align.center);
        btnBuildItemUnread.getImageCell().width(TAB_BUTTON_WIDTH).height(TAB_BUTTON_HEIGHT).align(Align.center);

        buttons = new ButtonGroup<>(buttonArray);
        buttons.setMaxCheckCount(1);
        buttons.setMinCheckCount(1);

        leftArea.row();

        leftContent = leftArea.add(new H5EStack()).grow().getActor();

        leftArea.row();

        lowerButtonRow = leftArea.add(new H5EStack()).fillX().left().padLeft(43).padBottom(4).width(232).height(30).getActor();

        knowledgeTab = addTab(new GCInventionKnowledgeTab(this), btnKnowledge);
        experimentsTab = addTab(new GCInventionExperimentsTab(this), btnExperiments);
        ideasTab = addTab(new GCInventionIdeasTab(this), btnIdeas);
        itemsTab = addTab(new GCInventionItemsTab(this), btnBuildItem);
        recipeTab = addTab(new GCInventionRecipeTab(this), btnRecipes);

        btnKnowledge.addCheckedButtonListener(() -> {
            loadDataForKnowledgeTab();
            TooltipManager.getInstance().hideAll();
        });
        btnExperiments.addCheckedButtonListener(() -> {
            loadDataForExperimentsTab();
            TooltipManager.getInstance().hideAll();
        });
        Callable0Args onIdeas = () -> {
            loadDataForIdeasTab();
            TooltipManager.getInstance().hideAll();
        };
        btnIdeas.addCheckedButtonListener(onIdeas);
        btnIdeasUnread.addCheckedButtonListener(onIdeas);

        Callable0Args onBuildItems = () -> {
            loadDataForItemsTab();
            TooltipManager.getInstance().hideAll();
        };
        btnBuildItem.addCheckedButtonListener(onBuildItems);
        btnBuildItemUnread.addCheckedButtonListener(onBuildItems);
        Callable0Args onRecipeTab = () -> {
            openRecipeTab();
            TooltipManager.getInstance().hideAll();
        };
        btnRecipes.addCheckedButtonListener(onRecipeTab);

        btnCancel = ButtonBuilder.inLayer(layer).withStyle("button-red").withText("Stop").build();
        btnCancel.addButtonListener(this::onCancelBtnPressed);
        btnCancel.setVisible(true);

        rightArea.row();
        progressBarArea = rightArea.add(new H5EStack()).fillX().right().padTop(10).align(Align.center).getActor();
        embeddedProgressBar = new H5EProgressBar(layer);
        progressBarTable = new Table();
        progressBarArea.add(progressBarTable);
        embeddedProgressBar.setRange(0, 1000);
        embeddedProgressBar.setDisplayMode(H5EProgressBar.DisplayMode.LABEL_CUSTOM);
        progressBarTable.add(embeddedProgressBar).pad(0,10,0,5).growX();
        progressBarTable.row();
        progressBarTable.add(btnCancel).pad(0,10,10,5).growX();
        progressBarTable.setVisible(false);


        clear();
        resetLayout();
        Tutorials.registerCheckerObject("window:invention", this);
        Tutorials.registerObject("button:invention:close", btnClose);
        navigation = new MatrixUINavigation(layer);
        navigation.setWindow(this, mainTable);
        navigation.ignoreActors(rightTitleArea,progressBarArea);
        navigation.addShortcut(NavigationShortcut.create(GCRecipeSlotListItem.class, leftArea, NavigationShortcut.Trigger.Delete));
        navigation.addShortcut(NavigationShortcut.create((Actor) null, null, NavigationShortcut.Trigger.LeftTrigger).setAction(this::previousTab));
        navigation.addShortcut(NavigationShortcut.create((Actor) null, null, NavigationShortcut.Trigger.RightTrigger).setAction(this::nextTab));
        navigation.addExtension(new ItemTreeNavigation(navigation));
        navigation.addExtension(new NumericInputGamepadExtension(navigation, 1, Integer.MAX_VALUE));
    }

    protected H5EButton getIdeasButton() {
        for (Actor child : horizontalGroup.getChildren()) {
            if (child == btnIdeas || child == btnIdeasUnread) {
                return (H5EButton) child;
            }
        }

        return null;
    }

    protected H5EButton getItemsButton() {
        for (Actor child : horizontalGroup.getChildren()) {
            if (child == btnBuildItem || child == btnBuildItemUnread) {
                return (H5EButton) child;
            }
        }

        return null;
    }

    protected H5EButton getRecipesButton() {
        for (Actor child : horizontalGroup.getChildren()) {
            if (child == btnRecipes || child == btnBuildItemUnread) {
                return (H5EButton) child;
            }
        }

        return null;
    }

    @Override
    protected void onClose() {
        super.onClose();
        Tutorials.trigger("close:window:invention");
    }

    private <T extends GCInventionTab> T addTab(T tab, H5EButton btn) {
        tabsAndButtons.put(tab, btn);
        return tab;
    }

    public void clear() {
        for (GCInventionTab tab : tabsAndButtons.keySet()) {
            tab.deactivate();
        }
        currentTab = knowledgeTab;
        btnKnowledge.setProgrammaticChangeEvents(false);
        btnKnowledge.setChecked(true);
        btnKnowledge.setProgrammaticChangeEvents(true);
    }

    protected void openTab(GCInventionTab tab) {
        Dev.checkNotNull(tab);

        H5EButton btn = tabsAndButtons.get(tab);
        if (btn != null) {
            btn.setProgrammaticChangeEvents(false);
            btn.setChecked(true);
            btn.setProgrammaticChangeEvents(true);
        }

        if (currentTab != tab) {
            currentTab.deactivate();
        }
        currentTab = tab;
        updateTab(tab);
        tab.show();

        if (currentTab == ideasTab) {
            setIdeasUnread(false);
        } else if (currentTab == itemsTab) {
            setItemsUnread(false);
        }
        unregisterTutorialComponents();
        registerTutorialComponents(this);
    }

    protected void updateTab(GCInventionTab tab){
        tab.windowStateChanged(lastState);
    }

    protected void setIdeasUnread(boolean unread) {
        if (unread) {
            horizontalGroup.removeActor(btnIdeas);
            horizontalGroup.addActorAt(2, btnIdeasUnread);
            btnIdeasUnread.setChecked(false);
        } else {
            horizontalGroup.removeActor(btnIdeasUnread);
            horizontalGroup.addActorAt(2, btnIdeas);
        }
    }

    protected void setItemsUnread(boolean unread) {
        if (unread) {
            horizontalGroup.removeActor(btnBuildItem);
            horizontalGroup.addActorAt(3, btnBuildItemUnread);
            btnBuildItemUnread.setChecked(false);
        } else {
            horizontalGroup.removeActor(btnBuildItemUnread);
            horizontalGroup.addActorAt(3, btnBuildItem);
        }
    }

    protected boolean isCurrentTab(GCInventionTab tab) {
        return currentTab == tab;
    }

    @Override
    public void open() {
        super.open(false);
        reloadCurrentTab();
        layer.setKeyboardFocus(null);
        Tutorials.trigger("window:invention");
    }

    public void openWithoutReload() {
        super.open(false);
        Tutorials.trigger("window:invention");
    }

    public void openKnowledgeTab(GCKnowledgeData knowledgeData, GCKnowledgeDefData defData) {
        clear();
        openTab(knowledgeTab);
        Tutorials.trigger("window:tab:knowledge");
        knowledgeTab.populateCategories(knowledgeData, defData);
        finishLoading();
    }

    public void openExperimentsTab(List<GCInventoryData> experimentsData) {
        openTab(experimentsTab);
        Tutorials.trigger("window:tab:experiment");
        experimentsTab.populateCategories(experimentsData);
        finishLoading();
    }

    public void openIdeasTab(GCIdeasData ideasData, GCIdeaCategoryData categoryData) {
        openTab(ideasTab);
        Tutorials.trigger("window:tab:ideas");
        ideasTab.populateCategories(ideasData, categoryData);
        finishLoading();
    }

    public void openItemsTab(GCSkillsData itemsData, GCIdeaCategoryData categoryData) {
        openTab(itemsTab);
        Tutorials.trigger("window:tab:builditem");
        itemsTab.populateCategories(itemsData, categoryData);
        finishLoading();
    }

    public void openRecipeTab(GCRecipeData recipeData) {
        openRecipeTab(recipeData, true);
    }

    public void openRecipeTab(GCRecipeData recipeData, boolean isFirst) {
        if (currentTab != recipeTab) {
            lastMainTab = currentTab;
            openTab(recipeTab);
        }
        navigation.clear();
        Tutorials.trigger("window:tab:recipe");
        if(recipeData!=null)
            recipeTab.initRecipeData(recipeData, isFirst);
        else recipeTab.clearAndOpen();
        recipeTab.resetRepeats();
        finishLoading();
    }

    public void showQueue(){
        queueTableCell.setActor(queuePane);
        queueTableCell.left().top().growX().height(64).padLeft(5).padRight(5);
    }

    public void hideQueue(){
        queueTableCell.clearActor();
        queueTableCell.height(0).pad(0);
    }

    public void openRecipeTab() {
        openRecipeTab(getCurrentOrLastRecipe());
    }

    public void setFixedRepeats(String value) {
        recipeTab.setFixedRepeats(value);
        experimentsTab.setFixedRepeats(value);
    }

    public void clearFixedRepeats() {
        recipeTab.resetRepeats();
        experimentsTab.resetRepeats();
    }

    public void updateRecipeTab(GCRecipeData recipeData) {
        if (currentTab == recipeTab) {
            recipeTab.updateRecipeData(recipeData);
        }
    }

    public void initQueueData(List<BuildQueueEntry> entries) {
        queuePane.getContent().clearChildren();
        if(entries==null ||entries.size()==0){
            H5ELabel emptyQueue=new H5ELabel("Empty job queue", layer);
            emptyQueue.setColor(Color.valueOf("#CCCCCC"));
            queuePane.getContent().stack(emptyQueue);
            return;
        }
        queueIconList = new ArrayList<>();
        boolean first = true;
        for (BuildQueueEntry entry : entries) {
            final boolean isFirst=first;
            GCQueueItemIcon<?> icon = new GCQueueItemIcon<>(layer);
            //noinspection unchecked
            icon.setItem(new BuildQueueEntryItemLike(entry));
            icon.setHighlightIcon("GUI/ui2/itemicon-selected1.png");
            queueIconList.add(icon);
            queuePane.getContent().add(icon).size(48);
            icon.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    for (GCQueueItemIcon<?> itemIcon : queueIconList) {
                        if(icon!=itemIcon)
                        itemIcon.setHighlighted(false);
                    }
                    if(!icon.isHighlighted()) {
                        icon.setHighlighted(true);
                        onQueueEntrySelected(entry, isFirst);
                    }else{
                        icon.setHighlighted(false);
                        openRecipeTab();
                    }

                }
            });
            first=false;
        }
        queuePane.getContent().add().growX();
    }

    public void updateExperimentsTab(List<GCInventoryData> inventoryData) {
        if (currentTab == experimentsTab) {
            experimentsTab.populateCategories(inventoryData);
        }
    }

    public void reloadCurrentTab() {
        loadDataForQueue();
        if (currentTab == knowledgeTab) {
            loadDataForKnowledgeTab();
        } else if (currentTab == experimentsTab) {
            loadDataForExperimentsTab();
        } else if (currentTab == ideasTab) {
            loadDataForIdeasTab();
        } else if (currentTab == itemsTab) {
            loadDataForItemsTab();
        } else if (currentTab == recipeTab) {
            if (omitClearingRecipeTabOnce) {
                omitClearingRecipeTabOnce = false;
            } else {
                clear();
                currentTab = lastMainTab;
                reloadCurrentTab();
            }
        }
    }

    public boolean isExperimentsTab() {
        return isCurrentTab(experimentsTab);
    }

    public boolean isIdeasTab() {
        return isCurrentTab(ideasTab);
    }

    public boolean isConstructItemsTab() {
        return isCurrentTab(itemsTab);
    }

    public boolean isRecipeTab() {
        return isCurrentTab(recipeTab);
    }

    public boolean isCancelButtonVisible() {
        if (btnCancel == null) return false;
        return btnCancel.isVisible();
    }

    public String getExperimentTabSelectedName() {
        if (experimentsTab == null) return "";
        return experimentsTab.getSelectedItemName();
    }

    public String getIdeasTabSelectedName() {
        if (ideasTab == null) return "";
        return ideasTab.getSelectedItemName();
    }

    public void reloadIdeasTab() {
        if (currentTab == ideasTab) loadDataForIdeasTab();
    }

    public void reloadExperimentsTab() {
        if (currentTab == experimentsTab) loadDataForExperimentsTab();
    }

    public void reloadItemsTab() {
        if (currentTab == itemsTab) loadDataForItemsTab();
    }

    public void reloadRecipeTab() {
        if (currentTab == recipeTab) loadDataForQueue();
    }

    protected void flagLoading() {
        loadingIndicator.activate(this);
    }

    protected void finishLoading() {
        loadingIndicator.deactivate();
    }

    private void resetLayout() {
        // Reconstruct the window based on device and orientation
        WindowState currentState = WindowState.DESKTOP;
        if (isFullscreen()) {
            currentState = getEngine().getWidth() > getEngine().getHeight() ? WindowState.LANDSCAPE : WindowState.PORTRAIT;
        }
        if (currentState == lastState) return;

        mainTable.clearChildren();
        leftArea.clearChildren();
        rightArea.clearChildren();
        switch (currentState) {
            case DESKTOP:
                // Default window
                setStyle(getSkin().get("ui2-invention-window", WindowStyle.class));
                setTitle("");
                mainTable.add(leftArea).left().top().growY().width(320);
                mainTable.add(rightArea).left().top().grow();

                leftArea.add(horizontalGroup).padTop(24).padBottom(36).padLeft(23).left().getActor();
                horizontalGroup.left();
                leftArea.row();
                leftArea.add(leftContent).grow().getActor();
                leftArea.row();
                leftArea.add(lowerButtonRow).fillX().left().padLeft(43).padBottom(4).width(232).height(30).getActor();

                rightArea.add(queuePane).left().top().growX().height(64).padLeft(5).padRight(5);
                rightArea.row();
                rightArea.add(rightTitleArea).left().top().height(32).growX().getActor();
                rightArea.row();
                rightArea.add(rightContent).left().top().grow().getActor();
                rightArea.row();
                rightArea.add(progressBarArea).fillX().right().padTop(10).align(Align.center).getActor();
                break;

            case LANDSCAPE:
                // Fullscreen, similar layout to desktop
                setStyle(getSkin().get("ui2-invention-window", WindowStyle.class));
                setTitle("Invention System");
                mainTable.add(leftArea).left().top().width(340).growY();
                mainTable.add(rightArea).left().top().grow().padRight(20).padTop(4).padBottom(24);

                leftArea.add(horizontalGroup).padTop(24).padBottom(36).padLeft(38).growX();
                horizontalGroup.left();
                leftArea.row();
                leftArea.add(leftContent).grow().padLeft(22);
                leftArea.row();
                leftArea.add(lowerButtonRow).left().padBottom(24).padLeft(58).width(232).height(30).padTop(10);

                rightArea.add(queuePane).left().top().growX().height(64).padLeft(5).padRight(5);
                rightArea.row();
                rightArea.add(rightTitleArea).left().top().height(32).growX();
                rightArea.row();
                rightArea.add(rightContent).left().top().grow();
                rightArea.row();
                rightArea.add(progressBarArea).fillX().right().padTop(5).padRight(-5).align(Align.center);
                break;

            case PORTRAIT:
                // Fullscreen, different layout from desktop
                setStyle(getSkin().get("default", WindowStyle.class));
                setTitle("Invention System");
                mainTable.defaults().top();
                mainTable.add(horizontalGroup).padTop(15).padBottom(15).growX();
                horizontalGroup.center();
                mainTable.row();
                mainTable.add(rightTitleArea).pad(5).padTop(0).growX().height(32);
                mainTable.row();
                mainTable.add(leftContent).pad(0,5,0,5).height(layer.getHeight()*0.25f).growX();
                mainTable.row();
                mainTable.add(rightContent).pad(0,5,0,5).grow();
                mainTable.row();
                mainTable.add(progressBarArea).growX();
                mainTable.row();
                mainTable.add(lowerButtonRow).pad(10).padBottom(4).growX().height(30);
                mainTable.row();
                mainTable.top();
                break;
        }
        lastState = currentState;
        updateTab(currentTab);
//        knowledgeTab.windowStateChanged(currentState);
//        experimentsTab.windowStateChanged(currentState);
//        ideasTab.windowStateChanged(currentState);
//        itemsTab.windowStateChanged(currentState);
//        recipeTab.windowStateChanged(currentState);
    }

    public void setTitleAreaHeight(int height){
        WindowState currentState = WindowState.DESKTOP;
        if (isFullscreen()) {
            currentState = getEngine().getWidth() > getEngine().getHeight() ? WindowState.LANDSCAPE : WindowState.PORTRAIT;
        }
        switch (currentState){
            case LANDSCAPE:
            case DESKTOP:
                rightArea.getCell(rightTitleArea).height(height);
                break;
            case PORTRAIT:
                mainTable.getCell(rightTitleArea).height(height);
                break;
        }
    }

    @Override
    protected void positionOnResize() {
        resetLayout();
        super.positionOnResize();
    }

    @Override
    public void setFullscreen(boolean fullscreen) {
        if (fullscreen == this.fullscreen) return;
        this.fullscreen = fullscreen;
        fullscreenTopBar.setVisible(fullscreen);
        fullScreenLeftCorner.setVisible(fullscreen);
        fullscreenRightCorner.setVisible(fullscreen);
        fullscreenTitleBacking.setVisible(fullscreen);

        int topPad = 0;
        int bottomPad = -20;
        int leftPad = -15;
        int rightPad = -15;
        if (fullscreen) {
            setMovable(false);
            previousSize.x = getWidth();
            previousSize.y = getHeight();
            addActor(btnClose);
            pad(getPadTop() + topPad, getPadLeft() + leftPad, getPadBottom() + bottomPad, getPadRight() + rightPad);
            fullscreenTitleLabel.setText(getTitleLabel().getText());
            fullscreenTitleBacking.setSize(fullscreenTitleLabel.getPrefWidth() + 60, fullscreenTitleLabel.getPrefHeight() + 22);
        } else {
            setMovable(true);
            pad(getPadTop() - topPad, getPadLeft() - leftPad, getPadBottom() - bottomPad, getPadRight() - rightPad);
            btnCell.setActor(btnClose);
            setSize(previousSize.x, previousSize.y);
        }

        positionOnResize();
        getWindowManager().updateFullscreen();
    }

    public void nextTab(){
        navigation.clear();
        if(buttons.getCheckedIndex()<buttons.getButtons().size-1){
            buttons.getButtons().get(buttons.getCheckedIndex()+1).setChecked(true);
        }else{
            buttons.getButtons().get(0).setChecked(true);
        }
    }

    public void previousTab(){
        navigation.clear();
        if(buttons.getCheckedIndex()>0){
            buttons.getButtons().get(buttons.getCheckedIndex()-1).setChecked(true);
        }else{
            buttons.getButtons().get(buttons.getButtons().size-1).setChecked(true);
        }
    }

    public boolean shouldShowSkillButtons() {
        return shouldShowSkillButtons;
    }

    public void setShouldShowSkillButtons(boolean shouldShowSkillButtons) {
        this.shouldShowSkillButtons = shouldShowSkillButtons;
    }

    public void showCancelBtn() {
        btnCancel.setVisible(true);
    }

    public void hideCancelBtn() {
        btnCancel.setVisible(false);
    }

    public void showEmbeddedProgressBar() {
        progressBarTable.setVisible(true);
    }

    public void setEmbeddedProgressBarMaximum(float maximum) {
        embeddedProgressBar.setRange(0, maximum);
    }

    public void setEmbeddedProgressBarProgress(float progress, String text) {
        embeddedProgressBar.setValue(progress);
        embeddedProgressBar.updateProgressText(text);
    }

    public void hideEmbeddedProgressBar() {
        progressBarTable.setVisible(false);
    }

    //////////////////////////////////////////////////////////////
    //     CLIENT INTEGRATION INTERFACE BELOW
    //

    /**
     * This hook signals that the user requested to open the "knowledge" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openKnowledgeTab}.
     */
    public abstract void loadDataForKnowledgeTab();

    /**
     * This hook signals that the user requested to open the "experiments" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openExperimentsTab}.
     */
    public abstract void loadDataForExperimentsTab();

    /**
     * This hook signals that the user requested to open the "ideas" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openIdeasTab}.
     */
    public abstract void loadDataForIdeasTab();

    /**
     * This hook signals that the user requested to open the "build-item" tab.
     * The implementer's responsibility is to load the relevant data and then call {@link #openItemsTab}.
     */
    public abstract void loadDataForItemsTab();

    public abstract void loadDataForQueue();

    public abstract void onBeginExperimentsBtnPressed(String selectionUid, int repeats);

    public abstract void onBeginPrototypingBtnPressed(Long ideaId);

    public abstract void onBuildItemBtnPressed(Long skillId);

    public abstract void onDeleteSkillBtnPressed(Long skillId);

    public abstract void onBuildBuildingBtnPressed(Long skillId);

    public abstract void onRecipeOkayBtnPressed(GCRecipeData recipe, int repeats);

    public abstract void onItemIconInfoClicked(Actor actor, GCInventoryData.GCInventoryDataItem data);

    public abstract void onSkillIconInfoClicked(Actor actor, GCSkillsData.GCSkillDataItem skillDataItem);

    public abstract void onCancelBtnPressed();

    public abstract void onFinishExperiment(List<String> icons);

    public abstract void onFinishDesignItem(String icon);

    public abstract void onFinishBuildItem(String icon);

    public abstract void onFinishDesignBuilding(String icon);

    public abstract void onQueueEntrySelected(BuildQueueEntry buildQueueEntry, boolean isFirst);

    public abstract void onSkillUpgradeButtonPressed(Long skillId);

    public abstract void onQueueCancelPressed();

    public abstract boolean validateRecipeSlotsFilled(GCRecipeData data);

    public abstract GCRecipeData getCurrentOrLastRecipe();

    public abstract boolean validateUserFieldValues(GCRecipeData data);
}
