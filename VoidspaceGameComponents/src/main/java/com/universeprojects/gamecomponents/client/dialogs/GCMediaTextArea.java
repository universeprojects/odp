package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable3Args;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.components.GCGenericINDMediaItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingSprite;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.vsdata.shared.BookData;
import com.universeprojects.vsdata.shared.IdeaData;
import com.universeprojects.vsdata.shared.MapMarkerData;


import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;

public class GCMediaTextArea extends Table {
    private H5ETextArea textArea;
    private H5ELayer layer;
    private List<String> lines;
    private boolean readOnlyMode;
    protected H5EScrollablePane parentPanel;
    private Callable1Args<GCIdeasData.GCIdeaDataItem> onIdeaLearn;
    private Callable1Args<GCSkillsData.GCSkillDataItem> onSkillLearn;
    private Callable1Args<MapMarkerData> onMarkerLearn;
    private Callable3Args<GCIdeasData, GCSkillsData, List<MapMarkerData>> onLearnAll;

    private GCSkillsData skills;
    private GCIdeasData ideas;

    private List<MapMarkerData> markerData;

    enum ContentType {
        Text, Icon, Idea, MapMarker, Skill
    }

    public GCMediaTextArea(H5ELayer layer) {
        super();
        this.layer = layer;
        textArea = new H5ETextArea(layer);
    }

    public void setParentPanel(H5EScrollablePane panel){
        parentPanel = panel;
    }

    public void setReadOnlyMode(boolean mode) {
        if (mode != readOnlyMode) {
            clearChildren();
            readOnlyMode = mode;
        }
    }

    public void setSkills(GCSkillsData skills) {
        this.skills = skills;
    }

    public void setIdeas(GCIdeasData ideas) {
        this.ideas = ideas;
    }

    public void setMarkerData(List<MapMarkerData> markerData) {
        this.markerData = markerData;
    }

    public ContentType getLineContentType(String line) {
        if (line.length() > 5) {
            if (line.substring(0, 4).equals(".img")) {
                return ContentType.Icon;
            }
            if (line.substring(0, 5).equals(".idea")) {
                return ContentType.Idea;
            }
            if (line.substring(0, 6).equals(".skill")) {
                return ContentType.Skill;
            }
            if (line.substring(0, 4).equals(".map")) {
                return ContentType.MapMarker;
            }
        }
        return ContentType.Text;
    }

    public void clearContent() {
        clearChildren();
        this.lines = new ArrayList<String>();
        textArea.setText("");
    }

    public String getText() {
        return textArea.getText();
    }

    public void processContent(String content) {
        processContent(content, true);
    }

    public void processContent(String content, boolean clear) {
        if (clear)
            clearContent();
        this.lines = Arrays.asList(content.split("\\r?\\n"));
        if (readOnlyMode) {
            for (String line : lines) {
                switch (getLineContentType(line)) {
                    case Icon:
                        addIcon(line);
                        break;
                    case Skill:
                        addSkill(line);
                        break;
                    case Idea:
                        addIdea(line);
                        break;
                    case MapMarker:
                        addMapMarker(line);
                        break;
                    case Text:
                        addLabel(line);
                        break;
                }
            }
            top().left();
        } else {
            if (getCells().size == 0) {
                add(textArea).grow();
            }
            textArea.setText(content);
            invalidate();
        }
    }

    public H5ETextArea getTextArea() {
        return textArea;
    }

    public void addLabel(String text) {
        H5ELabel label = new H5ELabel(text, layer);
        label.setStyle(StyleFactory.INSTANCE.labelSmallElectrolize);
        add(label).growX();
        label.setWrap(true);
        row();
    }

    public void addIcon(String line) {
        String url = line.substring(line.indexOf("url=") + 4);
        Cell<?> iconCell = add().top().maxHeight(200);
        H5EIcon newIcon = H5EIcon.fromSpriteType(layer, url, Scaling.fit, Align.center, (state, icon) -> {
            if (state == DownloadingSprite.DownloadState.LOADED) {
                H5EIcon newIcon2 = H5EIcon.fromSpriteType(layer, url, Scaling.fit, Align.center);
                iconCell.setActor(newIcon2);
            }
        });
        iconCell.setActor(newIcon);
        row();
    }

    public void addSkill(String line) {
        String name = line.substring(line.indexOf("name=") + 5);
        Cell<?> itemCell = add().top();
        for (int i = 0; i < skills.size(); i++) {
            if (skills.get(i).name.equals(name)) {
                GCGenericINDMediaItem<GCSkillsData.GCSkillDataItem> item = new GCGenericINDMediaItem<>(layer, new GCGenericINDListItemData<>(skills.get(i).name, skills.get(i).iconKey, skills.get(i).description, 0, skills.get(i)), parentPanel, this);
                itemCell.setActor(item);
                break;
            }
        }
        if(itemCell.getActor()==null){
            itemCell.setActor(new H5ELabel("[Skill is unavailable]", layer));
        }
        row();
    }

    public void addIdea(String line) {
        String name = line.substring(line.indexOf("name=") + 5);
        Cell<?> itemCell = add().top();
        for (int i = 0; i < ideas.size(); i++) {
            if (ideas.get(i).name.equals(name)) {
                GCGenericINDMediaItem<GCIdeasData.GCIdeaDataItem> item = new GCGenericINDMediaItem<>(layer, new GCGenericINDListItemData<>(ideas.get(i).name, ideas.get(i).iconKey, ideas.get(i).description, 0, ideas.get(i)), parentPanel, this);
                itemCell.setActor(item);
                break;
            }
        }
        if(itemCell.getActor()==null){
            itemCell.setActor(new H5ELabel("[Idea is unavailable]", layer));
        }
        row();
    }

    public void addMapMarker(String line) {
        String id = line.substring(line.indexOf("id=") + 3);
        Cell<?> itemCell = add().top();
        for (int i = 0; i < markerData.size(); i++) {
            if (markerData.get(i).id.equals(id)) {
                GCGenericINDMediaItem<MapMarkerData> item = new GCGenericINDMediaItem<>(layer, new GCGenericINDListItemData<>(markerData.get(i).name, markerData.get(i).icon, markerData.get(i).description, 0, markerData.get(i)), parentPanel, this);
                itemCell.setActor(item);
                break;
            }
        }
        if(itemCell.getActor()==null){
            itemCell.setActor(new H5ELabel("[Marker is unavailable]", layer));
        }
        row();
    }

    public void loadFromBook(BookData bookData){
        setReadOnlyMode(!bookData.editable);
        ideas = GCIdeasData.createNew();
        if(bookData.listOfIdeas!=null)
            for(IdeaData idea:bookData.listOfIdeas){
                String iconKey = "";
                if (!Strings.isEmpty(idea.icon)) {
                    iconKey = idea.icon;
                }
                ideas.add(idea.id, idea.name, idea.description, iconKey, idea.parentCategoryId, idea.createdAt == null ? 0 : idea.createdAt, idea.baseTimeToRunSecs);
            }
        if(bookData.listOfMarkers!=null)
            markerData = bookData.listOfMarkers;
        processContent(bookData.listOfChapters.get(0).content);
    }

    public void addText(String text) {
        textArea.setText(textArea.getText() + "\n" + text);
    }

    public void appendText(String text){
        textArea.appendText("\n" + text);
    }

    public void setListeners(Callable1Args<GCIdeasData.GCIdeaDataItem> onIdeaLearn, Callable1Args<GCSkillsData.GCSkillDataItem> onSkillLearn, Callable1Args<MapMarkerData> onMarkerLearn, Callable3Args<GCIdeasData, GCSkillsData, List<MapMarkerData>>  onLearnAll) {
        this.onIdeaLearn = onIdeaLearn;
        this.onSkillLearn = onSkillLearn;
        this.onMarkerLearn = onMarkerLearn;
        this.onLearnAll = onLearnAll;
    }

    public void learnAll(){
        if(onLearnAll != null)
            onLearnAll.call(ideas, skills, markerData);
    }

    public void onLearn(GCGenericINDMediaItem<?> item) {
        if (item.getData().data instanceof GCIdeasData.GCIdeaDataItem) {
            onIdeaLearn.call((GCIdeasData.GCIdeaDataItem) item.getData().data);
        }
        if (item.getData().data instanceof GCSkillsData.GCSkillDataItem) {
            onSkillLearn.call((GCSkillsData.GCSkillDataItem) item.getData().data);
        }
        if (item.getData().data instanceof MapMarkerData) {
            onMarkerLearn.call((MapMarkerData) item.getData().data);
        }
    }

    public void deselectAll() {
        for (Actor a : getChildren()) {
            if (a instanceof GCGenericINDMediaItem) {
                ((GCGenericINDMediaItem<?>) a).rebuildActor();
            }
        }
    }
}