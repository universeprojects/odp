package com.universeprojects.gamecomponents.client.auth;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public interface PlatformAuthOption {

    Button newSignInButton(H5ELayer layer);

    void setAuthCallback(Callable1Args<AuthCredentials> callback);

    void setOnErrorCallback(Callable1Args<String> onErrorCallback);

    /**
     * Reserved to logout of silent login
     */
    void signOut(Callable0Args onReady);

    /**
     * Will be called when user leaves the login page
     */
    void shutdown();

    /**
     * Authenticate with the last account for this API (if possible)
     * @param callback callback with the result of the auth check, null if was unable to resume the session
     */
    void silentLogin(Callable1Args<AuthCredentials> callback);
}