package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.components.*;
import com.universeprojects.gamecomponents.client.dialogs.invention.*;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EDropDown;
import com.universeprojects.vsdata.shared.MapMarkerData;
import com.universeprojects.vsdata.shared.MapMarkerGroupData;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GCBookInsertionDialog extends GCSimpleWindow {
    private final static int WINDOW_H = 500;
    private final static int WINDOW_W = 600;
    private final H5EDropDown dropDown;
    private final H5ELayer layer;
    private GCCategoryItemSplitComponent<GCSkillsData.GCSkillDataItem> skillsList;
    private GCCategoryItemSplitComponent<GCIdeasData.GCIdeaDataItem> ideasList;
    private GCCategoryItemSplitComponent<MapMarkerData> markers;
    private Map<String, Object> skillsData;
    private Map<String, Object> ideasData;
    private Map<String, Object> markersData;
    private Cell<?> listsCell;
    private Callable2Args<String, Object> onInsert;

    public GCBookInsertionDialog(H5ELayer layer, Callable2Args<String, Object> onInsert) {
        super(layer, "bookInsertion", "Insert...", WINDOW_W, WINDOW_H, false);
        this.layer=layer;
        this.onInsert=onInsert;
        positionProportionally(0.5f, 0.5f);
        setModal(true);
        setFullscreenOnMobile(true);
        List<String> options = new ArrayList<>();
        //options.add("Items");
        options.add("Ideas");
        options.add("Map markers");
        dropDown = new H5EDropDown(options, layer);
        dropDown.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                if(dropDown.getSelected().equals("Items")){
                    listsCell.setActor(skillsList);
                    skillsList.setDropdown(dropDown);
                    skillsList.setData(skillsData);
                }else if(dropDown.getSelected().equals("Ideas")){
                    listsCell.setActor(ideasList);
                    ideasList.setDropdown(dropDown);
                    ideasList.setData(ideasData);
                }else{
                    listsCell.setActor(markers);
                    markers.setDropdown(dropDown);
                    markers.setData(markersData);
                }
            }
        });
        skillsList = new GCCategoryItemSplitComponent<GCSkillsData.GCSkillDataItem>(layer, true,this::insertItem);
        skillsList.setDropdown(dropDown);
        ideasList = new GCCategoryItemSplitComponent<GCIdeasData.GCIdeaDataItem>(layer, true,this::insertIdea);
        markers = new GCCategoryItemSplitComponent<MapMarkerData>(layer, true,this::insertMarker);
        listsCell=add(skillsList).grow();
    }


    public void populateIdeaCategories(GCIdeasData ideaData, GCIdeaCategoryData categoryData) {
        List<GCIdeaCategoryData.GCIdeaCategoryDataItem> rootCategories = categoryData.children;
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        List<GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>> uncategorized = new ArrayList<GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>>();
        for(int i=0;i<ideaData.size();i++){
            if(ideaData.get(i).parentId==null) {
                uncategorized.add(new GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>(ideaData.get(i).name, ideaData.get(i).iconKey, ideaData.get(i).description, 0, ideaData.get(i)));
            }
        }
        data.put("Uncategorized", uncategorized);
        for (GCIdeaCategoryData.GCIdeaCategoryDataItem cat : rootCategories) {
            generateIdeasMapRecursively(data, ideaData, cat);
        }
        this.ideasData=data;
    }

    public boolean generateIdeasMapRecursively(Map<String, Object> map, GCIdeasData ideasData, GCIdeaCategoryData.GCIdeaCategoryDataItem category) {
        boolean put = false;
        if (category.children != null && category.children.size() > 0) {
            Map<String, Object> inner = new LinkedHashMap<String, Object>();
            map.put(category.name, inner);
            for (GCIdeaCategoryData.GCIdeaCategoryDataItem cat : category.children) {
                put |= generateIdeasMapRecursively(inner, ideasData, cat);
            }
            if(!put){
                map.remove(category.name);
            }
            return put;
        } else {
            List<GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>> list = new ArrayList<GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>>();
            for(int i=0;i<ideasData.size();i++){
                if(ideasData.get(i).parentId!=null && ideasData.get(i).parentId==category.id) {
                    list.add(new GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem>(ideasData.get(i).name, ideasData.get(i).iconKey, ideasData.get(i).description, 0, ideasData.get(i)));
                }
            }
            if(list.size()>0) {
                map.put(category.name, list);
                return true;
            }
        }
        return false;
    }

    public void populateSkillCategories(GCSkillsData skillData, GCIdeaCategoryData categoryData) {
        List<GCIdeaCategoryData.GCIdeaCategoryDataItem> rootCategories = categoryData.children;
        Map<String, Object> data = new LinkedHashMap<String, Object>();
        List<GCGenericINDListItemData<GCSkillsData.GCSkillDataItem>> uncategorized = new ArrayList<>();
        for(int i=0;i<skillData.size();i++){
            if(skillData.get(i).parentId==null) {
                uncategorized.add(new GCGenericINDListItemData<>(skillData.get(i).name, skillData.get(i).iconKey, skillData.get(i).description, 0, skillData.get(i)));
            }
        }
        data.put("Uncategorized", uncategorized);
        for (GCIdeaCategoryData.GCIdeaCategoryDataItem cat : rootCategories) {
            generateSkillsMapRecursively(data, skillData, cat);
        }
        this.skillsData=data;
    }

    public boolean generateSkillsMapRecursively(Map<String, Object> map, GCSkillsData skillsData, GCIdeaCategoryData.GCIdeaCategoryDataItem category) {
        boolean put = false;
        if (category.children != null && category.children.size() > 0) {
            Map<String, Object> inner = new LinkedHashMap<String, Object>();
            map.put(category.name, inner);
            for (GCIdeaCategoryData.GCIdeaCategoryDataItem cat : category.children) {
                put |= generateSkillsMapRecursively(inner, skillsData, cat);
            }
            if(!put){
                map.remove(category.name);
            }
            return put;
        } else {
            List<GCGenericINDListItemData<GCSkillsData.GCSkillDataItem>> list = new ArrayList<GCGenericINDListItemData<GCSkillsData.GCSkillDataItem>>();
            for(int i=0;i<skillsData.size();i++){
                if(skillsData.get(i).parentId==category.id) {
                    list.add(new GCGenericINDListItemData<>(skillsData.get(i).name, skillsData.get(i).iconKey, skillsData.get(i).description, 0, skillsData.get(i)));
                }
            }
            if(list.size()>0) {
                map.put(category.name, list);
                return true;
            }
        }
        return false;
    }

    public void populateMarkers(List<MapMarkerGroupData> groups){
        Map<String, Object> data = new LinkedHashMap<String,Object>();
        for (MapMarkerGroupData cat : groups) {
            List<GCGenericINDListItemData<MapMarkerData>> list=new ArrayList<>();
            for (MapMarkerData entry: cat.markers) {
                list.add(new GCGenericINDListItemData<>(entry.name,entry.icon,entry.description,0,entry));
            }
            data.put(cat.name, list);
        }
        this.markersData=data;
    }

    @Override
    public void open() {
        listsCell.setActor(skillsList);
        skillsList.setData(skillsData);
        skillsList.setDropdown(dropDown);
        dropDown.setSelectedIndex(0);
        super.open();
    }

    public void insertIdea(GCGenericINDListItemData<GCIdeasData.GCIdeaDataItem> idea){
        onInsert.call(".idea name="+idea.name, idea.data);
        close();
    }

    public void insertItem(GCGenericINDListItemData<GCSkillsData.GCSkillDataItem> item){
        onInsert.call(".skill name="+item.name, item.data);
        close();
    }

    public void insertMarker(GCGenericINDListItemData<MapMarkerData> marker){
        if(!marker.data.name.contains("id="))
            onInsert.call(".map name= "+ marker.data.name +" id="+marker.data.id, marker.data);
        else onInsert.call(".map id="+marker.data.id, marker.data);
        close();
    }
}
