package com.universeprojects.gamecomponents.client.tutorial.visualization.animations;

public class GeneralTooltipAnimation extends EasedAnimation {
    private float time = 0;
    private float duration;
    private boolean callbackFired = false;
    private Runnable callback = null;

    public GeneralTooltipAnimation(float duration) {
        this.duration = duration;
    }

    public void reset() {
        time = 0;
        callbackFired = false;
    }

    public void setCallback(Runnable callback) {
        this.callback = callback;
    }

    public float animate(float delta) {
        if (time + delta > duration) {
            if (!callbackFired) {
                callbackFired = true;
                callback.run();
            }
            return 1f;
        }

        // In this animation we don't reset the time,
        // since it is not looped
        time += delta;

        return quadInOutEasing(time, 1f, duration);
    }
}
