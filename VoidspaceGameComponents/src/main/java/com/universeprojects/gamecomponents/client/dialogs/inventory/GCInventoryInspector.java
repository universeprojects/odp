package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.universeprojects.gamecomponents.client.common.EquipmentNavigation;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCInventoryInspector<T extends GCInventoryItem> extends GCItemInspectorWithActions<Integer, T> {

    public GCInventoryInspector(H5ELayer layer) {
        super(layer, true);
        setNavigation(new EquipmentNavigation(layer, this, null));
        getNavigation().setWindow(this, this);
    }
}
