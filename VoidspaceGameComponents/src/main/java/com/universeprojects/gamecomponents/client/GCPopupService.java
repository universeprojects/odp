package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.ReturningCallable1Args;
import com.universeprojects.gamecomponents.client.dialogs.DestroyableDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCBasicInputDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCInfoDialog;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.Callables;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

@SuppressWarnings("unused")
public class GCPopupService {
    private static final String NAME = "GCPopupService";

    private final List<DestroyableDialog> dialogs = new ArrayList<>();
    private final Deque<GCWindow> dialogQueue = new LinkedList<>();
    private boolean showingScheduledDialog;

    private final H5ELayer layer;

    public static GCPopupService getInstance(Actor actor) {
        return getInstance((H5ELayer) actor.getStage());
    }

    public static GCPopupService getInstance(H5ELayer layer) {
        if(layer == null || layer.getEngine() == null) {
            return noopService;
        }
        return layer.getEngine().getOrCreateEngineBoundObject(NAME, () -> new GCPopupService(layer));
    }

    private static final GCPopupService noopService = new GCPopupService(null);

    public GCPopupService(H5ELayer layer) {
        this.layer = layer;
    }

    public synchronized void schedule(GCWindow dialog) {
        dialog.onClose.registerHandler(this::showNextScheduledPopup);
        dialogQueue.push(dialog);
        showScheduledPopups();
    }

    private void showScheduledPopups() {
        if(showingScheduledDialog) {
            return;
        }
        showNextScheduledPopup();
    }

    private synchronized void showNextScheduledPopup() {
        showingScheduledDialog = false;
        if(dialogQueue.isEmpty()) {
            return;
        }
        GCWindow dialog = dialogQueue.pollFirst();
        if(dialog.getStage() == null) {
            dialogQueue.clear();
            return;
        }
        showingScheduledDialog = true;
        dialog.positionProportionally(.5f, .7f);
        dialog.open();
        registerDialog((DestroyableDialog) dialog);
    }

    public GCInfoDialog createYesNoPopup(final String title, final List<String> caption, final Callable0Args onYes, final Callable0Args onNo) {
        return createConfirmPopup(title, caption, "Yes", "No", onYes, onNo);
    }

    public GCInfoDialog createConfirmPopup(final String title, final List<String> caption, String confirmText, String denyText, final Callable0Args onConfirm, final Callable0Args onDeny) {
        return createPopup(title, caption, confirmText, denyText, onDeny, onConfirm, onDeny);
    }

    public GCInfoDialog createConfirmPopup(final String title, final String[] caption, String confirmText, String denyText, final Callable0Args onConfirm, final Callable0Args onDeny) {
        return createPopup(title, caption, confirmText, denyText, onDeny, onConfirm, onDeny);
    }

    public GCInfoDialog createOKPopup(final String title, final String[] caption) {
        return createOKPopup(title, caption, Callables.emptyCallable0Args());
    }

    public GCInfoDialog createOKPopup(final String title, final String[] caption, final Callable0Args onOK) {
        return createPopup(title, caption, "OK", null, onOK, onOK, null);
    }

    public GCInfoDialog createPopup(final String title, final List<String> caption, final String buttonLeftText, final String buttonRightText,
                            final Callable0Args onClose, final Callable0Args onLeftButton, final Callable0Args onRightButton) {
        String[] captionArray = caption.toArray(new String[0]);
        return createPopup(title, captionArray, buttonLeftText, buttonRightText, onClose, onLeftButton, onRightButton);
    }

    public GCInfoDialog createPopup(final String title, final String[] caption, final String buttonLeftText, final String buttonRightText,
                            final Callable0Args onClose, final Callable0Args onLeftButton, final Callable0Args onRightButton) {
        final H5ELayer uiLayer = getUILayer();
        if (UPUtils.isServer() || uiLayer == null) {
            return null;
        }
        GCInfoDialog confirmationDialog = new GCInfoDialog(uiLayer, true) {
            @Override
            protected String getTitleText() {
                return title;
            }

            @Override
            protected String[] getCaptionText() {
                return caption;
            }

            @Override
            protected String getButtonLeftText() {
                return buttonLeftText;
            }

            @Override
            protected String getButtonRightText() {
                return buttonRightText;
            }

            @Override
            protected void onDismiss() {
                if (onClose != null) {
                    onClose.call();
                }
            }

            @Override
            protected void onLeftButtonPressed() {
                if (onLeftButton != null) {
                    onLeftButton.call();
                }
            }

            @Override
            protected void onRightButtonPressed() {
                if (onRightButton != null) {
                    onRightButton.call();
                }
            }
        };
        confirmationDialog.open();
        confirmationDialog.positionProportionally(.5f, .5f);
        confirmationDialog.activateFullscreenIfNecessary();
        registerDialog(confirmationDialog);
        return confirmationDialog;
    }

    public void createTextInputPopup(String title, String caption, String buttonText, Callable1Args<String> onSubmit) {
        createTextInputPopup(title, caption, buttonText, (msg) -> null, onSubmit);
    }

    public GCBasicInputDialog createTextInputPopup(String title, String caption, String buttonText, ReturningCallable1Args<String, String> validator, Callable1Args<String> onSubmit) {
        final H5ELayer uiLayer = getUILayer();
        if (UPUtils.isServer() || uiLayer == null) {
            return null;
        }
        GCBasicInputDialog inputDialog = new GCBasicInputDialog(uiLayer, "text-input") {
            @Override
            public String getTitle() {
                return title;
            }

            @Override
            public String getCaption() {
                return caption;
            }

            @Override
            public String getButtonText() {
                return buttonText;
            }

            @Override
            public String validate() {
                return validator.call(getInput());
            }

            @Override
            public void handleSubmit() {
                onSubmit.call(getInput());
            }
        };
        inputDialog.open();
        return inputDialog;
    }

    public H5ELayer getUILayer() {
        return layer;
    }

    public synchronized void clear() {
        for (DestroyableDialog dialog : new ArrayList<>(dialogs)) {
            dialog.destroy();
        }
        dialogs.clear();
        for (GCWindow dialog : new ArrayList<>(dialogQueue)) {
            dialog.destroy();
        }
        dialogQueue.clear();
        showingScheduledDialog = false;
    }

    public void registerDialog(DestroyableDialog dialog) {
        dialogs.add(dialog);
    }
}
