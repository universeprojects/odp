package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.selection.GCSelectionMenuClickable;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;

import java.util.ArrayList;
import java.util.List;

public class MatrixUINavigation extends UINavigation {

    private static final float MIN_POS_DIFFERENCE = 0;
    private int maxScrollpaneSize = 6;

    protected final List<Actor> actorsSnapshot;
    protected Actor lastListItem;
    protected Actor nearest;
    private final Vector2 currentPosVec;
    private final Vector2 nearestPosVec;

    public MatrixUINavigation(H5ELayer layer) {
        super(layer);
        actorsSnapshot = new ArrayList<>();
        currentPosVec = new Vector2(0, 0);
        nearestPosVec = new Vector2(0, 0);
    }

    @Override
    public void setWindow(Window window, Group rootGroup) {
        super.setWindow(window, rootGroup);
        currentGroup = rootGroup;
    }

    public void setMaxScrollpaneSize(int maxScrollpaneSize) {
        this.maxScrollpaneSize = maxScrollpaneSize;
    }

    @Override
    public void update() {
        actorsSnapshot.clear();
        if (currentGroup != null) {
            fetchActors(currentGroup);
            if ((currentActor == null || !currentActor.isVisible() || currentActor.getStage()==null) && actorsSnapshot.size() > 0)
                currentActor = getLeftTopCornerActor();
            focusOnCurrent();
        }
    }

    @Override
    public void clear() {
        currentActor = null;
        currentGroup = rootGroup;
        actorsSnapshot.clear();
        indicator.deactivate();
    }

    @Override
    public void exit() {
        if (tryExit()) {
            if (currentGroup != rootGroup) {
                if (isInsidePanel(currentActor) != null) {
                    lastListItem = currentActor;
                }
                currentActor = currentGroup;
                currentGroup = rootGroup;
                update();
            } else {
                if (!indicator.isActive() && window instanceof GCWindow && ((GCWindow) window).isCloseButtonEnabled()) {
                    ((GCWindow) window).close();
                } else {
                    indicator.deactivate();
                }
            }
        }
    }

    @Override
    public void focusOn(Actor actor) {
        if (actor instanceof Group) {
            clear();
            currentGroup = (Group) actor;
            update();
        } else {
            super.focusOn(actor);
        }
    }

    @Override
    public void focusOnCurrent() {
        unFocusAll();
        if (currentActor != null) {
            indicator.activate(currentActor);
            curScrollablePane = isInsidePanel(currentActor);
            moveScrollbar();
            if (currentActor instanceof H5EButton) {
                ((H5EButton) currentActor).setOver(true);
            }
        }
    }

    @Override
    protected void findActor() {

    }

    @Override
    protected boolean isFocusable(Actor actor) {
        if (actor != null && actor.isVisible() && actor.getStage() != null && !ignoredActors.contains(actor)) {
            if (actor instanceof Button) {
                return true;
            }
            if (actor instanceof TextField) {
                return true;
            }
            if (actor instanceof H5EScrollablePane && getGroupLength((H5EScrollablePane) actor) > maxScrollpaneSize) {
                return true;
            }
            if(actor instanceof GCSelectionMenuClickable){
                return true;
            }
            for (NavigationExtension extension : extensions) {
                if (extension.isCompatible(actor)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void enter() {
        if (tryEnter()) {
            update();
            if (currentActor != null) {
                if (currentActor instanceof Button) {
                    if(!((Button) currentActor).isDisabled())
                        Gdx.app.postRunnable(() -> ((Button) currentActor).setChecked(!((Button) currentActor).isChecked()));
                } else if (currentActor instanceof GCItemIcon) {
                    for (Object listener : currentActor.getListeners()) {
                        if (listener instanceof ClickListener) {
                            ((ClickListener) listener).clicked(new InputEvent(), 0, 0);
                        }
                    }
                } else if (currentActor instanceof H5ETextArea) {
                    ((H5ETextArea) currentActor).focus();
                } else if (currentActor instanceof H5EInputBox) {
                    ((H5EInputBox) currentActor).focus();
                } else if (currentActor instanceof H5EScrollablePane) {
                    currentGroup = ((H5EScrollablePane) currentActor);
                    if (lastListItem == null) {
                        currentActor = null;
                    } else {
                        currentActor = (isInsidePanel(lastListItem) == currentActor) ? lastListItem : null;
                    }
                    update();
                }
            }
        }
    }

    @Override
    public void move(Direction direction) {
        if (tryMove(direction)) {
            Callable0Args moveFunction = () -> {
            };
            switch (direction) {
                case UP:
                    moveFunction = this::moveUp;
                    break;
                case LEFT:
                    moveFunction = this::moveLeft;
                    break;
                case DOWN:
                    moveFunction = this::moveDown;
                    break;
                case RIGHT:
                    moveFunction = this::moveRight;
                    break;
            }
            update();
            if(currentActor==null)
                return;
            nearest = currentActor;
            moveFunction.call();
            if (nearest != currentActor) {
                currentActor = nearest;
            } else {
                if (currentGroup != rootGroup) {
                    Actor temp = currentActor;
                    Group tempGroup = currentGroup;
                    exit();
                    currentActor = temp;
                    moveFunction.call();
                    currentActor = nearest;
                    if (currentActor == temp) {
                        currentActor = tempGroup;
                    }
                }
            }
            focusOnCurrent();
        }
    }

    @Override
    public void moveLeft() {
        float minDistance = Float.MAX_VALUE;
        currentActor.localToStageCoordinates(currentPosVec.set(5, currentActor.getHeight() / 2));
        for (Actor actor : actorsSnapshot) {
            actor.localToStageCoordinates(nearestPosVec.set(actor.getWidth(), actor.getHeight() / 2));
            if (nearestPosVec.x < currentPosVec.x - MIN_POS_DIFFERENCE && currentPosVec.dst(nearestPosVec) < minDistance) {
                minDistance = currentPosVec.dst(nearestPosVec);
                nearest = actor;
            }
        }

    }

    @Override
    public void moveUp() {
        float minDistance = Float.MAX_VALUE;
        currentActor.localToStageCoordinates(currentPosVec.set(currentActor.getWidth() / 2, currentActor.getHeight()));
        for (Actor actor : actorsSnapshot) {
            actor.localToStageCoordinates(nearestPosVec.set(actor.getWidth() / 2, 5));
            if (nearestPosVec.y > currentPosVec.y + MIN_POS_DIFFERENCE && currentPosVec.dst(nearestPosVec) < minDistance) {
                minDistance = currentPosVec.dst(nearestPosVec);
                nearest = actor;
            }
        }
    }

    @Override
    public void moveRight() {
        float minDistance = Float.MAX_VALUE;
        currentActor.localToStageCoordinates(currentPosVec.set(currentActor.getWidth(), currentActor.getHeight() / 2));
        for (Actor actor : actorsSnapshot) {
            actor.localToStageCoordinates(nearestPosVec.set(5, actor.getHeight() / 2));
            if (nearestPosVec.x > currentPosVec.x + MIN_POS_DIFFERENCE && currentPosVec.dst(nearestPosVec) < minDistance) {
                minDistance = currentPosVec.dst(nearestPosVec);
                nearest = actor;
            }
        }
    }

    @Override
    public void moveDown() {
        float minDistance = Float.MAX_VALUE;
        currentActor.localToStageCoordinates(currentPosVec.set(currentActor.getWidth() / 2, 5));
        for (Actor actor : actorsSnapshot) {
            actor.localToStageCoordinates(nearestPosVec.set(actor.getWidth() / 2, actor.getHeight()));
            if (nearestPosVec.y < currentPosVec.y - MIN_POS_DIFFERENCE && currentPosVec.dst(nearestPosVec) < minDistance) {
                minDistance = currentPosVec.dst(nearestPosVec);
                nearest = actor;
            }
        }
    }

    @Override
    protected void unFocusAll() {
        for (Actor actor : actorsSnapshot) {
            if (actor instanceof H5EButton) {
                ((H5EButton) actor).setOver(false);
            }
        }
    }

    public void fetchActors(Group group) {
        for (int i = 0; i < group.getChildren().size; i++) {
            Actor actor = group.getChildren().get(i);
            if (!ignoredActors.contains(actor)) {
                if (isFocusable(actor)) {
                    actorsSnapshot.add(actor);
                } else {
                    tryToExtractActors(actor);
                }
            }
        }
    }

    public void tryToExtractActors(Actor actor) {
        if (actor instanceof H5EScrollablePane) {
            fetchActors(((H5EScrollablePane) actor));
        } else if (actor instanceof Group) {
            fetchActors((Group) actor);
        }
    }

    public void moveScrollbar() {
        moveScrollbar(currentActor, curScrollablePane);
    }

    public void moveScrollbar(Actor actor, H5EScrollablePane pane) {
        if (pane != null && actor != null) {
            actor.localToAscendantCoordinates(pane.getContent(), currentPosVec.set(0, actor.getHeight()));
            pane.setScrollY(pane.getContent().getHeight() - currentPosVec.y);
            pane.setScrollX(currentPosVec.x);
        }
    }

    public H5EScrollablePane isInsidePanel(Actor actor) {
        if (actor.getParent() instanceof H5EScrollablePane) {
            H5EScrollablePane parentOfParentScroll = isInsidePanel(actor.getParent());
            if(parentOfParentScroll!=null) return parentOfParentScroll;
            return (H5EScrollablePane) actor.getParent();
        } else if (actor.getParent() != null) {
            return isInsidePanel(actor.getParent());
        } else return null;
    }

    public int getGroupLength(Group group) {
        int count = 0;
        for (Actor actor : group.getChildren()) {
            if (isFocusable(actor)) {
                count++;
            } else if (actor instanceof Group) {
                count += getGroupLength((Group) actor);
            }
        }
        return count;
    }

    public Actor getLeftTopCornerActor() {
        Actor leftTopActor = null;
        float minDistance = Float.MAX_VALUE;
        Vector2 screenCoordinates = new Vector2(0, layer.getEngine().getHeight());
        for (Actor actor : actorsSnapshot) {
            actor.localToStageCoordinates(currentPosVec.set(0, actor.getHeight()));
            if (currentPosVec.dst(screenCoordinates) < minDistance) {
                minDistance = currentPosVec.dst(screenCoordinates);
                leftTopActor = actor;
            }
        }
        return leftTopActor;
    }
}
