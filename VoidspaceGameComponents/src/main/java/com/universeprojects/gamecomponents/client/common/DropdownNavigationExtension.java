package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EDropDown;

public class DropdownNavigationExtension extends NavigationExtension {

    private H5EDropDown dropDown;

    public DropdownNavigationExtension(UINavigation navigation) {
        super(navigation);
    }

    @Override
    public void enter() {
        navigation.exit();
        dropDown.hideList();
    }

    @Override
    public void exit() {
        active = false;
        dropDown.hideList();
    }

    @Override
    public void moveUp() {
        super.moveUp();
        if(dropDown.getSelectedIndex()>0) {
            dropDown.setSelectedIndex(dropDown.getSelectedIndex() - 1);
            dropDown.hideList();
            dropDown.showList();
        }
    }

    @Override
    public void moveDown() {
        super.moveDown();
        if(dropDown.getSelectedIndex() < dropDown.getItems().size-1) {
            dropDown.setSelectedIndex(dropDown.getSelectedIndex() + 1);
            dropDown.hideList();
            dropDown.showList();
        }
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof H5EDropDown;
    }

    @Override
    public void init(Actor actor) {
        dropDown = ((H5EDropDown)actor);
        dropDown.showList();
        active = true;
    }
}
