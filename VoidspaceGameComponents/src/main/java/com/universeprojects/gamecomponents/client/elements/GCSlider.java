package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class GCSlider extends Slider implements GraphicElement {

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public GCSlider(H5ELayer layer) {
        super(0, 100, 1, false, layer.getEngine().getSkin());
    }

    private H5EInputBox inputBox;

    public void setRange(float min, float max, float step) {
        if (min >= max) {
            throw new IllegalArgumentException("minimum value must be less than maximum value (min=" + min + ", max=" + max + ")");
        }
        if (step <= 0) {
            throw new IllegalArgumentException("step must be positive (step=" + step + ")");
        }
        if (step > max) {
            throw new IllegalArgumentException("step must be greater than the maximum value (max=" + max + ", step=" + step + ")");
        }
        setRange(min, max);
        setStepSize(step);
    }

    public void attachInputBox(H5EInputBox inputBox) {
        if (inputBox == null) {
            throw new IllegalArgumentException("input box reference can't be null");
        }
        if (this.inputBox != null) {
            throw new IllegalStateException("an input box is already attached");
        }

        this.inputBox = inputBox;
        inputBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                setValueInternal(parseFloat(inputBox.getText()), true);
                event.handle();
            }

            private float parseFloat(String val) {
                try {
                    return Float.parseFloat(val);
                } catch (Throwable ex) {
                    return 0F;
                }
            }
        });
    }

    public boolean setValue(float value) {
        setValueInternal(value, false);
        return true;
    }

    private void setValueInternal(float value, boolean skipInputBox) {
//        if (!isInRange(value)) {
//            throw new IllegalArgumentException("value must be within range [" + getMinValue() + ".." + getMaxValue() + "] Given: " + value);
//        }

        super.setValue(value);//Takes care of snapping

        if (inputBox != null && !skipInputBox) {
            setInputBoxValue(getValue());
        }
    }

    private boolean isInRange(float value) {
        return value >= getMinValue() && value <= getMaxValue();
    }

    private void setInputBoxValue(float value) {
        inputBox.setText(Integer.toString((int) value));
    }
}
