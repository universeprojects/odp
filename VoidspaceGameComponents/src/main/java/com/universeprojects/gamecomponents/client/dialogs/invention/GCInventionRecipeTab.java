package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.GCInventoryData.GCInventoryDataItem;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCRecipeData.GCRecipeSlotDataItem;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.List;

class GCInventionRecipeTab extends GCInventionTab {

    private final GCSlotItemSelector selector;
    private final H5EInputBox txtRepeats;
    private final H5ELabel noContentsLabel;
    private final Cell<?> repeatsCell;
    private final H5ELabel fixedRepeats;
    private final H5EButton btnStart;
    private final H5ELabel operationLabel;
    private final GCSkillSettingsWindow skillSettingsWindow;
    private final H5EScrollablePane optionsListContainer;
    private final H5EScrollablePane slotListContainer;
    private final Cell queueMobile;

    GCInventionRecipeTab(GCInventionSystemDialog dialog) {
        super(dialog, "");

        slotListContainer = new H5EScrollablePane(layer);
        optionsListContainer = new H5EScrollablePane(layer);
        queueMobile=leftContent.add().colspan(2).growX();
        leftContent.row();
        leftContent.add(slotListContainer).top().left().grow().colspan(2);
        rightContent.row();
        skillSettingsWindow = new GCSkillSettingsWindow(layer, this::onSkillSettingsDone);
        rightContent.row();
        rightContent.add(optionsListContainer).top().left().padLeft(5).grow();

        slotListContainer.getContent().top().left();
        optionsListContainer.getContent().top().left();



        selector = new GCSlotItemSelector(layer, slotListContainer, optionsListContainer) {
            @Override
            protected boolean onSlotItemSelected(GCRecipeSlotDataItem slot, GCInventoryDataItem selectedItem) {
                return false;
            }

            @Override
            protected boolean onSlotItemDeselected(GCRecipeSlotDataItem slot, GCInventoryDataItem deselectedItem) {
                return false; // no data reload
            }

            @Override
            protected void onSlotInfoClicked(Actor actor, GCInventoryDataItem item) {
                dialog.onItemIconInfoClicked(actor, item);
            }
        };

        leftContent.row().padBottom(5);

        filterCell.clearActor().colspan(2);
        final H5ELabel txtRepeatsLabel = leftContent.add(new H5ELabel(layer)).padLeft(10).fill().getActor();
        txtRepeatsLabel.setText("Repeats: ");

        repeatsCell = leftContent.add().fillY().growX();
        fixedRepeats = new H5ELabel(layer);
        fixedRepeats.setAlignment(Align.center);

        txtRepeats = new H5EInputBox(layer);
        txtRepeats.setTypeNumber();
        txtRepeats.setAlignment(Align.right);
        txtRepeats.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                txtRepeats.selectAll();
            }
        });
        resetRepeats();

       btnStart = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Start").withTutorialId("button:invention-start-recipe").build();
        btnStart.addButtonListener(() -> {
            if(!GCInventionRecipeTab.this.isOpen()){
                return;
            }
            boolean valid = dialog.validateRecipeSlotsFilled(selector.getDataDirect());
            if(!valid) {
                return;
            }
            if(!selector.getDataDirect().getData().needsConfiguration()) {
                onSkillSettingsDone();
            }else{
                skillSettingsWindow.setData(selector.getDataDirect());
                skillSettingsWindow.open();
            }
        });
        btnStart.setDisabled(true);
        lowerButtonRow.add(btnStart).growY().width(Value.percentWidth(1f, lowerButtonRow));
        final H5EButton btnBack = ButtonBuilder.inLayer(layer).withStyle("button2").withText("Cancel").build();
        btnBack.addButtonListener(dialog::onQueueCancelPressed);
        //lowerButtonRow.add(btnBack).growY().width(Value.percentWidth(1/3f, lowerButtonRow));
        noContentsLabel = new H5ELabel("No material or tool required to design this concept.", layer);
        noContentsLabel.setTouchable(Touchable.disabled);
        Container<H5ELabel> container = new Container<>(noContentsLabel);
        dialog.mainStack.add(container);
        noContentsLabel.setVisible(false);
        operationLabel = new H5ELabel("", layer);
        operationLabel.setColor(Color.valueOf("#AAAAAA"));

        UPUtils.tieButtonToKey(txtRepeats, Input.Keys.ENTER, btnStart);
    }

    /**
     * Reset the repeats input box
     */
    public void resetRepeats () {
        txtRepeats.setText("1");
        repeatsCell.setActor(txtRepeats).padRight(10);
    }

    public void setFixedRepeats(String value) {
        fixedRepeats.setText(value);
        repeatsCell.setActor(fixedRepeats);
    }

    @Override
    protected void clearTab() {
        selector.clear();
        btnStart.setDisabled(true);
        if(noContentsLabel != null) {
            noContentsLabel.setVisible(false);
        }
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);

        if (!visible) {
            if(noContentsLabel != null) {
                noContentsLabel.setVisible(false);
            }
        }
    }

    public void clearAndOpen(){
        clearTab();
        operationLabel.setText("Select recipe");
        titleLabel.setText("");
        noContentsLabel.setVisible(false);
    }

    void initRecipeData(GCRecipeData recipeData) {
        boolean hasSlots = recipeData.size() > 0;
        String title = "";
        switch(recipeData.getMode()){
            case ITEM_IDEA:
                if(recipeData.getSkillId()!=null){
                    title = "Upgrading skill";
                }else{
                    title = "Prototyping idea";
                }
                break;
            case STRUCTURE_SKILL:
            case ITEM_SKILL:
                title = "Executing skill";
                break;
            case STRUCTURE_IDEA:
                title = "Designing idea";
                break;
        }
        operationLabel.setText(title);
        titleLabel.setText(recipeData.getName());
        titleLabel.getLabel().setFontScale(1f);
        noContentsLabel.setVisible(!hasSlots);
        selector.init(recipeData, false, null);

        skillSettingsWindow.setData(selector.getDataDirect());
        btnStart.setDisabled(false);
    }

    @Override
    void show() {
        dialog.lowerButtonRow.add(lowerButtonRow);
        dialog.leftContent.add(leftContent);
        dialog.rightContent.add(rightContent);
        dialog.rightTitleArea.add(rightTitleArea);
        setVisible(true);
        rightTitleArea.clear();
        rightTitleArea.add(operationLabel).padLeft(5).growX();
        rightTitleArea.row();
        rightTitleArea.add(titleLabel).padLeft(5).growX();
        dialog.setTitleAreaHeight(64);
    }

    void initRecipeData(GCRecipeData recipeData, boolean isFirst) {
        initRecipeData(recipeData);
        if(recipeData.getData().needsConfiguration()) {
            btnStart.setText("Next");
        }else{
            if(isFirst){
                btnStart.setText("Start");
            }else{
                btnStart.setText("Update");
            }
        }
    }

    public void onSkillSettingsDone(){
        boolean valid = dialog.validateUserFieldValues(selector.getDataDirect());
        if(!valid) {
            return;
        }
        skillSettingsWindow.close();
        int repeats = 1;
        try {
            repeats = Integer.parseInt(txtRepeats.getText());
            Tutorials.trigger("process:start:recipe");
        } catch (NumberFormatException e) {
            //Ignore
        }
        int maxRepeats = 30;
        if (selector.getDataDirect().getSkillId() != null) maxRepeats = 1000;
        if (selector.getDataDirect().getMode() == GCRecipeData.RecipeMode.ITEM_IDEA) {
            if (repeats > maxRepeats) {
                repeats = maxRepeats;
                txtRepeats.setText(maxRepeats + "");
            }
        }
        dialog.onRecipeOkayBtnPressed(selector.getDataDirect(), repeats);
    }

    void updateRecipeData(GCRecipeData recipeData) {
        titleLabel.setText(recipeData.getName());
        if(recipeData.size() == 0) {
            if(noContentsLabel != null) {
                noContentsLabel.setVisible(true);
            }
        }
        else {
            if(noContentsLabel != null) {
                noContentsLabel.setVisible(false);
            }
        }
        selector.update(recipeData);
        skillSettingsWindow.setData(recipeData);
        btnStart.setDisabled(false);
    }

    @SuppressWarnings("unused")
    public void openSkillSettingsWindow(){
        skillSettingsWindow.open();
    }

    @Override
    public void windowStateChanged(GCInventionSystemDialog.WindowState windowState) {
        super.windowStateChanged(windowState);
        leftContent.setBackground(rightTitleArea.getBackground());
        rightContent.setBackground(rightTitleArea.getBackground());
        selector.setColumnsCount(1);
        queueMobile.clearActor();
        queueMobile.height(0);
        slotListContainer.setStyle(layer.getEngine().getSkin().get("default", ScrollPane.ScrollPaneStyle.class));
        optionsListContainer.setStyle(layer.getEngine().getSkin().get("default", ScrollPane.ScrollPaneStyle.class));
        skillSettingsWindow.setHeight(500);
        skillSettingsWindow.positionProportionally(0.5f,0.5f);
        switch (windowState){
            case DESKTOP:
                break;
            case PORTRAIT:
                slotListContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
                optionsListContainer.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueTopAndBottomBorderOpaque);
                selector.setColumnsCount(2);
                queueMobile.setActor(dialog.queuePane);
                queueMobile.height(64);
                break;
            case LANDSCAPE:
                skillSettingsWindow.setHeight(400);
                skillSettingsWindow.positionProportionally(0.5f,0.5f);
                break;
        }
    }
}
