package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.common.shared.util.VoidspaceNumberFormat;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.common.KeyUINavigation;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.elements.GCSlider;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gefcommon.shared.elements.RecipeValidator;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfigurationType;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EProgressBar;
import com.universeprojects.vsdata.shared.DistributedFieldConfigurationData;
import com.universeprojects.vsdata.shared.UserFieldDistributionConfigurationData;
import com.universeprojects.vsdata.shared.UserFieldUpgradeConfigurationData;
import com.universeprojects.vsdata.shared.UserFieldValueConfigurationData;

public class GCSkillSettingsWindow extends GCSimpleWindow {

    private final ProgressBar.ProgressBarStyle progressBarStyleGood = StyleFactory.INSTANCE.progressBarHorizontalDefault;
    private final ProgressBar.ProgressBarStyle progressBarStyleBad = StyleFactory.INSTANCE.progressBarHorizontalRed;
    private final H5ELayer layer;
    private final H5EScrollablePane userFieldValueListContainer;
    private final H5EScrollablePane userDistributionContainer;
    private final H5EScrollablePane upgradeFieldsContainer;
    private final Table userFieldValueListTable;
    private final Table userDistributionListTable;
    private final Table upgradeFieldsTable;
    private final H5EButton submitBtn;
    private final H5EButton cancelBtn;
    private Cell<?> userFieldValueListCell;
    private Cell<?> userDistributionCell;
    private Cell<?> upgradeCell;

    public GCSkillSettingsWindow(H5ELayer layer, Callable0Args onSubmit) {
        super(layer, "skillsettings", "Skill settings", 500, 400, false, "popup-window");
        positionProportionally(0.5f, 0.5f);
        setModal(true);
        disableCloseButton();
        //setFullscreenOnMobile(true);
        this.layer = layer;

        userFieldValueListContainer = new H5EScrollablePane(layer);
        userFieldValueListTable = userFieldValueListContainer.getContent();
        userFieldValueListTable.defaults().left().top();
        userFieldValueListTable.padLeft(10);
        userFieldValueListTable.padBottom(10);
        userFieldValueListTable.background(StyleFactory.INSTANCE.panelInternal12);

        userDistributionContainer = new H5EScrollablePane(layer);
        userDistributionListTable = userDistributionContainer.getContent();
        userDistributionListTable.defaults().left().top();
        userDistributionListTable.padLeft(10);
        userDistributionListTable.padBottom(10);
        userDistributionListTable.background(StyleFactory.INSTANCE.panelInternal12);

        upgradeFieldsContainer = new H5EScrollablePane(layer);
        upgradeFieldsTable = upgradeFieldsContainer.getContent();
        upgradeFieldsTable.defaults().left().top();
        upgradeFieldsTable.padLeft(10).padBottom(10).background(StyleFactory.INSTANCE.panelInternal12);

        submitBtn = new H5EButton("Start", layer);
        submitBtn.addButtonListener(onSubmit);
        cancelBtn = new H5EButton("Cancel", layer);
        cancelBtn.addButtonListener(this::close);
        navigation = new KeyUINavigation(layer);
        navigation.setWindow(this, this);
    }

    public void setData(GCRecipeData recipeData) {
        userFieldValueListTable.clearChildren();
        clear();
        if (!recipeData.getData().getUserFieldValueConfigurationsSafe().isEmpty() && recipeData.getSkillId()==null) {
            userFieldValueListCell = add(userFieldValueListContainer).padTop(20).grow().colspan(2);
            row();
        }
        if (!recipeData.getData().getUserFieldDistributionConfigurationsSafe().isEmpty() && recipeData.getSkillId()==null) {
            userDistributionCell = add(userDistributionContainer).padTop(20).grow().colspan(2);
            row();
        }
        for (UserFieldValueConfigurationData valueData : recipeData.getData().getUserFieldValueConfigurationsSafe()) {
            userFieldValueListTable.row();
            H5ELabel nameLabel = new H5ELabel(valueData.description, layer);
            if (valueData.isRequiredSafe()) {
                nameLabel.getText().append(" *");
                nameLabel.setColor(Color.RED);
            }
            userFieldValueListTable.add(nameLabel).left().top();
            if (valueData.type == UserFieldValueConfigurationType.STRING) {

                TextField textField = new TextField(Strings.nullToEmpty(recipeData.getUserFieldValueData().get(valueData.targetFieldName)), layer.getEngine().getSkin());
                textField.setText(Strings.nullToEmpty(recipeData.getUserFieldValueData().get(valueData.targetFieldName)));
                textField.setMaxLength(Dev.withDefault(valueData.maxLength, 0));
                userFieldValueListTable.add(textField).padLeft(10f).left().top();
                userFieldValueListTable.row();

                H5ELabel errorLabel = new H5ELabel(valueData.formattingErrorMessage, layer);
                errorLabel.setWrap(true);
                errorLabel.setColor(Color.RED);
                errorLabel.setVisible(false);
                userFieldValueListTable.add(errorLabel).colspan(2);
                textField.setTextFieldListener((field, c) -> {
                    final String text = field.getText();
                    final boolean valid = RecipeValidator.INSTANCE.validateUserFieldValue(valueData, text);
                    recipeData.addUserFieldValueConfig(valueData.targetFieldName, text);
                    errorLabel.setVisible(!valid);
                });
            }
        }
        if(recipeData.getSkillId() != null){
            submitBtn.setDisabled(false);
            upgradeCell = add(upgradeFieldsContainer).padTop(20).grow().colspan(2);
            row();
            upgradeFieldsTable.clear();
            upgradeFieldsTable.left().top();
            upgradeFieldsTable.add(new H5ELabel("Select which properties should be upgraded:", layer)).colspan(2).growX().top().padBottom(15);
            for(UserFieldUpgradeConfigurationData upgradeConfigurationData:recipeData.getData().getUserFieldUpgradeConfigurationsSafe()){
                upgradeFieldsTable.row();
                H5ELabel nameLabel = new H5ELabel(upgradeConfigurationData.getName(), layer);
                nameLabel.setColor(Color.valueOf("#CCCCCC"));
                H5EButton checkbox = ButtonBuilder.inLayer(layer).withStyle(StyleFactory.INSTANCE.buttonStyleCheckbox).build();
                upgradeFieldsTable.add(checkbox).left().top();
                nameLabel.setAlignment(Align.left);
                upgradeFieldsTable.add(nameLabel).growX().padLeft(5).left().top();
                checkbox.addButtonListener(()->{
                    if(checkbox.isChecked()) {
                        recipeData.addUpgradeFieldConfig(upgradeConfigurationData.fieldName);
                    }else{
                        recipeData.removeFieldConfig(upgradeConfigurationData.fieldName);
                    }
                });
                checkbox.setChecked(true);
                recipeData.addUpgradeFieldConfig(upgradeConfigurationData.fieldName);
            }
            upgradeFieldsTable.row().growY();
        }
        userFieldValueListTable.setVisible(userFieldValueListTable.hasChildren());
        userFieldValueListTable.row();
        userFieldValueListTable.add().grow();


        userDistributionListTable.clearChildren();
        if(recipeData.getSkillId()==null) {
            for (UserFieldDistributionConfigurationData distributionConfig : recipeData.getData().getUserFieldDistributionConfigurationsSafe()) {
                H5ELabel distributionNameLabel = new H5ELabel(distributionConfig.getName(), layer);
                userDistributionListTable.row();
                userDistributionListTable.add(distributionNameLabel).colspan(2).left().top();
                H5EProgressBar progressBar = new H5EProgressBar(layer);
                progressBar.setRange(0, distributionConfig.getTotalPoints().floatValue());
                progressBar.setDisplayMode(H5EProgressBar.DisplayMode.LABEL_CUSTOM);
                userDistributionListTable.row();
                userDistributionListTable.add(progressBar).colspan(2).left().top().growX();
                double factor = UPMath.cap(distributionConfig.getTotalPoints() / distributionConfig.getTotalPointsPossible(), 0d, 1d);
                double totalValue = 0d;
                for (DistributedFieldConfigurationData fieldConfig : distributionConfig.getFieldConfigurationsSafe()) {
                    userDistributionListTable.row();

                    H5ELabel fieldNameLabel = new H5ELabel(fieldConfig.getDisplayName(), layer);
                    userDistributionListTable.add(fieldNameLabel).left().top();

                    H5ELabel expectedValueLabel = new H5ELabel("", layer);
                    expectedValueLabel.setColor(0.5f, 0.5f, 0.5f, 1);
                    userDistributionListTable.add(expectedValueLabel).growX().padLeft(5);

                    userDistributionListTable.row();

                    GCSlider slider = new GCSlider(layer);
                    slider.setRange(0, fieldConfig.maxPoints.floatValue(), 1);
                    userDistributionListTable.add(slider).growX();

                    H5EInputBox inputBox = new H5EInputBox(layer);
                    inputBox.setWidth(40);
                    inputBox.setTypeNumber();
                    inputBox.setText("0");
                    userDistributionListTable.add(inputBox).fillX();
                    slider.attachInputBox(inputBox);

//                userDistributionListTable.row();


                    double startValue = fieldConfig.getMaxPoints() * factor;
                    if (totalValue + startValue > distributionConfig.getTotalPoints()) {
                        startValue = UPMath.capMin(totalValue - distributionConfig.getTotalPoints(), 0);
                    }
                    totalValue += startValue;
                    recipeData.addUserFieldDistributionData(fieldConfig.fieldName, startValue);
                    slider.addListener(new ChangeListener() {
                        @Override
                        public void changed(ChangeEvent event, Actor actor) {
                            setDistributionValue(recipeData, distributionConfig, progressBar, fieldConfig, slider, expectedValueLabel);
                        }
                    });
                    slider.setValue((float) startValue);
                }

                setProgressBarValue(progressBar, distributionConfig.getTotalPoints(), totalValue);
            }
        }
        userDistributionListTable.setVisible(userDistributionListTable.hasChildren());
        userDistributionListTable.row();
        userDistributionListTable.add().grow();
        add(submitBtn).growX().left().top();
        add(cancelBtn).growX().left().top();
    }

    private void setDistributionValue(GCRecipeData recipeData, UserFieldDistributionConfigurationData distributionConfig, H5EProgressBar progressBar, DistributedFieldConfigurationData fieldConfig, GCSlider slider, H5ELabel expectedValueLabel) {
        double value = UPMath.cap(Math.round(slider.getValue()), 0, fieldConfig.getMaxPoints());
        double totalOtherValues = 0d;
        for (DistributedFieldConfigurationData config : distributionConfig.getFieldConfigurationsSafe()) {
            if(config == fieldConfig) {
                continue;
            }
            final double fieldValue = Dev.withDefault(recipeData.getUserFieldDistributionData().get(config.fieldName), 0d);
            totalOtherValues += fieldValue;
        }

        recipeData.addUserFieldDistributionData(fieldConfig.fieldName, value);
        double minValue = fieldConfig.baseMinValue;
        double maxValue = fieldConfig.baseMaxValue;
        double factor = value / fieldConfig.getMaxPoints();
        double multiplier = fieldConfig.multiplierMinValue + (fieldConfig.multiplierMaxValue - fieldConfig.multiplierMinValue) * factor;
        minValue *= multiplier;
        maxValue *= multiplier;
        VoidspaceNumberFormat.FormattedValue maxValueFormat = VoidspaceNumberFormat.formatValue(UPMath.round(maxValue));
        expectedValueLabel.setText(VoidspaceNumberFormat.formatSame(UPMath.round(minValue), maxValueFormat).numberWithPrefix()+" - "+VoidspaceNumberFormat.formatSame(UPMath.round(maxValue), maxValueFormat).numberWithPrefix());

        final double currentTotal = totalOtherValues + value;
        setProgressBarValue(progressBar, currentTotal, distributionConfig.getTotalPoints());
    }

    private void setProgressBarValue(H5EProgressBar progressBar, double currentTotal, double totalPoints) {
        progressBar.setValue((float) currentTotal);
        if((long)currentTotal <= (long)totalPoints) {
            progressBar.setStyle(progressBarStyleGood);
            submitBtn.setDisabled(false);
        } else{
            progressBar.setStyle(progressBarStyleBad);
            submitBtn.setDisabled(true);
        }
        progressBar.updateProgressText((long) currentTotal +"/"+ (long)totalPoints);
    }


}
