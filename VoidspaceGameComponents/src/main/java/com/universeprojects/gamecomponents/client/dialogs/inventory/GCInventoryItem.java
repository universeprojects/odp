package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.List;
import java.util.Map;

public interface GCInventoryItem {
    String getUid();

    Object getRawEntity();

    String getName();

    String getItemClass();

    String getDescription();

    long getQuantity();

    boolean isStackable();

    String getIconSpriteKey();

    String getRarityIconStyle();

    String getRarityText();

    Color getRarityColor();

    Map<String, InspectorParameter> getAdditionalProperties();

    boolean createAdditionalInspectorComponents(H5ELayer layer, Table inspectorTable, List<GCInventory<?>> inventories);

    GCInternalItemInventory getInternalInventory();

    GCInternalTank getInternalTank();

    String getIconOverlayKey();

    boolean canBeExperimentedOn();

    boolean canBeCustomized();
}
