package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.util.NumberFormat;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.CreditItemData;
import com.universeprojects.gamecomponents.client.elements.CreditItemPriceData;
import com.universeprojects.gamecomponents.client.elements.GCCreditStoreItem;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

public abstract class GCCreditStoreComponent<L extends LoginInfo> extends Table {

    protected Table mainArea;
    protected Table paymentOptionsTable;
    private Table buyTable;
    private Table totalTable;
    private H5EScrollablePane scrollArea;
    private Cell<H5EScrollablePane> scrollAreaCell;

    private H5EButton buyBtn;
    private H5ELabel chargesLabel;
    private H5ELabel totalLabel;

    protected Integer storeFrontPrice; //The product price as listed on the actual store (playstore, appstore, etc.)

    protected String paymentMethod;

    protected GCCreditStoreItem.GCCreditStoreItemHandler handler;

    protected CreditItemData selectedItem;

    protected List<CreditItemData> creditItemDataList = new ArrayList<>();

    protected GCList<GCCreditStoreItem> itemList;

    protected L loginInfo;

    protected H5ELayer layer;


    public GCCreditStoreComponent(H5ELayer layer, L loginInfo) {
        this.loginInfo = loginInfo;
        this.layer = layer;

        mainArea = add(new Table()).grow().getActor();

        paymentOptionsTable = mainArea.add(new Table()).growX().fillX().colspan(2).pad(5).padTop(10).padBottom(10).center().getActor();
        mainArea.row();

        scrollAreaCell = mainArea.add(new H5EScrollablePane(layer, "scrollpane-backing-4")).colspan(2).top().center().height(250).growX().padBottom(5);
        scrollArea = scrollAreaCell.getActor();
        scrollArea.setScrollingDisabled(false, true);
        mainArea.row();

        totalTable = mainArea.add(new Table()).bottom().left().align(Align.left).pad(5).padRight(10).getActor();
        chargesLabel = new H5ELabel("", layer);
        chargesLabel.setFontScale(0.7f);
        totalTable.add(chargesLabel).left().align(Align.left).top();
        totalTable.row();
        H5ELabel disclaimerLabel = new H5ELabel("*Additional taxes may apply", layer);
        disclaimerLabel.setFontScale(0.7f);
        H5ELabel approx = new H5ELabel("*Prices approximate", layer);
        approx.setFontScale(0.7f);
        totalTable.add(disclaimerLabel).left().align(Align.left);
        totalTable.row();
        totalTable.add(approx).left().align(Align.left);
        totalLabel = new H5ELabel("Total: ", layer);
        totalTable.row();
        totalTable.add(totalLabel).left().bottom().align(Align.left);


        buyTable = mainArea.add(new Table()).bottom().pad(5).padLeft(10).right().getActor();
        buyBtn = ButtonBuilder.inLayer(layer).withDefaultStyle().withText("Buy").build();
        buyBtn.addButtonListener(this::buyItem);
        buyTable.add(buyBtn).width(120).right().align(Align.right);

        handler = new GCCreditStoreItem.GCCreditStoreItemHandler();
        itemList = new GCList<>(layer, 1, 5, 5, true);

    }

    protected abstract void buyItem();

    protected abstract void fetchCreditData();

    protected void updatePaymentMethod(String method) {
        paymentMethod = method;
        updateList();
        updateTotal();
    }

    private void updateList() {
        float storeIconHeight = mainArea.getHeight()*0.50f;
        if (storeIconHeight>400) storeIconHeight = 400f;
        scrollAreaCell.height(storeIconHeight+45);
        itemList.clear();
        Long selectedId = selectedItem != null ? selectedItem.id : null;
        GCCreditStoreItem itemToSelect = null;
        for (CreditItemData data : creditItemDataList) {
            if(!data.processorPriceData.containsKey(paymentMethod)) {
                continue;
            }
            GCCreditStoreItem item = new GCCreditStoreItem(layer, handler, data, this);
            item.setPixelIconPixelHeight((int)storeIconHeight);
            item.setPriceForPaymentMethod(paymentMethod);
            itemList.addItem(item);
            if(selectedId != null && selectedId == data.id) {
                itemToSelect = item;
            }
        }
        if(itemToSelect != null) {
            handler.setSelection(itemToSelect);
        } else {
            selectedItem = null;
        }
    }

    public void updateTotal() {
        if (paymentMethod == null || selectedItem == null) {
            return;
        }
        CreditItemPriceData priceData = selectedItem.processorPriceData.get(paymentMethod);
        long price = priceData.price;
        long fee = priceData.fees;
        NumberFormat format = NumberFormat.getDefaultInstance();
        String formatted = format.decimalFormat((price + fee) / 100f, 2);

        totalLabel.setText("Total: ~$" + formatted + " USD");
        chargesLabel.setText("*Payment method fees: $" + format.decimalFormat(fee / 100f, 2));
    }

    public void updatedSelected(CreditItemData selected, GCCreditStoreItem listItem) {
        if (selected == null || listItem == null) {
            return;
        }
        selectedItem = selected;
        if (!handler.isItemSelected() || handler.getSelectedItem() != listItem) {
            handler.setSelection(listItem);
        }
        updateTotal();
    }

    protected void displayCreditOptions() {
        handler.reset();
        itemList.clear();
        itemList.remove();
        itemList = null;
        itemList = new GCList<>(layer, creditItemDataList.size(), 5, 5, true);
        itemList.top().left();
        scrollArea.getContent().clear();
        scrollArea.add(itemList).top().left();

        updateList();
    }
}
