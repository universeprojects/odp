package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.H5EStack;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

public abstract class GCSchematicViewer extends GCWindow {
    private final static Color DEFAULT_CLOSED_COLOR = new Color(9 / 255F, 13 / 255F, 42 / 255F, 0.8F);

    private final H5ELayer layer;
    private final GCEquipmentDialog<?, ?> equipmentDialog;
    private final GCActionBar actionBar;
    private EquipmentType currentType = EquipmentType.SHIP;
    private EquipmentType auxType = null;

    private boolean expanded;
    private boolean isConsoleOpen;
    protected final H5EIcon mainIcon;
    private final H5EIcon auxIcon;
    private final Table equipmentTable;


    // Origin of ship icon relative to bottom right corner of screen
    private float SHIP_ICON_XPOS = 85f;
    private float SHIP_ICON_YPOS = 190f;
    private float SHIP_ICON_XPOS_MOBILE = 45f;
    private float SHIP_ICON_YPOS_MOBILE = 190f;
    private final float SHIP_ICON_XPOS_DESKTOP = 85f;
    private final float SHIP_ICON_YPOS_DESKTOP = 50f;


    private float spriteWidth;
    private float spriteHeight;

    // Movement variables
    private boolean iconVisible = true;
    private float ICON_SCALE = 0.2f;
    private final float ICON_SCALE_DESKTOPMODE = 0.2f;
    private final float ICON_SCALE_MOBILEMODE = 0.2f;
    private final float FULL_SCALE_DESKTOPMODE = 0.7f;
    private final float FULL_SCALE_MOBILEMODE = 1f;
    private float FULL_SCALE = 0.7f;//1.0
    private boolean acting = false;
    private boolean showAux = false;
    private final Vector2 pos = new Vector2();
    private final Vector2 target = new Vector2();
    private float dist = 0.0f;
    private float scale;
    private float scaleLast;
    private float scaleTarget;
    private final Color color = new Color();
    private final Color colorLast = new Color();
    private final Color colorTarget = new Color();

    // Positions
    private float schematicPosX = 0.7f;
    private float schematicPosY = 0.5f;
    private float inventoryPosX = 0.3f;
    private float inventoryPosY = 0.5f;


    public GCSchematicViewer(H5ELayer layer, GCEquipmentDialog<?, ?> equipmentDialog, GCActionBar actionBar) {
        super(layer,1000,1000,"clear-window");
        setCloseButtonEnabled(false);
        setCloseable(false);
        setBackground(new BaseDrawable());
        setClickThrough(true);
        setTouchable(Touchable.childrenOnly);


        this.layer = layer;
        this.equipmentDialog = equipmentDialog;
        this.actionBar = actionBar;

        String spriteKey = "ships/harbor/harbor-g7-class1a-schematic-backdrop";
        final H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteKey);
        mainIcon = new H5EIcon(layer, spriteType);
//        sprite.setColor(0.2f, 0.8f, 1, 0.7f);
        mainIcon.setUserObject("button:equipment"); // TODO: Replace the name
        mainIcon.setScale(ICON_SCALE);
        Tutorials.registerObject("button:equipment", mainIcon);

        spriteWidth = spriteType.getWidth();
        spriteHeight = spriteType.getHeight();

        updateSize();
        setPackOnOpen(false);


        H5EStack stack = new H5EStack();
        add(stack).grow();

        Table windowTable = new Table();
        windowTable.setFillParent(true);
        stack.add(windowTable);
        windowTable.add(mainIcon).expand().fill().center().align(Align.center);

        expanded = false;

        equipmentTable = new Table();
//        equipmentTable.debug();
        stack.add(equipmentTable);
        equipmentTable.add(equipmentDialog).fill();
        equipmentDialog.setVisible(false);

        auxIcon = new H5EIcon(layer, (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteKey));
        auxIcon.setScaling(Scaling.fit);
        auxIcon.setColor(DEFAULT_CLOSED_COLOR);
        auxIcon.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                openAux();
            }
        });

        addActor(auxIcon);//.maxHeight(120F).width(120).left().top().padLeft(-400);
        auxIcon.setSize(120,120);
        auxIcon.setPosition(0, spriteHeight, Align.right);
        auxIcon.setVisible(false);

        invalidateHierarchy();

        // Listener for sprite button
        mainIcon.addListener(new InputListener() {

            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                if (!expanded && !acting) {
                    openSchematicViewerDialog();
                    return true;
                }
                return false;
            }
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (!expanded) {
                    mainIcon.setOrigin(Align.center);
                    mainIcon.setScale(ICON_SCALE * 1.1f);
                }
            }
            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (!expanded) mainIcon.setScale(ICON_SCALE);
            }

        });

        pos.x = 1.0f - SHIP_ICON_XPOS/layer.getEngine().getWidth();
        pos.y = SHIP_ICON_YPOS/layer.getEngine().getHeight();
        scale = scaleTarget = scaleLast = ICON_SCALE;

        color.set(DEFAULT_CLOSED_COLOR);
        colorTarget.set(color);
        colorLast.set(color);

        mainIcon.setColor(color);
        Tutorials.registerCheckerObject("window:equipment", this);


        // Setting positions for the start
        updatePosition();
        open();
        this.layer.getEngine().onResize.registerHandler(()->{
            updatePosition();
            Gdx.app.postRunnable(()->mainIcon.setOrigin(Align.center));
        });
        Gdx.app.postRunnable(()-> {
            mainIcon.setOrigin(Align.center);
            mainIcon.setScale(ICON_SCALE);
        });
    }

    protected abstract void setSchematicViewerType(EquipmentType auxType);

    protected abstract void openSchematicViewerDialog();

    protected abstract void onClosed();

    @Override
    public void open() {
        super.open();

        mainIcon.setScale(ICON_SCALE);
    }

    private void updateSize() {
        setWidth(spriteWidth * FULL_SCALE);
        setHeight(spriteHeight * FULL_SCALE);
    }

    public boolean isViewerTypeShip() {
        return currentType == EquipmentType.SHIP;
    }

    public boolean isViewerTypeSuit() {
        return currentType == EquipmentType.SUIT;
    }

    public void setAuxEquipment(EquipmentType type, H5ESpriteType spriteType) {
        if (this.auxType != type) {
            if (this.auxType != null) {
                Tutorials.removeObject(this.auxType == EquipmentType.SHIP ? "button:equipment:ship" : "button:equipment:suit");
            }
            this.auxType = type;
            if (this.auxType != null) {
                Tutorials.registerObject(this.auxType == EquipmentType.SHIP ? "button:equipment:ship" : "button:equipment:suit", auxIcon);
            }
        }

        if (spriteType == null) {
            auxIcon.setDrawable(null);
            auxIcon.setVisible(false);
        } else {
            auxIcon.setDrawable(new TextureRegionDrawable(spriteType.getGraphicData()));
        }

    }

    public void toggle() {
        if (acting) {
            // We ignore commands when the Schematics Dialog
            // is doing something already
            return;
        }
        acting = true;
        expanded = !expanded;
        if (expanded) {
            // Open
            target.x = schematicPosX;
            target.y = schematicPosY;
            scaleTarget = FULL_SCALE;
            colorTarget.set(1, 1, 1, 1);
            auxIcon.setVisible(true);
            open();
        } else {
            // Close
            target.x = 1.0f - SHIP_ICON_XPOS / layer.getEngine().getWidth();
            target.y = SHIP_ICON_YPOS / layer.getEngine().getHeight();
            scaleTarget = ICON_SCALE;
            colorTarget.set(DEFAULT_CLOSED_COLOR);
            if (equipmentDialog.getInspector() != null) equipmentDialog.getInspector().close();
            equipmentDialog.setVisible(false);
            auxIcon.setVisible(false);
            onClosed();
        }
        mainIcon.setOrigin(Align.center);
        scaleLast = scale;
        colorLast.set(color);
        dist = new Vector2(pos.x - target.x, pos.y - target.y).len();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void act(float delta) {
        super.act(delta);
        if (acting) {
            if (iconVisible) {
                float spd = delta * 60.0f;
                pos.lerp(target, 0.2f * spd);
                pos.x = approach(pos.x, 0.001f * spd, target.x);
                pos.y = approach(pos.y, 0.001f * spd, target.y);
                positionProportionally(pos.x, pos.y);

                float ratio = new Vector2(pos.x - target.x, pos.y - target.y).len() / dist;
                scale = scaleTarget + ratio * (scaleLast - scaleTarget);
                mainIcon.setScale(scale);
                color.a = colorTarget.a + ratio * (colorLast.a - colorTarget.a);
                color.r = colorTarget.r + ratio * (colorLast.r - colorTarget.r);
                color.g = colorTarget.g + ratio * (colorLast.g - colorTarget.g);
                color.b = colorTarget.b + ratio * (colorLast.b - colorTarget.b);
                mainIcon.setColor(color);

                if (ratio == 0) {
                    // Done animation
                    acting = false;
                    if (!expanded) {
                        if(showAux){
                            showAux = false;
                            toggle();
                        }
                        // Do nothing
                    } else {
                        equipmentDialog.setVisible(true);
                    }
                    scaleLast = scaleTarget;

                }
            } else {
                // Icon is invisible, just go to end position
                positionProportionally(target.x, target.y);
                pos.x = target.x;
                pos.y = target.y;
                scale = scaleTarget;
                color.set(colorTarget);

                mainIcon.setScale(scale);
                mainIcon.setColor(color);
//                sprite.setColor(0.2f, 0.8f, 1, alpha);

                acting = false;
                if (!expanded) {
                    if(showAux){
                        showAux = false;
                        toggle();
                    }
                    // Do nothing
                } else {
                    equipmentDialog.setVisible(true);
                }
            }
            H5ELayer.invalidateBufferForActor(this);
        }
    }

    private float approach(float val, float amt, float lim) {
        if (val == lim) return lim;
        if (lim > val) return Math.min(lim,val+amt);
        return Math.max(lim,val-amt);
    }

    @Override
    public void onClose() {
        if (expanded) toggle();
    }

    public boolean isExpanded() {
        return expanded;
    }

    public boolean isActing() {
        return acting;
    }

    public void updateAuxIconPosition(){
        if (actionBar.getMode() == GCActionBar.ActionBarMode.MOBILE) {
            auxIcon.setPosition(getWidth(), getHeight()/2f, Align.left);
        }
        else {
            auxIcon.setPosition(0, getHeight(), Align.right);
        }
    }


    public void updatePosition() {
        int width = layer.getEngine().getWidth();
        int height = layer.getEngine().getHeight();
        updateAuxIconPosition();
        if (actionBar.getMode() == GCActionBar.ActionBarMode.MOBILE) {
            if (isConsoleOpen) {
                if(height>width) {
                    SHIP_ICON_YPOS_MOBILE = actionBar.getY()+200f;
                }else{
                    SHIP_ICON_YPOS_MOBILE = 190f;
                }
            } else {
                if(height>width) {
                    SHIP_ICON_YPOS_MOBILE = actionBar.getY()+200f;
                }else{
                    SHIP_ICON_YPOS_MOBILE = 90f;
                }
            }
            SHIP_ICON_XPOS = SHIP_ICON_XPOS_MOBILE;
            SHIP_ICON_YPOS = SHIP_ICON_YPOS_MOBILE;
        }
        else {
            SHIP_ICON_XPOS = SHIP_ICON_XPOS_DESKTOP;
            SHIP_ICON_YPOS = SHIP_ICON_YPOS_DESKTOP;
        }
        iconVisible = true;
        // Scale and positioning
        if (width > height) {
            FULL_SCALE = UPMath.min(1f, height / spriteHeight * FULL_SCALE_DESKTOPMODE);
            ICON_SCALE = ICON_SCALE_DESKTOPMODE;

            schematicPosX = 0.7f;
            schematicPosY = 0.5f;
            inventoryPosX = 0.3f;
            inventoryPosY = 0.5f;
        } else {
            FULL_SCALE = UPMath.min(1f, height / 2f / spriteHeight * FULL_SCALE_MOBILEMODE);
            ICON_SCALE = ICON_SCALE_MOBILEMODE;

            schematicPosX = 0.5f;
            schematicPosY = 0.7f;
            inventoryPosX = 0.5f;
            inventoryPosY = 0.3f;
        }
        // Dialog size
        if (expanded && !acting) {
            mainIcon.setOrigin(Align.center);
            pos.x = schematicPosX;
            pos.y = schematicPosY;
            positionProportionally(pos.x, pos.y);
            mainIcon.setScale(FULL_SCALE);
        }
        else if (!expanded && !acting) {
            mainIcon.setScale(ICON_SCALE);
            pos.x = 1.0f - SHIP_ICON_XPOS/layer.getEngine().getWidth();
            pos.y = SHIP_ICON_YPOS/layer.getEngine().getHeight();
            positionProportionally(pos.x, pos.y);
        }
        equipmentTable.clearChildren();
        equipmentTable.add(equipmentDialog).fill();
        updateSize();
    }

    public float getInventoryPosX() {
        return inventoryPosX;
    }

    public float getInventoryPosY() {
        return inventoryPosY;
    }

    public void setEquipmentSpriteType(SpriteType spriteType) {
        mainIcon.setDrawable(new TextureRegionDrawable(((H5ESpriteType)spriteType).getTextureRegion()));
        mainIcon.setOrigin(Align.center);
        spriteWidth = spriteType.getWidth();
        spriteHeight = spriteType.getHeight();
        updateSize();
        invalidateHierarchy();
        updatePosition();
        if (!isExpanded()) {
            open();
        }
    }

    public boolean findItem(String itemID) {
        boolean[] result = {false};
        equipmentDialog.forEachItem(i -> {
            if(i.getName().equals(itemID)) {
                result[0] = true;
            }
        });

        return result[0];
    }

    public H5EIcon getSprite() {
        return mainIcon;
    }

    public void consoleOpened(boolean isOpen){
        isConsoleOpen = isOpen;
        if(!expanded) {
            updatePosition();
//            pos.x = 1.0f - SHIP_ICON_XPOS/layer.getEngine().getWidth();
//            pos.y = SHIP_ICON_YPOS/layer.getEngine().getHeight();
//            positionProportionally(pos.x, pos.y);
        }
    }

    public void openAux(){
        if(auxType == null) {
            return;
        }
        showAux = true;
        currentType = auxType;
        setSchematicViewerType(auxType);
    }
}
