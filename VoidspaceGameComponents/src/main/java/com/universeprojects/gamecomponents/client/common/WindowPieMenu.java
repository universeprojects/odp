package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WindowPieMenu extends WidgetGroup {

    private static final Vector2 SLOT_POSITION = new Vector2(173, 136f);
    private static final Vector2 SLOT_DIAGONAL_POSITION = new Vector2(152, 113f);
    private static final float TOLERANCE = 0.7f;

    private final Map<Integer, Callable0Args> slotsMap;
    private final Map<Integer, Drawable> bannersMap;
    private final Map<Integer, String> bannerTexts;
    private final H5EIcon banner;
    private final H5ELabel middleText;
    private final List<H5EIcon> slots;
    private final H5ELayer layer;
    private final Timer timer;
    private final Timer.Task unselect = new Timer.Task() {
        @Override
        public void run() {
            middleText.setText("Please make selection with Left Stick");
            banner.setVisible(false);
            if (selectedSlot >= 0) {
                slots.get(selectedSlot).setColor(Color.LIGHT_GRAY);
                slots.get(selectedSlot).getColor().a = 0.6f;
                selectedSlot = -1;
            }
        }
    };

    private int selectedSlot = -1;
    private final Vector2 tempVec2D;
    private float stickX;
    private float stickY;

    public WindowPieMenu(H5ELayer layer) {
        super();
        this.layer = layer;
        slotsMap = new HashMap<>();
        bannersMap = new HashMap<>();
        bannerTexts = new HashMap<>();
        slots = new ArrayList<>();
        timer = new Timer();
        setPosition(layer.getEngine().getWidth() / 2f, layer.getEngine().getHeight() / 2f);
        tempVec2D = new Vector2(0, 0);
        layer.addActorToTop(this);
        this.setVisible(false);
        for (int i = 1; i <= 8; i++) {
            H5EIcon slot = H5EIcon.fromSpriteType(layer, "gamepad/radial-menu-slot-backing1.png");
            slot.setOrigin(Align.center);
            switch (i) {
                case 1:
                    tempVec2D.set(0, SLOT_POSITION.y);
                    break;
                case 2:
                    tempVec2D.set(SLOT_DIAGONAL_POSITION.x, SLOT_DIAGONAL_POSITION.y);
                    break;
                case 3:
                    tempVec2D.set(SLOT_POSITION.x, 0);
                    break;
                case 4:
                    tempVec2D.set(SLOT_DIAGONAL_POSITION.x, -SLOT_DIAGONAL_POSITION.y);
                    break;
                case 5:
                    tempVec2D.set(0, -SLOT_POSITION.y);
                    break;
                case 6:
                    tempVec2D.set(-SLOT_DIAGONAL_POSITION.x, -SLOT_DIAGONAL_POSITION.y);
                    break;
                case 7:
                    tempVec2D.set(-SLOT_POSITION.x, 0);
                    break;
                case 8:
                    tempVec2D.set(-SLOT_DIAGONAL_POSITION.x, SLOT_DIAGONAL_POSITION.y);
                    break;
            }
            slot.setPosition(tempVec2D.x, tempVec2D.y, Align.center);
            slot.setColor(Color.LIGHT_GRAY);
            slot.getColor().a = 0.6f;
            addActor(slot);
            slots.add(slot);
        }
        H5EIcon middle = H5EIcon.fromSpriteType(layer, "gamepad/radial-menu-middle1.png");
        middle.setOrigin(Align.center);
        middle.setPosition(0, 0, Align.center);
        middle.getColor().a = 0.6f;
        addActor(middle);
        banner = H5EIcon.fromSpriteType(layer, "gamepad/radial-menu-banner-invention1.png");
        banner.setOrigin(Align.topLeft);
        //TODO: make banner fit in middle without scaling
        banner.setPosition(middle.getX(Align.topLeft) + 17.5f, middle.getY(Align.topLeft) - 17f, Align.topLeft);
        banner.setScaling(Scaling.stretch);
        banner.setScale(0.9865f, 0.97f);
        banner.setVisible(false);
        addActor(banner);

        middleText = new H5ELabel("Please make selection with DPad", layer);
        middleText.setWrap(true);
        middleText.setOrigin(Align.topLeft);
        middleText.setAlignment(Align.topLeft);
        //TODO:set Position relatively
        middleText.setPosition(-120, -150, Align.topLeft);
        middleText.setWidth(250);
        middleText.setHeight(190);
        addActor(middleText);
    }


    public void registerAction(Callable0Args callback, String icon, String text, String banner) {
        for (int i = 0; i < 8; i++) {
            if (!slotsMap.containsKey(i)) {
                registerAction(callback, icon, text, banner, i);
                return;
            }
        }
    }

    /**
     * @param slot Slot id by clockwise(0-7)
     */
    public void registerAction(Callable0Args callback, String icon, String text, String banner, int slot) {
        registerAction(callback, icon, text, slot);
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(banner);
        bannersMap.put(slot, new TextureRegionDrawable(spriteType.getGraphicData()));
    }

    public void registerAction(Callable0Args callback, String icon, String text, int slot) {
        H5EIcon slotIcon= H5EIcon.fromSpriteType(layer, icon);
        slotIcon.setOrigin(Align.center);
        addActor(slotIcon);
        slotIcon.setSize(40,40);
        slotIcon.setPosition(slots.get(slot).getX(Align.center), slots.get(slot).getY(Align.center), Align.center);
        slotsMap.put(slot, callback);
        bannerTexts.put(slot, text);
    }

    public void show() {
        if(isVisible()) return;
        setVisible(true);
        toFront();
        selectedSlot = -1;
    }

    public void hide() {
        if(!isVisible()) return;
        setVisible(false);
        if (selectedSlot >= 0) {
            if (slotsMap.get(selectedSlot) != null)
                slotsMap.get(selectedSlot).call();
            unselect.run();
        }
    }


    public void setStickX(float stickX) {
        float delta = Math.abs(Math.abs(stickX) - Math.abs(this.stickX));
        if(delta>0.5f) {
            this.stickX = stickX;
            processStick();
        }

    }

    public void setStickY(float stickY) {
        float delta = Math.abs(Math.abs(stickY) - Math.abs(this.stickY));
        if(delta>0.5f) {
            this.stickY = stickY;
            processStick();
        }
    }

    public void processStick() {
        if (isVisible() && (Math.abs(stickX)>TOLERANCE || Math.abs(stickY)>TOLERANCE)) {
            banner.setVisible(false);
            unselect.run();
            timer.clear();
            timer.stop();
            tempVec2D.set(Math.round(stickX), Math.round(stickY));
            selectedSlot = (int) ((tempVec2D.angleDeg(Vector2.Y)) / 45f);
            if (bannersMap.get(selectedSlot) != null) {
                banner.setDrawable(bannersMap.get(selectedSlot));
                banner.setVisible(true);
            }
            middleText.setText(bannerTexts.get(selectedSlot));
            slots.get(selectedSlot).setColor(Color.WHITE);
        } else {
            timer.clear();
            timer.stop();
            timer.scheduleTask(unselect, 0.3f);
            timer.start();
        }
    }

}
