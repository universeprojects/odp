package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.json.shared.parser.JSONParserFactory;


/**
 * Creates the UI for players to create a new account.
 */
public class GCRegisterScreen<L extends LoginInfo> extends GCLoginInformation {

    private H5EInputBox fieldCharacterName;
    private H5EInputBox fieldPasswordConfirm;
    private H5ELabel characterWarningText;
    private H5ELabel passwordConfirmWarningText;

    private final GCLoginManager<L, ?, ?> loginManager;

    private final GCLoadingIndicator loadingIndicator;

    public GCRegisterScreen(H5ELayer layer, GCLoginManager<L, ?, ?> loginManager) {
        super(layer);
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        window.setId("register-screen");
        window.setTitle("Enter Login Information");
        window.setCloseButtonEnabled(false);
        window.setY(window.getY() - 50);
        //Create "character" text
        window.row().padTop(10);
        H5ELabel characterNameLabel = new H5ELabel(layer);
        characterNameLabel.setFontScale(loginInfoFontSize);
        characterNameLabel.setText("Character Name");
        window.add(characterNameLabel);

        //Space to enter character name
        window.row().padTop(5);
        fieldCharacterName = new H5EInputBox(layer);
        window.add(fieldCharacterName).width(LOGIN_FIELD_W).center().fillX();

        //Warning text for invalid character name
        window.row().padTop(5);
        characterWarningText = new H5ELabel(layer);
        characterWarningText.setWrap(true);
        characterWarningText.setFontScale(warningTextFontSize);
        characterWarningText.setColor(warningColor);
        characterWarningText.setAlignment(Align.center);
        characterWarningText.setText("Character name is in use.");
        characterWarningText.setVisible(false);
        window.add(characterWarningText);

        // "Email" text (from superclass);
        window.row().padTop(10);
        window.add(emailLabel);

        //Space to enter email
        window.row().padTop(5);
        window.add(fieldEmail).width(LOGIN_FIELD_W);

        //Warning text for invalid emails
        window.row().padTop(5);
        window.add(emailWarningText);

        //Password text
        window.row().padTop(5);
        window.add(passwordLabel);

        //Space to enter password
        window.row().padTop(5);
        window.add(fieldPassword).width(LOGIN_FIELD_W);

        //Warning text for invalid passwords
        window.row().padTop(5);
        window.add(passwordWarningText);

        //Password confirmation text
        window.row().padTop(5);
        H5ELabel passwordConfirmLabel = new H5ELabel(layer);
        passwordConfirmLabel.setFontScale(loginInfoFontSize);
        passwordConfirmLabel.setText("Confirm Password");
        window.add(passwordConfirmLabel);

        //Space to enter password confirmation
        window.row().padTop(5);
        fieldPasswordConfirm = new H5EInputBox(layer);
        fieldPasswordConfirm.setWidth(LOGIN_FIELD_W);
        fieldPasswordConfirm.setPasswordMode(true);
        window.add(fieldPasswordConfirm).width(LOGIN_FIELD_W);

        //Warning text for non matching passwords
        window.row().padTop(5);
        passwordConfirmWarningText = new H5ELabel(layer);
        passwordConfirmWarningText.setWrap(true);
        passwordConfirmWarningText.setFontScale(warningTextFontSize);
        passwordConfirmWarningText.setColor(warningColor);
        passwordConfirmWarningText.setAlignment(Align.center);
        passwordConfirmWarningText.setText("Passwords do not match.");
        passwordConfirmWarningText.setVisible(false);
        window.add(passwordConfirmWarningText);

        //Register Button
        window.row().padTop(5);
        H5EButton btnSignUp = new H5EButton("Sign Up", layer);
        btnSignUp.addButtonListener(this::signUp);
        btnSignUp.getStyle().checked = null;

        //Back to Login Button
        H5EButton btnBack = new H5EButton("Back", layer);
        btnBack.addButtonListener(() -> {
            close();
            loginManager.openLoginScreen();
        });
        btnBack.getStyle().checked = null;

        //Table container for signup and
        Table buttonTable = new Table();
        window.add(buttonTable).colspan(2).center();
        final Cell<H5EButton> btnSignUpCell = buttonTable.add(btnSignUp);
        final Cell<H5EButton> btnBackCell = buttonTable.add(btnBack);
        btnSignUpCell.padTop(10).width(150);
        btnBackCell.padTop(10).width(150);

        //Tie all input fields to enter key
        UPUtils.tieButtonToKey(fieldCharacterName, Input.Keys.ENTER, btnSignUp);
        UPUtils.tieButtonToKey(fieldEmail, Input.Keys.ENTER, btnSignUp);
        UPUtils.tieButtonToKey(fieldPassword, Input.Keys.ENTER, btnSignUp);
        UPUtils.tieButtonToKey(fieldPasswordConfirm, Input.Keys.ENTER, btnSignUp);

    }

    public String getCharacterName() {
        return fieldCharacterName.getText();
    }

    private boolean isValidCharacterName() {

        String characterName = fieldCharacterName.getText();
        if (characterName.isEmpty()) {
            characterWarningText.setText("Please enter your character name.");
            characterWarningText.setVisible(true);
            fieldCharacterName.setColor(warningColor);
            return false;
        }
        return true;
    }


    private boolean isSamePassword() {
        String password = fieldPassword.getText();
        String confirmPassword = fieldPasswordConfirm.getText();
        if (password.equals(confirmPassword)) {
            return true;
        }

        passwordConfirmWarningText.setVisible(true);
        fieldPasswordConfirm.setColor(warningColor);
        return false;
    }

    private void signUp() {
        characterWarningText.setVisible(false);
        emailWarningText.setVisible(false);
        passwordWarningText.setVisible(false);
        passwordConfirmWarningText.setVisible(false);
        fieldCharacterName.setColor(normalColor);
        fieldEmail.setColor(normalColor);
        fieldPassword.setColor(normalColor);
        fieldPasswordConfirm.setColor(normalColor);

        if (!isValidCharacterName()) {
            characterWarningText.setVisible(true);
            characterWarningText.setText("Invalid character name.");
            fieldCharacterName.setColor(warningColor);
            return;
        }
        if (!isValidEmail()) {
            emailWarningText.setVisible(true);
            emailWarningText.setText("Please enter a valid email.");
            fieldEmail.setColor(warningColor);
            return;
        }
        if (!isValidPassword()) {
            passwordWarningText.setVisible(true);
            passwordWarningText.setText("Invalid password.");
            fieldPassword.setColor(warningColor);
            return;
        }
        if (!isSamePassword()) {
            passwordConfirmWarningText.setVisible(true);
            passwordConfirmWarningText.setText("Passwords do not match");
            fieldPasswordConfirm.setColor(warningColor);
            return;
        }
        loadingIndicator.activate(window);
        loginManager.register(getEmail(), getPassword(), getCharacterName(), (loginInfo) -> {
            loadingIndicator.deactivate();
            close();
            loginManager.onSuccessfulLogin(loginInfo);
        }, (error) -> {
            loadingIndicator.deactivate();

            try {
                final Object parsed = JSONParserFactory.getParser().parse(error.getMessage());
                final JSONObject json = (JSONObject) parsed;

                if (json.get("status").equals("busy")) {
                    characterWarningText.setVisible(true);
                    characterWarningText.setText((String) json.get("Sorry the servers appear to be busy right now, please try again in a moment."));
                }

                if (!json.get("emailResult").equals("ok")) {
                    emailWarningText.setVisible(true);
                    emailWarningText.setText((String) json.get("emailResult"));
                    fieldEmail.setColor(warningColor);
                }

                if (!json.get("usernameResult").equals("ok")) {
                    characterWarningText.setVisible(true);
                    characterWarningText.setText((String) json.get("usernameResult"));
                    fieldCharacterName.setColor(warningColor);
                }

                if (!json.get("passwordResult").equals("ok")) {
                    passwordWarningText.setVisible(true);
                    passwordWarningText.setText((String) json.get("passwordResult"));
                    fieldPassword.setColor(warningColor);
                }
            } catch (Throwable ex) {
                characterWarningText.setVisible(true);
                characterWarningText.setText("Invalid server response.");
            }
        });
    }


}
