package com.universeprojects.gamecomponents.client.tutorial;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;

import java.util.List;

public interface TutorialManager {
    void addCompletedStage(String stageId);

    void requestCompletedStages(Callable1Args<Boolean> toggleCallback, Callable1Args<List<String>> stageCallback);

    void setTutorialToggleStatus(boolean status);

    void resetCompletedStages(Callable0Args callable);
}
