package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.universeprojects.gamecomponents.client.common.NavigationShortcut;
import com.universeprojects.gamecomponents.client.common.UILayerNavigation;
import com.universeprojects.gamecomponents.client.dialogs.Confirmable;
import com.universeprojects.gamecomponents.client.dialogs.GCConfirmationScreen;
import com.universeprojects.gamecomponents.client.elements.GCList;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLoadingIndicator;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to select their characters.
 */
public class GCCharacterSelectScreen<L extends LoginInfo, S extends ServerInfo, C extends CharacterInfo> implements Confirmable {

    private final H5EButton btnSelect;
    private final H5EButton btnDeleteCharacter;
    private final H5ELabel selectWarningText;
    private final GCWindow window;
    private final GCList<GCCharacter<C>> characterListing;
    private final H5ELayer layer;
    private final GCListItemActionHandler<GCCharacter<C>> actionHandler;
    private final GCLoadingIndicator loadingIndicator;
    private GCConfirmationScreen deleteConfirmationScreen;

    private final GCLoginManager<L, S, C> loginManager;

    private L loginInfo;
    private S serverInfo;

    private boolean selectIsActive = true;

    public GCCharacterSelectScreen(H5ELayer layer, GCLoginManager<L, S, C> loginManager) {
        window = new GCWindow(layer, 500, 550);
        this.layer = layer;
        this.loginManager = loginManager;
        loadingIndicator = new GCLoadingIndicator(layer);
        window.setId("character-select");
        window.setTitle("Character Select");
        window.setCloseButtonEnabled(false);
        window.setMovable(false);
        window.defaults().left().top();
        window.setNavigation(new UILayerNavigation(layer));
        window.getNavigation().addShortcut(NavigationShortcut.create(GCCharacter.class, null, NavigationShortcut.Trigger.Enter).setAction((item) -> {
            select((C)item.getCharacterInfo());
        }));
        window.getNavigation().addShortcut(NavigationShortcut.create(GCCharacter.class, null, NavigationShortcut.Trigger.Delete).setAction((item)->{
            item.setChecked(true);
            removeCharacter();
            window.getNavigation().update();
        }));

        H5EScrollablePane characterScrollPane = new H5EScrollablePane(window.getLayer(), "scrollpane-backing-1");
        characterScrollPane.setScrollingDisabled(true, false);

        characterListing = new GCList<>(layer, 1, 5, 5, true);

        Image characterEquipmentImage = new Image(layer.getEngine().getSkin(), "character-equipment");

        //window.add(sprite).expandY().fillY();
        window.add(characterEquipmentImage);
        characterScrollPane.add(characterListing).expand().fill().padTop(10);
        //characterScrollPane.add(characterListing).expandY().fillY().padTop(10);
        window.add(characterScrollPane).fill();
        characterScrollPane.layout();

        window.row();

        final H5EButton btnBack = new H5EButton("Back", layer);
        btnBack.addButtonListener(this::back);
        btnBack.getLabel().setFontScale(0.8f);
        window.add(btnBack).width(225);

        btnSelect = new H5EButton("Enter game", layer);
        btnSelect.addButtonListener(this::characterSelect);
        btnSelect.getLabel().setFontScale(0.8f);
        window.add(btnSelect).width(225);

        window.row();

        H5EButton btnCreateCharacter = new H5EButton("Create New Character", layer);
        btnCreateCharacter.addButtonListener(this::onCreateCharacterButtonClick);
        btnCreateCharacter.getLabel().setFontScale(0.8f);
        window.add(btnCreateCharacter).width(225);

        btnDeleteCharacter = new H5EButton("Delete Character", layer);
        btnDeleteCharacter.addButtonListener(this::openRemoveCharacterScreen);
        btnDeleteCharacter.getLabel().setFontScale(0.8f);
        window.add(btnDeleteCharacter).center().width(225);

        window.row();

        selectWarningText = new H5ELabel(layer);
        selectWarningText.setWrap(true);
        selectWarningText.setText("Please select a character");
        selectWarningText.setColor(com.badlogic.gdx.graphics.Color.RED);
        selectWarningText.setFontScale(0.8f);
        selectWarningText.setVisible(false);
        window.add(selectWarningText).center().colspan(2).width(450);

        window.positionProportionally(0.5f, 0.5f);

        this.actionHandler = new GCListItemActionHandler<GCCharacter<C>>() {
            @Override
            protected void onSelectionUpdate(GCCharacter<C> lastSelectedItem) {
                boolean anySelected = getSelectedItem() != null;
                btnSelect.setDisabled(!anySelected);
                btnDeleteCharacter.setDisabled(!anySelected);
            }

            @Override
            protected void onDoubleClick(GCCharacter<C> item) {
                select(item.getCharacterInfo());
            }
        };
        checkButtonDisabled();
        window.getNavigation().setWindow(window, window);
    }

    private void back() {
        close();
        loginManager.openServerSelectScreen(loginInfo);
    }

    public void checkButtonDisabled() {
        boolean anySelected = characterListing.getButtonGroup().getChecked() != null;
        btnSelect.setDisabled(!anySelected);
        btnDeleteCharacter.setDisabled(!anySelected);
    }

    public void openRemoveCharacterScreen() {
        selectWarningText.setVisible(false);
        if (characterListing.getButtonGroup().getCheckedIndex() == -1) {
            selectWarningText.setVisible(true);
        } else {
            if (deleteConfirmationScreen == null) {
                deleteConfirmationScreen = new GCConfirmationScreen(layer, this, "Are You Sure you want to delete this character?");
            }
            deleteConfirmationScreen.open();
            this.close();

        }
    }

    public void removeCharacter() {
        final GCCharacter<C> character = characterListing.getButtonGroup().getChecked();
        loadingIndicator.activate(window);
        loginManager.deleteCharacter(loginInfo, serverInfo, character.getCharacterInfo(), this::refreshGameCharacterList, this::showWarning);
    }

    private void select(C characterInfo) {
        if (selectIsActive) {
            selectIsActive = false;
            loginManager.joinWithExistingCharacter(loginInfo, serverInfo, characterInfo, this::close, this::showWarning);
        }
    }

    public void characterSelect() {
        selectWarningText.setVisible(false);
        if (characterListing.getButtonGroup().getCheckedIndex() == -1) {
            showWarning("Please select a character");
        } else {
            select(characterListing.getButtonGroup().getChecked().getCharacterInfo());
        }
    }

    private void showWarning(String text) {
        selectWarningText.setVisible(true);
        selectWarningText.setText(text);
        selectIsActive = true;
    }

    protected void onCreateCharacterButtonClick() {
        loginManager.openCharacterCreateScreen(loginInfo, serverInfo, null);
        this.close();
    }

    public void refreshGameCharacterList() {
        characterListing.clear();
        checkButtonDisabled();
        if (!loadingIndicator.isVisible()) {
            loadingIndicator.activate(window);
        }
        loginManager.loadGameCharacters(loginInfo, serverInfo, (characters) -> {
            loadingIndicator.deactivate();
            characterListing.clear();
            for (C charInfo : characters) {
                final GCCharacter<C> item = new GCCharacter<>(layer, actionHandler, charInfo);
                characterListing.addItem(item);
            }
            checkButtonDisabled();
        }, (error) -> {
            loadingIndicator.deactivate();
            showWarning(error);
        });
    }

    public void open(L loginInfo, S serverInfo) {
        this.loginInfo = loginInfo;
        this.serverInfo = serverInfo;
        refreshGameCharacterList();
        selectIsActive = true;
        window.open();
        window.positionProportionally(0.5f, 0.5f);
    }

    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    /**
     * Called if the user confirms that they would like to delete the selected character.
     */

    @Override
    public void confirmed() {
        if (deleteConfirmationScreen.isConfirmed()) {
            removeCharacter();
        }
        window.open();
    }
}
