package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;

public class GCSelectionIndicator extends Image {

    private boolean active = false;
    private Window window;
    private Actor curActor;
    private Vector2 tempVec2D;

    public GCSelectionIndicator(Window curWindow, H5ELayer layer) {
        super(getDrawable(layer));
        this.setVisible(false);
        this.setOrigin(Align.center);
        this.setTouchable(Touchable.disabled);
        this.window = curWindow;
        tempVec2D = new Vector2(0, 0);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (active && curActor != null && curActor.getStage() != null) {
            Vector2 pos = curActor.localToActorCoordinates(window, tempVec2D.set(0, 0));
            this.setRotation(0);
            this.setPosition(pos.x - 10, pos.y - 10);
            this.setSize(curActor.getWidth() + 20, curActor.getHeight() + 20);
            this.toFront();
        }
    }

    public void activate(Actor actor) {
        active = true;
        this.curActor = actor;
        Vector2 pos = curActor.localToActorCoordinates(window, tempVec2D.set(0, 0));
        this.setRotation(0);
        this.setPosition(pos.x - 10, pos.y - 10);
        this.setSize(curActor.getWidth() + 20, curActor.getHeight() + 20);
        window.addActor(this);
        this.setVisible(true);
    }

    public void deactivate() {
        active = false;
        this.setVisible(false);
    }

    public void setWindow(Window window){
        this.window.removeActor(this);
        this.window = window;
        if(isActive()){
            window.addActor(this);
        }
    }

    public static NinePatchDrawable getDrawable(H5ELayer layer) {
        H5ESprite sprite = new H5ESprite(layer, "images/GUI/ui2/gamepad_navigation_highlight1.9.png");
        return new NinePatchDrawable(new NinePatch(sprite.getSpriteType().getTextureRegion(), 20, 20, 20, 20));
    }

    public boolean isActive() {
        return active;
    }
}
