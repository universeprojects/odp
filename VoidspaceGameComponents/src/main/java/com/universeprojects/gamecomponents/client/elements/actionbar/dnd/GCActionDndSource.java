package com.universeprojects.gamecomponents.client.elements.actionbar.dnd;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCActionDndSource extends DragAndDrop.Source {
    private final H5ELayer layer;
    private GCOperation action;

    public GCActionDndSource(Actor actor, GCOperation action, H5ELayer layer) {
        super(actor);
        this.action = action;
        this.layer = layer;
    }

    @Override
    public DragAndDrop.Payload dragStart(InputEvent event, float x, float y, int pointer) {
        DragAndDrop.Payload payload =  action == null ? null : new GCActionBarDndPayload(layer, action);
        if(payload!=null)
            InventoryManager.getInstance(layer.getEngine()).getDragAndDrop().notifyTargetsDrag(this, payload);
        return payload;
    }

    @Override
    public void dragStop(InputEvent event, float x, float y, int pointer, DragAndDrop.Payload payload, DragAndDrop.Target target) {
        InventoryManager.getInstance(layer.getEngine()).getDragAndDrop().notifyTargetsDrop();
        super.dragStop(event, x, y, pointer, payload, target);
    }


    public void setAction(GCOperation action) {
        this.action = action;
    }
}
