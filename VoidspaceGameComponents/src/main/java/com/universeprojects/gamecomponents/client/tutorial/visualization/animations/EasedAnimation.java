package com.universeprojects.gamecomponents.client.tutorial.visualization.animations;

public abstract class EasedAnimation {

    /**
     * Quad In/Out animation easing function as defined on
     * <p>
     * http://www.gizma.com/easing/
     */
    public static float quadInOutEasing(float time, float delta, float duration) {
        time /= duration / 2;
        if (time < 1) return (delta / 2 * time * time);
        time--;
        return (-delta / 2 * (time * (time - 2) - 1));
    }

    public static float quadOutEasing(float time, float delta, float duration) {
        time /= duration;
        return -delta * time * (time - 2);
    }
}
