package com.universeprojects.gamecomponents.client.dialogs.quests;


import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.GameComponents;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItem;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the graphical component for representing the quests in the quest quick-view sidebar
 */
public class GCQuestSidebarListItem extends GCListItem {

    // Quest properties begin here -------------------------------------------------------------------------------------

    /**
     * The quest's name
     */
    private final String name;

    /**
     * The "done-ness" required to complete the quest
     */
    private final double requiredCompletion;

    /**
     * The quest's current "done-ness"
     */
    private double currentCompletion;

    // Quest properties end here ---------------------------------------------------------------------------------------

    // Graphical components begin here ---------------------------------------------------------------------------------

    /**
     * The layer for the graphical components
     */
    private final H5ELayer layer;

    // Graphical components end here -----------------------------------------------------------------------------------

    /**
     * A constructor for the graphical component
     * @param objective The graphical component to copy properties from
     */
    GCQuestSidebarListItem (H5ELayer layer, GCListItemActionHandler actionHandler, GCQuestObjective objective) {
        super(layer, actionHandler);
        setStyle(getSkin().get("gc-list-blank", ButtonStyle.class));
        this.layer = layer;

        this.name = objective.getName();
        this.requiredCompletion = objective.getRequiredCompletion();
        this.currentCompletion  = objective.getCurrentCompletion();

        initializeGraphics();
    }

    /**
     * Recreate the graphics specific to the quest quick view.
     */
    private void initializeGraphics() {
        H5EButton checkBox = ButtonBuilder.inLayer(layer).withStyle("default-checkable").build();
        checkBox.setChecked(currentCompletion >= requiredCompletion);
        add(checkBox).padLeft(4);

        //Create the name label
        H5ELabel nameLabel = add(new GCLabel(layer)).left().grow().padLeft(5).getActor();
        nameLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        nameLabel.setAlignment(Align.left);
        nameLabel.setText(name + " ");
//        nameLabel.setWidth(70);
        nameLabel.setWrap(true);

        //Create the text label for displaying the quest's completion
        H5ELabel progressText = add(new GCLabel(layer)).right().padLeft(5).padRight(5).height(20).getActor();
        progressText.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        progressText.setAlignment(Align.right);

        //Only show this as a fraction if the quests has more than one objective
        if (requiredCompletion > 1) {
            progressText.setText((int) currentCompletion + " / " + (int) requiredCompletion);
        } else {
            progressText.setText("");
        }

//        progressText.setFontScale(0.6f);
        progressText.setAlignment(Align.right);
    }

}
