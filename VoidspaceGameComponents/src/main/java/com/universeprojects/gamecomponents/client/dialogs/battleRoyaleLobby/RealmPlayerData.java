package com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby;

public class RealmPlayerData {
    public boolean isYou;
    public String name;
    public String title;
    public int rank;
    public RealmShipData ship;
    public int games;
    public int wins;
    public int kills;

    public RealmPlayerData(boolean isYou, String name, String title, int rank, RealmShipData ship, int games, int wins, int kills) {
        this.isYou = isYou;
        this.name = name;
        this.title = title;
        this.rank = rank;
        this.ship = ship;
        this.games = games;
        this.wins = wins;
        this.kills = kills;
    }

}