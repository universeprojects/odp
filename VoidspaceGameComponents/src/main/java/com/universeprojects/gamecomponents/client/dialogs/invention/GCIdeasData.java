package com.universeprojects.gamecomponents.client.dialogs.invention;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GCIdeasData {
    private boolean sorted = false;

    private final List<GCIdeaDataItem> items = new ArrayList<>();

    private GCIdeasData() {
    }

    public static GCIdeasData createNew() {
        return new GCIdeasData();
    }

    public GCIdeasData add(GCIdeasData other) {
        for (GCIdeaDataItem item : other.items) {
            // we call the add() method sequentially in order for new indices to be assigned
            add(item.id, item.name, item.description, item.iconKey, item.parentId, item.createdTime, item.durationSecs);
        }
        return this;
    }

    public void sort() {
        if (sorted)
            return;
        sorted = true;
        //noinspection Java8ListSort
        Collections.sort(items, new Comparator<GCIdeaDataItem>() {
            @Override
            public int compare(GCIdeaDataItem o1, GCIdeaDataItem o2) {
                return Long.compare(o2.createdTime, o1.createdTime);
            }
        });
    }

    public GCIdeasData add(Long id, String name, String description, String iconKey, Long parentId, Long createdTime, Long durationSecs) {
        sorted = false;
        int index = items.size();
        items.add(new GCIdeaDataItem(index, id, name, description, iconKey, parentId, createdTime, durationSecs));
        return this;
    }

    public GCIdeaDataItem get(int index) {
        return items.get(index);
    }

    public void remove(int index) {
        sorted = false;
        items.remove(index);
    }

    public int size() {
        return items.size();
    }

    public static class GCIdeaDataItem {
        final int index;

        public Long id;
        public String name;
        public final String description;
        public String iconKey;
        public Long parentId;
        public Long createdTime;
        public Long durationSecs;

        GCIdeaDataItem(int index, Long id, String name, String description, String iconKey, Long parentId, Long createdTime, Long durationSecs) {
            this.index = index;

            this.id = id;
            this.name = name;
            this.description = description;
            this.iconKey = iconKey;
            this.parentId = parentId;
            this.createdTime = createdTime;
            this.durationSecs = durationSecs;
        }

        @Override
        public String toString() {
            return new StringBuilder()
                .append("index: ").append(index).append(", ")
                .append("id: ").append(id).append(", ")
                .append("name: ").append(name).append(", ")
                .append("description: ").append(description).append(", ")
                .append("iconKey: ").append(iconKey).append(", ")
                .append("parentId: ").append(parentId).append(", ")
                .toString();

        }
    }

}


