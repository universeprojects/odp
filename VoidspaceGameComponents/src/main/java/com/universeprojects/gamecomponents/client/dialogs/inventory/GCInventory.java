package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.dialogs.GCStackSplitter;
import com.universeprojects.gamecomponents.client.elements.GCLevelGauge;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCWindowManager;
import com.universeprojects.gamecomponents.client.windows.WindowManager;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.shared.UPUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public abstract class GCInventory<T extends GCInventoryItem> extends GCInventoryBase<Integer, T, GCInventoryItemSlotComponent<Integer, T>> implements InventoryAux<T> {

    public static final int SCROLL_PANE_PADDING = 5;
    public static final int MOBILE_SCROLLSPACE=50;
    public static final Integer KEY_ANY_SLOT = -1;
    private final Map<Integer, T> items = new TreeMap<>();
    private final Map<Integer, T> items_view = Collections.unmodifiableMap(items);
    private final Table inventoryTable;
    private boolean updateColumns = true;
    private GCInventoryInspector<T> inspector;
    private GCStackSplitter stackSplitter;
    public GCLevelGauge volumeGauge;


    public GCInventory(H5ELayer layer, int count, Float currentVolume, Float maxVolume) {
        super(layer);
        left().top();
        defaults().left().top();
        for (int i = 0; i < count; i++) {
            items.put(i, null);
        }
        this.inventoryTable = new Table();
        inventoryTable.left().top();
        inventoryTable.defaults().left().top();
        //final ScrollPane scrollPane = new ScrollPane(inventoryTable, getEngine().getSkin());
        final H5EScrollablePane scrollPane = new H5EScrollablePane(layer);
        scrollPane.add(inventoryTable).grow();
        scrollPane.setFadeScrollBars(false);
        scrollPane.setOverscroll(false, false);
        add(scrollPane).grow().pad(SCROLL_PANE_PADDING);
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if(event.getTarget() == GCInventory.this || event.getTarget() == inventoryTable || event.getTarget() == scrollPane) {
                    select(null);
                }
            }
        });

        volumeGauge = new GCLevelGauge(layer);
        if (currentVolume!=null && maxVolume!=null) {
            volumeGauge.setValue(currentVolume, maxVolume);
            volumeGauge.setLabel("Inventory Volume (" + UPUtils.ccToHumanReadable(maxVolume - currentVolume) + " free)");
        }
        else {
            volumeGauge.hide();
        }

        row();
        add(volumeGauge).growX();

    }

    @Override
    public Table left () {
        super.left();

        if (inventoryTable != null) {
            inventoryTable.left();
            inventoryTable.defaults().left();
        }

        return this;
    }

    @Override
    public Table right () {
        super.right();

        if (inventoryTable != null) {
            inventoryTable.right();
            inventoryTable.defaults().right();
        }

        return this;
    }

    @Override
    public Table top () {
        super.top();

        if (inventoryTable != null) {
            inventoryTable.top();
            inventoryTable.defaults().top();
        }

        return this;
    }

    @Override
    public Table bottom () {
        super.bottom();

        if (inventoryTable != null) {
            inventoryTable.bottom();
            inventoryTable.defaults().bottom();
        }

        return this;
    }

    public void forEachItem(Callable1Args<T> callable) {
        if(Gdx.app.getType() == Application.ApplicationType.iOS) {
            for(T item : items.values()) {
                if(item != null) {
                    callable.call(item);
                }
            }
        }
        else {
            for(T item : items.values()) {
                if(item != null) {
                    callable.call(item);
                }
            }
        }
    }

    public Map<Integer, T> getItems() {
        return items_view;
    }

    public void addItem(Integer slot, T item) {
        items.put(slot, item);
        invalidateItems();
    }

    public void clearItems() {
        for (Map.Entry<Integer, T> entry : items.entrySet()) {
            if (entry.getValue() != null) {
                Tutorials.removeObject("inventory:item:" + entry.getValue().getName());
            }
            entry.setValue(null);
        }
        invalidateItems();
    }

    public void removeItem(Integer slot) {
        if(items.get(slot) != null) {
            Tutorials.removeObject("inventory:item:" + items.get(slot).getName());
        }
        items.put(slot, null);
        invalidateItems();
    }

    @Override
    protected void sizeChanged() {
        updateColumns = true;
        super.sizeChanged();
    }

    @Override
    public void clearComponents() {
        super.clearComponents();
        if(inspector != null) {
            inspector.close();
        }
    }

    @Override
    public void layout() {
        updateComponents();
        updateItems();
        if(updateColumns) {
            inventoryTable.clearChildren();
            final float width = getWidth() - (SCROLL_PANE_PADDING*2+2)-(WindowManager.getInstance(getEngine()).getMobileMode() ? MOBILE_SCROLLSPACE:0);
            float curWidth = 0;
            for(GCInventoryItemSlotComponent<Integer, T> component : getComponents().values()) {
                final float componentWidth = component.getPrefWidth();
                curWidth += componentWidth;
                if(curWidth >= width) {
                    curWidth = componentWidth;
                    inventoryTable.row();
                }
                inventoryTable.add(component).pad(getPadding());
            }
            updateColumns = false;
        }
        super.layout();
    }

    private float getPadding() {
        return 0;
    }

    @Override
    protected GCInventoryItemSlotComponent<Integer, T> createComponent(Integer key) {
        return new GCInventoryItemSlotComponent<>(this, key, getSlotConfig(key));
    }

    protected abstract SlotConfig getSlotConfig(Integer key);

    @Override
    protected Collection<Integer> getSlots() {
        return items.keySet();
    }

    @Override
    protected T getItem(Integer key) {
        if(key == null) {
            return null;
        }
        return items.get(key);
    }

    @Override
    protected void onSelected(GCInventoryItemSlotComponent<Integer, T> component) {
        if(inspector == null) {
            if(component == null) {
                return;
            } else {
                inspector = createInspector();
            }
        }
        if(component == null) {
            inspector.close();
        } else {
            inspector.setSlotComponent(component);
            inspector.open(true);
            Tutorials.trigger("window:inspect");
        }
    }

    protected void onSplitStack(Integer key, T item) {
        if(stackSplitter != null) {
            stackSplitter.close();
        }
        stackSplitter = new GCStackSplitter(getLayer(), item.getIconSpriteKey(), item.getName(), (int) item.getQuantity()) {
            @Override
            public void onSubmit(int amountToSplit) {
                onStackSplit(key, item, amountToSplit);
            }

            @Override
            public void onClose() {
                this.destroy();
                stackSplitter = null;
            }
        };
        stackSplitter.open();
    }

    protected abstract void onStackSplit(Integer key, T item, int amountToSplit);

    protected abstract void onMergeAll(T item);

    /**
     * Create and return an inspector window
     */
    protected GCInventoryInspector<T> createInspector() {
        H5ELayer layer = getLayer();
        if (layer == null)
            throw new IllegalStateException("Attempted to open inspector for GCInventory without layer disconnected from the stage");

        return new GCInventoryInspector<>(layer);
    }

    public abstract void update();

    protected GCInventoryInspector getInspector () {
        return inspector;
    }

    public void setInspector(GCInventoryInspector<T> inspector) {
        if (inspector == null && this.inspector != null) {
            if (!this.inspector.isOpen()) {
                this.inspector.close();
            }
            this.inspector.remove();
        }
        this.inspector = inspector;
    }
}
