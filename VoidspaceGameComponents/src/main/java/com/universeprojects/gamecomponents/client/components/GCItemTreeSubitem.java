package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.gamecomponents.client.common.ButtonBuilder;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

// A clickable leaf of an item tree
public class GCItemTreeSubitem extends Table {

    protected final GCItemInternalTree itemTree;
    protected final H5EButton itemButton;

    public GCItemTreeSubitem(H5ELayer layer, GCItemInternalTree itemTree, String name, Callable0Args callable) {
        this.itemTree = itemTree;

        itemButton = ButtonBuilder.inLayer(layer).withStyle("button-tree-leaf").build();
        H5ELabel itemLabel = itemButton.add(new H5ELabel(name, layer)).padTop(-2).spaceLeft(3).left().growX().getActor();
        itemLabel.setColor(Color.valueOf("#89cdfe"));
        itemLabel.setStyle(layer.getEngine().getSkin().get("label-electrolize-small", Label.LabelStyle.class));
        add(itemButton).padBottom(1).padTop(1);
        itemButton.addCheckedButtonListener(() -> {
            if (itemTree.selection != null) {
                itemTree.selection.itemButton.setChecked(false);
                itemTree.selection.itemButton.setTouchable(Touchable.enabled);
            }
            itemTree.selection = this;
            itemButton.setTouchable(Touchable.disabled);

            callable.call();
        });
    }

    public void click() {
        itemButton.setChecked(true);
    }
}
