package com.universeprojects.gamecomponents.client.dialogs.login;


import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCLabel;
import com.universeprojects.gamecomponents.client.elements.GCListItemActionHandler;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

/**
 * Creates the UI for players to login.
 */
public class GCAdditionalGameMode<T extends ServerInfo> extends GCServerSelectorItem {

    private final AdditionalMode mode;

    public GCAdditionalGameMode(H5ELayer layer, GCListItemActionHandler<GCServerSelectorItem> actionHandler, AdditionalMode gameMode) {
        super(layer, actionHandler);

        this.mode = gameMode;


        final H5ELabel nameLabel = new GCLabel(layer);
        nameLabel.setText(mode.getName());
        nameLabel.setAlignment(Align.left);

        add(nameLabel).left().growX().padLeft(10);

        center();
    }

    public AdditionalMode getMode() {
        return mode;
    }

    @Override
    public String toString() {
        return mode.getName();
    }

    public enum AdditionalMode {
        BATTLE_ROYALE("Battle Royale (disabled)"),
        SINGLE_PLAYER("Single Player Graphics Test Arena");

        private final String name;

        AdditionalMode(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}





