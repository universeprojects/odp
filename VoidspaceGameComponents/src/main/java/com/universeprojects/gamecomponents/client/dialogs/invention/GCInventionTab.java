package com.universeprojects.gamecomponents.client.dialogs.invention;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollLabel;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

abstract class GCInventionTab {

    protected final GCInventionSystemDialog dialog;
    protected final H5ELayer layer;

    protected final Table lowerButtonRow;
    protected final Table leftContent;
    protected final Table rightContent;
    protected final Table rightTitleArea;
    protected final H5EScrollLabel titleLabel;
    protected final H5EInputBox filter;
    protected final Cell filterCell;
    protected String filterText="";

    protected GCInventionTab(GCInventionSystemDialog dialog, String title) {
        this.dialog = dialog;
        this.layer = dialog.getLayer();

        lowerButtonRow = new Table();
        lowerButtonRow.defaults().left().top();
        lowerButtonRow.left().top();

        filter=new H5EInputBox(layer);
        filter.setMessageText("Filter");
        filter.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent changeEvent, Actor actor) {
                onFilter();
                changeEvent.handle();
            }
        });
        leftContent = new Table();
        leftContent.defaults().left().top();
        leftContent.left().top();
        leftContent.setFillParent(true);
        filterCell=leftContent.add(filter).growX();
        leftContent.row();

        rightContent = new Table();
        rightContent.defaults().left().top();
        rightContent.left().top();

        rightTitleArea = new Table();
        rightTitleArea.defaults().left().top();
        rightTitleArea.left().top();

        titleLabel = new H5EScrollLabel(title, layer);
        rightTitleArea.add(titleLabel).growX().padLeft(5);
        titleLabel.getLabel().setColor(Color.WHITE);
        titleLabel.getLabel().setFontScale(1.2f);
        titleLabel.setAutoScroll(true);

        hide(); // start hidden
    }

    void show() {
        dialog.lowerButtonRow.add(lowerButtonRow);
        dialog.leftContent.add(leftContent);
        dialog.rightContent.add(rightContent);
        dialog.rightTitleArea.add(rightTitleArea);
        rightTitleArea.clear();
        rightTitleArea.add(titleLabel).growX().padLeft(5);
        titleLabel.getLabel().setFontScale(1.2f);
        dialog.setTitleAreaHeight(32);
        setVisible(true);
    }

    void hide() {
        dialog.lowerButtonRow.removeActor(lowerButtonRow);
        dialog.leftContent.removeActor(leftContent);
        dialog.rightContent.removeActor(rightContent);
        dialog.rightTitleArea.removeActor(rightTitleArea);
        setVisible(false);
    }

    public void setVisible(boolean visible) {
        leftContent.setVisible(visible);
        rightTitleArea.setVisible(visible);
        rightContent.setVisible(visible);
        lowerButtonRow.setVisible(visible);
    }

    void deactivate() {
        clearTab();
        hide();
    }

    public boolean isOpen(){
        return rightContent.isVisible();
    }

    abstract protected void clearTab();

    protected void onFilter(){
        filterText=filter.getText();
    }

    public void removeById(long id) {

    }

    public void windowStateChanged(GCInventionSystemDialog.WindowState windowState){
        switch (windowState){
            case DESKTOP:
                rightContent.setBackground(rightTitleArea.getBackground());
                leftContent.setBackground(rightTitleArea.getBackground());
                break;
            case PORTRAIT:
                dialog.mainTable.getCell(dialog.leftContent).height(layer.getHeight()*0.25f).fill(true, false).expand(true, false);
                dialog.mainTable.getCell(dialog.rightContent).grow();
                rightContent.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
                leftContent.setBackground(StyleFactory.INSTANCE.panelStyleBlueTopAndBottomBorderOpaque);
                break;
            case LANDSCAPE:
                rightContent.setBackground(rightTitleArea.getBackground());
                leftContent.setBackground(rightTitleArea.getBackground());
                break;
        }
    }
    //Click button when a key is pressed

}
