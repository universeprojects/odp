package com.universeprojects.gamecomponents.client.dialogs;


import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCEnergyNodeListItem.GCEnergyNodeItemActionHandler;
import com.universeprojects.gamecomponents.client.elements.GCLevelGauge;
import com.universeprojects.gamecomponents.client.elements.GCSwitch;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.Map;

public abstract class GCEnergyNodeControlDialog {

    private final GCSimpleWindow window;
    private final H5ELabel systemName;
    private final GCLevelGauge energyLevel;
    private final GCSwitch switchOnOff;

    private final GCEnergyNodeList sourcesList;
    private final GCEnergyNodeList consumersList;

    private final int CONTENT_W = 375;

    private final float ENERGY_LEVEL_VISUAL_STEP = .1f;

    public GCEnergyNodeControlDialog(H5ELayer layer) {
        window = new GCSimpleWindow(layer, "energy-node-control", "Energy System Control");
        window.setPackOnOpen(true);
        window.onClose.registerHandler(this::onDismiss);

        systemName = window.addH1("Energy node name").getActor();

        window.addEmptyLine(10);
        energyLevel = window.add(new GCLevelGauge(layer)).colspan(2).growX().getActor();
        energyLevel.setLabel("Energy level");
        energyLevel.setRange(100, ENERGY_LEVEL_VISUAL_STEP);
        energyLevel.setValue(0);

        window.addEmptyLine(15);
        window.addH2("Switch system on/off:").getActor();
        switchOnOff = window.add(new GCSwitch(layer)).right().getActor();
        switchOnOff.setChecked(false);
        switchOnOff.addButtonListener(() -> onSwitchOnOff(switchOnOff.isChecked()));

        window.addEmptyLine(5);
        final H5ELabel sourcesLabel = window.add(new H5ELabel("Power sources:", layer)).top().getActor();
        sourcesList = new GCEnergyNodeList(layer, CONTENT_W, actionHandler);
        sourcesList.attachLabel(sourcesLabel);
        window.add(sourcesList);

        window.addEmptyLine(5);
        final H5ELabel consumersLabel = window.add(new H5ELabel("Power output:", layer)).top().getActor();
        consumersList = new GCEnergyNodeList(layer, CONTENT_W, actionHandler);
        consumersList.attachLabel(consumersLabel);
        window.add(consumersList);
    }

    /**
     * Because there's only one action handler, the user can't select more than one item
     */
    final GCEnergyNodeItemActionHandler actionHandler = new GCEnergyNodeItemActionHandler() {
        @Override
        void handleActionBtn(GCEnergyNodeListItem item) {
            onOpenBtn(item.getUid());
        }
    };

    public void open() {
        window.open();
    }

    public void close() {
        window.close();
    }

    public void destroy() {
        window.destroy();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void activate() {
        window.activate();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }

    public void setName(String name) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("Energy system name can't be blank");
        }
        systemName.setText(name);
        window.pack();
    }

    public void setCapacity(float capacity) {
        if (capacity < 0) {
            throw new IllegalArgumentException("Capacity can't be negative");
        }
        capacity = Math.round(capacity * 10) / 10f; //Make sure the capacity is compatible with 0.1 steps
        energyLevel.setRange(capacity, ENERGY_LEVEL_VISUAL_STEP);
    }

    public void disableEnergyLevelGauge() {
        energyLevel.hide();
    }

    public void enableEnergyLevelGauge() {
        energyLevel.show();
    }

    public void setEnergyLevel(float level) {
        energyLevel.setValue(level);
    }


    public void setEnabled(boolean state) {
        switchOnOff.setChecked(state);
    }

    public void setEnabledWithoutEvent(boolean state) {
        switchOnOff.setProgrammaticChangeEvents(false);
        switchOnOff.setChecked(state);
        switchOnOff.setProgrammaticChangeEvents(true);
    }

    public void setSources(Map<String, String> sources) {
        sourcesList.update(sources);
        window.pack();
    }

    public void setConsumers(Map<String, String> consumers) {
        consumersList.update(consumers);
        window.pack();
    }

    /**
     * To be implemented by child class.
     * Called when the dialog is dismissed by user interaction.
     */
    protected abstract void onDismiss();

    /**
     * To be implemented by child class.
     * Called when the on/off switch changes its state.
     */
    protected abstract void onSwitchOnOff(boolean newState);

    /**
     * To be implemented by child class.
     * Called when the "open" button is clicked on a list item.
     */
    protected abstract void onOpenBtn(String uid);

}
