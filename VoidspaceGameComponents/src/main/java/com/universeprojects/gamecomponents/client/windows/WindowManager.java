package com.universeprojects.gamecomponents.client.windows;

import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.common.UINavigation;
import com.universeprojects.html5engine.client.framework.H5EEngine;

public abstract class WindowManager {

    private static WindowManager noopWindowManager;

    public static WindowManager getInstance(H5EEngine engine) {
        if(engine == null) {
            if(noopWindowManager == null) {
                noopWindowManager = new NoopWindowManager();
            }
            return noopWindowManager;
        }
        return engine.getOrCreateEngineBoundObject(GCWindowManager.class.getCanonicalName(), () -> new GCWindowManager(engine));
    }

    /**
     * Closes all the open windows
     */
    public abstract void closeAll();

    /**
     * Checks if any windows are in fullscreen mode
     */
    public abstract void updateFullscreen();

    public abstract void disableOptionsButtonInFullscreen(boolean disableOptionsButton);

    public abstract boolean getMobileMode();

    public abstract void setMobileMode(boolean mobileMode);

    public abstract void subscribeOnWindowFullscreen(Callable2Args<Boolean, Boolean> handler);

    public abstract boolean isActive(GCWindow window);

    public abstract boolean isUINavigationActive();

    public abstract void registerNavigation(UINavigation navigation);

    public abstract void unregisterNavigation(UINavigation navigation);

    public abstract UINavigation getActiveNavigation();

    abstract void activateWindow(GCWindow window);

    abstract void windowClosed(GCWindow window);

    public abstract boolean canUseKeyboardNavigation();

    public abstract void setKeyboardNavigationMode(boolean navigationMode);
}
