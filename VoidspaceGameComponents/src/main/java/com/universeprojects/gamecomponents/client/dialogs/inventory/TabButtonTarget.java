package com.universeprojects.gamecomponents.client.dialogs.inventory;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

/**
 * Class for setting buttons as drag and drop targets.
 * When a button is dragged over, the button set as a target is clicked.
 */
public class TabButtonTarget extends DragAndDrop.Target {

    public static final Hook<Callable1Args<TabButtonTarget>> HOOK_OnFinishedTabCountdown = Hooks.createHook1Args();
    public static final Hook<Callable1Args<TabButtonTarget>> HOOK_BeginTabCountdown = Hooks.createHook1Args();
    public static final Hook<Callable0Args> HOOK_TabCountdownAborted = Hooks.createHook0Args();

    GCInventory inventory;

    public TabButtonTarget(Actor actor, GCInventory inventory) {
        super(actor);
        this.inventory = inventory;
        Hooks.subscribe(HOOK_OnFinishedTabCountdown, this::dragTimerFinished);
    }

    private void dragTimerFinished(TabButtonTarget target) {
        if(target == this) {
            ((H5EButton) getActor()).setChecked(true);
        }
    }

    /**
     * Activates button click upon drag over. Unused parameters.
     */
    @Override
    public boolean drag(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        return dragInternal(source, payload);
    }

    @SuppressWarnings("unchecked")
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> boolean dragInternal(DragAndDrop.Source source, DragAndDrop.Payload payload) {
        if(!(payload instanceof ItemPayload)) return false;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>)payload;
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>)source;
        GCInventoryBase<KT, IT, CT> targetSource = (GCInventoryBase<KT, IT, CT>)inventory;

        Hooks.call(HOOK_BeginTabCountdown, this);

        if(componentSource.getActor().inventoryBase.equals(inventory)) {
            return false;
        }


        return componentSource.getActor().inventoryBase.canCrossInventoryDropFromHere(componentSource.getActor().getKey(), itemPayload.getObject(), targetSource)
            && targetSource.canCrossInventoryDropToHere(componentSource.getActor().inventoryBase, componentSource.getActor().getKey(), itemPayload.getObject(), null);
    }

    @Override
    public void drop(DragAndDrop.Source source, DragAndDrop.Payload payload, float x, float y, int pointer) {
        if (!(payload instanceof ItemPayload)) return;
        ItemPayload<?, ?, ?> itemPayload = (ItemPayload<?, ?, ?>) payload;
        ComponentSource<?, ?, ?> componentSource = (ComponentSource<?, ?, ?>) source;

        drop(componentSource, itemPayload);
    }

    @SuppressWarnings("unchecked")
    private <KT, IT extends GCInventoryItem, CT extends GCInventorySlotBaseComponent<KT, IT, CT>> void drop(ComponentSource<?, ?, ?> componentSourceUntyped, ItemPayload<?, ?, ?> itemPayloadUntyped) {
        ComponentSource<KT, IT, CT> componentSource = (ComponentSource<KT, IT, CT>)componentSourceUntyped;
        ItemPayload<KT, IT, CT> itemPayload = (ItemPayload<KT, IT, CT>)itemPayloadUntyped;
        final GCInventoryBase<KT, IT, CT> sourceInventory = componentSource.getActor().inventoryBase;
        final KT sourceKey = componentSource.getActor().getKey();

        boolean canTransfer =
            sourceInventory.canCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), inventory) &&
            inventory.canCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), null);

        if(canTransfer) {
            sourceInventory.onCrossInventoryDropFromHere(sourceKey, itemPayload.getObject(), inventory, true);
            inventory.onCrossInventoryDropToHere(sourceInventory, sourceKey, itemPayload.getObject(), null);
        }
    }

    @Override
    public void reset(DragAndDrop.Source source, DragAndDrop.Payload payload) {
        super.reset(source, payload);
        Hooks.call(HOOK_TabCountdownAborted);
    }
}
