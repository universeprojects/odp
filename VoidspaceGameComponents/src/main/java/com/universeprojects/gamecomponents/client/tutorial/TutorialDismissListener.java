package com.universeprojects.gamecomponents.client.tutorial;

import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.universeprojects.common.shared.callable.Callable0Args;

public class TutorialDismissListener implements EventListener {
    private final Callable0Args callback;

    public TutorialDismissListener(Callable0Args callback) {
        this.callback = callback;
    }

    @Override
    public boolean handle(Event event) {
        if(event instanceof TutorialDismissEvent) {
            callback.call();
            return true;
        }

        return false;
    }
}
