package com.universeprojects.gamecomponents.client.tutorial;

public enum TutorialSystemState {
    ENABLED(true, false, true, true),       // persisted, server-side value = ON , visualisation ON , objective tracking ON
    INHIBITED(true, true, true, false),     // temporary, server-side value = ON , visualisation OFF, objective tracking ON
    DISABLED(false, false, false, false);   // persisted, server-side value = OFF, visualisation OFF, objective tracking OFF

    private final boolean serverSideEnabled;
    private final boolean temporary;
    private final boolean trackObjectives;
    private final boolean showVisualization;

    TutorialSystemState(boolean serverSideEnabled, boolean temporary, boolean trackObjectives, boolean showVisualization) {
        this.serverSideEnabled = serverSideEnabled;
        this.temporary = temporary;
        this.trackObjectives = trackObjectives;
        this.showVisualization = showVisualization;
    }

    public boolean isTrackObjectives() {
        return trackObjectives;
    }

    public boolean isServerSideEnabled() {
        return serverSideEnabled;
    }

    public boolean isTemporary() {
        return temporary;
    }

    public boolean isShowVisualization() {
        return showVisualization;
    }
}
