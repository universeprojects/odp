package com.universeprojects.gamecomponents.client.elements.actionbar;

import com.badlogic.gdx.scenes.scene2d.HierarchyReachableEvent;
import com.badlogic.gdx.scenes.scene2d.HierarchyReachableListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TooltipManager;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.universeprojects.common.shared.util.ActivateDeactivateWrapper;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.dnd.GCActionDndSource;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCTextTooltip;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.shared.ActionIconData;

public final class GCActionButton extends H5EButton {
    public static final int ACTION_BUTTON_SIZE = 40;
    private static final String BUTTON_PARENT_STYLE = "button2";
    private final ActivateDeactivateWrapper<GCActionDndSource> dndSource;
    private final ReactiveActorValueCompound<ActionIconData> icon;
    private final GCOperation action;

    private GCActionButton(H5ELayer layer, GCOperation action) {
        super("", layer, BUTTON_PARENT_STYLE);
        this.icon = new ReactiveActorValueCompound<>(this);
        this.icon.setCallback(icon -> {
            if (icon != null && icon.actionIcon != null) {
                setIcon(icon);
            } else {
                setStyle(layer.getEngine().getSkin().get(BUTTON_PARENT_STYLE, ImageTextButtonStyle.class));
            }
        });

        DragAndDrop dragAndDrop = InventoryManager.getInstance(layer.getEngine()).getDragAndDrop();
        this.dndSource = new ActivateDeactivateWrapper<>();
        this.dndSource.setDeactivateListener(dragAndDrop::removeSource);
        this.dndSource.setActivateListener(dragAndDrop::addSource);
        this.action = action;
        this.icon.setValue(action.getIcons().get(0));
        this.setSize(ACTION_BUTTON_SIZE, ACTION_BUTTON_SIZE);

        if (getStage() != null && isDescendantOf(getStage().getRoot())) {
            startObserving();
        }

        addListeners(action);
    }

    private void startObserving() {
        action.activate();
        dndSource.set(new GCActionDndSource(GCActionButton.this, action, getLayer()));
    }

    private void stopObserving() {
        action.deactivate();
        dndSource.set(null);
    }

    private void addListeners(GCOperation action) {
        addListener(new HierarchyReachableListener() {
            @Override
            public void handle(HierarchyReachableEvent event) {
                if(event.becameReachable()) {
                    startObserving();
                } else {
                    stopObserving();
                }
            }
        });
        String actionHumanName = action.getActionHumanName();
        addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                action.call();
                if(actionHumanName != null) {
                    Tutorials.trigger("action:" + actionHumanName);
                }
                TooltipManager.getInstance().hideAll();
            }
        });
        addListener(new GCTextTooltip(actionHumanName, getLayer().getEngine().getSkin()));
    }

    public static GCActionButton newActionButton(H5ELayer layer, GCOperation action) {
        return new GCActionButton(layer, action);
    }

    public void dispose() {
        stopObserving();
    }

    @Override
    public float getMaxWidth() {
        return ACTION_BUTTON_SIZE;
    }

    @Override
    public float getMaxHeight() {
        return ACTION_BUTTON_SIZE;
    }

    @Override
    public float getMinWidth() {
        return ACTION_BUTTON_SIZE;
    }

    @Override
    public float getMinHeight() {
        return ACTION_BUTTON_SIZE;
    }

    protected void setIcon(ActionIconData icon) {
        H5ESpriteType spriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(icon.actionIcon);
        Dev.checkNotNull(spriteType, "Invalid to find icon with path " + icon.actionIcon);
        setColor(icon.color);
        ImageTextButtonStyle newStyle = new ImageTextButtonStyle(getLayer().getEngine().getSkin().get(BUTTON_PARENT_STYLE, ImageTextButtonStyle.class));
        newStyle.imageUp = new TextureRegionDrawable(spriteType.getGraphicData());
        newStyle.checked = null;
        newStyle.checkedOver = null;
        setStyle(newStyle);
    }


    public GCActionDndSource getDndSource() {
        return dndSource.get();
    }

    public ReactiveActorValueCompound<ActionIconData> getIcon() {
        return icon;
    }
}
