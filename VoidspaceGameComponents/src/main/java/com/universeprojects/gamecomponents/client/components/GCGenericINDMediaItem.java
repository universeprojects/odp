package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.GCMediaTextArea;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

public class GCGenericINDMediaItem<T> extends GCGenericINDListItem<T>{

    private final Table buttonTable;
    public GCGenericINDMediaItem(H5ELayer layer, GCGenericINDListItemData<T> data, H5EScrollablePane parentScrollPane, GCMediaTextArea textArea) {
        super(layer,null,data,parentScrollPane);
        addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                textArea.deselectAll();
                rebuildActor();
                subTable.row();
                showLearnBtn(true);
                setChecked(false);
                super.clicked(event, x, y);
            }
        });
        H5EButton learnButton=new H5EButton("Learn", layer);
        learnButton.addButtonListener(()-> textArea.onLearn(GCGenericINDMediaItem.this));
        learnButton.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);;
        H5EButton learnAllButton = new H5EButton("Learn all", layer);
        learnAllButton.addButtonListener(textArea::learnAll);
        learnAllButton.setStyle(StyleFactory.INSTANCE.buttonStyleBlueYellow);;
        buttonTable = new Table();
        buttonTable.add(learnButton);
        buttonTable.add(learnAllButton).padLeft(5);
    }

    public void showLearnBtn(boolean show){
        if(show){
            subTable.add(buttonTable).right().pad(5);
        }
    }
}
