package com.universeprojects.gamecomponents.client.elements.actionbar.dnd;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.utils.DragAndDrop;
import com.universeprojects.gamecomponents.client.dialogs.inventory.InventoryManager;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCActionBar;
import com.universeprojects.gamecomponents.client.elements.actionbar.GCOperation;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.ActionIconData;

public class GCActionBarDndPayload extends DragAndDrop.Payload {
    public static final Color COLOR_INVALID = new Color(0.5f, 0f, 0f, 1f);
    public static final Color COLOR_VALID = new Color(0.6f, 1f, 0.6f, 1f);
    private final GCOperation action;

    public GCActionBarDndPayload(H5ELayer layer, GCOperation action) {
        this.action = action;

        ActionIconData iconData = action == null ? null : action.getIcons().get(0).getValue();


        String iconName = iconData == null || iconData.actionIcon == null
                ? GCActionBar.MISSING_ACTION_ICON
                : iconData.actionIcon;

        H5EIcon icon = H5EIcon.fromSpriteType(layer, iconName);
        final DragAndDrop dragAndDrop = InventoryManager.getInstance(layer.getEngine()).getDragAndDrop();
        dragAndDrop.setDragActorPosition(icon.getWidth() / 2, -icon.getHeight() / 2);

        setDragActor(icon);
        icon.setStage(null);//Necessary for DragAndDrop to work

        H5EIcon invalidIcon = H5EIcon.fromSpriteType(layer, iconName);
        invalidIcon.setColor(COLOR_INVALID);
        setInvalidDragActor(invalidIcon);
        invalidIcon.setStage(null);//Necessary for DragAndDrop to work

        H5EIcon validIcon = H5EIcon.fromSpriteType(layer, iconName);
        validIcon.setColor(COLOR_VALID);
        setValidDragActor(validIcon);
        validIcon.setStage(null);//Necessary for DragAndDrop to work
    }

    public GCOperation getAction() {
        return action;
    }
}
