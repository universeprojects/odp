package com.universeprojects.gamecomponents.client.windows;

import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.common.UINavigation;

public class NoopWindowManager extends WindowManager {
    @Override
    public void closeAll() {
    }

    @Override
    public void updateFullscreen() {
    }

    @Override
    public void disableOptionsButtonInFullscreen(boolean disableOptionsButton) {
    }

    @Override
    public boolean getMobileMode() {
        return false;
    }

    @Override
    public void setMobileMode(boolean mobileMode) {
    }

    @Override
    public void subscribeOnWindowFullscreen(Callable2Args<Boolean, Boolean> handler) {
    }

    @Override
    public boolean isActive(GCWindow window) {
        return false;
    }

    @Override
    void activateWindow(GCWindow window) {
    }

    @Override
    void windowClosed(GCWindow window) {
    }

    @Override
    public boolean canUseKeyboardNavigation() {
        return false;
    }

    @Override
    public boolean isUINavigationActive() {
        return false;
    }

    @Override
    public void registerNavigation(UINavigation navigation) {

    }

    @Override
    public void unregisterNavigation(UINavigation navigation) {

    }

    @Override
    public UINavigation getActiveNavigation() {
        return null;
    }

    @Override
    public void setKeyboardNavigationMode(boolean navigationMode) {

    }
}
