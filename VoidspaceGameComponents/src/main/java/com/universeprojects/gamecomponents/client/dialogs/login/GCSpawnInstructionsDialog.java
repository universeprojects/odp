package com.universeprojects.gamecomponents.client.dialogs.login;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.gamecomponents.client.dialogs.GCMediaTextArea;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class GCSpawnInstructionsDialog extends GCSimpleWindow {
    private final GCMediaTextArea contentArea;

    public GCSpawnInstructionsDialog(H5ELayer layer) {
        super(layer, "spawninstructions", "Getting started", 500, 550, false);
        positionProportionally(0.8f, 0.5f);
        setFullscreenOnMobile(true);
        final H5EScrollablePane pane = new H5EScrollablePane(layer);
        add(pane).grow();
        pane.setStyle(StyleFactory.INSTANCE.scrollPaneStyleBlueCornersSemiTransparent);
        contentArea = new GCMediaTextArea(layer);
        contentArea.setParentPanel(pane);
        contentArea.setReadOnlyMode(true);
        pane.add(contentArea).grow();
    }

    public void clear() {
        contentArea.clear();
    }

    public void loadContent(String content) {
        contentArea.processContent(content, false);
    }

    public void addLink(String name, String url) {
        H5ELabel linkLabel = new H5ELabel(name, getLayer());
        linkLabel.setColor(0, 0.1f, 1, 1f);
        linkLabel.setWrap(true);
        linkLabel.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                Gdx.net.openURI(url);
            }
        });
        contentArea.row();
        contentArea.add(linkLabel).growX();
    }


}
