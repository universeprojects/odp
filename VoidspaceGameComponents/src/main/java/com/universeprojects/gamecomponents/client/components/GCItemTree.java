package com.universeprojects.gamecomponents.client.components;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.common.StyleFactory;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class GCItemTree extends Table {

    protected final H5ELayer layer;
    protected GCItemInternalTree internalTree = null;

    public GCItemTree(H5ELayer layer) {
        this.layer = layer;
    }

    public void setInternalTree(GCItemInternalTree internalTree) {
        this.internalTree = internalTree;
        add(internalTree).top();
    }

    public void clearTree() {
        internalTree.clear();
    }

    public GCItemTreeSubitem getSelection() {
        return internalTree.getSelection();
    }
}
