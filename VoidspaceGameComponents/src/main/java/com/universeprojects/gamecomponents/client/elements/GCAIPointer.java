package com.universeprojects.gamecomponents.client.elements;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.UPViewport;

public class GCAIPointer extends Group {
    private static final String ARROW = "images/icons/tutorial-marker.png";
    private static final Vector2 petal1 = new Vector2(0, 1);
    private static final Vector2 petal2 = new Vector2(-1, -1).nor();
    private static final Vector2 petal3 = new Vector2(1, -1).nor();

    private H5EIcon arrow1;
    private H5EIcon arrow2;
    private H5EIcon arrow3;

    private Vector2 globalPosition;
    private Callable1Args<GCAIPointer> onFadeOut;

    private float arrowsPositionMultiplier = 70f;

    public GCAIPointer(H5ELayer layer, Vector2 globalPosition, Callable1Args<GCAIPointer> onFadeOut, float scaleModifier) {
        super();
        this.globalPosition = globalPosition;
        this.onFadeOut = onFadeOut;
        layer.addActorToTop(this);
        arrow1 = H5EIcon.fromSpriteType(layer, ARROW);
        arrow1.setOrigin(Align.top);
        arrow1.setRotation(180);
        addActor(arrow1);
        arrow2 = H5EIcon.fromSpriteType(layer, ARROW);
        arrow2.setOrigin(Align.top);
        arrow2.setRotation(-60);
        addActor(arrow2);
        arrow3 = H5EIcon.fromSpriteType(layer, ARROW);
        arrow3.setOrigin(Align.top);
        arrow3.setRotation(60);
        addActor(arrow3);
        arrow1.setPosition(petal1.x * 70, petal1.y * 70, Align.top);
        arrow2.setPosition(petal2.x * 70, petal2.y * 70, Align.top);
        arrow3.setPosition(petal3.x * 70, petal3.y * 70, Align.top);
        arrow1.getColor().r = 0;
        arrow2.getColor().r = 0;
        arrow3.getColor().r = 0;
        getColor().a = 0;
        setScale(0.2f*layer.getEngine().getUiScale()*layer.getEngine().getGameZoom()*scaleModifier);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        setPosition(globalPosition.x, globalPosition.y);
        if (arrowsPositionMultiplier > 10) {
            arrowsPositionMultiplier -= delta * 140f;
        }
        arrow1.setPosition(arrowsPositionMultiplier * petal1.x, arrowsPositionMultiplier * petal1.y, Align.top);
        arrow2.setPosition(arrowsPositionMultiplier * petal2.x, arrowsPositionMultiplier * petal2.y, Align.top);
        arrow3.setPosition(arrowsPositionMultiplier * petal3.x, arrowsPositionMultiplier * petal3.y, Align.top);
        if (arrowsPositionMultiplier < 40) {
            getColor().a -= delta;
            if (getColor().a <= 0) {
                onFadeOut.call(this);
            }
        } else {
            getColor().a += delta * 3;
        }
    }
}
