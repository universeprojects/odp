package com.universeprojects.gamecomponents.client.dialogs;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.GCUtils;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5EContainer;
import com.universeprojects.html5engine.client.framework.H5EIcon;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;
import com.universeprojects.html5engine.shared.UPUtils;


public abstract class GCStackSplitter {

    private final GCSimpleWindow window;

    private final H5EInputBox amountBox;

    private int curAmount = 1;
    private final int totalAmount;
    public GCStackSplitter(H5ELayer layer, String iconSpriteTypeKey, String objectName, int totalAmount) {
        Dev.checkNotNull(layer);
        Dev.checkNotEmpty(objectName);
        if (totalAmount <= 0) {
            throw new IllegalArgumentException("Total amount must be greater than 0");
        }
        this.totalAmount = totalAmount;

        window = new GCSimpleWindow(layer, "stack-splitter", "Split Stack", 300, 265);
        window.onClose.registerHandler(this::handleClose);
        window.positionProportionally(0.5f,0.5f);
        final int iconSize = 60;

        // space occupied even if there is no icon
        H5EContainer iconPlaceholder = new H5EContainer(layer);
        final Cell<H5EContainer> iconPlaceHolderCell = window.add(iconPlaceholder);
        iconPlaceHolderCell.center();
        iconPlaceHolderCell.width(iconSize);
        iconPlaceHolderCell.height(iconSize);

        if (iconSpriteTypeKey != null) {
            // place the icon on top of the invisible placeholder
            H5EIcon icon = H5EIcon.fromSpriteType(layer, iconSpriteTypeKey);
            iconPlaceholder.add(icon).height(iconSize).width(iconSize);
        }

        final float titleFontSize = 1f;
        final float contentFontSize = 0.8f;

        Table nameTable = new Table();
        window.add(nameTable).colspan(2).left();

        H5ELabel objectNameLabel = new H5ELabel(layer);
        objectNameLabel.setFontScale(titleFontSize);
        objectNameLabel.setText(objectName);
        final Cell<H5ELabel> objectNameLabelCell = nameTable.add(objectNameLabel);
        objectNameLabelCell.padLeft(10).left();

        nameTable.row();
        H5ELabel objectTotalAmountLabel = new H5ELabel(layer);
        objectTotalAmountLabel.setFontScale(contentFontSize);
        objectTotalAmountLabel.setText("Quantity: " + totalAmount);
        nameTable.add(objectTotalAmountLabel).padLeft(15).left();

        window.addEmptyLine(25);

       // Text quantityErrorMessage = new Text("Quantity not valid");

        H5EButton buttonLess = window.addSmallButton("-").getActor();
        buttonLess.getLabel().setFontScale(1);
        buttonLess.addButtonListener(() -> {
            if (curAmount > totalAmount -1) {
                setAmount(totalAmount -1);
            }
            else if (curAmount > 1) {
                setAmount(curAmount -1);
            }
            else {
                setAmount(1);
            }
        });



        Cell<H5EInputBox> amountBoxCell = window.addInputBox(120);

        amountBoxCell.center().padLeft(10).padRight(10);
        amountBox = amountBoxCell.getActor();
        amountBox.setText(Integer.toString(curAmount));
        amountBox.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                updateAmount();
                event.handle();
            }
        });
        amountBox.setTextFieldListener((textField, c) -> {
            if(c == '\r') {
                submitDialog();
            }
        });

        H5EButton buttonMore = window.addSmallButton("+").left().getActor();
        buttonMore.getLabel().setFontScale(1);
        buttonMore.addButtonListener(() ->{
            if (curAmount < 1) {
                setAmount(1);
            }
            else if (curAmount < totalAmount -1) {
                setAmount(curAmount + 1);
            }
            else {
                setAmount(totalAmount -1);
            }
        });

        window.addEmptyLine(15);

        window.add();
        H5EButton buttonOk = window.addMediumButton("Split").fill().getActor();
        buttonOk.addButtonListener(this::submitDialog);
        buttonOk.setUserObject("button:split");
        UPUtils.tieButtonToKey(amountBox, Input.Keys.ENTER, buttonOk);

        GCUtils.preventSchematicWindowClosingFor(window);

    }

    private void updateAmount() {
        int amount;
        try {
            amount = Integer.parseInt(amountBox.getText());
        } catch (NumberFormatException ex) {
            return; // no change
        }
       validateAmountColor(amount);
            setAmount(amount);

    }

    private void setAmount(int amount) {
        curAmount = amount;
        validateAmountColor(amount);
        amountBox.setText(Integer.toString(curAmount));
    }

    private void handleClose() {
        onClose();
    }

    private void submitDialog() {
        Tutorials.trigger("button:split");
        if (curAmount < totalAmount && curAmount > 0) {
            close();
            onSubmit(curAmount);
        }
    }

    private void validateAmountColor(int amount) {
        if (amount >= totalAmount || amount <=0) {
            amountBox.setColor(Color.RED);
        }
        else {
            amountBox.setColor(1,1,1,1);
        }
    }




    //////////////////////////////////////////////////////////////
    //		PUBLIC INTERFACE
    //


    public void open() {
        window.open();
        amountBox.setDisabled(false);
        amountBox.focus();
        amountBox.selectAll();
    }

    public void close() {
        window.close();
    }

    public boolean isOpen() {
        return window.isOpen();
    }

    public void destroy() {
        window.destroy();
    }

    public void positionProportionally(float propX, float propY) {
        window.positionProportionally(propX, propY);
    }

//    public void positionLike(GCSimpleWindow other) {
//        window.positionLike(other);
//    }

//    public void positionBelow(GCSimpleWindow other) {
//        window.positionBelow(other);
//    }

    /**
     * To be implemented by child class
     */
    public abstract void onSubmit(int amountToSplit);

    /**
     * To be implemented by child class
     */
    public abstract void onClose();


}
