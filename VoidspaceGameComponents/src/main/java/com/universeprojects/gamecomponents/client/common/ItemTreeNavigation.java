package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.gamecomponents.client.components.GCItemInternalTree;
import com.universeprojects.gamecomponents.client.components.GCItemTreeItem;
import com.universeprojects.gamecomponents.client.components.GCItemTreeSubitem;

import java.util.ArrayList;
import java.util.List;

public class ItemTreeNavigation extends NavigationExtension{


    private GCItemInternalTree tree;
    private Cell currentCell;
    private List<Cell> directTree = new ArrayList<>();

    public ItemTreeNavigation(UINavigation navigation){
        super(navigation);
    }

    @Override
    public void init(Actor actor){
        tree=(GCItemInternalTree)actor;
        clearAndFetch();
        if(directTree.size()>0){
            currentCell=directTree.get(0);
            focus();
        }
        active = true;
    }

    private void clearAndFetch(){
        directTree.clear();
        fetchItemsRecursively(tree);
    }

    private void fetchItemsRecursively(Table subTree){
        for(Cell cell:subTree.getCells()) {
            if(cell.getActor()!=null ){
                if(cell.getActor() instanceof GCItemTreeSubitem){
                    directTree.add(cell);
                }else if(cell.getActor() instanceof GCItemTreeItem){
                    directTree.add(cell);
                    if(((GCItemTreeItem) cell.getActor()).isOpened()){
                        fetchItemsRecursively(((GCItemTreeItem) cell.getActor()).getListTable());
                    }
                }
            }
        }
    }
    @Override
    public void moveDown() {
        int cellIndex=directTree.indexOf(currentCell);
        cellIndex++;
        if(cellIndex<directTree.size()){
            currentCell=directTree.get(cellIndex);
        }
        focus();
    }

    @Override
    public void moveUp() {
        int cellIndex=directTree.indexOf(currentCell);
        cellIndex--;
        if(cellIndex>=0){
            currentCell=directTree.get(cellIndex);
        }
       focus();
    }

    private void focus(){
        if(currentCell.getActor() instanceof GCItemTreeItem){
            navigation.indicator.activate(((GCItemTreeItem) currentCell.getActor()).getCategoryButton());
        }else {
            navigation.indicator.activate(currentCell.getActor());
        }
        if(navigation.curScrollablePane!=null){
            float scrollPositionY = currentCell.getActor().localToAscendantCoordinates(navigation.curScrollablePane.getContent(),new Vector2(0, currentCell.getActorHeight())).y;
            navigation.curScrollablePane.setScrollY(navigation.curScrollablePane.getContent().getHeight() - scrollPositionY);
        }
    }

    @Override
    public void enter() {
        if(currentCell.getActor() instanceof GCItemTreeItem){
            GCItemTreeItem category = (GCItemTreeItem) currentCell.getActor();
            ((GCItemTreeItem) currentCell.getActor()).getCategoryButton().click();
            clearAndFetch();
            if(!category.isOpened()){
                for(Cell cell:directTree){
                    if(cell.getActor() == category){
                        currentCell = cell;
                    }
                }
            }

        }else if(currentCell.getActor() instanceof GCItemTreeSubitem){
            ((GCItemTreeSubitem) currentCell.getActor()).click();
        }
    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public boolean isCompatible(Actor actor) {
        if(actor instanceof GCItemInternalTree){
            return true;
        }
        return false;
    }
}
