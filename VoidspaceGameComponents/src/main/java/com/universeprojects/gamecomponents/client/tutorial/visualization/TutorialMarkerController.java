package com.universeprojects.gamecomponents.client.tutorial.visualization;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.gamecomponents.client.elements.GCSelectionIndicator;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarker;
import com.universeprojects.gamecomponents.client.tutorial.entities.TutorialMarkerStyle;
import com.universeprojects.gamecomponents.client.tutorial.visualization.animations.GeneralTooltipAnimation;
import com.universeprojects.gamecomponents.client.tutorial.visualization.animations.TutorialArrowAnimation;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

import java.util.ArrayList;
import java.util.List;

/**
 * This components received a list of registered actors
 * and corresponding TutorialMarkers from {@link TutorialObjectTracker}
 * and performs further checks to find a visible tutorial marker target with
 * highest priority.
 * <p>
 * Once the target was identified, this classes updates decides which visual
 * clues will be used for this marker and updates their visability (hiding
 * unnecessary objects and showing relevant ones).
 * <p>
 * After visual clues have been initialized, this class is also responsible for
 * tracking target's position and moving visual clues
 */
public class TutorialMarkerController extends Actor {
    private static final float ARROW_OFFSET = 40;
    private static final float ANIMATION_DURATION = 2F;
    private static final float FADE_DURATION = 1F;
    private static final float TEXT_OFFSET_Y = 20F;

    @SuppressWarnings("unused")
    private final TutorialController tutorialController;
    private final H5ELayer layer;
    private final H5ESprite arrowActor;
    private H5ELabel arrowActionText;

    private final List<ActorMarkerPair> targets;
    private ActorMarkerPair lastActiveActor;
    private boolean forceStateUpdate = false;
    private Image indicator;

    private TutorialArrowAnimation arrowBounceAnimation;
    private TutorialArrowAnimation indicatorFadeAnimation;
    private GeneralTooltipAnimation generalTooltipAnimation;
    private final Vector2 globalCoordsVector = new Vector2();
    private final Vector3 tempVec = new Vector3();
    private final Vector3 tempVec2 = new Vector3();
    private final Vector2 tempVec2D = new Vector2();
    private final Vector2 tempVec2D2 = new Vector2();

    public TutorialMarkerController(TutorialController tutorialController, H5ELayer layer) {
        this.tutorialController = tutorialController;
        this.layer = layer;

        arrowBounceAnimation = new TutorialArrowAnimation(ANIMATION_DURATION, ARROW_OFFSET);
        indicatorFadeAnimation = new TutorialArrowAnimation(FADE_DURATION, 1f);
        generalTooltipAnimation = new GeneralTooltipAnimation(0.15f);
        targets = new ArrayList<>();

        indicator= new Image(GCSelectionIndicator.getDrawable(layer));
        layer.addActorToTop(indicator);
        arrowActionText=new H5ELabel(layer);
        arrowActionText.setOrigin(Align.center);
        arrowActionText.setAlignment(Align.center);
        layer.addActorToTop(arrowActionText);
        indicator.setOrigin(Align.center);
        indicator.setTouchable(Touchable.disabled);
        indicator.setColor(0,1,0,1);
        indicator.setVisible(false);
        arrowActor = new H5ESprite(layer, "images/icons/tutorial-marker.png");
        arrowActor.addTo(layer);
        arrowActor.setVisible(false);
        arrowActor.setTouchable(Touchable.disabled);
        arrowActor.setOrigin(Align.center);

    }

    /**
     * Checks if object is visible on the screen
     * and updates globalCoordsVector with its
     * screen coordinates.
     * <p>
     * This method is basically a combination of
     * Actor.isHierarchyVisible() and Actor.localToStageCoordinates()
     * with the intent to combine both of them to minimize the performance
     * drop due to the necessity to travel the layout tree
     */
    protected boolean checkHierarchyFindCoords(Actor actor, boolean worldActor) {
        if(worldActor) {
            if(actor.getStage() != null && actor.getStage().getCamera() != null) {
                tempVec2D.set(actor.getOriginX(),actor.getOriginY());
                tempVec.set(actor.localToStageCoordinates(tempVec2D),0);
                actor.getStage().getCamera().project(tempVec);
                tempVec2D.set(tempVec.x, tempVec.y);
            }
        }
        globalCoordsVector.set(tempVec2D.x, tempVec2D.y);
        return true;
    }

    protected ActorMarkerPair findFirstVisibleTarget() {
        float centerX = layer.getEngine().getWidth() /2f;
        float centerY = layer.getEngine().getHeight() /2f;
        ActorMarkerPair closestPair = null;
        float closestDist2 = Float.MAX_VALUE;
        Vector2 vec = new Vector2();
        for (ActorMarkerPair pair : targets) {
            // If has no requirements for actor visibility, or
            // target actor is visible
            final Actor actor = pair.getActor();
            if (actor == null) {
                if(closestPair == null) {
                    closestPair = pair;
                }
                continue;
            }

            boolean found = false;

            if (pair.getMarker().getStyle() == TutorialMarkerStyle.ARROW) {
                final Object userObject = actor.getUserObject();
                final boolean isWorldActor = userObject instanceof String && ((String) userObject).contains("world:");
                final boolean foundValidTarget = checkHierarchyFindCoords(actor, isWorldActor);
                if (foundValidTarget) {
                    found = true;
                }
            }

            if(found || (actor.getStage() != null && isHierarchyVisible(actor))) {
                vec.setZero();
                actor.localToScreenCoordinates(vec);
                float dist2 = vec.dst2(centerX, centerY);
                if(dist2 < closestDist2) {
                    closestDist2 = dist2;
                    closestPair = pair;
                }
            }
        }

        return closestPair;
    }

    private boolean isHierarchyVisible(Actor actor){
        Actor currentActor = actor;
        do {
            if (!currentActor.isVisible()) {
                return false;
            }
            currentActor = currentActor.getParent();
        } while (currentActor != null);
        return true;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        ActorMarkerPair target = findFirstVisibleTarget();
        if (target != lastActiveActor || forceStateUpdate) {
            updateState(target);
            lastActiveActor = target;
            forceStateUpdate = false;
        }

        if (target == null)
            return;

        TutorialMarker marker = target.getMarker();

        if (marker.getStyle() == TutorialMarkerStyle.ARROW) {
            updateArrowPosition(delta, target.getActor(), (target.getActor().getUserObject() != null && ((String) target.getActor().getUserObject()).contains("world:")));
        }
    }

    private void updateArrowPosition(float delta, Actor targetActor, boolean worldActor) {
        // Attention! It's critically important that globalCoordsVector has been updated
        // with actual data. At this moment it is being done using checkHierarchyFindCoords method
        arrowActionText.setText("");
        indicator.setVisible(true);
        float width = targetActor.getWidth() * targetActor.getScaleX();
        if(worldActor) {
            tempVec2D.set(targetActor.getOriginX(),targetActor.getOriginY());
            tempVec.set(targetActor.localToStageCoordinates(tempVec2D),0);
            if(targetActor.getStage() != null && targetActor.getStage().getCamera() != null) {
                targetActor.getStage().getCamera().project(tempVec);
            }
            globalCoordsVector.set(tempVec.x, tempVec.y).scl(1f / layer.getEngine().getUiScale());
            width = 0;
        }else{
            tempVec2D.set(0,0);
            globalCoordsVector.set(targetActor.localToStageCoordinates(tempVec2D));
        }
        float arrowWidth = arrowActor.getWidth();
        float arrowHeight = arrowActor.getHeight();

        float arrowBaseX = globalCoordsVector.x + (width - arrowWidth) / 2;
        float arrowBaseY = globalCoordsVector.y - ARROW_OFFSET - arrowHeight;

        if(!worldActor) {
            if (layer.getHeight() - arrowBaseY > layer.getHeight()-arrowHeight) {
                arrowBaseY += arrowHeight * 2;
                arrowActor.setRotation(180);
            } else {
                arrowActor.setRotation(0);
            }
        }

        tempVec2D2.set(0, 1);

        float textOffsetX=0;
        float textOffsetY= TEXT_OFFSET_Y +arrowHeight/2;
        if(worldActor) {
            boolean rotate = false;
            if(arrowBaseX < arrowWidth) {
                arrowBaseX = arrowWidth;
                textOffsetX=80;
                rotate = true;
                arrowActionText.setText("Go this way");
            }
            else {
                int screenWidth = layer.getViewport().getScreenWidth();
                if(arrowBaseX > screenWidth -(arrowWidth)) {
                    arrowBaseX = screenWidth -(arrowWidth);
                    rotate = true;
                    textOffsetX=-80;
                    arrowActionText.setText("Go this way");
                }
            }
            if(arrowBaseY < arrowHeight*2) {
                arrowBaseY = arrowHeight*2;
                rotate = true;
                arrowActionText.setText("Go this way");
            }
            else {
                int screenHeight = layer.getViewport().getScreenHeight();
                if(arrowBaseY > screenHeight -(arrowHeight*2)) {
                    arrowBaseY = screenHeight -(arrowHeight*2);
                    rotate = true;
                    textOffsetY*=-1;
                    arrowActionText.setText("Go this way");
                }
            }

            if(rotate) {
                tempVec2.set(arrowBaseX, arrowBaseY, 0); //to object
                tempVec.set(globalCoordsVector.x, globalCoordsVector.y, 0).sub(tempVec2);
                tempVec2D.set(0, 1);
                tempVec2D2.set(tempVec.x, tempVec.y);
                float angle = tempVec2D.angle(tempVec2D2);
                arrowActor.setRotation(angle);
                tempVec2D2.nor();
            }
            else {
                arrowActor.setRotation(0);
            }
        }
        if(!worldActor){
            H5EScrollablePane actorParentPane=getParentPanel(targetActor);
            if(actorParentPane!=null){
                if(targetActor.localToAscendantCoordinates(actorParentPane,tempVec2D.set(0,0)).y<0){
                    arrowBaseX=actorParentPane.localToStageCoordinates(tempVec2D.set(actorParentPane.getWidth()/2,0)).x - arrowWidth/2;
                    arrowBaseY=actorParentPane.localToStageCoordinates(tempVec2D.set(actorParentPane.getWidth()/2,0)).y - ARROW_OFFSET + arrowHeight;
                    tempVec2D2.y*=-1;
                    arrowActor.setRotation(180);
                    arrowActionText.setText("Scroll down");
                    indicator.setVisible(false);
                }else if(targetActor.localToAscendantCoordinates(actorParentPane,tempVec2D.set(0,0)).y>actorParentPane.getHeight()){
                    arrowBaseX=actorParentPane.localToStageCoordinates(tempVec2D.set(actorParentPane.getWidth()/2,actorParentPane.getHeight())).x - arrowWidth/2;
                    arrowBaseY=actorParentPane.localToStageCoordinates(tempVec2D.set(actorParentPane.getWidth()/2,actorParentPane.getHeight())).y - ARROW_OFFSET - arrowHeight;
                    arrowActionText.setText("Scroll up");
                    indicator.setVisible(false);
                    textOffsetY*=-1;
                }
            }
        }

        float rotation=layer.getEngine().getRotation()+targetActor.getRotation();

        float animatedAmount = arrowBounceAnimation.animate(delta);

        arrowActor.setPosition(arrowBaseX + (tempVec2D2.x * animatedAmount),
                arrowBaseY + (tempVec2D2.y * animatedAmount));
        arrowActionText.setPosition(arrowActor.getX(Align.center)+textOffsetX,arrowActor.getY(Align.center)+textOffsetY,Align.center);
        arrowActionText.setZIndex(Integer.MAX_VALUE);
        arrowActor.setZIndex(Integer.MAX_VALUE);

        //checking is actor an UI element
        if(targetActor instanceof Widget || targetActor instanceof WidgetGroup){
            indicator.setRotation(0);
            indicator.setOrigin(0,0);
            tempVec2D.set(0,0);
            tempVec2D2.set(targetActor.localToStageCoordinates(tempVec2D));
            indicator.setPosition(tempVec2D2.x-10,tempVec2D2.y-10);
            indicator.setSize(targetActor.getWidth()*targetActor.getScaleX()+20f,targetActor.getHeight() * targetActor.getScaleY()+20f);
        }else {
            tempVec2D.set(-15f*layer.getEngine().getGameZoom(),-15f*layer.getEngine().getGameZoom());
            tempVec.set(targetActor.localToStageCoordinates(tempVec2D),0);
            layer.getEngine().getGameViewport().project(tempVec);
            indicator.setRotation(rotation);
            indicator.setOrigin(0,0);
            indicator.setSize(targetActor.getWidth()*targetActor.getScaleX()/layer.getEngine().getGameZoom()+20f,targetActor.getHeight()*targetActor.getScaleY()/layer.getEngine().getGameZoom()+20f);
            indicator.setPosition(tempVec.x,tempVec.y);
        }

        indicator.getColor().a=indicatorFadeAnimation.animate(delta);

    }

    @SuppressWarnings("Duplicates")
    private void updateState(ActorMarkerPair target) {
        if (target == null) {
            arrowActor.setVisible(false);
            arrowActionText.setVisible(false);
            indicator.setVisible(false);
            return;
        }

        TutorialMarker marker = target.getMarker();

        // Update tooltips
        if (marker.getStyle() == TutorialMarkerStyle.ARROW) {
            arrowActor.setVisible(true);
            arrowActionText.setVisible(true);
            indicator.setVisible(true);
            arrowActor.toFront();

        } else if (marker.getStyle() == TutorialMarkerStyle.TOOLTIP) {
            arrowActor.setVisible(false);
            arrowActionText.setVisible(false);
            indicator.setVisible(false);
            generalTooltipAnimation.reset();
        }
    }

    public void clearTargets() {
        targets.clear();
        lastActiveActor = null;
        forceStateUpdate = true;
    }

    public void addTarget(ActorMarkerPair pair) {
        if (pair.getActor() == null && pair.getMarker().getStyle() == TutorialMarkerStyle.ARROW)
            throw new IllegalArgumentException("TutorialMarker must have a target actor when in ARROW mode");

        targets.add(pair);
    }

    public H5EScrollablePane getParentPanel(Actor actor){
        if(actor.getParent()!=null) {
            return actor.getParent() instanceof H5EScrollablePane ? (H5EScrollablePane) actor.getParent() : getParentPanel(actor.getParent());
        }else{
            return null;
        }
    }
}
