package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCItemIcon;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5EScrollablePane;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ETextArea;

public class KeyUINavigation extends UINavigation {

    protected Group currentGroup;

    public KeyUINavigation(H5ELayer layer) {
        super(layer);
    }

    @Override
    protected void findActor() {
        if (currentActor == null) {
            if (currentGroup instanceof Table) {
                for (Cell cell : ((Table) currentGroup).getCells()) {
                    if (isFocusable(cell.getActor())) {
                        currentCell = cell;
                        currentActor = cell.getActor();
                        break;
                    }
                }
                moveScrollbar();
            } else if (currentGroup instanceof HorizontalGroup) {
                for (Actor actor : currentGroup.getChildren()) {
                    if (isFocusable(actor)) {
                        currentCell = null;
                        currentActor = actor;
                        break;
                    }
                }
            }
            if (currentActor != null)
                focusOnCurrent();
        }
    }

    public void setActorAndGroup(Group group, Actor actor) {
        currentActor = actor;
        currentGroup = group;
        currentCell = null;
        if (currentGroup instanceof Table) {
            currentCell = ((Table) currentGroup).getCell(actor);
        }
        focusOnCurrent();
    }

    @Override
    public void focusOnCurrent() {
        if (currentActor != null) {
            indicator.activate(currentActor);
            if (currentActor instanceof H5EButton) {
                ((H5EButton) currentActor).setOver(true);
            } else if (currentGroup instanceof Table) {
                currentCell = ((Table) currentGroup).getCell(currentActor);
            }
        }
    }

    @Override
    protected boolean isFocusable(Actor actor) {
        if (actor != null && actor.isVisible() && !ignoredActors.contains(actor)) {
            if (actor instanceof Button) {
                return true;
            }
            if (actor instanceof Group) {
                return isNotEmpty((Group) actor);
            }
            if (actor instanceof TextField) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void moveDown() {
        if (currentGroup instanceof Table) {
            if (currentCell != null && currentCell.getRow() < ((Table) currentGroup).getRows() - 1) {
                for (int i = 0; i < ((Table) currentGroup).getCells().size; i++) {
                    if (isFocusable(((Table) currentGroup).getCells().get(i).getActor())) {
                        if (((Table) currentGroup).getCells().get(i).getRow() >= currentCell.getRow() + 1 && ((Table) currentGroup).getCells().get(i).getColumn() == currentCell.getColumn()) {
                            currentCell = ((Table) currentGroup).getCells().get(i);
                            currentActor = currentCell.getActor();
                            moveScrollbar();
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void moveRight() {
        if (currentGroup instanceof Table) {
            if (currentCell != null && currentCell.getColumn() < ((Table) currentGroup).getColumns() - 1) {
                for (int i = 0; i < ((Table) currentGroup).getCells().size; i++) {
                    if (isFocusable(((Table) currentGroup).getCells().get(i).getActor())) {
                        if (((Table) currentGroup).getCells().get(i).getColumn() >= currentCell.getColumn() + 1 && ((Table) currentGroup).getCells().get(i).getRow() == currentCell.getRow()) {
                            currentCell = ((Table) currentGroup).getCells().get(i);
                            currentActor = currentCell.getActor();
                            break;
                        }
                    }
                }
            }
        } else if (currentGroup instanceof HorizontalGroup) {
            int index = currentGroup.getChildren().indexOf(currentActor, true);
            if (index < currentGroup.getChildren().size - 1) {
                for (int i = index + 1; i < currentGroup.getChildren().size; i++) {
                    if (isFocusable(currentGroup.getChildren().get(i))) {
                        currentActor = currentGroup.getChildren().get(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void moveUp() {
        if (currentGroup instanceof Table) {
            if (currentCell != null && currentCell.getRow() > 0) {
                for (int i = ((Table) currentGroup).getCells().size - 1; i >= 0; i--) {
                    if (isFocusable(((Table) currentGroup).getCells().get(i).getActor())) {
                        if (((Table) currentGroup).getCells().get(i).getRow() <= currentCell.getRow() - 1 && ((Table) currentGroup).getCells().get(i).getColumn() == currentCell.getColumn()) {
                            currentCell = ((Table) currentGroup).getCells().get(i);
                            currentActor = currentCell.getActor();
                            moveScrollbar();
                            break;
                        }
                    }
                }
            }
        }
    }

    @Override
    public void moveLeft() {
        if (currentGroup instanceof Table) {
            if (currentCell != null && currentCell.getColumn() > 0) {
                for (int i = ((Table) currentGroup).getCells().size - 1; i >= 0; i--) {
                    if (isFocusable(((Table) currentGroup).getCells().get(i).getActor())) {
                        if (((Table) currentGroup).getCells().get(i).getColumn() <= currentCell.getColumn() - 1 && ((Table) currentGroup).getCells().get(i).getRow() == currentCell.getRow()) {
                            currentCell = ((Table) currentGroup).getCells().get(i);
                            currentActor = currentCell.getActor();
                            break;
                        }
                    }
                }
            }
        } else if (currentGroup instanceof HorizontalGroup) {
            int index = currentGroup.getChildren().indexOf(currentActor, true);
            if (index > 0) {
                for (int i = index - 1; i >= 0; i--) {
                    if (isFocusable(currentGroup.getChildren().get(i))) {
                        currentActor = currentGroup.getChildren().get(i);
                        break;
                    }
                }
            }
        }
    }

    @Override
    public void exit() {
        if (tryExit())
            if (currentGroup == rootGroup) {
                if (((GCWindow)window).isCloseButtonEnabled())
                    ((GCWindow)window).close();
                return;
            }
        if (currentGroup.getParent() != null && currentGroup != rootGroup) {
            exitGroup(currentGroup);
            focusOnCurrent();
        } else {
            currentGroup = rootGroup;
            currentCell = null;
            currentActor = null;
            focusOnCurrent();
        }
    }

    @Override
    public void enter() {
        enter(true);
    }

    @Override
    public void move(Direction direction) {
        if (tryMove(direction)) {
            switch (direction) {
                case LEFT:
                    moveLeft();
                    break;
                case UP:
                    moveUp();
                    break;
                case RIGHT:
                    moveRight();
                    break;
                case DOWN:
                    moveDown();
                    break;
            }
            focusOnCurrent();
        }
    }

    public void enter(boolean click) {
        if (tryEnter())
            if (currentActor != null) {
                if (currentActor instanceof Button) {
                    if (click)
                        Gdx.app.postRunnable(() -> ((Button) currentActor).setChecked(!((Button) currentActor).isChecked()));
                    //((H5EButton) currentActor).click();
                } else if (currentActor instanceof GCItemIcon) {
                    if (click)
                        for (Object listener : currentActor.getListeners()) {
                            if (listener instanceof ClickListener) {
                                ((ClickListener) listener).clicked(new InputEvent(), 0, 0);
                            }
                        }
                } else if (currentActor instanceof H5ETextArea) {
                    ((H5ETextArea) currentActor).focus();
                } else if (currentActor instanceof H5EInputBox) {
                    ((H5EInputBox) currentActor).focus();
                } else if (currentActor instanceof Group) {
                    if (currentActor instanceof H5EScrollablePane) {
                        curScrollablePane = (H5EScrollablePane) currentActor;
                    }
                    if (getFocusableChildrenCount((Group) currentActor) == 1) {
                        currentGroup = (Group) currentActor;
                        currentCell = null;
                        for (Actor actor : currentGroup.getChildren()) {
                            if (isFocusable(actor)) {
                                currentActor = actor;
                                enter(false);
                                focusOnCurrent();
                                break;
                            }
                        }
                    } else {
                        currentGroup = (Group) currentActor;
                        currentCell = null;
                        currentActor = null;
                        findActor();
                    }
                }
            }
    }

    @Override
    public void setWindow(Window window, Group rootGroup) {
        super.setWindow(window, rootGroup);
        currentGroup = rootGroup;
    }

    protected void exitGroup(Group group) {
        if (group.getParent() != null) {
            if (group.getParent() instanceof Stack) {
                currentGroup = group.getParent().getParent();
                if (getFocusableChildrenCount(currentGroup) == 1) {
                    exitGroup(currentGroup);
                } else {
                    currentActor = group.getParent();
                    if (currentActor instanceof H5EScrollablePane) {
                        curScrollablePane = null;
                    }
                }
            } else {
                currentActor = currentGroup;
                if (currentActor instanceof H5EScrollablePane) {
                    curScrollablePane = null;
                }
                currentGroup = group.getParent();
                if (getFocusableChildrenCount(currentGroup) <= 1) {
                    exitGroup(currentGroup);
                }
            }
            if (currentGroup instanceof Table) {
                currentCell = ((Table) currentGroup).getCell(currentActor);
            }

        }
    }

    @Override
    public void focusOn(Actor actor) {
        currentGroup = actor.getParent();
        super.focusOn(actor);
    }

    protected int getFocusableChildrenCount(Group group) {
        int count = 0;
        for (Actor actor : group.getChildren()) {
            if (isFocusable(actor)) {
                count++;
            }
        }
        return count;
    }

    protected boolean isNotEmpty(Group group) {
        boolean notEmpty = false;
        for (Actor child : group.getChildren()) {
            if (child instanceof Group && !(child instanceof Button)) {
                notEmpty |= group.isVisible() && isNotEmpty((Group) child);
            } else {
                notEmpty |= group.isVisible() && child.isVisible();
            }
        }
        return notEmpty;
    }

    @Override
    protected void unFocusAll() {
        for (Actor actor : currentGroup.getChildren()) {
            if (actor instanceof H5EButton) {
                ((H5EButton) actor).setOver(false);
            }
        }
    }

    public void moveScrollbar() {
        if (curScrollablePane != null && currentActor != null) {
            float scrollPositionY = currentActor.localToAscendantCoordinates(curScrollablePane.getContent(), new Vector2(0, currentActor.getHeight())).y;
            curScrollablePane.setScrollY(curScrollablePane.getContent().getHeight() - scrollPositionY);
        }
    }

}

