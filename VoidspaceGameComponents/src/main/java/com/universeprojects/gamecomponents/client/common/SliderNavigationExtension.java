package com.universeprojects.gamecomponents.client.common;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.universeprojects.gamecomponents.client.elements.GCSlider;

import java.util.HashMap;
import java.util.Map;

public class SliderNavigationExtension extends NavigationExtension{
    private GCSlider currentSlider;
    private final Map<GCSlider, Float> overriddenStepSizes;
    private float relativeStepSize;

    /**
     * @param stepSize If stepSize == 0 default defined step size of slider will be used
     */
    public SliderNavigationExtension(UINavigation navigation, float stepSize) {
        super(navigation);
        this.relativeStepSize = stepSize;
        overriddenStepSizes = new HashMap<>();
    }

    public void setRelativeStepSize(float relativeStepSize) {
        this.relativeStepSize = relativeStepSize;
    }

    public void addStepSize(GCSlider slider, float stepSize){
        overriddenStepSizes.put(slider, stepSize);
    }

    @Override
    public void enter() {
        navigation.exit();
    }

    @Override
    public void exit() {
        active = false;
    }

    @Override
    public void moveRight() {
        super.moveRight();
        if(!currentSlider.isTouchable()||currentSlider.isDisabled()||!currentSlider.isVisible()) return;
        if(currentSlider.getValue() < currentSlider.getMaxValue()) {
            float stepSize = (overriddenStepSizes.containsKey(currentSlider) ? overriddenStepSizes.get(currentSlider):relativeStepSize*currentSlider.getMaxValue());
            currentSlider.setValue(currentSlider.getValue() + (stepSize > 0 ? stepSize: currentSlider.getStepSize()));
        }else{
            currentSlider.setValue(0);
        }
    }

    @Override
    public void moveLeft() {
        super.moveLeft();
        if(!currentSlider.isTouchable()||currentSlider.isDisabled()||!currentSlider.isVisible()) return;
        if(currentSlider.getValue() > 0) {
            float stepSize = (overriddenStepSizes.containsKey(currentSlider) ? overriddenStepSizes.get(currentSlider):relativeStepSize*currentSlider.getMaxValue());
            currentSlider.setValue(currentSlider.getValue() - (stepSize > 0 ? stepSize: currentSlider.getStepSize()));
        }else{
            currentSlider.setValue(currentSlider.getMaxValue());
        }
    }

    @Override
    public void moveUp() {
        navigation.exit();
    }

    @Override
    public void moveDown() {
        navigation.exit();
    }

    @Override
    public boolean isCompatible(Actor actor) {
        return actor instanceof GCSlider;
    }

    @Override
    public void init(Actor actor) {
        currentSlider = (GCSlider) actor;
        active = true;
    }
}
