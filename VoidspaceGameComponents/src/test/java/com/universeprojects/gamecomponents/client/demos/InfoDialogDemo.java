package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.DevException;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.gamecomponents.client.dialogs.GCInfoDialog;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;

import java.util.Deque;
import java.util.LinkedList;

@SuppressWarnings("SpellCheckingInspection")
public class InfoDialogDemo extends Demo {

    private final Logger log = Logger.getLogger(InfoDialogDemo.class);

    private final H5ELayer layer;
    private final GCSimpleWindow controlWindow;
    private final H5EButton btnOpenNewDialog;

    private Deque<GCInfoDialog> openDialogs = new LinkedList<>();

    private int nextDialogIndex = 0;

    public InfoDialogDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        controlWindow = new GCSimpleWindow(layer, "info-dialog-demo", "Info Dialog Demo");
        controlWindow.onClose.registerHandler(this::cleanup);
        controlWindow.positionProportionally(.6f, .9f);

        btnOpenNewDialog = controlWindow.addLargeButton("Open new dialog").left().growX().getActor();
        btnOpenNewDialog.addButtonListener(this::openNewDialog);

        H5EButton btnCloseOldestDialog = controlWindow.addLargeButton("Close oldest dialog").right().growX().getActor();
        btnCloseOldestDialog.addButtonListener(this::closeOldestDialog);
    }

    @Override
    public void open() {
        controlWindow.open();

        openNewDialog();
        openNewDialog();
        openNewDialog();
        openNewDialog();
    }

    @Override
    public void close() {
        controlWindow.close();
    }

    private void cleanup() {
        while (!openDialogs.isEmpty()) {
            openDialogs.pollFirst().close();
        }
    }

    @Override
    public boolean isOpen() {
        return controlWindow.isOpen();
    }

    private void openNewDialog() {
        final GCInfoDialog dialog;
        final float propX;
        final float propY;

        int choice = nextDialogIndex++ % 4;
        switch (choice) {
            case 0:
                dialog = new DemoDialog0();
                propX = .15f;
                propY = .3f;
                break;
            case 1:
                dialog = new DemoDialog1();
                propX = .45f;
                propY = .3f;
                break;
            case 2:
                dialog = new DemoDialog2();
                propX = .15f;
                propY = .6f;
                break;
            case 3:
                dialog = new DemoDialog3();
                propX = .45f;
                propY = .6f;

                break;
            default:
                throw new DevException("Unsupported value: " + choice);
        }

        dialog.positionProportionally(propX, propY);
        dialog.open();

        if (openDialogs.size() >= 4) {
            closeOldestDialog();
        }
        openDialogs.addLast(dialog);
    }

    private void closeOldestDialog() {
        GCInfoDialog oldestDialog = openDialogs.pollFirst();
        if (oldestDialog != null) {
            oldestDialog.close();
        }
    }

    class DemoDialog0 extends GCInfoDialogDemoImpl {

        public DemoDialog0() {
            super(layer);
        }

        @Override
        protected String getTitleText() {
            return "Demo Dialog 0";
        }

        @Override
        protected String[] getCaptionText() {
            return new String[]{
                "Lorem ipsum dolor sit amet?"
            };
        }

        @Override
        protected String getButtonLeftText() {
            return "Oui";
        }

        @Override
        protected String getButtonRightText() {
            return "Non";
        }

    }

    class DemoDialog1 extends GCInfoDialogDemoImpl {

        public DemoDialog1() {
            super(layer);
        }

        @Override
        protected String getTitleText() {
            return "Demo Dialog 1";
        }

        @Override
        protected String[] getCaptionText() {
            return new String[]{
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.",
            };
        }

        @Override
        protected String getButtonLeftText() {
            return null;
        }

        @Override
        protected String getButtonRightText() {
            return "OK";
        }

    }

    class DemoDialog2 extends GCInfoDialogDemoImpl {

        public DemoDialog2() {
            super(layer);
        }

        @Override
        protected String getTitleText() {
            return "Demo Dialog 2";
        }

        @Override
        protected String[] getCaptionText() {
            return new String[]{
                "Lorem ipsum dolor sit amet",
                "Aenean massa. Cum sociis natoque penatibus",
                "Donec quam felis, ultricies nec,",
                "Donec pede justo, fringilla",
            };
        }

        @Override
        protected String getButtonLeftText() {
            return "Accept";
        }

        @Override
        protected String getButtonRightText() {
            return null;
        }

    }

    class DemoDialog3 extends GCInfoDialogDemoImpl {

        public DemoDialog3() {
            super(layer);
        }

        @Override
        protected String getTitleText() {
            return "Demo Dialog 3";
        }

        @Override
        protected String[] getCaptionText() {
            return new String[]{
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.",
                "Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
                "Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.",
                "Donec pede justo, fringilla vel, aliquet nec, vulputate"
            };
        }

        @Override
        protected String getButtonLeftText() {
            return "Accept";
        }

        @Override
        protected String getButtonRightText() {
            return "Decline";
        }

    }

    abstract class GCInfoDialogDemoImpl extends GCInfoDialog {

        public GCInfoDialogDemoImpl(H5ELayer layer) {
            super(layer, true);
        }

        @Override
        protected void onLeftButtonPressed() {
            log.info(getTitleText() + ": pressed button " + Strings.inQuotes(getButtonLeftText()));
            done();
        }

        @Override
        protected void onRightButtonPressed() {
            log.info(getTitleText() + ": pressed button " + Strings.inQuotes(getButtonRightText()));
            done();
        }

        @Override
        public void onDismiss() {
            log.info(getTitleText() + ": dialog dismissed");
            done();
        }

        private void done() {
            openDialogs.remove(this);
        }

    }

}
