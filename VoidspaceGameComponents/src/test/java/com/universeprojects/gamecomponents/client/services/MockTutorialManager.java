package com.universeprojects.gamecomponents.client.services;

import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.gamecomponents.client.tutorial.TutorialManager;

import java.util.Collections;
import java.util.List;

public class MockTutorialManager implements TutorialManager {

    @Override
    public void addCompletedStage(String stageId) {

    }

    @Override
    public void requestCompletedStages(Callable1Args<Boolean> toggleCallback, Callable1Args<List<String>> stageCallback) {
        toggleCallback.call(false);
        stageCallback.call(Collections.emptyList());
    }

    @Override
    public void setTutorialToggleStatus(boolean status) {

    }

    @Override
    public void resetCompletedStages(Callable0Args callable) {

    }
}
