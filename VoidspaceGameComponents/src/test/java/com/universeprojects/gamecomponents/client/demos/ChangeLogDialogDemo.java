package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCChangeLogDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class ChangeLogDialogDemo extends Demo {
    private final GCChangeLogDialog settings;

    public ChangeLogDialogDemo(H5ELayer layer) {
        settings = new GCChangeLogDialog(layer);
    }

    @Override
    public void open() {
        settings.open();
    }

    @Override
    public void close() {
        settings.close();
    }

    @Override
    public boolean isOpen() {
        return settings.isOpen();
    }
}
