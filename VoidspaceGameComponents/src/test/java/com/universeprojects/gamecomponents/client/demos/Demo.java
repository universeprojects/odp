package com.universeprojects.gamecomponents.client.demos;

public abstract class Demo {

    public abstract void open();

    public abstract void close();

    public abstract boolean isOpen();

    public final void toggle() {
        if (isOpen())
            close();
        else
            open();
    }

    public void animate() {

    }


}
