package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCColorChooser;
import com.universeprojects.gamecomponents.client.windows.GCWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class ColorChooserDemo extends Demo {

    private final GCWindow window;
    private final GCColorChooser colorChooser;

    public ColorChooserDemo(H5ELayer layer) {
        window = new GCWindow(layer, 300, 200);
        colorChooser = new GCColorChooserImpl(layer);
        window.add(colorChooser).grow();
        window.positionProportionally(0.55f, 0.35f);
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

    static class GCColorChooserImpl extends GCColorChooser {
        public GCColorChooserImpl(H5ELayer layer) {
            super(layer, 10, 60, 120);
        }
    }

}
