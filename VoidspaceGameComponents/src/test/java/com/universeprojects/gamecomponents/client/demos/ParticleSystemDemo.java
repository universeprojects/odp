package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.gamecomponents.client.windows.Alignment;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.gamecomponents.client.windows.Position;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EInputBox;
import com.universeprojects.html5engine.client.framework.uicomponents.H5ELabel;

public class ParticleSystemDemo extends Demo {

    private final Logger log = Logger.getLogger(ParticleSystemDemo.class);

    private final H5ELayer layer;
    private final GCSimpleWindow window;
    private H5ELabel emitterLabel;
    private H5EInputBox inputTicksPerParticle, inputLifetimeSeconds, inputDrag, inputRotationDrag, inputFadeoutBeginSeconds, inputBlendingMode, inputAnimatedSpriteType, inputScale,
            inputVelocityMin, inputVelocityMax,
            inputVelocityDirectionMin, inputVelocityDirectionMax,
            inputRotationVelocityMin, inputRotationVelocityMax;
    private float scale = 1f;
    private float ticksPerParticle = 1;
    private float lifetimeSeconds = 1.6f;
    private float drag = 0.5f;
    private float rotationDrag = 0.5f;
    private float fadeoutBeginSeconds = 0.5f;
    private float velocityMin = 1000f;
    private float velocityMax = 1200f;
    private float velocityDirectionMin = 80;
    private float velocityDirectionMax = 110;
    private float rotationVelocityMin = 80;
    private float rotationVelocityMax = 110;
    private String blendingMode = "LIGHTER";
    private String animatedSpriteType = "Explosion1-short";
    public ParticleSystemDemo(H5ELayer layer) {
        this.layer = layer;

        window = new GCSimpleWindow(layer, "switch-demo", "Particle System Editor");
        window.positionProportionally(.5f, .5f);
        window.setY(150);

        window.addH2("Ticks per particle:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputTicksPerParticle = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputTicksPerParticle.setText(ticksPerParticle + "");
        inputTicksPerParticle.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Lifetime seconds:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputLifetimeSeconds = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputLifetimeSeconds.setText(lifetimeSeconds + "");
        inputLifetimeSeconds.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Drag:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputDrag = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputDrag.setText(drag + "");
        inputDrag.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Rotation drag:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputRotationDrag = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputRotationDrag.setText(rotationDrag + "");
        inputRotationDrag.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Fadeout begin seconds:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputFadeoutBeginSeconds = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputFadeoutBeginSeconds.setText(fadeoutBeginSeconds + "");
        inputFadeoutBeginSeconds.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Blending mode:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputBlendingMode = window.addInputBox(100, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputBlendingMode.setText(blendingMode);
        inputBlendingMode.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Animated sprite type:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputAnimatedSpriteType = window.addInputBox(150, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputAnimatedSpriteType.setText(animatedSpriteType);
        inputAnimatedSpriteType.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Scale:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputScale = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputScale.setText(scale + "");
        inputScale.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Velocity Min:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputVelocityMin = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputVelocityMin.setText(velocityMin + "");
        inputVelocityMin.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Velocity Max:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputVelocityMax = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputVelocityMax.setText(velocityMax + "");
        inputVelocityMax.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });


        window.addH2("Velocity Direction Min:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputVelocityDirectionMin = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputVelocityDirectionMin.setText(velocityDirectionMin + "");
        inputVelocityDirectionMin.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Velocity Direction Max:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputVelocityDirectionMax = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputVelocityDirectionMax.setText(velocityDirectionMax + "");
        inputVelocityDirectionMax.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });


        window.addH2("Rotation Velocity Min:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputRotationVelocityMin = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputRotationVelocityMin.setText(rotationVelocityMin + "");
        inputRotationVelocityMin.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });

        window.addH2("Rotation Velocity Max:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
        inputRotationVelocityMax = window.addInputBox(50, Position.SAME_LINE, Alignment.RIGHT, 0, 0);
        inputRotationVelocityMax.setText(rotationVelocityMax + "");
        inputRotationVelocityMax.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                handleInput((H5EInputBox) actor);
            }
        });


        // This functions as the emitter
        this.emitterLabel = new H5ELabel(layer);
        layer.addActorToTop(this.emitterLabel);
        emitterLabel.setText("Emitter");
        emitterLabel.setX(800);
        emitterLabel.setY(400);
        emitterLabel.setVisible(false);


        // This is the emitter stuff
        updateEmitter();
        close();
    }

    @Override
    public void open() {
        window.open();
        emitterLabel.setVisible(true);
    }

    @Override
    public void close() {
        window.close();
        emitterLabel.setVisible(false);
    }

    @Override
    public boolean isOpen() {
        return emitterLabel.isVisible();
    }

    @Override
    public void animate() {
    }

    public void handleInput(H5EInputBox inputBox) {
        try {
            inputBox.setColor(Color.valueOf("#FFFFFF"));
            blendingMode = inputBlendingMode.getText();
            if (inputBox == inputTicksPerParticle) {
                String val = inputBox.getText();
                float value = Float.parseFloat(val);
                if (value > 0) {
                    ticksPerParticle = value;
                }
            }
            if (inputBox == inputAnimatedSpriteType) {
                String val = inputBox.getText();
                if (layer.getEngine().getResourceManager().getAnimatedSpriteType(val) == null)
                    throw new RuntimeException();
                animatedSpriteType = val;
            }
            if (inputBox == inputBlendingMode) {
                String val = inputBox.getText();
                inputTicksPerParticle.setColor(Color.valueOf("#FFFFFF"));
                ticksPerParticle = Float.parseFloat(val);
            }

            if (inputBox == inputDrag) {
                String val = inputBox.getText();
                drag = Float.parseFloat(val);
            }

            if (inputBox == inputRotationDrag) {
                String val = inputBox.getText();
                rotationDrag = Float.parseFloat(val);
            }

            if (inputBox == inputFadeoutBeginSeconds) {
                String val = inputBox.getText();
                fadeoutBeginSeconds = Float.parseFloat(val);
            }

            if (inputBox == inputScale) {
                String val = inputBox.getText();
                scale = Float.parseFloat(val);
            }

            if (inputBox == inputVelocityMin) {
                String val = inputBox.getText();
                velocityMin = Float.parseFloat(val);
            }

            if (inputBox == inputVelocityMax) {
                String val = inputBox.getText();
                velocityMax = Float.parseFloat(val);
            }

            if (inputBox == inputVelocityDirectionMin) {
                String val = inputBox.getText();
                velocityDirectionMin = Float.parseFloat(val);
            }

            if (inputBox == inputVelocityDirectionMax) {
                String val = inputBox.getText();
                velocityDirectionMax = Float.parseFloat(val);
            }

            if (inputBox == inputRotationVelocityMin) {
                String val = inputBox.getText();
                rotationVelocityMin = Float.parseFloat(val);
            }

            if (inputBox == inputRotationVelocityMax) {
                String val = inputBox.getText();
                rotationVelocityMax = Float.parseFloat(val);
            }


        } catch (Exception e) {
            inputBox.setColor(Color.valueOf("#FFAAAA"));
        }


        updateEmitter();
    }

    private void updateEmitter() {

    }
}
