package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.html5engine.client.framework.H5ELayer;

public class LoginProcessDemo extends Demo {

    private final DemoLoginManager loginManager;

    public LoginProcessDemo(H5ELayer layer) {
         this.loginManager = new DemoLoginManager(layer);
    }

    @Override
    public void open() {
        loginManager.openLoginScreen();
    }

    @Override
    public void close() {

    }

    @Override
    public boolean isOpen() {
        return false;
    }
}
