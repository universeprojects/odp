package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCDemoCompleteDialog;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class DemoCompleteDialogDemo extends Demo {
    enum StoreTypes {
        IOS,
        Android,
        Steam,
        None
    }
    private int storeIndex = 0;

    private final GCDemoCompleteDialog window;

    public DemoCompleteDialogDemo(H5ELayer layer) {
        window = new GCDemoCompleteDialog(layer, "IOS", null);
        window.positionProportionally(0.55f, 0.35f);
    }

    @Override
    public void open() {
        // Every time the window opens we're going to increase the store index

        window.setStoreType(StoreTypes.values()[storeIndex].toString());
        window.open();

        storeIndex++;
        if (storeIndex>=StoreTypes.values().length)
            storeIndex = 0;
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }
}
