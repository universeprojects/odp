package com.universeprojects.gamecomponents.client;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.common.KeyUINavigation;
import com.universeprojects.gamecomponents.client.demos.*;
import com.universeprojects.gamecomponents.client.dialogs.GCBookCreationDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCCharacterSwitchDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCGameRatingDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCItemCategoriesDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCReviewDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCSpawnPointSelectionDialog;
import com.universeprojects.gamecomponents.client.dialogs.WalletTransactionsDemo;
import com.universeprojects.gamecomponents.client.dialogs.battleRoyaleLobby.BattleRoyaleLobby;
import com.universeprojects.gamecomponents.client.tutorial.TutorialController;
import com.universeprojects.gamecomponents.client.tutorial.TutorialSystemState;
import com.universeprojects.gamecomponents.client.tutorial.Tutorials;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EButton;
import com.universeprojects.vsdata.shared.BookData;
import com.universeprojects.vsdata.shared.CharacterData;
import com.universeprojects.vsdata.shared.OrganizationReviewData;
import com.universeprojects.vsdata.shared.RatingClientInfo;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("CommentedOutCode")
class MainMenu {

    private final H5ELayer layer;
    private final GCSimpleWindow window;

    private final List<Demo> demos = new ArrayList<>();
    private final List<Demo> autoOpenDemos = new ArrayList<>();

    private int demoCounter;

    //test for editing review
    private OrganizationReviewData editableData;
    //test for spawn point selection window callback
    private final GCSpawnPointSelectionDialog spawnPointSelectionDemo;
    //test for characterSelection
    private GCCharacterSwitchDialog characterSelectionDemo;
    //test for book dialog
    private BookData bookData;

    MainMenu(H5ELayer layer, TutorialController tutorialController) {
        this.layer = Dev.checkNotNull(layer);
        // setup animation event
        layer.addAction(new Action() {
            @Override
            public boolean act(float delta) {
                for (Demo demo : demos) {
                    if (demo.isOpen()) {
                        demo.animate();
                    }
                }
                return false;
            }
        });

        window = new GCSimpleWindow(layer, "main-menu", "Main Menu")
        {{
            navigation=new KeyUINavigation(layer);
            navigation.setWindow(this,this);
        }};
        H5EButton btnAllDemos = addButton("Open all UI demos");
        btnAllDemos.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                for (Demo demo : demos) {
                    if (!demo.isOpen()) {
                        demo.open();
                    }
                }
            }
        });
        window.addEmptyLine(15);
        demoCounter = 0;

//        window.addLabel("Miscellaneous:", Position.NEW_LINE, Alignment.LEFT, 0, 0);
//        addDemo(false, "Benchmarks", new BenchmarksDemo());
//        addDemo(false, "Sound", new SoundDemo(layer));

        window.addLabel("Basic building blocks:");
        addDemo("Labels", new LabelsDemo(layer));
        addDemo("Level Gauges", new LevelGaugeDemo(layer));
        addDemo("Sliders", new SliderDemo(layer));
        addDemo("Switches", new SwitchDemo(layer));
        addDemo("Color Chooser", new ColorChooserDemo(layer));
        addDemo("Action Progress Bar", new ActionProgressBarDemo(layer));
        addDemo("Action Bar", new ActionBarDemo(layer));
        addDemo("Scrollable Pane", new ScrollablePaneDemo(layer));
        addDemo("Composite Sprite Demo", new CompositeSpriteDemo(layer));
        addDemo("Particle System Demo", new ParticleSystemDemo(layer));
        //addDemo("Rate and Review Demo",new ReviewDemo(layer));
        //creating review
        //#test for callback
        OrganizationReviewData readyData = new OrganizationReviewData(0, "Some organization", "", 0);
        editableData = new OrganizationReviewData(0, "Some organization", "", 0);
        GCReviewDialog reviewDemo = new GCReviewDialog(layer, readyData, data -> {
            //some callback related to creation of review
            System.err.println(data.toString());
            //changing editable review to new
            editableData = data;
        });
        addDemoButton("Review Organization", () -> reviewDemo.openAndSet(readyData));

        GCReviewDialog editReview = new GCReviewDialog(layer, readyData, data -> {
            //some callback related to editing review
            System.err.println(data.toString());
            //changing editable review to updated
            editableData = data;
        });
        //editing review
        addDemoButton("Edit organization review", () -> editReview.openAndSet(editableData));
        //#endtest

//        addDemo("Cached container", new CachedContainerDemo(layer));
//        addDemo("Composite Sprite", new CompositeSpriteDemo(layer));

        window.addEmptyLine(15);
        demoCounter = 0;

        window.addLabel("Voidspace dialog windows:");
        addDemo("Info Dialog", new InfoDialogDemo(layer));
        addDemo("Selection Menu", new SelectionMenuDemo(layer));
        addDemo("Inventory V2", new InventoryV2Demo(layer));
        addDemo("Stack Splitter", new StackSplitterDemo(layer));
        addDemo("Trade Dialog", new TradeDialogDemo(layer)); //not in use
//       addDemo("Group Viewer", new GroupViewerDemo(layer)); //not in use
        addDemo("Energy System Control", new EnergySystemControlDemo(layer));
        addDemo("Invention System Dialog", new InventionSystemDemo(layer));
        addDemo("Blending Modes", new BlendingModeDemo(layer));
        addDemo("Login Process Demo", new LoginProcessDemo(layer));
        addDemo("Quest Dialog", new QuestDialogDemo(layer));
        addDemo("Store Dialog", new StoreDialogDemo(layer));
        addDemo("Store Manager Dialog", new StoreManagerDialogDemo(layer));

        BattleRoyaleLobby brlobby = new BattleRoyaleLobby(layer);
        addDemoButton("Battle Royale Lobby", brlobby::open);

        addDemoButton("Enable Tutorial System", () -> tutorialController.setSystemState(TutorialSystemState.ENABLED));

        addDemo("Customize Item", new CustomizeItemDemo(layer));
        addDemo("Purchased Products", new PurchasedProductBrowserDemo(layer));

        // Popups
        addDemo("New Idea Popup", new PopupNewDiscoveryDemo(layer));

        //Spawn point selection menu
        spawnPointSelectionDemo = new GCSpawnPointSelectionDialog(layer,
                (data) -> System.err.println("Selected:" + data.name),
                () -> {
                    characterSelectionDemo = new GCCharacterSwitchDialog(layer, "Select characters", "Some description hereeeeeeeeeeeeeeeeeeeeeee", "Done", (dataList) -> {
                    }, () -> {
                    });
                    List<CharacterData> dataList = new ArrayList<>();
                    dataList.add(new CharacterData("1", "Ilon Musk", "chickchilinka"));
                    dataList.add(new CharacterData("2", "Steve Jobs", "chickchilinka"));
                    dataList.add(new CharacterData("3", "Bill Gates", "chickchilinka"));
                    characterSelectionDemo.processData(dataList);
                    characterSelectionDemo.open();
                    System.err.println("Switch character call");
                });
        addDemoButton("Spawnpoint selection", () -> {
            List<SpawnOptionData> dataList = new ArrayList<>();
            SpawnOptionData data1 = new SpawnOptionData("0", "Some point#1", "short description", "images/bases/survival/survival-silo-with-dock1.png", null);
            SpawnOptionData data2 = new SpawnOptionData("0", "Some point#2", "looooooooooooooooooooooooooooooong description", "images/drones/drone1.png", null);
            SpawnOptionData data3 = new SpawnOptionData("0", "Some point#3", "very very very very very very very very very looooooooooooooooooooooooooooooong description", "images/explosion/explosion_10002.png", null);
            SpawnOptionData data4 = new SpawnOptionData("0", "Some point#4", "short description", "images/bases/Shipyard1.png", null);
            dataList.add(data1);
            dataList.add(data2);
            dataList.add(data3);
            dataList.add(data4);
            spawnPointSelectionDemo.processData(dataList);
            spawnPointSelectionDemo.open();
        });
        GCItemCategoriesDialog categoriesDialogDemo = new GCItemCategoriesDialog(layer, (data) -> {
            for (int i = 0; i < data.size(); i++) {
                System.err.println(data.get(i).name);
            }
        });
        addDemoButton("Category item selection demo", () -> {
            categoriesDialogDemo.demoWork();
            categoriesDialogDemo.open(false);
        });

        addDemo("Book creation", new BookCreationDemo(layer, BookCreationDemo.Mode.Creator));
        addDemo("Book reader", new BookCreationDemo(layer, BookCreationDemo.Mode.Reader));
        addDemo("Book editor", new BookCreationDemo(layer, BookCreationDemo.Mode.Editor));

        GCGameRatingDialog ratingDialog = new GCGameRatingDialog(layer, new RatingClientInfo("Steam", true),
            (submitData) -> {
                System.err.println("Submitted");
                System.err.println("Rating:" + submitData.rating);
                System.err.println("Never show:" + submitData.neverShowAgain);
            },
            () -> System.err.println("Sure!"),// on 4-5 stars and sure click
            (problemData) -> System.err.println("Problem submitted:" + problemData.text)
        );
        addDemoButton("Rate app dialog", ratingDialog::open);
        addDemo("Transactions", new WalletTransactionsDemo(layer));
        StationDialogDemo stationDialog=new StationDialogDemo(layer);
        addDemo("Station dialog",stationDialog );
        addDemo("Map markers", new MapMarkersDemo(layer));

        window.pack();
        window.positionProportionally(.2f, 0.5f);
        window.open();
    }

    private Demo addDemo(String caption, final Demo demo) {
        return addDemo(false, caption, demo);
    }

    private Demo addDemo(boolean autoOpen, String caption, final Demo demo) {
        demos.add(Dev.checkNotNull(demo));
        if (autoOpen) {
            this.autoOpenDemos.add(demo);
        }
        Runnable callback = demo::toggle;
        addDemoButton(caption, callback);

        return demo;
    }

    private void addDemoButton(String caption, Runnable callback) {
        demoCounter++;
        if (demoCounter % 3 == 1) {
            window.row();
        }
        H5EButton btn = addButton(caption);
        btn.setUserObject("button:menu:" + caption);
        btn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                callback.run();
                Tutorials.trigger("button:menu:" + caption);
            }
        });
        //        Cell<H5EButton> cell = window.getWindow().getCell(btn);
        //        cell.uniform();
    }

    private H5EButton addButton(String caption) {
        H5EButton btn = new H5EButton(caption, layer);
//        btn.getLabel().setFontScale(1);
        return window.add(btn).growX().getActor();
    }

    public void open() {
        window.open();
        for (Demo demo : autoOpenDemos) {
            demo.open();
        }
    }

}
