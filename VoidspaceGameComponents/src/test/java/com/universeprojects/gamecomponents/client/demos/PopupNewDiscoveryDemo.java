package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.inventory.GCPopupNewDiscovery;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class PopupNewDiscoveryDemo extends Demo {

    private final GCPopupNewDiscovery inspector;

    public PopupNewDiscoveryDemo(H5ELayer layer) {
        inspector = new GCPopupNewDiscovery(layer,
                "New Idea",
                "images/ships/dirty/dirty-class1a-icon.png",
//                "images/ships/oldworld/old-ship-wreck1.png",
                "Build Dirty Ship - Class 1",
                "You have an idea to build a small ship. You think you can do it using the workstation that " +
                        "survived the trip into the void. Your idea is to make a simple ship that does everything 'good enough'.");
        inspector.positionProportionally(0.5f, 0.7f);
    }

    @Override
    public void open() {
        inspector.open();
    }

    @Override
    public void close() {
        inspector.close();
    }

    @Override
    public boolean isOpen() {
        return inspector.isOpen();
    }
}
