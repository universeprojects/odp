package com.universeprojects.gamecomponents.client.demos;


import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.utils.Timer;
import com.universeprojects.gamecomponents.client.dialogs.GCBookCreationDialog;
import com.universeprojects.gamecomponents.client.dialogs.GCMediaTextArea;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeaCategoryData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCIdeasData;
import com.universeprojects.gamecomponents.client.dialogs.invention.GCSkillsData;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.resourceloader.AtlasResourceLoader;
import com.universeprojects.vsdata.shared.BookData;
import com.universeprojects.vsdata.shared.BookEditableMode;
import com.universeprojects.vsdata.shared.MapMarkerData;
import com.universeprojects.vsdata.shared.MapMarkerGroupData;

import java.util.ArrayList;
import java.util.List;

public class BookCreationDemo extends Demo {


    public enum Mode {
        Creator, Reader, Editor
    }

    private GCBookCreationDialog dialog;
    private Mode mode;
    private static BookData bookData;

    public BookCreationDemo(H5ELayer layer, Mode mode) {
        super();
        this.mode = mode;
        if (mode == Mode.Creator)
            dialog = new GCBookCreationDialog(layer, "Book creation", new GCMediaTextArea(layer), (data) -> {
                bookData = data;
            });
        else if (mode == Mode.Reader)
            dialog = new GCBookCreationDialog(layer, "Book reader", new GCMediaTextArea(layer));
        else
            dialog = new GCBookCreationDialog(layer, "Book editor", new GCMediaTextArea(layer), (data) -> {
                bookData = data;
            });
        dialog.setListeners((idea) -> {
            System.err.println("Idea learn");
        }, (skill) -> {
            System.err.println("Skill learn");
        }, (marker) -> {
            System.err.println("Marker learn");
        },(ideas, skills, markers)->{
            System.err.println("Learn all");
        });
    }

    public void createItems() {
        GCSkillsData skillsData = GCSkillsData.createNew()
                .add(100L, 0L, "Timed-activation Mine", "An explosive device with an activation timer", AtlasResourceLoader.UNKNOWN_ICON_KEY, 0L, 5L, Color.WHITE)
                .add(101L, 0L, "Some item 1", "Some description 1", AtlasResourceLoader.UNKNOWN_ICON_KEY, 1L, 8L, Color.BLUE)
                .add(102L, 0L, "Some item 2", "Some description 2", AtlasResourceLoader.UNKNOWN_ICON_KEY, 3L, 8L, Color.BLUE)
                .add(103L, 0L, "Some item 3", "Some description 3", AtlasResourceLoader.UNKNOWN_ICON_KEY, 3L, 8L, Color.BLUE);
        GCIdeaCategoryData categoryData = new GCIdeaCategoryData();
        categoryData.children.add(categoryData.add(1, "Some cat1", false, ""));
        GCIdeaCategoryData.GCIdeaCategoryDataItem catItem = categoryData.add(2, "Some cat2", false, "");
        catItem.children.add(catItem.add(3, "Some cat3", false, ""));
        categoryData.children.add(catItem);

        GCIdeasData ideasData = GCIdeasData.createNew()
                .add(100L, "Timed-activation Mine", "An explosive device with an activation timer", AtlasResourceLoader.UNKNOWN_ICON_KEY, 1L, 0L, 5L)
                .add(101L, "Proximity Mine", "An explosive device with a proximity sensor to trigger the explosion", AtlasResourceLoader.UNKNOWN_ICON_KEY, 3L, 0L, 5L)
                .add(102L, "Bozo Mine", "An explosive device with a speaker, that plays a circus song for 5 seconds before the explosion is triggered", AtlasResourceLoader.UNKNOWN_ICON_KEY, 3L, 0L, 5L);

        MapMarkerGroupData group1 = new MapMarkerGroupData("group1", "Group 1", new ArrayList<MapMarkerData>());
        MapMarkerGroupData group2 = new MapMarkerGroupData("group2", "Group 2", new ArrayList<MapMarkerData>());
        group1.markers.add(new MapMarkerData("some id", "Marker 1", "icons/copper-coil1.png", "some description"));
        group1.markers.add(new MapMarkerData("some id", "Marker 2", "null icon"));
        group1.markers.add(new MapMarkerData("some id", "Marker 3", "null icon"));
        group2.markers.add(new MapMarkerData("some id", "Marker 4", "null icon"));
        group2.markers.add(new MapMarkerData("some id", "Marker 5", "null icon"));
        List<MapMarkerGroupData> groups = new ArrayList<>();
        groups.add(group1);
        groups.add(group2);


        dialog.loadItems(skillsData, categoryData);
        dialog.loadIdeas(ideasData, categoryData);
        dialog.loadMarkers(groups);
    }

    @Override
    public void close() {
        dialog.close();
    }

    @Override
    public boolean isOpen() {
        return dialog.isOpen();
    }

    @Override
    public void open() {
        createItems();
        if (mode == Mode.Creator) {
            dialog.open();
            dialog.hideLoadingIndicator();
            dialog.newBook();
        } else if (mode == Mode.Reader) {
            if (bookData != null) {
                bookData.editable = false;
                dialog.open(false);
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        dialog.processBook(bookData);
                    }
                }, 1f);

            }
        } else {
            if (bookData != null) {
                bookData.editable = true;
                bookData.editableMode = BookEditableMode.EVERYONE;
                dialog.clearForm();
                dialog.open(false);
                Timer.schedule(new Timer.Task() {
                    @Override
                    public void run() {
                        dialog.processBook(bookData);
                    }
                }, 1f);
            }
        }

    }
}
