package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.gamecomponents.client.components.GCGenericINDListItemData;
import com.universeprojects.gamecomponents.client.dialogs.GCCategoryItemSplitComponent;
import com.universeprojects.gamecomponents.client.windows.GCSimpleWindow;
import com.universeprojects.html5engine.client.framework.H5ELayer;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class PurchasedProductBrowserDemo extends Demo {

    private final H5ELayer layer;
    private final GCSimpleWindow window;
    private final GCCategoryItemSplitComponent<Void> component;

    public PurchasedProductBrowserDemo(H5ELayer layer) {
        this.layer = Dev.checkNotNull(layer);

        Map<String, Object> data = new LinkedHashMap<String, Object>(){{
            put("Accounts", new ArrayList<GCGenericINDListItemData<Void>>(){{
                add(new GCGenericINDListItemData<>( "Premium Membership for Gifting", "https://i.imgur.com/v3HqseQ.png", "Gifting", 2, null));
            }});
            put("Ships", new LinkedHashMap<String, Object>(){{
                put("Class 1", new ArrayList<GCGenericINDListItemData<Void>>(){{
                    add(new GCGenericINDListItemData<>( "Blade Ship", "ships/blade/blade-ship-class1a.png", "Cosmetic", 1, null));
                }});
                put("Class 2", new ArrayList<GCGenericINDListItemData<Void>>(){{
                    add(new GCGenericINDListItemData<>( "Blade Ship", "ships/blade/blade-ship-class2a.png", "Cosmetic", 5, null));
                }});
            }});
        }};

        component = new GCCategoryItemSplitComponent<>(this.layer);
        component.setData(data);
        window = new GCSimpleWindow(layer, "product-store-demo"," Product Store Demo");
        window.add(component).grow();
        window.setFullscreenOnMobile(true);
        window.positionProportionally(0.5f, 0.5f);
    }

    @Override
    public void open() {
        window.open();
    }

    @Override
    public void close() {
        window.close();
    }

    @Override
    public boolean isOpen() {
        return window.isOpen();
    }

}
