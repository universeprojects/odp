package com.universeprojects.gamecomponents.client.demos;

import com.universeprojects.gamecomponents.client.dialogs.GCCustomizationDialog;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCInventoryItem;
import com.universeprojects.gamecomponents.client.dialogs.inventory.GCSimpleInventoryItemLike;
import com.universeprojects.gamecomponents.client.dialogs.inventory.Rarity;
import com.universeprojects.html5engine.client.framework.H5ELayer;

public class CustomizeItemDemo extends Demo {

    private final GCCustomizationDialog<Long, GCInventoryItem> dialog;

    public CustomizeItemDemo(H5ELayer layer) {
        GCSimpleInventoryItemLike skillExample = new GCSimpleInventoryItemLike(
                null,
                "Create Clay from Fine Dust",
                "images/itemsprites/clay1.png",
                "Skill",
                Rarity.NONE);

        dialog = new GCCustomizationDialog<>(layer, "Customize Skill", skillExample,
            (selectedItem) -> {
                //Yay?
            });
        dialog.addItem("(Class 1) Viper Skin", null, "Changes the icon, and the in-game graphic of a ship. Weapon, accessory, and engine exhaust mount positions will also change.", 1, 123L);
        dialog.addItem("(Class 1) Tesla Skin", null, "Changes the icon, and the in-game graphic of a ship. Weapon, accessory, and engine exhaust mount positions will also change.", 1, 123L);
        dialog.addItem("(Class 1) Millenium Falcon Skin", null, "Changes the icon, and the in-game graphic of a ship. Weapon, accessory, and engine exhaust mount positions will also change.", 1, 123L);
        dialog.addItem("(Class 1) Ipsum loria long name of a skin skin", null, "Changes the icon, and the in-game graphic of a ship. Weapon, accessory, and engine exhaust mount positions will also change.", 1, 123L);
        dialog.positionProportionally(0.5f, 0.5f);
    }

    @Override
    public void open() {
        dialog.open();
    }

    @Override
    public void close() {
        dialog.close();
    }

    @Override
    public boolean isOpen() {
        return dialog.isOpen();
    }

}
