package com.universeprojects.gamecomponents.client.demos;

import com.badlogic.gdx.graphics.Color;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.gamecomponents.client.auth.AuthCredentials;
import com.universeprojects.gamecomponents.client.dialogs.login.CharacterInfo;
import com.universeprojects.gamecomponents.client.dialogs.login.GCLoginManager;
import com.universeprojects.gamecomponents.client.dialogs.login.GCSpawnSelectDialog;
import com.universeprojects.gamecomponents.client.dialogs.login.LoginError;
import com.universeprojects.gamecomponents.client.dialogs.login.LoginInfo;
import com.universeprojects.gamecomponents.client.dialogs.login.ServerInfo;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.json.shared.JSONObject;
import com.universeprojects.vsdata.shared.SpawnOptionData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DemoLoginManager extends GCLoginManager<DemoLoginManager.DemoLoginInfo, DemoLoginManager.DemoServerInfo, DemoLoginManager.DemoCharacterInfo> {
    public DemoLoginManager(H5ELayer layer) {
        super(layer);
    }

    @Override
    protected boolean showAdminControls() {
        return true;
    }

    @Override
    public void startSingleplayer() {

    }

    @Override
    protected DemoLoginInfo getSavedToken() {
        return null;
    }

    @Override
    protected void setSavedToken(DemoLoginInfo token) {

    }

    @Override
    protected void verify(DemoLoginInfo token, Callable1Args<DemoLoginInfo> successCallback, Callable1Args<LoginError> errorCallback) {
        successCallback.call(new DemoLoginInfo());
    }

    @Override
    protected void login(AuthCredentials credentials, Callable1Args<DemoLoginInfo> successCallback, Callable1Args<LoginError> errorCallback) {
        successCallback.call(new DemoLoginInfo());
    }

    @Override
    protected void register(String email, String password, String userName, Callable1Args<DemoLoginInfo> successCallback, Callable1Args<LoginError> errorCallback) {
        successCallback.call(new DemoLoginInfo());
    }

    @Override
    protected void loadPrivacyPolicy(Callable1Args<String> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call("PRIVACY");
    }

    @Override
    protected void loadTermsOfService(Callable1Args<String> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call("TOS");
    }

    @Override
    protected void loadServers(DemoLoginInfo loginInfo, Callable1Args<List<DemoServerInfo>> successCallback, Callable1Args<String> errorCallback) {
        List<DemoServerInfo> list = new ArrayList<>();
        for(int i=1;i<=20;i++) {
            list.add(new DemoServerInfo("Server "+i));
        }
        successCallback.call(list);
    }

    @Override
    public void onServerChosen(DemoServerInfo serverInfo, Callable1Args<DemoServerInfo> onDone) {
        onDone.call(serverInfo);
    }

    @Override
    protected void findRealmServer(DemoLoginInfo loginInfo, Callable2Args<DemoServerInfo, Boolean> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(new DemoServerInfo("Server Battle Royale"), true);
    }

    @Override
    protected void pingServer(DemoServerInfo serverInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void loadGameCharacters(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, Callable1Args<List<DemoCharacterInfo>> successCallback, Callable1Args<String> errorCallback) {
        List<DemoCharacterInfo> list = new ArrayList<>();
        for(int i=1;i<=5;i++) {
            list.add(new DemoCharacterInfo("InGame "+i));
        }
        successCallback.call(list);
    }

    @Override
    protected void loadUserCharacters(DemoLoginInfo loginInfo, Callable1Args<List<String>> successCallback, Callable1Args<String> errorCallback) {
        List<String> list = new ArrayList<>();
        for(int i=1;i<=5;i++) {
            list.add("NotInGame "+i);
        }
        successCallback.call(list);
    }

    @Override
    protected void loadUserFriends(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, Callable1Args<Map<String, Map<String, Long>>> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(new HashMap<>());
    }

    @Override
    protected void loadUserOrganisations(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, Callable1Args<Map<Long, String>> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(new HashMap<>());
    }

    @Override
    protected void deleteCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void joinWithNewCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, String characterName, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void joinWithExistingCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected void joinWithRealmCharacter(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, String characterName, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    public void respawn(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, String spawnType, JSONObject spawnParams, Callable0Args successCallback, Callable1Args<String> errorCallback) {
        successCallback.call();
    }

    @Override
    protected GCSpawnSelectDialog.GCSpawnSelectData getDefaultSpawnConfigs() {
        return new GCSpawnSelectDialog.GCSpawnSelectData()
            .add(new SpawnOptionData( "FAR_OUT", "Start in the middle of nowhere", "Your character begins very far from any known players. This option offers the lowest chance of ever running into another player.", "foo.png"))
            .add(new SpawnOptionData( "FAR_CIV", "Start far from civilization", "Your character begins a long distance from any known player settlements but the odds of seeing another player is pretty good.", "foo.png"))
            .add(new SpawnOptionData( "NEAR_CIV", "Start close to civilization", "Your character begins quite close to a known player settlement. You should be able to find them without having to search for too long.", "foo.png"))
            .add(new SpawnOptionData( "FRIEND", "Join a friend", "This option allows your to spawn in a friend's base or ship. Your friend must first add you to their party for this option to work.", "foo.png"));
    }

    @Override
    public void backToCharacterSelection() {

    }

    @Override
    protected void loadRespawnOptions(DemoLoginInfo loginInfo, DemoServerInfo serverInfo, DemoCharacterInfo characterInfo, Callable1Args<List<SpawnOptionData>> successCallback, Callable1Args<String> errorCallback) {
        successCallback.call(Collections.emptyList());
    }

    public static class DemoLoginInfo implements LoginInfo {

    }

    public static class DemoServerInfo implements ServerInfo {

        private final String name;

        public DemoServerInfo(String name) {
            this.name = name;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public int getPopulation() {
            return 3;
        }

        @Override
        public Integer getMaxPopulation() {
            return 8;
        }

        @Override
        public String getStatusLabel() {
            return null;
        }

        @Override
        public Color getStatusColor() {
            return null;
        }
    }

    public static class DemoCharacterInfo implements CharacterInfo {

        private final String name;

        public DemoCharacterInfo(String name) {
            this.name = name;
        }


        @Override
        public String getName() {
            return name;
        }

        @Override
        public Long getId() {
            return null;
        }
    }
}
