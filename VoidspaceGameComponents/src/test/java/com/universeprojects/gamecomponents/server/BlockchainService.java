package com.universeprojects.gamecomponents.server;

import com.universeprojects.vsdata.shared.CryptoTransactionData;

import java.util.List;

public interface BlockchainService {
    List<CryptoTransactionData> getTransactions(String address);

    List<CryptoTransactionData> getTransactions(String address, int count);
}
