/**
 *
 */
package com.universeprojects.html5engine.shared;

/**
 * @author Crokoking
 */
@SuppressWarnings({"unused"})
public class CollisionManagerTest {
//
//    /**
//     * @throws java.lang.Exception
//     */
//    @BeforeClass
//    public static void setUpBeforeClass() throws Exception {
//    }
//
//    /**
//     * @throws java.lang.Exception
//     */
//    @AfterClass
//    public static void tearDownAfterClass() throws Exception {
//    }
//
//    /**
//     * @throws java.lang.Exception
//     */
//    @Before
//    public void setUp() throws Exception {
//    }
//
//    /**
//     * @throws java.lang.Exception
//     */
//    @After
//    public void tearDown() throws Exception {
//    }
//
//    @Test
//    public void testGetCollisionPointIntIntIntIntSpriteInt() {
//        SpriteType spt = new ServerSpriteType();
//        spt.setAreaWidth(100);
//        spt.setAreaWidth(100);
//        spt.setAreaX(0);
//        spt.setAreaY(0);
//        MarkerRectangle mr = new ServerMarkerRectangle();
//        MarkerCircle mc = new ServerMarkerCircle();
//        MarkerVector mv = new ServerMarkerVector();
//        final int MARKER_Rect = 0;
//        spt.addMarker(mr);
//        final int MARKER_Circ = 1;
//        spt.addMarker(mc);
//        final int MARKER_Vect = 2;
//        spt.addMarker(mv);
//        Sprite sprite = new ServerSprite(spt);
//        sprite.setX(0);
//        sprite.setY(0);
//        sprite.setRotation(0);
//        sprite.setScale(1);
//        sprite.setStretchFactorX(1);
//        sprite.setStretchFactorY(1);
//
//
//        int markerIndex = -1;
//        Vector rayStart = new Vector(0, 0);
//        Vector rayEnd = new Vector(0, 0);
//        Vector result;
//        Vector correctResult = null;
//        Vector correctResultRayFlipped = null;
//
//        //TODO: More Scenarios
//        //Scenarios:(all normal and with the ray flipped):
//        //0 1a)      |
//        //        -------
//        //        |  |  |
//        //        |     |
//        //        -------
//        //
//        //
//        //1 1b)      |
//        //        -------
//        //        |  |  |
//        //        |  |  |
//        //        -------
//        //           |
//        //
//        //2 1c)  \
//        //        -------
//        //        |\    |
//        //        |     |
//        //        -------
//        //
//        //
//        //3 1d)  \
//        //        -------
//        //        |\    |
//        //        | \   |
//        //        -------
//        //            \
//        //
//        //4 2a) like 1a) with sprite 90� rotated
//        //5 2b) like 1b) with sprite 90� rotated
//        //6 2c) like 1c) with sprite 90� rotated
//        //7 2d) like 1d) with sprite 90� rotated
//
//        for (int i = 0; i <= 7; i++) {
//            switch (i) {
//                case 0:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(0);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 0;
//                    mr.y = 4;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(0, -1);
//                    rayEnd.set(0, 2);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = rayEnd.copy();
//                    break;
//                case 1:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(0);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 0;
//                    mr.y = 4;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(0, -1);
//                    rayEnd.set(0, 9);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = new Vector(0, 8);
//                    break;
//                case 2:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(0);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 4;
//                    mr.y = 4;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(-1, -1);
//                    rayEnd.set(1, 1);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = rayEnd.copy();
//                    break;
//                case 3:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(0);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 0;
//                    mr.y = 0;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(-5, -5);
//                    rayEnd.set(5, 5);
//                    correctResult = new Vector(-4, -4);
//                    correctResultRayFlipped = new Vector(4, 4);
//                    break;
//                case 4:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(90);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 4;
//                    mr.y = 0;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(0, -1);
//                    rayEnd.set(0, 2);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = rayEnd.copy();
//                    break;
//                case 5:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(90);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 4;
//                    mr.y = 0;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(0, -1);
//                    rayEnd.set(0, 9);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = new Vector(0, 8);
//                    break;
//                case 6:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(90);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 4;
//                    mr.y = -4;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(-1, -1);
//                    rayEnd.set(1, 1);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = rayEnd.copy();
//                    break;
//                case 7:
//                    sprite.setX(0);
//                    sprite.setY(0);
//                    sprite.setRotation(90);
//                    sprite.setScale(1);
//                    sprite.setStretchFactorX(1);
//                    sprite.setStretchFactorY(1);
//                    markerIndex = MARKER_Rect;
//                    mr.x = 0;
//                    mr.y = 0;
//                    mr.width = 8;
//                    mr.height = 8;
//                    rayStart.set(-5, -5);
//                    rayEnd.set(5, 5);
//                    correctResult = new Vector(-4, -4);
//                    correctResultRayFlipped = new Vector(4, 4);
//                    break;
//            }
//
//            result = CollisionManager.getCollisionPoint((int) rayStart.getX(), (int) rayStart.getY(), (int) rayEnd.getX(), (int) rayEnd.getY(), sprite.getWorldTransformation(), sprite.getSpriteType(), markerIndex);
//            assertTrue("Scenario " + i + " expected:" + correctResult + " but was:" + result, pointsAreEqual(correctResult, result));
//            result = CollisionManager.getCollisionPoint((int) rayEnd.getX(), (int) rayEnd.getY(), (int) rayStart.getX(), (int) rayStart.getY(), sprite.getWorldTransformation(), sprite.getSpriteType(), markerIndex);
//            assertTrue("Scenario " + i + " expected:" + correctResultRayFlipped + " but was:" + result, pointsAreEqual(correctResultRayFlipped, result));
//        }
//    }
//
//    /**
//     * Test method for {@link CollisionManager#getRayLineCollision(Vector, Vector, Vector, Vector)}.
//     */
//    @Test
//    public void testGetRayLineCollisionH5EPointH5EPointH5EPointH5EPoint() {
//        Vector rayStart = new Vector();
//        Vector rayEnd = new Vector();
//        Vector testLineStart = new Vector();
//        Vector testLineEnd = new Vector();
//        boolean correctResult = false;
//        boolean result;
//        //Scenarios(all normal and with lines switched):
//        //0 1a) 90�-angle and intersecting
//        //1 1b) 90�-angle and non-intersecting
//        //2 2a) 45�-angle and intersecting
//        //3 2b) 45�-angle and non-intersecting
//        //4 3a) parallel
//        //5 3b) collinear and intersecting
//        //6 3c) collinear and non-intersecting
//        for (int i = 0; i <= 6; i++) {
//            switch (i) {
//                case 0:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(0, -1);
//                    testLineEnd.set(0, 1);
//                    correctResult = true;
//                    break;
//                case 1:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(0, -3);
//                    testLineEnd.set(0, -1);
//                    correctResult = false;
//                    break;
//                case 2:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-1, -1);
//                    testLineEnd.set(1, 1);
//                    correctResult = true;
//                    break;
//                case 3:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(1, 1);
//                    testLineEnd.set(3, 3);
//                    correctResult = false;
//                    break;
//                case 4:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-1, 1);
//                    testLineEnd.set(1, 1);
//                    correctResult = false;
//                    break;
//                case 5:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-2, 0);
//                    testLineEnd.set(0, 0);
//                    correctResult = true;
//                    break;
//                case 6:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-4, 0);
//                    testLineEnd.set(-2, 0);
//                    correctResult = false;
//                    break;
//            }
//            result = CollisionManager.getRayLineCollision(rayStart, rayEnd, testLineStart, testLineEnd);
//            assertEquals("Scenario " + i, correctResult, result);
//            result = CollisionManager.getRayLineCollision(rayEnd, rayStart, testLineStart, testLineEnd);
//            assertEquals("Scenario " + i + " with ray flipped", correctResult, result);
//            result = CollisionManager.getRayLineCollision(rayStart, rayEnd, testLineEnd, testLineStart);
//            assertEquals("Scenario " + i + " with testLine flipped", correctResult, result);
//            result = CollisionManager.getRayLineCollision(rayEnd, rayStart, testLineEnd, testLineStart);
//            assertEquals("Scenario " + i + " with ray and testLine flipped", correctResult, result);
//            result = CollisionManager.getRayLineCollision(testLineStart, testLineEnd, rayStart, rayEnd);
//            assertEquals("Scenario " + i + " with ray and line switched", correctResult, result);
//        }
//    }
//
//    /**
//     * Test method for {@link CollisionManager#getRayLineCollisionPoint(Vector, Vector, Vector, Vector)}.
//     */
//    @SuppressWarnings("ConstantConditions")
//    @Test
//    public void testGetRayLineCollisionPointH5EPointH5EPointH5EPointH5EPoint() {
//        Vector rayStart = new Vector();
//        Vector rayEnd = new Vector();
//        Vector testLineStart = new Vector();
//        Vector testLineEnd = new Vector();
//        Vector correctResult = null;
//        Vector correctResultRayFlipped = null;
//        Vector correctResultTestLineFlipped = null;
//        Vector correctResultRayAndTestLineFlipped = null;
//        Vector correctResultRayAndTestLineSwitched = null;
//        Vector result;
//        //Scenarios(all normal and with lines switched):
//        //0 1a) 90�-angle and intersecting
//        //1 1b) 90�-angle and non-intersecting
//        //2 2a) 45�-angle and intersecting
//        //3 2b) 45�-angle and non-intersecting
//        //4 3a) parallel
//        //5 3b) collinear and intersecting
//        //6 3c) collinear and non-intersecting
//        for (int i = 0; i <= 6; i++) {
//            switch (i) {
//                case 0:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(0, -1);
//                    testLineEnd.set(0, 1);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//                case 1:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(0, -3);
//                    testLineEnd.set(0, -1);
//                    correctResult = null;
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//                case 2:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-1, -1);
//                    testLineEnd.set(1, 1);
//                    correctResult = new Vector(0, 0);
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//                case 3:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(1, 1);
//                    testLineEnd.set(3, 3);
//                    correctResult = null;
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//                case 4:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-1, 1);
//                    testLineEnd.set(1, 1);
//                    correctResult = null;
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//                case 5:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-2, 0);
//                    testLineEnd.set(0, 0);
//                    correctResult = rayStart.copy();
//                    correctResultRayFlipped = rayEnd.copy();
//                    correctResultTestLineFlipped = rayStart.copy();
//                    correctResultRayAndTestLineFlipped = rayEnd.copy();
//                    correctResultRayAndTestLineSwitched = testLineStart.copy();
//                    break;
//                case 6:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    testLineStart.set(-4, 0);
//                    testLineEnd.set(-2, 0);
//                    correctResult = null;
//                    correctResultRayFlipped = correctResult;
//                    correctResultTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineFlipped = correctResult;
//                    correctResultRayAndTestLineSwitched = correctResult;
//                    break;
//            }
//            result = CollisionManager.getRayLineCollisionPoint(rayStart, rayEnd, testLineStart, testLineEnd);
//            assertTrue("Scenario " + i + " expected:" + correctResult + " but was:" + result, pointsAreEqual(correctResult, result));
//            result = CollisionManager.getRayLineCollisionPoint(rayEnd, rayStart, testLineStart, testLineEnd);
//            assertTrue("Scenario " + i + " with ray flipped" + " expected:" + correctResultRayFlipped + " but was:" + result, pointsAreEqual(correctResultRayFlipped, result));
//            result = CollisionManager.getRayLineCollisionPoint(rayStart, rayEnd, testLineEnd, testLineStart);
//            assertTrue("Scenario " + i + " with testLine flipped" + " expected:" + correctResultTestLineFlipped + " but was:" + result, pointsAreEqual(correctResultTestLineFlipped, result));
//            result = CollisionManager.getRayLineCollisionPoint(rayEnd, rayStart, testLineEnd, testLineStart);
//            assertTrue("Scenario " + i + " with ray and testLine flipped" + " expected:" + correctResultRayAndTestLineFlipped + " but was:" + result, pointsAreEqual(correctResultRayAndTestLineFlipped, result));
//            result = CollisionManager.getRayLineCollisionPoint(testLineStart, testLineEnd, rayStart, rayEnd);
//            assertTrue("Scenario " + i + " with ray and line switched" + " expected:" + correctResultRayAndTestLineSwitched + " but was:" + result, pointsAreEqual(correctResultRayAndTestLineSwitched, result));
//        }
//    }
//
//    @SuppressWarnings("RedundantIfStatement")
//    private boolean pointsAreEqual(Vector p1, Vector p2) {
//        if (p1 == null || p2 == null) {
//            if (p1 == null && p2 == null) {
//                return true;
//            }
//            return false;
//        }
//        return p1.equalWithinEpsilon(p2, CollisionManager.epsilon);
//    }
//
//    /**
//     * Test method for {@link CollisionManager#getRayCircleCollision(Vector, Vector, Vector, double)}.
//     */
//    @Test
//    public void testGetRayCircleCollision() {
//        Vector rayStart = new Vector();
//        Vector rayEnd = new Vector();
//        Vector circleCenter = new Vector();
//        double radius = 0;
//        boolean result;
//        boolean correctResult = false;
//        //Scenarios(lines normal and with end/starting point flipped)
//        //0 1) line out of the circle
//        //1 2a) line tangent to the circle with tangent in the middle
//        //2 2b) line tangent to the circle with one endpoint as tangent-point
//        //3 3a) line intersecting with the circle
//        //4 3b) line intersecting with the circle with one endPoint inside
//        //5 4) line completele inside the circle
//        for (int i = 0; i <= 5; i++) {
//            switch (i) {
//                case 0:
//                    rayStart.set(-2, 1);
//                    rayEnd.set(-1, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = false;
//                    break;
//                case 1:
//                    rayStart.set(-1, 1);
//                    rayEnd.set(1, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = true;
//                    break;
//                case 2:
//                    rayStart.set(-2, 1);
//                    rayEnd.set(0, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = true;
//                    break;
//                case 3:
//                    rayStart.set(-1, 0.2);
//                    rayEnd.set(1, 0.2);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = true;
//                    break;
//                case 4:
//                    rayStart.set(-2, 0.2);
//                    rayEnd.set(-0.5, 0.2);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = true;
//                    break;
//                case 5:
//                    rayStart.set(-0.5, 0.1);
//                    rayEnd.set(0.5, 0.1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = true;
//                    break;
//            }
//            result = CollisionManager.getRayCircleCollision(rayStart, rayEnd, circleCenter, radius);
//            assertEquals("Scenario " + i, correctResult, result);
//            result = CollisionManager.getRayCircleCollision(rayEnd, rayStart, circleCenter, radius);
//            assertEquals("Scenario " + i + " with ray flipped", correctResult, result);
//        }
//
//    }
//
//    /**
//     * Test method for {@link CollisionManager#getRayCircleCollisionPoint(Vector, Vector, Vector, double)}.
//     */
//    @Test
//    public void testGetRayCircleCollisionPoint() {
//        Vector rayStart = new Vector();
//        Vector rayEnd = new Vector();
//        Vector circleCenter = new Vector();
//        double radius = 0;
//        Vector result;
//        Vector correctResult = null;
//        Vector correctResultFlipped = null;
//        //Scenarios(lines normal and with end/starting point flipped)
//        //0 1) line out of the circle
//        //1 2a) line tangent to the circle with tangent in the middle
//        //2 2b) line tangent to the circle with one endpoint as tangent-point
//        //3 3a) line intersecting with the circle
//        //4 3b) line intersecting with the circle with one endPoint inside
//        //5 4) line completele inside the circle
//        for (int i = 0; i <= 5; i++) {
//            switch (i) {
//                case 0:
//                    rayStart.set(-2, 1);
//                    rayEnd.set(-1, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = null;
//                    correctResultFlipped = null;
//                    break;
//                case 1:
//                    rayStart.set(-1, 1);
//                    rayEnd.set(1, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = new Vector(0, 1);
//                    correctResultFlipped = new Vector(0, 1);
//                    break;
//                case 2:
//                    rayStart.set(-2, 1);
//                    rayEnd.set(0, 1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = rayEnd.copy();
//                    correctResultFlipped = rayEnd.copy();
//                    break;
//                case 3:
//                    rayStart.set(-2, 0);
//                    rayEnd.set(2, 0);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = new Vector(-1, 0);
//                    correctResultFlipped = new Vector(1, 0);
//                    break;
//                case 4:
//                    rayStart.set(-2, 0);
//                    rayEnd.set(-0.5, 0);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = new Vector(-1, 0);
//                    correctResultFlipped = rayEnd.copy();
//                    break;
//                case 5:
//                    rayStart.set(-0.5, 0.1);
//                    rayEnd.set(0.5, 0.1);
//                    circleCenter.set(0, 0);
//                    radius = 1;
//                    correctResult = rayStart.copy();
//                    correctResultFlipped = rayEnd.copy();
//                    break;
//            }
//            result = CollisionManager.getRayCircleCollisionPoint(rayStart, rayEnd, circleCenter, radius);
//            assertTrue("Scenario " + i + " expected:" + correctResult + " but was:" + result, pointsAreEqual(correctResult, result));
//            result = CollisionManager.getRayCircleCollisionPoint(rayEnd, rayStart, circleCenter, radius);
//            assertTrue("Scenario " + i + " with ray flipped" + " expected:" + correctResultFlipped + " but was:" + result, pointsAreEqual(correctResultFlipped, result));
//        }
//    }
//
//    /**
//     * Test method for {@link CollisionManager#getRayPointCollision(Vector, Vector, Vector)}.
//     */
//    @Test
//    public void testGetRayPointCollisionH5EPointH5EPointH5EPoint() {
//        Vector rayStart = new Vector();
//        Vector rayEnd = new Vector();
//        Vector point = new Vector();
//        boolean result;
//        boolean correctResult = false;
//        //Scenarios
//        //0 1) Point perpendicular to line
//        //1 2) Point collinear but out of ray
//        //2 3a) Point collinear on one of the ray-points
//        //3 3b) Point collinear in the middle of the ray
//        for (int i = 0; i <= 3; i++) {
//            switch (i) {
//                case 0:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    point.set(0, 1);
//                    correctResult = false;
//                    break;
//                case 1:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    point.set(2, 0);
//                    correctResult = false;
//                    break;
//                case 2:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    point.set(1, 0);
//                    correctResult = true;
//                    break;
//                case 3:
//                    rayStart.set(-1, 0);
//                    rayEnd.set(1, 0);
//                    point.set(0, 0);
//                    correctResult = true;
//                    break;
//            }
//            result = CollisionManager.getRayPointCollision(rayStart, rayEnd, point);
//            assertEquals("Scenario " + i, correctResult, result);
//            result = CollisionManager.getRayPointCollision(rayEnd, rayStart, point);
//            assertEquals("Scenario " + i + " with ray flipped", correctResult, result);
//        }
//    }

}
