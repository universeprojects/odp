package com.universeprojects.html5engine.shared.abstractFramework;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MarkerVectorTest {
    @Test
    public void rotationTest() {
        MarkerVector markerVector;
        //left
        markerVector = new MarkerVector();
        markerVector.deltaX = -10;
        markerVector.deltaY = 0;
        assertEquals(90, (markerVector.getRotation() + 360) % 360, 1);

        //right
        markerVector = new MarkerVector();
        markerVector.deltaX = 10;
        markerVector.deltaY = 0;
        assertEquals(270, (markerVector.getRotation() + 360) % 360, 1);

        //down
        markerVector = new MarkerVector();
        markerVector.deltaX = 0;
        markerVector.deltaY = 10;
        assertEquals(180, (markerVector.getRotation() + 360) % 360, 1);

        //up
        markerVector = new MarkerVector();
        markerVector.deltaX = 0;
        markerVector.deltaY = -10;
        assertEquals(0, (markerVector.getRotation() + 360) % 360, 1);

        markerVector = new MarkerVector();
        markerVector.deltaX = -10;
        markerVector.deltaY = -10;
        assertEquals(45, (markerVector.getRotation() + 360) % 360, 1);
    }
}