package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @author Crokoking
 */
public abstract class AnimatedSpriteTypeFrame {

    /**
     * The scale adjustment that will be applied to the graphic before it is rendered.
     */
    protected float adjustedScale = 0;
    /**
     * The pixel adjustment X that will be applied to the graphic before it is rendered.
     */
    protected int adjustedX = 0;
    /**
     * The pixel adjustment Y that will be applied to the graphic before it is rendered.
     */
    protected int adjustedY = 0;
    /**
     * The duration that this frame will be displayed in milliseconds.
     */
    protected int duration = 0;
    /**
     * A reference to the engine so we can do work inside this class and can be self sufficient
     */
    protected AbstractResourceManager resourceManager = null;

    public float getAdjustedScale() {
        return adjustedScale;
    }

    public int getAdjustedX() {
        return adjustedX;
    }

    public int getAdjustedY() {
        return adjustedY;
    }

    public int getDurationMs() {
        if (duration==0) duration = 50;
        return duration;
    }

    public abstract String getSpriteTypeKey();

    public void setAdjustedScale(float adjustedScale) {
        this.adjustedScale = adjustedScale;
    }

    public void setAdjustedX(int adjustedX) {
        this.adjustedX = adjustedX;
    }

    public void setAdjustedY(int adjustedY) {
        this.adjustedY = adjustedY;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
