package com.universeprojects.html5engine.shared.abstractFramework;

public interface Sprite extends Container {

    /**
     * This method simply retrieves the spriteType being used to draw this sprite.
     *
     * @return The current H5ESpriteType that is being used for this sprite's image data
     */
    SpriteType getSpriteType();

    /**
     * TODO: javadoc
     */
    void setSpriteType(SpriteType spriteType);

    void setAreaAdjustments(float areaXAdjust, float areaYAdjust, float areaWidthAdjust, float areaHeightAdjust);

}