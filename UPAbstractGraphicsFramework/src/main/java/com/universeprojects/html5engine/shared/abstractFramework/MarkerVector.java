package com.universeprojects.html5engine.shared.abstractFramework;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;

/**
 * @author Crokoking
 */
public class MarkerVector extends Marker {

    public static final String markerClassString = "MarkerVector";
    public int deltaX = 20;
    public int deltaY = 20;

    private Float rotation = null;
    private Float length = null;

    public float getLength() {
        if (length == null) {
            length = UPMath.sqrt(deltaX * deltaX + deltaY * deltaY);
        }
        return length;
    }

    public float getRotation() {
        if (rotation == null) {
            Vector2 baseVector = new Vector2(0, 1);
            Vector2 vector = new Vector2(deltaX, -deltaY);
            rotation = baseVector.angle(vector);
        }
        return rotation;
    }

    @Override
    public String getMarkerClassString() {
        return markerClassString;
    }

    // See TODO on parent class
    /*
    @Override
    public Object clone() throws CloneNotSupportedException {
        H5EAbstractMarkerVector mv = (H5EAbstractMarkerVector) super.clone();
        mv.deltaX = deltaX;
        mv.deltaY = deltaY;
        return mv;
    }
    */

    public UPVector calculatePosition(UPVector location, float rotation, SpriteType spriteType) {
        return calculatePosition(location, rotation, spriteType, 1f);
    }

    public UPVector calculatePosition(UPVector location, float rotation, SpriteType spriteType, float scale) {
        return calculatePosition(location.x, location.y, rotation, spriteType.getWidth() / 2f, spriteType.getHeight() / 2f, scale, spriteType.getHeight());
    }

    public UPVector calculatePosition(float physX, float physY, float rotation, float originX, float originY, float scale, float height) {
        UPVector markerCenterPosition =
            getPosition(originX, originY, height);
        UPVector markerRotatedPosition =
            markerCenterPosition.rotateDeg(rotation).scale(scale);

        return markerRotatedPosition.add(physX, physY);
    }

    public Vector2 calculatePosition(Vector2 out, UPVector location, float rotation, SpriteType spriteType, float scale) {
        return calculatePosition(out, location.x, location.y, rotation, spriteType.getWidth() / 2f, spriteType.getHeight() / 2f, scale, spriteType.getHeight());
    }

    public Vector2 calculatePosition(Vector2 out, float physX, float physY, float rotation, float originX, float originY, float scale, float height) {
        Vector2 markerCenterPosition =
            getPosition(out, originX, originY, height);
        Vector2 markerRotatedPosition =
            markerCenterPosition.rotate(rotation).scl(scale);

        return markerRotatedPosition.add(physX, physY);
    }

    /**
     * Produces a global rotation value for this marker, given a live sprite instance
     */
    public float calculateRotation(float rotation) {
        return rotation + this.getRotation();
    }
}
