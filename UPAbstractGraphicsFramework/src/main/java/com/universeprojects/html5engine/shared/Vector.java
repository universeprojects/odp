/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.universeprojects.html5engine.shared;

import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Strings;

/**
 * @author Crokoking
 */
@Deprecated
public class Vector {

    public double x, y;
    private boolean isFinal = false;


    public Vector copy() {
        return new Vector(x, y);
    }


    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public Vector set(double x, double y) {
        Dev.check(!isFinal);
        this.x = x;
        this.y = y;
        return this;
    }

    public Vector set(Vector other) {
        return set(other.x, other.y);
    }

    public Vector set(UPVector other) {
        return set(other.x, other.y);
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Vector() {
        this(0, 0);
    }

    public Vector setFinal() {
        isFinal = true;
        return this;
    }

    public boolean isFinal() {
        return isFinal;
    }

    public Vector(double[] v) {
        this(v[0], v[1]);
    }

    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Vector v) {
        this(v.x, v.y);
    }

    public Vector(UPVector v) {
        this(v.x, v.y);
    }

    public Vector norm() {
        Dev.check(!isFinal);
        double norm = UPMath.sqrt(x * x + y * y);
        x = x / norm;
        y = y / norm;
        return this;
    }

    public Vector translate(double x, double y) {
        Dev.check(!isFinal);
        this.x += x;
        this.y += y;
        return this;
    }

    public Vector translate(Vector vec) {
        return translate(vec.x, vec.y);
    }

    public Vector translate(Vector vec, double scale) {
        return translate(vec.x * scale, vec.y * scale);
    }

    public double length() {
        return UPMath.distance(0, 0, x, y);
    }

    public double length2() {
        return UPMath.distance2(0, 0, x, y);
    }

    public Vector add(Vector p) {
        return copy().translate(p.x, p.y);
    }

    public Vector sub(Vector p) {
        return copy().translate(-p.x, -p.y);
    }

    public Vector mul(double v) {
        return copy().scale(v);
    }

    public Vector divide(double v) {
        return copy().scale(1. / v);
    }

    public double dot(Vector p) {
        return dot(this, p);
    }

    public static double dot(Vector a, Vector b) {
        return a.x * b.x + a.y * b.y;
    }

    public Vector rotate(double angleRad) {
        return rotateDeg(UPMath.toDegrees(angleRad));
    }

    public Vector rotateDeg(double deg) {
        Dev.check(!isFinal);
        if (deg % 360 != 0) {
            double sin = UPMath.sinDeg(deg);
            double cos = UPMath.cosDeg(deg);
            double newX = x * cos - y * sin;
            double newY = x * sin + y * cos;
            this.x = newX;
            this.y = newY;
        }
        return this;
    }

    public Vector scale(double sc) {
        return scale(sc, sc);
    }

    public Vector scale(double scaleX, double scaleY) {
        Dev.check(!isFinal);
        this.x *= scaleX;
        this.y *= scaleY;
        return this;
    }

    public double distance(Vector p) {
        return distance(p.x, p.y);
    }

    public double distance(double x, double y) {
        return UPMath.distance(this.x, this.y, x, y);
    }

    public double distance2(Vector p) {
        return distance2(p.x, p.y);
    }

    public double distance2(double x, double y) {
        return UPMath.distance2(this.x, this.y, x, y);
    }

    /**
     * Sets this vector to the cross between v and a and returns this.
     */
    public Vector cross(Vector v, double a) {
        return cross(v, a, this);
    }

    /**
     * Sets this vector to the cross between a and v and returns this.
     */
    public Vector cross(double a, Vector v) {
        return cross(a, v, this);
    }

    /**
     * Returns the scalar cross between this vector and v. This is essentially
     * the length of the cross product if this vector were 3d. This can also
     * indicate which way v is facing relative to this vector.
     */
    public double cross(Vector v) {
        return cross(this, v);
    }

    public static Vector cross(Vector v, double a, Vector out) {
        double x = v.y * a;
        double y = v.x * -a;
        out.set(x, y);
        return out;
    }

    public static Vector cross(double a, Vector v, Vector out) {
        double x = v.y * -a;
        double y = v.x * a;
        out.set(x, y);
        return out;
    }

    public static double cross(Vector a, Vector b) {
        return a.x * b.y - a.y * b.x;
    }

    /**
     * @return Returns the angle in degrees between this and the given vector in the region [-180,180]
     */
    public double angleDeg(Vector otherVec) {
        double a = UPMath.toDegrees(UPMath.atan2(otherVec.y, otherVec.x) - UPMath.atan2(y, x));
        while (a < -180)
            a += 360;
        while (a > 180)
            a -= 360;
        return a;
    }

    @Override
    public String toString() {
        String sx = Strings.fromDouble(x, 4);
        String sy = Strings.fromDouble(y, 4);

        return "( " + sx + " , " + sy + " )";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @SuppressWarnings("RedundantIfStatement")
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Vector)) {
            return false;
        }
        Vector other = (Vector) obj;
        if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x)) {
            return false;
        }
        if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y)) {
            return false;
        }
        return true;
    }

    public boolean equalWithinEpsilon(Object obj, double epsilon) {
        if (equals(obj)) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Vector)) {
            return false;
        }
        Vector other = (Vector) obj;
        return distance2(other) <= epsilon * epsilon;
    }

    public void normalize() {
        double lenSq = length2();

        if (lenSq > 0.) {
            double invLen = 1.0 / UPMath.sqrt(lenSq);
            x *= invLen;
            y *= invLen;
        }
    }

    /**
     * Returns an array of allocated Vec2 of the requested length.
     */
    public static Vector[] arrayOf(int length) {
        Vector[] array = new Vector[length];

        while (--length >= 0) {
            array[length] = new Vector();
        }

        return array;
    }

    public UPVector toUPVector() {
        return new UPVector((float) x, (float) y);
    }

}
