package com.universeprojects.html5engine.shared.abstractFramework;

/**
 * @author Crokoking
 */
public abstract class Audio {

    /**
     * This is the original url used to find the image. It is likely a relative path
     */
    protected String url = null;

    /**
     * The original url used to load the audio file. It is likely a relative url.
     *
     * @return The original url string used when loadAudio() was called
     */
    public String getOriginalUrl() {
        return url;
    }

    /**
     * The original url used to load the audio file
     *
     * @return The original url used to load the audio file
     */
    public abstract String getURL();

}
