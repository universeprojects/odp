package com.universeprojects.html5engine.shared.abstractFramework;


public enum AnchorRelationship {

    /**
     * The anchored element is positioned relative to the top-left corner of the anchor's bounding box
     */
    TOP_LEFT,

    /**
     * The anchored element is positioned relative to the top-left corner of the anchor's bounding box
     */
    TOP_RIGHT,

    /**
     * The anchored element is positioned relative to the bottom-left corner of the anchor's bounding box
     */
    BOTTOM_LEFT,

    /**
     * The anchored element is positioned relative to the bottom-right corner of the anchor's bounding box
     */
    BOTTOM_RIGHT,

    /**
     * The anchored element is positioned relative to the anchor's origin
     */
    ORIGIN

}
