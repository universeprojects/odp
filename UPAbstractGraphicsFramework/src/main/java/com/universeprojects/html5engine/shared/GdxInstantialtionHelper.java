package com.universeprojects.html5engine.shared;

import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.universeprojects.common.shared.reflection.InstantiationHelper;
import com.universeprojects.common.shared.reflection.ReflectionException;

public class GdxInstantialtionHelper extends InstantiationHelper {
    public static void initialize() {
        if (InstantiationHelper.INSTANCE == null) {
            InstantiationHelper.INSTANCE = new GdxInstantialtionHelper();
        }
    }

    @Override
    public <T> T instantiate(String className, Class<T> targetClass) throws ReflectionException {
        try {
            final Class<?> aspectReflectionDataClass = ClassReflection.forName(className);
            //noinspection unchecked
            return (T) ClassReflection.newInstance(aspectReflectionDataClass);
        } catch (com.badlogic.gdx.utils.reflect.ReflectionException e) {
            throw new ReflectionException(e);
        }
    }
}
