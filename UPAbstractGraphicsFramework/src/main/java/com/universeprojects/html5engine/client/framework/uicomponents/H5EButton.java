package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageTextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.universeprojects.common.shared.callable.Callable0Args;
import com.universeprojects.html5engine.client.framework.H5EAudio;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESpriteType;
import com.universeprojects.html5engine.client.framework.H5ETextTooltip;
import com.universeprojects.html5engine.client.framework.inputs.H5EButtonInvalidateListener;
import com.universeprojects.html5engine.shared.UPUtils;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;


public class H5EButton extends ImageTextButton implements GraphicElement {
    private static final String SOUND_MOUSE_OVER = "https://resources.universeprojects.com/audio/sfx/ui/UI_mouse_over" + UPUtils.GetAudioDotFiletype();
    private static final String SOUND_MOUSE_CLICK = "https://resources.universeprojects.com/audio/sfx/ui/UI_mouse_click" + UPUtils.GetAudioDotFiletype();
    private static final String MaserVolumeKey = "voidspace.clientSettings.audioMasterVolume";
    private static final String SFXVolumeKey = "voidspace.clientSettings.audioSFXVolume";
    private static final String MasterMuteKey = "voidspace.clientSettings.audioMasterMute";
    private static final String SFXMuteKey = "voidspace.clientSettings.audioSfxMute";
    private Preferences clientPreferences = Gdx.app.getPreferences("client");
    private boolean isCheckable=false;
    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public H5EButton(H5ELayer layer) {
        this("", layer, "default");
    }

    public H5EButton(String text, H5ELayer layer) {
        this(text, layer, "default");
    }

    public H5EButton(String text, H5ELayer layer, String style) {
        super(text.toUpperCase(), layer.getEngine().getSkin(), style);
        setStage(layer);
        H5EAudio enterAudio = (H5EAudio) getEngine().getResourceManager().getAudio(SOUND_MOUSE_OVER);
        H5EAudio clickAudio = (H5EAudio) getEngine().getResourceManager().getAudio(SOUND_MOUSE_CLICK);
        this.addListener(new ClickListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                if (pointer == -1) {
                    playAudio(enterAudio, getVolume() * 0.5f);
                }
                event.handle();
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                playAudio(clickAudio, getVolume() * 0.75f);
                event.handle();
            }
        });
        addListener(new H5EButtonInvalidateListener(this));
        getLabel().setTouchable(Touchable.disabled);

    }

    H5ETextTooltip tooltip = null;

    public void setTooltip(String text) {
        if (tooltip != null) {
            this.removeListener(tooltip);
        }
        this.tooltip = new H5ETextTooltip(text, this.getLayer().getEngine().getSkin());
        this.addListener(tooltip);
    }

    @Override
    public void setChecked(boolean isChecked) {
        super.setChecked(isChecked);
        H5ELayer.invalidateBufferForActor(this);
    }

    public void setOver(boolean isOver) {
        Vector2 falsePointerPos = this.localToStageCoordinates(new Vector2(0, 0));
        if (isOver) {
            this.getClickListener().enter(new InputEvent() {{
                setType(Type.enter);
            }}, falsePointerPos.x, falsePointerPos.y, -1, null);
            if(tooltip!=null) {
                tooltip.enter(new InputEvent(){{setListenerActor(H5EButton.this);}}, 0, getHeight()/2, -1, null);
            }
        } else {
            this.getClickListener().exit(new InputEvent() {{
                setType(Type.exit);
            }}, falsePointerPos.x, falsePointerPos.y, -1, this);
            if(tooltip!=null) {
                tooltip.exit(new InputEvent(){{setListenerActor(H5EButton.this);}}, 0,  getHeight()/2, -1, null);
            }
        }
    }

    @Override
    public void setDisabled(boolean isDisabled) {
        super.setDisabled(isDisabled);
        H5ELayer.invalidateBufferForActor(this);
    }

    public H5EButton(H5ELayer layer, ImageTextButtonStyle style) {
        this("", layer, style);
    }

    public H5EButton(String text, H5ELayer layer, ImageTextButtonStyle style) {
        super(text.toUpperCase(), style);
        setStage(layer);
        H5EAudio enterAudio = (H5EAudio) getEngine().getResourceManager().getAudio(SOUND_MOUSE_OVER);
        H5EAudio clickAudio = (H5EAudio) getEngine().getResourceManager().getAudio(SOUND_MOUSE_CLICK);
        this.addListener(new ClickListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                super.enter(event, x, y, pointer, fromActor);
                if (pointer == -1) {
                    playAudio(enterAudio, getVolume() * 0.5f);
                }
                event.handle();
            }

            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                playAudio(clickAudio, getVolume() * 0.75f);
                event.handle();
            }
        });
        addListener(new H5EButtonInvalidateListener(this));
        getLabel().setTouchable(Touchable.disabled);
        Image myImage = getImage();
        if (myImage != null) {
            myImage.setTouchable(Touchable.disabled);
        }
    }

    private void playAudio(H5EAudio audio, float volume) {
        if (audio == null || audio.getSound() == null) {
            return;
        }

        audio.getSound().play(volume);
    }

    private float getVolume() {
        if (clientPreferences == null) {
            return 0;
        }
        float volume = 0;

        if (!clientPreferences.getBoolean(MasterMuteKey) && !clientPreferences.getBoolean(SFXMuteKey)) {
            volume = clientPreferences.getFloat(MaserVolumeKey) * clientPreferences.getFloat(SFXVolumeKey) / 10000f;
        }
        return volume;
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, H5ESpriteType spriteType) {
        return createSpriteBacked(layer, spriteType, "");
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, H5ESpriteType spriteType, String text) {
        if (spriteType == null) {
            throw new IllegalArgumentException("The given spriteType is null.");
        }
        return new H5EButton(text, layer);
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, String spriteTypeKey) {
        return createSpriteBacked(layer, spriteTypeKey, "");
    }

    @Deprecated
    public static H5EButton createSpriteBacked(H5ELayer layer, String spriteTypeKey, String text) {
        H5ESpriteType spriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
        if (spriteType == null) {
            throw new RuntimeException("The sprite type '" + spriteTypeKey + "' was not found.");
        }
        return createSpriteBacked(layer, spriteType, text);
    }

    @Deprecated
    public static H5EButton createRectBacked(H5ELayer layer) {
        return createRectBacked(layer, "");
    }

    @Deprecated
    public static H5EButton createRectBacked(H5ELayer layer, String text) {
        return new H5EButton(text, layer);
    }

    @Deprecated
    public static H5EButton createNoBacking(H5ELayer layer, String text) {
        return new H5EButton(text, layer);
    }

    public void addButtonListener(Callable0Args callable) {
        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                callable.call();
                event.handle();
            }
        });
    }

    public void addCheckedButtonListener(Callable0Args callable) {
        isCheckable=true;
        addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (isChecked()) {
                    callable.call();
                }
                event.handle();
            }
        });
    }

    @Override
    public void setText(CharSequence text) {
        super.setText(text.toString().toUpperCase());
    }

    public void setRawText(CharSequence text) {
        super.setText(text);
    }

    public void click(){
        if(isCheckable){
         setChecked(!isChecked());
        }else {
            for (Object listener : getListeners()) {
                if (listener instanceof ChangeListener) {
                    ((ChangeListener) listener).changed(new ChangeListener.ChangeEvent(), this);
                }
            }
        }
    }


    public void setCheckedWithoutEvent(boolean checked) {
        setProgrammaticChangeEvents(false);
        setChecked(checked);
        setProgrammaticChangeEvents(true);
    }

}
