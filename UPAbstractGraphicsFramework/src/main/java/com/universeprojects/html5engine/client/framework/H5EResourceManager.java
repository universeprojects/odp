package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.hooks.Hook;
import com.universeprojects.common.shared.hooks.Hooks;
import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.html5engine.client.framework.resourceloader.AssetLoader;
import com.universeprojects.html5engine.client.framework.resourceloader.ResourceLoader;
import com.universeprojects.html5engine.shared.abstractFramework.AbstractResourceManager;
import com.universeprojects.html5engine.shared.abstractFramework.AnimatedSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerCircle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerRectangle;
import com.universeprojects.html5engine.shared.abstractFramework.MarkerVector;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public final class H5EResourceManager extends AbstractResourceManager {

    public final Hook<Callable1Args<String>> onDownloadableSpriteUpdated = Hooks.createHook1Args();

    private static final Logger log = Logger.getLogger(H5EResourceManager.class);

    private final ResourceLoader resourceLoader;

    // Operating variables...
    /**
     * A total number of resources that have been added to this resource manager
     */
    private int totalItems = 0;
    /**
     * An array of key/src string pairs. For example: ["house",
     * "images/house.png", "test", "images/test.gif"][]
     */
    private final List<String> imageLoadQueue = new ArrayList<>();
    /**
     * All images loaded for this resource manager but stored by key
     */
    private final Map<String, H5EImage> images = new LinkedHashMap<>();
    /**
     * An array of key/src string pairs. For example: ["ping", "audio/ping.oog",
     * "background", "audio/background.wav"][]
     */
    private final List<String> audioLoadQueue = new ArrayList<>();

    /**
     * All audio files loaded for this resource manager but stored by key
     */
    private final Map<String, H5EAudio> audioMap = new HashMap<>();

    /**
     * All sprite types loaded for this resource manager
     */
    private final Map<String, H5ESpriteType> spriteTypes = new LinkedHashMap<>();
    /**
     * All animated sprite types loaded for this resource manager
     */
    private final Map<String, H5EAnimatedSpriteType> animatedSpriteTypes = new LinkedHashMap<>();

    public H5EResourceManager(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @Override
    protected MarkerVector newMarkerVector() {
        return new MarkerVector();
    }

    @Override
    protected MarkerRectangle newMarkerRectangle() {
        return new MarkerRectangle();
    }

    @Override
    protected MarkerCircle newMarkerCircle() {
        return new MarkerCircle();
    }

    /**
     * Creates a new sprite type using the given imageKey and stores it in the
     * resource manager under the given spriteTypeKey.
     *
     * @param imageKey      The key used to identify a loaded image that will be used as
     *                      the graphic for the spriteType
     * @param spriteTypeKey The key used to identify this spriteType so it can be used
     *                      later. This parameter can be skipped and the imageKey will be
     *                      used instead.
     * @return The new H5ESpriteType object
     */
    @Override
    public H5ESpriteType newSpriteType(String imageKey, String spriteTypeKey) {
        if (spriteTypeKey == null) {
            spriteTypeKey = imageKey;
        }
        H5ESpriteType spriteType = new H5ESpriteType(this, imageKey, spriteTypeKey);
        spriteTypes.put(spriteTypeKey, spriteType);
        return spriteType;
    }

    /**
     * This method will accept a spriteType object as a parameter and add it to
     * this resource manager's list of sprite types. This method is similar to
     * newSpriteType() except newSpriteType() will actually create the
     * spriteType object for you. The purpose of this method is to allow
     * extended sprite types to be stored by the resource manager.
     *
     * @param spriteTypeKey The key used to access this sprite type object later
     * @param spriteType    The sprite type object you wish to be stored in the resource
     *                      manager
     */
    @Override
    public void addSpriteType(String spriteTypeKey,
                              SpriteType spriteType) {
        spriteTypes.put(spriteTypeKey, (H5ESpriteType) spriteType);
    }

    /**
     * Returns the sprite type that is associted with the given spriteTypeKey.
     *
     * @param spriteTypeKey A key (string) that is used to identify a particular
     *                      spriteType within the resource manager
     * @return The associated H5ESpriteType or null if the sprite type could not
     * be found
     */
    @Override
    public H5ESpriteType getSpriteType(String spriteTypeKey) {
        final H5EAnimatedSpriteType animatedSpriteType = getAnimatedSpriteType(spriteTypeKey);
        if(animatedSpriteType != null) {
            return animatedSpriteType;
        }
        return getNonAnimatedSpriteType(spriteTypeKey);
    }

    @Override
    public H5ESpriteType getNonAnimatedSpriteType(String spriteTypeKey) {
        return getSpriteType(spriteTypeKey, true);
    }

    private H5ESpriteType getSpriteType(String spriteTypeKey, boolean loadDefault) {
        if(spriteTypeKey == null) {
            return spriteTypes.get(resourceLoader.getDefaultSpriteKey());
        }

        final H5ESpriteType spriteType = spriteTypes.get(spriteTypeKey);

        // If the spriteType is invalid because it is either null or is incorrect
        if (spriteType == null) {
            final H5EImage image = resourceLoader.loadSingleImage(spriteTypeKey);

            if(image == null) {
                if(!loadDefault) {
                    return null;
                }
                log.warn("Missing sprite "+spriteTypeKey);
                final H5ESpriteType defaultSprite = getSpriteType(resourceLoader.getDefaultSpriteKey(), false);
                if (defaultSprite == null) {
                    return null;
                }
                final H5ESpriteType newSpriteType = new H5ESpriteType(this, defaultSprite.getImageKey(), spriteTypeKey);
                newSpriteType.setImage(defaultSprite.getImage());
                spriteTypes.put(spriteTypeKey, newSpriteType);
                return newSpriteType;
            } else {
                images.put(spriteTypeKey, image);
                final H5ESpriteType newSpriteType = new H5ESpriteType(this, spriteTypeKey, spriteTypeKey);
                newSpriteType.setImage(image);
                spriteTypes.put(spriteTypeKey, newSpriteType);
                return newSpriteType;
            }
        }
        return spriteType;
    }

    /**
     * Returns all H5ESpriteType objects stored in this resource manager in an
     * Array.
     *
     * @return An array of H5ESpriteType objects.
     */
    public Collection<H5ESpriteType> getSpriteTypes() {
        return spriteTypes.values();
    }

    /**
     * Returns all keys associated with H5ESpriteType objects, stored in this
     * resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against sprite
     * types in this resource manager
     */
    @Override
    public Collection<String> getSpriteTypeKeys() {
        return spriteTypes.keySet();
    }

    /**
     * Removes the sprite type that is associated with the given spriteTypeKey.
     *
     * @param spriteTypeKey A key (string) that is used to identify a particular
     *                      spriteType within the resource manager
     */
    @Override
    public void removeSpriteType(String spriteTypeKey) {
        spriteTypes.remove(spriteTypeKey);
    }

    /**
     * Creates a new animated sprite type and stores it in the resource manager
     * under the given animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey The key used to identify this animatedSpriteType so it can be
     *                              used later.
     * @return The new H5EAnimatedSpriteType object
     */
    @Override
    public H5EAnimatedSpriteType newAnimatedSpriteType(String animatedSpriteTypeKey) {
        H5EAnimatedSpriteType animSpriteType = new H5EAnimatedSpriteType(this, animatedSpriteTypeKey);
        animatedSpriteTypes.put(animatedSpriteTypeKey, animSpriteType);
        return animSpriteType;
    }

    /**
     * This method will accept an animatedSpriteType object as a parameter and
     * add it to this resource manager's list of animated sprite types. This
     * method is similar to newAnimatedSpriteType() except
     * newAnimatedSpriteType() will actually create the animatedSpriteType
     * object for you. The purpose of this method is to allow extended animated
     * sprite types to be stored by the resource manager.
     *
     * @param animatedSpriteTypeKey The key used to access this animated sprite type object later
     * @param animatedSpriteType    The animated sprite type object you wish to be stored in the
     *                              resource manager
     */
    @Override
    public void addAnimatedSpriteType(String animatedSpriteTypeKey,
                                      AnimatedSpriteType animatedSpriteType) {
        animatedSpriteTypes.put(animatedSpriteTypeKey,
            (H5EAnimatedSpriteType) animatedSpriteType);
    }

    /**
     * Returns the animated sprite type that is associted with the given
     * animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey A key (string) that is used to identify a particular
     *                              animatedSpriteType within the resource manager
     * @return The associated H5EAnimatedSpriteType or null if the animated
     * sprite type could not be found
     */
    @Override
    public H5EAnimatedSpriteType getAnimatedSpriteType(String animatedSpriteTypeKey) {
        return animatedSpriteTypes.get(animatedSpriteTypeKey);
    }

    /**
     * Returns all H5EAnimatedSpriteType objects stored in this resource manager
     * in an Array.
     *
     * @return An array of H5EAnimatedSpriteType objects.
     */
    public Collection<H5EAnimatedSpriteType> getAnimatedSpriteTypes() {
        return animatedSpriteTypes.values();
    }

    /**
     * Returns all keys associated with H5ESpriteType objects, stored in this
     * resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against animated
     * sprite types in this resource manager
     */
    @Override
    public Collection<String> getAnimatedSpriteTypeKeys() {
        return animatedSpriteTypes.keySet();
    }

    /**
     * Removes the animated sprite type that is associated with the given
     * animatedSpriteTypeKey.
     *
     * @param animatedSpriteTypeKey A key (string) that is used to identify a particular
     *                              animatedSpriteType within the resource manager
     */
    @Override
    public void removeAnimatedSpriteType(String animatedSpriteTypeKey) {
        animatedSpriteTypes.remove(animatedSpriteTypeKey);
    }

    /**
     * This method will add a single image into the resource manager's queue for
     * loading.
     * <p/>
     * Note: Actual loading will not start until .load() is called.
     *
     * @param key A string identifier that will be used to get the image from
     *            the resourceManager again later. You may use the url for this
     * @param src The url pointing to the image to load
     */
    @Override
    public void addImage(String key, String src) {
        if (src == null) {
            src = key;
        }
        imageLoadQueue.add(key);
        imageLoadQueue.add(src);
        totalItems++;
    }

    /**
     * Gets an image stored in this resource manager by the given key.
     *
     * @param key The string identifier that was used when loading the image
     * @return The image associated with the given key
     */
    @Override
    public H5EImage getImage(String key) {
        return images.get(key);
    }

    /**
     * Returns all H5EImage objects stored in this resource manager in an Array.
     *
     * @return An array of H5EImage objects.
     */
    public Collection<H5EImage> getImages() {
        return images.values();
    }

    /**
     * Returns all keys associated with H5EImage objects, stored in this
     * resource manager, in an Array.
     *
     * @return An array of Strings; all keys that are stored against images in
     * this resource manager
     */
    @Override
    public Collection<String> getImageKeys() {
        return images.keySet();
    }

    /**
     * Sets an image stored in this resource manager to null according to the
     * given key.
     *
     * @param key The string identifier that was used when loading the image
     */
    @Override
    public void removeImage(String key) {
        images.remove(key);
    }

    @Override
    public void removeAllImages() {
        images.clear();
        imageLoadQueue.clear();
    }

    @Override
    public void removeAllAudio() {
        audioMap.clear();
        audioLoadQueue.clear();
    }

    @Override
    public void removeAllSpriteTypes() {
        spriteTypes.clear();
    }

    /**
     * This method will add a single audio file into the resource manager's
     * queue for loading.
     * <p/>
     * Note: Actual loading will not start until .load() is called.
     *
     * @param key A string identifier that will be used to get the audio file
     *            from the resourceManager again later. You may use the url for
     *            this
     * @param src The url pointing to the audio file to load
     */
    @Override
    public void addAudio(String key, String src) {
        if (src == null) {
            src = key;
        }
        audioLoadQueue.add(key);
        audioLoadQueue.add(src);
        totalItems++;
    }

    /**
     * Gets an audio file stored in this resource manager by the given key.
     *
     * @param key The string identifier that was used when loading the audio
     *            file
     * @return The audio file associated with the given key
     */
    @Override
    public H5EAudio getAudio(String key) {
        final H5EAudio audio = audioMap.get(key);
        if(audio == null) {
            final H5EAudio newAudio = new H5EAudio(key);
            AssetLoader.getInstance().downloadSound(newAudio.getURL(), newAudio::setSound, (ex) -> Log.error("Error loading audio: "+ex));
            audioMap.put(key, newAudio);
            return newAudio;
        }
        return audio;
    }

    @Override
    public Collection<String> getAudioKeys() {
        return Collections.unmodifiableCollection(audioMap.keySet());
    }

    public Collection<H5EAudio> getAudioObjects() {
        return Collections.unmodifiableCollection(audioMap.values());
    }

    /**
     * Sets an audio file stored in this resource manager to null according to
     * the given key. The data should be garbage collected when it is
     * appropriate by the browser.
     *
     * @param key The string identifier that was used when loading the audio
     *            file
     */
    @Override
    public void removeAudio(String key) {
        audioMap.remove(key);
    }

    /**
     * This starts the process of loading all resources. Once the load is
     * complete the onComplete event will fire. If there was an error, the
     * onError event will fire. If the load was aborted, the onError event will
     * fire. Every time a single resource (image, audio..etc) is finished, the
     * onProgress event will fire.
     */
    @Override
    public void loadResources() {
        int totalImagesToLoad = imageLoadQueue.size();
        for (int i = 0; i < totalImagesToLoad; i = i + 2) {
            // Ok get the key and src that is next in the queue
            String key = imageLoadQueue.remove(0);
            String src = imageLoadQueue.remove(0);

            // Now trigger the load...
            H5EImage image = new H5EImage(src);
            images.put(key, image);
        }

        // Load all images queued...
        int totalSoundsToLoad = audioLoadQueue.size();
        for (int i = 0; i < totalSoundsToLoad; i = i + 2) {
            // Ok get the key and src that is next in the queue
            String key = audioLoadQueue.remove(0);
            String src = audioLoadQueue.remove(0);

            // Construct the Audio Object
            H5EAudio aud = new H5EAudio(src);
            //Put it in the audioMap
            audioMap.put(key, aud);
        }

        try {
            resourceLoader.loadResources(images.values(), audioMap.values());
        } catch (Throwable ex) {
            log.error("Error loading the library", ex);
        }
    }

    public void importLibrary(String h5lFileContents) {
        // Read the library file settings into the resource manager
        // (ourselves)...
        readLibraryFile(h5lFileContents);
        // Begin the image/audio/video load sequence
        loadResources();
    }

    @Override
    public void clear() {
        // TODO implement this functionality
        throw new RuntimeException("Not implemented yet");
    }

}
