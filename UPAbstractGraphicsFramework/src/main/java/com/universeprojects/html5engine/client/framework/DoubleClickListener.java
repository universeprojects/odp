package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

public abstract class DoubleClickListener extends ActorGestureListener {

    private boolean handled;

    @Override
    public void tap(InputEvent event, float x, float y, int count, int button) {
        if (button != Input.Buttons.LEFT || count != 2) {
            return;
        }
        handled = onDoubleClick(event, x, y);
    }

    @Override
    public boolean handle(Event e) {
        handled = false;
        super.handle(e);
        return handled;
    }

    public abstract boolean onDoubleClick(InputEvent event, float x, float y);
}
