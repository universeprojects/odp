package com.universeprojects.html5engine.client.framework;

import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONObject;

/**
 * Created by Owner on 12/13/2017.
 */
public class CompositeSpriteEffects {

    private static JSONObject infectedConfig;

    static {
        final JSONObject json = new JSONObject();

        final JSONArray arr = new JSONArray();
        JSONObject layer = new JSONObject();
        layer.put("type", "Sprite");
        layer.put("spriteTypeKey", "infected-effect1");
        layer.put("blendingMode", "SOURCE_IN");
        arr.add(layer);

        layer = new JSONObject();
        layer.put("type", "Sprite");
        layer.put("spriteTypeKey", "${spriteKey}");
        layer.put("transparency", 0.9f);
        layer.put("blendingMode", "MULTIPLY");
        arr.add(layer);

        json.put("layerDefinitions", arr);
        infectedConfig = json;
    }

    public static CompositeSpriteConfiguration infected(String spriteKey) {
        final CompositeSpriteConfiguration config = new CompositeSpriteConfiguration(infectedConfig);
        config.setVariable("spriteKey", spriteKey);
        return config;
    }
}
