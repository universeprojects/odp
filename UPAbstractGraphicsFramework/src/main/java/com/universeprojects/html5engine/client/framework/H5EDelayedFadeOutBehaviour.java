package com.universeprojects.html5engine.client.framework;

public class H5EDelayedFadeOutBehaviour extends H5EBehaviour {
    private boolean started = false;
    private float delay;
    private float period;
    private float time;

    public <T extends H5EGraphicElement> H5EDelayedFadeOutBehaviour(T graphicElement) {
        super(graphicElement);
    }

    public void start(float delay, float period) {
        show();
        this.started = true;
        this.delay = delay;
        this.period = period;
        this.time = 0;
    }

    public void show() {
        this.started = false;
        element.getColor().a = 1;
    }

    public void hide() {
        this.started = false;
        element.getColor().a = 0;
    }

    @Override
    public void act(float delta) {
        if (!started) return;
        time += delta;
        if (time < delay) return;

        float animationStage = (time - delay) / period;
        if (animationStage > 1) {
            animationStage = 1;
            started = false;
        }

        element.getColor().a = 1 - animationStage;
    }

    @Override
    public String getName() {
        return "DelayedFadeOut";
    }
}
