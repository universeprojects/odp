package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.universeprojects.common.shared.util.Dev;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class MasterInputProcessor implements InputProcessor {

    public static final MasterInputProcessor INSTANCE = new MasterInputProcessor();
    public static final InputProcessor NO_OP_PROCESSOR = new InputAdapter();
    private InputProcessor delegate = NO_OP_PROCESSOR;
    private final Map<Integer, InputProcessor> processors = new HashMap<>();

    public synchronized void addProcessor(int priority, InputProcessor inputProcessor) {
        Dev.checkNotNull(inputProcessor);
        processors.put(priority, inputProcessor);
        updateDelegate();
    }

    public synchronized void removeProcessor(int priority) {
        processors.remove(priority);
        updateDelegate();
    }

    private void updateDelegate() {
        if (processors.isEmpty()) {
            delegate = NO_OP_PROCESSOR;
            return;
        }
        Integer maxPriority = Collections.max(processors.keySet());
        delegate = processors.get(maxPriority);
    }


    @Override
    public boolean keyDown(int keycode) {
        return delegate.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return delegate.keyUp(keycode);
    }

    @Override
    public boolean keyTyped(char character) {
        return delegate.keyTyped(character);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return delegate.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return delegate.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return delegate.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return delegate.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return delegate.scrolled(amountX, amountY);
    }
}
