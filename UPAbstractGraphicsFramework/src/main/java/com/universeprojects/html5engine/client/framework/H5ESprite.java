package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.universeprojects.common.shared.callable.ReturningCallable2Args;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.html5engine.shared.abstractFramework.LevelAwareGraphicsElement;
import com.universeprojects.html5engine.shared.abstractFramework.ResourceManager;
import com.universeprojects.html5engine.shared.abstractFramework.Sprite;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.SpriteTypeReference;
import com.universeprojects.json.shared.JSONArray;
import com.universeprojects.json.shared.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("unused")
public class H5ESprite extends H5EGraphicElement implements Sprite, LevelAwareGraphicsElement {

    public static final int MIXED_SPRITE_TYPE_PRIMARY_INDEX = -1;
    private static final float FACTOR_3D_SHIFT = 20f;
    private final Vector2 tmp = new Vector2();
    private final Vector2 tmp2 = new Vector2();
    /**
     * The sprite type being used
     */
    protected H5ESpriteType primarySpriteType;
    protected H5ESpriteType primarySpriteTypeHD;
    private Integer level;
    private H5EAnimatedSpriteType animatedSpriteType;
    private ReturningCallable2Args<Boolean, Float, Float> hitCheck;
    private int frameIndex;
    private int primaryFrameIndex;
    private boolean animationPlaying = true;
    private boolean animationLoop = true;
    private boolean animationReversed = false;
    private boolean skipLastFrameOnReversedAnimation = false;
    private long animationStartupDelayMs;
    private long animationStartTimeMs;
    private float animationSpeedMultiplier = 1;
    private boolean hideWhenFinished = false;
    private BlendingMode blendingMode = BlendingMode.DEFAULT;
    private float shift3DZ = 0;
    private float areaXAdjust = 0;
    private float areaYAdjust = 0;
    private float areaWidthAdjust = 0;
    private float areaHeightAdjust = 0;
    private CompositeSpriteConfiguration compositeSpriteConfiguration;
    private boolean revertViewportZoomScaling = false;
    private boolean revertViewportRotation = false;
    private ShaderProgram shader;
    private Map<String, H5ESpriteType> layerSpriteTypes;

    public H5ESprite(H5ELayer layer, SpriteTypeReference spriteTypeRef) {
        this(layer, (H5ESpriteType) spriteTypeRef.getSpriteType(layer.getEngine().getResourceManager()));
    }

    public H5ESprite(H5ELayer layer, H5ESpriteType spriteType) {
        super(layer);
        if (spriteType == null) {
            throw new IllegalArgumentException("Sprite type reference can't be null");
        }
        if (spriteType instanceof H5EAnimatedSpriteType) {
            this.animatedSpriteType = (H5EAnimatedSpriteType) spriteType;
            this.primaryFrameIndex = 0;
            this.primarySpriteType = animatedSpriteType.getFrame(primaryFrameIndex).getSpriteType();
        } else {
            this.animatedSpriteType = null;
            this.primaryFrameIndex = 0;
            this.primarySpriteType = spriteType;
        }
        setWidth(spriteType.getWidth());
        setHeight(spriteType.getHeight());
    }

    public H5ESprite(H5ELayer layer, String spriteTypeKey) {
        super(layer);
        if (Strings.isEmpty(spriteTypeKey)) {
            throw new IllegalArgumentException("Sprite type key can't be null or empty");
        }

        final ResourceManager resourceManager = layer.getEngine().getResourceManager();
        final H5EAnimatedSpriteType tmpAnimatedSpriteType = (H5EAnimatedSpriteType) resourceManager.getAnimatedSpriteType(spriteTypeKey);
        final H5ESpriteType tmpSpriteType;
        if (tmpAnimatedSpriteType != null) {
            tmpSpriteType = tmpAnimatedSpriteType.getFrame(primaryFrameIndex).getSpriteType();
        } else {
            tmpSpriteType = (H5ESpriteType) resourceManager.getSpriteType(spriteTypeKey);
        }

        if (tmpSpriteType == null) {
            Log.error("Could not obtain sprite type for key: " + spriteTypeKey);
        }
        this.animatedSpriteType = tmpAnimatedSpriteType;
        this.primarySpriteType = tmpSpriteType;
        if (primarySpriteType != null) {
            setWidth(primarySpriteType.getWidth());
            setHeight(primarySpriteType.getHeight());
        }
        this.primaryFrameIndex = 0;
    }

    public void setSpriteTypeHD(H5ESpriteType spriteTypeHD) {
        this.primarySpriteTypeHD = spriteTypeHD;
    }

    @Override
    public void setAreaAdjustments(float areaXAdjust, float areaYAdjust, float areaWidthAdjust, float areaHeightAdjust) {
        if (this.areaXAdjust == areaXAdjust && this.areaYAdjust == areaYAdjust
                && this.areaWidthAdjust == areaWidthAdjust && this.areaHeightAdjust == areaHeightAdjust) {
            return;
        }
        this.areaXAdjust = areaXAdjust;
        this.areaYAdjust = areaYAdjust;
        this.areaWidthAdjust = areaWidthAdjust;
        this.areaHeightAdjust = areaHeightAdjust;
        checkAreaOverrideBounds();
    }

    public BlendingMode getBlendingMode() {
        return blendingMode;
    }

    public void setBlendingMode(BlendingMode blendingMode) {
        this.blendingMode = blendingMode;
    }

    public float getZ() {
        return shift3DZ;
    }

    public void setZ(float new3DShiftZValue) {
        this.shift3DZ = new3DShiftZValue;
    }

    public void setPositionForOrigin(float x, float y) {
        setX(x - getOriginX());
        setY(y - getOriginY());
    }

    public float getPositionXForOrigin() {
        return getX() - getOriginX();
    }

    public float getPositionYForOrigin() {
        return getY() - getOriginY();
    }

    public void setSpriteType(H5EAnimatedSpriteType animatedSpriteType, int primaryFrameIndex) {
        if (animatedSpriteType == this.animatedSpriteType) {
            return;
        }
        this.animatedSpriteType = animatedSpriteType;
        this.primaryFrameIndex = primaryFrameIndex;
        setPrimarySpriteType(animatedSpriteType.getFrame(primaryFrameIndex).getSpriteType());
    }

    public void setSpriteType(H5EAnimatedSpriteType animatedSpriteType, H5ESpriteType primarySpriteType) {
        if (animatedSpriteType == this.animatedSpriteType && this.primarySpriteType == primarySpriteType) {
            return;
        }
        this.animatedSpriteType = animatedSpriteType;
        this.primaryFrameIndex = MIXED_SPRITE_TYPE_PRIMARY_INDEX;
        this.frameIndex = MIXED_SPRITE_TYPE_PRIMARY_INDEX;
        setPrimarySpriteType(primarySpriteType);
    }

    private void setPrimarySpriteType(SpriteType newSpriteType) {
        if (primarySpriteType == newSpriteType) {
            return;
        }
        primarySpriteType = (H5ESpriteType) newSpriteType;
        // H5ELayer.invalidateBufferForActor(this);
        checkAreaOverrideBounds();
    }

    /**
     * This method simply retrieves the spriteType being used to draw this sprite.
     *
     * @return The current H5ESpriteType that is being used for this sprite's image data
     */
    @Override
    public H5ESpriteType getSpriteType() {
        return primarySpriteType;
    }

    /**
     * This method will change the spriteType the sprite is using. The next time
     * this sprite is drawn; the new spriteType will be displayed.
     *
     * @param newSpriteType The sprite type to be used for the graphical data
     */
    @Override
    public void setSpriteType(SpriteType newSpriteType) {
        if (newSpriteType instanceof H5EAnimatedSpriteType) {
            setSpriteType((H5EAnimatedSpriteType) newSpriteType, 0);
        } else {
            animatedSpriteType = null;
            primaryFrameIndex = 0;
            setPrimarySpriteType(newSpriteType);
        }
    }

    public void setSpriteType(H5EAnimatedSpriteType animatedSpriteType) {
        setSpriteType(animatedSpriteType, 0);
    }

    public H5EAnimatedSpriteType getAnimatedSpriteType() {
        return animatedSpriteType;
    }

    public int getFrame() {
        return frameIndex;
    }

    public void setFrame(int frameIndex) {
        this.frameIndex = frameIndex;
    }

    public int getPrimaryFrameIndex() {
        return primaryFrameIndex;
    }

    public void setPrimaryFrameIndex(int primaryFrameIndex) {
        if (this.primaryFrameIndex == primaryFrameIndex) {
            return;
        }
        this.primaryFrameIndex = primaryFrameIndex;
        setPrimarySpriteType(animatedSpriteType.getFrame(primaryFrameIndex).getSpriteType());
    }

    public void startAnimation(boolean loop, float delay) {
        animationPlaying = true;
        animationStartTimeMs = System.currentTimeMillis() + (int) (delay * 1000f);
        animationLoop = loop;
    }

    public void startAnimation(boolean loop) {
        startAnimation(loop, 0f);
    }

    public void startAnimation() {
        startAnimation(true, 0f);
    }

    public void stopAnimation() {
        animationPlaying = false;
    }

    public boolean isAnimationPlaying() {
        return isAnimated() && animationPlaying;
    }

    public boolean isAnimationLoop() {
        return animationLoop;
    }

    public void setAnimationLoop(boolean animationLoop) {
        this.animationLoop = animationLoop;
    }

    public boolean isAnimated() {
        return animatedSpriteType != null;
    }

    /**
     * Returns the width for this sprite. This will return the dimensions
     * of the final sprite and not necessarily the dimensions of the image data used.
     *
     * @return The width of the sprite as it is drawn on the layer
     */
    @Override
    public float getWidth() {
        if (primarySpriteType == null) {
            return 0;
        } else {
            return primarySpriteType.getWidth() + areaWidthAdjust;
        }
    }

    @Override
    public float getHeight() {
        if (primarySpriteType == null) {
            return 0;
        } else {
            return primarySpriteType.getHeight() + areaHeightAdjust;
        }
    }

    private void checkAreaOverrideBounds() {
        if (areaXAdjust == 0 && areaYAdjust == 0 && areaWidthAdjust == 0 && areaHeightAdjust == 0) {
            return;
        }
        primarySpriteType.stabilize();
        float sourceImageX = primarySpriteType.getAreaX();
        float sourceImageY = primarySpriteType.getAreaY();
        float sourceImageWidth = primarySpriteType.getAreaWidth();
        float sourceImageHeight = primarySpriteType.getAreaHeight();
        float maxX = sourceImageX + sourceImageWidth;
        float maxY = sourceImageY + sourceImageHeight;

        sourceImageX += areaXAdjust;
        sourceImageY += areaYAdjust;
        sourceImageWidth += areaWidthAdjust;
        sourceImageHeight += areaHeightAdjust;
        if (sourceImageX + sourceImageWidth > maxX) {
            areaWidthAdjust = maxX - sourceImageX - primarySpriteType.getAreaWidth();
        }
        if (sourceImageY + sourceImageHeight > maxY) {
            areaHeightAdjust = maxY - sourceImageY - primarySpriteType.getAreaHeight();
        }
    }

    protected int getAnimationTimeMs(long now, int totalTimeMs) {
        final int animationTimeMs;
        if (animationLoop) {
            animationTimeMs = (int) ((now - animationStartTimeMs) % totalTimeMs);
        } else {
            animationTimeMs = (int) (now - animationStartTimeMs);
        }
        return animationTimeMs;
    }

    protected int getTotalTimeMs() {
        return (int) (animatedSpriteType.getAnimationTimeMs() / animationSpeedMultiplier);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (isAnimationPlaying()) {
            final long now = System.currentTimeMillis();
            if (animationStartTimeMs == 0) {
                animationStartTimeMs = now;
            }
            if (animationStartTimeMs > now) {
                return;
            }
            final List<H5EAnimatedSpriteTypeFrame> frames = animatedSpriteType.frames;
            if(frames.size() == 1 && animationLoop) {
                frameIndex = 0;
                return;
            }
            int totalTimeMs = getTotalTimeMs();
            boolean skipLastFrame = skipLastFrameOnReversedAnimation && animationReversed;
            if(skipLastFrame) {
                totalTimeMs -= frames.get(frames.size()-1).getDurationMs();
            }
            final int animationTimeMs = getAnimationTimeMs(now, totalTimeMs);
            final boolean animationFinished = !animationLoop && (frames.size() <= 1 || totalTimeMs <= 0 || animationTimeMs > totalTimeMs);
            if (animationFinished) {
                if(animationReversed) {
                    frameIndex = 0;
                } else {
                    frameIndex = frames.size() - 1;
                }
                if (hideWhenFinished) {
                    setVisible(false);
                }
                animationPlaying = false;
                return;
            }

            if(animationReversed) {
                int currentTimeMs = 0;
                int startFrame = skipLastFrame ? frames.size() - 2 : frames.size() - 1;
                for (int i = frames.size() - 1; i >= 0; i--) {
                    H5EAnimatedSpriteTypeFrame frame = frames.get(i);
                    final int frameDurationMs = (int) (frame.getDurationMs() / animationSpeedMultiplier);
                    if (animationTimeMs < currentTimeMs + frameDurationMs) {
                        frameIndex = i;
                        break;
                    }
                    currentTimeMs += frameDurationMs;
                }
            } else {
                int currentTimeMs = 0;
                for (int i = 0; i < frames.size(); i++) {
                    H5EAnimatedSpriteTypeFrame frame = frames.get(i);
                    final int frameDurationMs = (int) (frame.getDurationMs() / animationSpeedMultiplier);
                    if (animationTimeMs < currentTimeMs + frameDurationMs) {
                        frameIndex = i;
                        break;
                    }
                    currentTimeMs += frameDurationMs;
                }
            }

        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        final H5ESpriteType spriteType;
        final H5ESpriteType spriteTypeHD;
        final int frameAdjustX;
        final int frameAdjustY;
        final float frameAdjustScale;
        boolean shouldUseHDSprite = shouldUseHDSprite();
        if (isAnimated()) {
            if (isAnimationPlaying() && animationStartTimeMs > System.currentTimeMillis()) {
                return;
            }

            final H5EAnimatedSpriteTypeFrame frame = animatedSpriteType.frames.get(frameIndex);
            spriteType = frame.getSpriteType();
            spriteTypeHD = null;
            frameAdjustX = frame.getAdjustedX();
            frameAdjustY = primarySpriteType.getHeight() - frame.getSpriteType().getHeight() - frame.getAdjustedY();
            frameAdjustScale = 1f + frame.getAdjustedScale();
        } else {
            spriteType = primarySpriteType;
            spriteTypeHD = primarySpriteTypeHD;
            frameAdjustX = 0;
            frameAdjustY = 0;
            frameAdjustScale = 1f;
        }

        if (spriteType == null) {
            return;
        }

        final float hdSpriteScaleAdjust;
        final H5ESpriteType chosenSpriteTypeForGraphics;
        final float hdOriginXOffset;
        final float hdOriginYOffset;

        if(shouldUseHDSprite) {
            chosenSpriteTypeForGraphics = spriteTypeHD;
            hdSpriteScaleAdjust = (float) spriteType.getAreaWidth() / (float) spriteTypeHD.getAreaWidth();
            hdOriginXOffset = - getOriginX() / hdSpriteScaleAdjust + getOriginX();
            hdOriginYOffset = - getOriginY() / hdSpriteScaleAdjust + getOriginY();
        } else {
            chosenSpriteTypeForGraphics = spriteType;
            hdSpriteScaleAdjust = 1f;
            hdOriginXOffset = 0;
            hdOriginYOffset = 0;
        }

        final TextureRegion textureRegion = chosenSpriteTypeForGraphics.getTextureRegion();
        if (textureRegion == null) {
            return;
        }

        chosenSpriteTypeForGraphics.stabilize();

        int sourceImageX = (int) (textureRegion.getRegionX() + chosenSpriteTypeForGraphics.getAreaX() + areaXAdjust);
        int sourceImageY = (int) (textureRegion.getRegionY() + chosenSpriteTypeForGraphics.getAreaY() + areaYAdjust);
        int regionWidth = chosenSpriteTypeForGraphics.getAreaWidth() > 0 ? chosenSpriteTypeForGraphics.getAreaWidth() : textureRegion.getRegionWidth();
        int regionHeight = chosenSpriteTypeForGraphics.getAreaHeight() > 0 ? chosenSpriteTypeForGraphics.getAreaHeight() : textureRegion.getRegionHeight();

        int sourceImageWidth = (int) (regionWidth + areaWidthAdjust);
        int sourceImageHeight = (int) (regionHeight + areaHeightAdjust);

        if (sourceImageWidth <= 0 || sourceImageHeight <= 0) {
            return;
        }

        int oldModeSrcCol = batch.getBlendSrcFunc();
        int oldModeDstCol = batch.getBlendDstFunc();
        int oldModeSrcAl = batch.getBlendSrcFuncAlpha();
        int oldModeDstAl = batch.getBlendDstFuncAlpha();
        boolean sameMode = blendingMode.isSameAsBatch(batch);
        if (!sameMode) {
            blendingMode.applyToBatch(batch);
        }

        ShaderProgram oldShader = null;
        if (shader != null) {
            oldShader = batch.getShader();
            batch.setShader(shader);
        }

        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

        float scaleX = getScaleX() * frameAdjustScale;
        float scaleY = getScaleY() * frameAdjustScale;

        if (revertViewportZoomScaling) {
            if (isInGameLayer()) {
                scaleX *= getEngine().getGameZoom();
                scaleY *= getEngine().getGameZoom();
            } else if (getLayer() instanceof H5EZoomAwareLayer) {
                float zoom = ((H5EZoomAwareLayer) getLayer()).getZoom();
                scaleX *= zoom;
                scaleY *= zoom;
            }
        }

        float rotation = getRotation();
        if (revertViewportRotation && isInGameLayer()) {
            rotation -= getEngine().getRotation();
        }

        // Calculate 3D shift if applicable
        float shiftX = 0;
        float shiftY = 0;
        float shiftScale = 1;
        if (shift3DZ != 0) {
            OrthographicCamera camera = getEngine().getGameCamera();
            float shiftFactor = (1 / (FACTOR_3D_SHIFT * (camera.zoom + 5))) * shift3DZ;

            shiftScale = 1f+(shiftFactor*5+0.3f);

            Vector2 coords = tmp;
            coords.x = getOriginX();
            coords.y = getOriginY();
            // It's possible we could avoid doing these transformations by using the batch's matrices somehow?
            localToStageCoordinates(coords);
            getStage().stageToScreenCoordinates(coords);


            coords.x = (coords.x - camera.viewportWidth / 2) * shiftFactor;
            coords.y = (coords.y - camera.viewportHeight / 2) * shiftFactor;

            coords.rotateDeg(getEngine().getRotation());

            shiftX = coords.x;
            shiftY = coords.y;
        }

        batch.draw(textureRegion.getTexture(),
                getX() + shiftX + frameAdjustX + hdOriginXOffset, getY() - shiftY + frameAdjustY + hdOriginYOffset,
                (getOriginX() - frameAdjustX) / hdSpriteScaleAdjust, (getOriginY() - frameAdjustY) / hdSpriteScaleAdjust,
                sourceImageWidth, sourceImageHeight,
                scaleX*shiftScale*hdSpriteScaleAdjust, scaleY*shiftScale*hdSpriteScaleAdjust,
                rotation,
                sourceImageX,
                sourceImageY,
                sourceImageWidth,
                sourceImageHeight,
                false,
                false);

        if (compositeSpriteConfiguration != null) {
            JSONObject config = compositeSpriteConfiguration.getFinalConfiguration();
            JSONArray layerDefinitions = (JSONArray) config.get("layerDefinitions");

            if (layerDefinitions.isEmpty()) {
                return;
            }

            JSONObject compositePrimarySpriteJson = (JSONObject) layerDefinitions.get(0);
            String composeitePrimarySpriteTypeKey = (String) compositePrimarySpriteJson.get("spriteTypeKey");
            H5ESpriteType compositePrimarySpriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(composeitePrimarySpriteTypeKey);
            if (compositePrimarySpriteType == null) {
                throw new IllegalStateException("Sprite type '" + composeitePrimarySpriteTypeKey + "' could not be found.");
            }

            // Go through each of the layers in the raw json layer definition and generate child graphic elements from it
            for (Object layerDefinitionObj : layerDefinitions) {
                JSONObject layerDefinition = (JSONObject) layerDefinitionObj;
                String layerType = (String) layerDefinition.get("type");

                if (layerType.equals("Sprite")) {
                    drawLayerTypeSprite(batch, layerDefinition, scaleX, scaleY);
                }
                //TODO: Handle color layer types
                else {
                    throw new IllegalArgumentException("Unhandled type: " + layerType);
                }

            }
        }
        if (shader != null) {
            batch.setShader(oldShader);
        }
    }

    protected boolean isInGameLayer() {
        return getStage().getViewport() == getEngine().getGameViewport();
    }

    @Override
    public Actor hit(float x, float y, boolean touchable) {
        final Vector2 global = tmp2;
        global.set(x, y);
        localToStageCoordinates(global);
        if (touchable && this.getTouchable() != Touchable.enabled) {
            return null;
        }
        if (revertViewportZoomScaling) {
            final Vector2 vec = tmp;
            vec.set(x, y);
            localToParentCoordinates(vec);
            parentToLocalCoordinatesWithRevertedZoomScaling(vec);
            x = vec.x;
            y = vec.y;
        }
        if(super.hit(x, y, touchable)==this){
            if(hitCheck==null || hitCheck.call(global.x, global.y)){
                return this;
            }
        }
        return null;
    }

    public void setHitCheck(ReturningCallable2Args<Boolean, Float, Float> callable2Args){
        this.hitCheck = callable2Args;
    }

    private Vector2 parentToLocalCoordinatesWithRevertedZoomScaling(Vector2 parentCoords) {
        final float rotation = getRotation();
        float zoomFactor = getLayer() instanceof H5EZoomAwareLayer
                ? ((H5EZoomAwareLayer) getLayer()).getZoom()
                : getEngine().getGameZoom();
        final float scaleX = getScaleX() * zoomFactor;
        final float scaleY = getScaleY() * zoomFactor;
        final float childX = getX();
        final float childY = getY();
        final float originX = this.getOriginX();
        final float originY = this.getOriginY();
        if (rotation == 0) {
            if (scaleX == 1 && scaleY == 1) {
                parentCoords.x -= childX;
                parentCoords.y -= childY;
            } else {

                parentCoords.x = (parentCoords.x - childX - originX) / scaleX + originX;
                parentCoords.y = (parentCoords.y - childY - originY) / scaleY + originY;
            }
        } else {
            final float cos = (float) Math.cos(rotation * MathUtils.degreesToRadians);
            final float sin = (float) Math.sin(rotation * MathUtils.degreesToRadians);
            final float tox = parentCoords.x - childX - originX;
            final float toy = parentCoords.y - childY - originY;
            parentCoords.x = (tox * cos + toy * sin) / scaleX + originX;
            parentCoords.y = (tox * -sin + toy * cos) / scaleY + originY;
        }
        return parentCoords;
    }

    private void drawLayerTypeSprite(Batch batch, JSONObject layerDefinition, float scaleX, float scaleY) {
        // Get the sprite type
        String spriteTypeKey = (String) layerDefinition.get("spriteTypeKey");
        if (layerSpriteTypes == null) {
            layerSpriteTypes = new HashMap<>();
        }
        H5ESpriteType spriteType = layerSpriteTypes.get(spriteTypeKey);
        if (spriteType == null) {
            spriteType = (H5ESpriteType) getEngine().getResourceManager().getSpriteType(spriteTypeKey);
            if (spriteType == null) {
                throw new IllegalStateException("Sprite type '" + spriteTypeKey + "' could not be found.");
            }
            layerSpriteTypes.put(spriteTypeKey, spriteType);
        }

        // Get the parameters for this layer
        Number transparency = (Number) layerDefinition.get("transparency");
        if (transparency == null) {
            transparency = 1f;
        }
        BlendingMode blendingMode = BlendingMode.DEFAULT;
        if (layerDefinition.get("blendingMode") != null) {
            blendingMode = BlendingMode.valueOf((String) layerDefinition.get("blendingMode"));
        }

        blendingMode.applyToBatch(batch);
        batch.setColor(Color.WHITE);
        batch.getColor().a = transparency.floatValue();

        final TextureRegion textureRegion = spriteType.getTextureRegion();

        final float width = getWidth();
        final float height = getHeight();
        int sourceImageWidth = (int) Math.min(textureRegion.getRegionWidth(), width);
        int sourceImageHeight = (int) Math.min(textureRegion.getRegionHeight(), height);


        float rotation = getRotation();
        if (revertViewportRotation) {
            rotation -= getEngine().getRotation();
        }

        batch.draw(textureRegion.getTexture(),
                getX(), getY(),
                getOriginX(), getOriginY(),
                width, height,
                scaleX, scaleY,
                rotation,
                textureRegion.getRegionX(),
                textureRegion.getRegionY(),
                sourceImageWidth,
                sourceImageHeight,
                false,
                false);
    }

    public CompositeSpriteConfiguration getCompositeSpriteConfiguration() {
        return compositeSpriteConfiguration;
    }

    public void setCompositeSpriteConfiguration(CompositeSpriteConfiguration compositeSpriteConfiguration) {
        this.compositeSpriteConfiguration = compositeSpriteConfiguration;
    }

    public boolean isRevertViewportZoomScaling() {
        return revertViewportZoomScaling;
    }

    public void setRevertViewportZoomScaling(boolean revertViewportZoomScaling) {
        this.revertViewportZoomScaling = revertViewportZoomScaling;
    }

    public boolean isRevertViewportRotation() {
        return revertViewportRotation;
    }

    public void setRevertViewportRotation(boolean revertViewportRotation) {
        this.revertViewportRotation = revertViewportRotation;
    }

    public float getAnimationSpeedMultiplier() {
        return animationSpeedMultiplier;
    }

    public void setAnimationSpeedMultiplier(float animationSpeedMultiplier) {
        this.animationSpeedMultiplier = animationSpeedMultiplier;
    }

    public boolean hidesWhenFinished() {
        return hideWhenFinished;
    }

    public void setHideWhenFinished(boolean hideWhenFinished) {
        this.hideWhenFinished = hideWhenFinished;
    }

    public boolean isAnimationReversed() {
        return animationReversed;
    }

    public void setAnimationReversed(boolean animationReversed) {
        this.animationReversed = animationReversed;
    }

    public boolean isSkipLastFrameOnReversedAnimation() {
        return skipLastFrameOnReversedAnimation;
    }

    public void setSkipLastFrameOnReversedAnimation(boolean skipLastFrameOnReversedAnimation) {
        this.skipLastFrameOnReversedAnimation = skipLastFrameOnReversedAnimation;
    }

    @Override
    public int getLevel() {
        if (level == null) {
            return getLayer().getDefaultLevel();
        }
        return level;
    }

    @Override
    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public boolean isVisible() {
        final boolean visible = super.isVisible();
        if (visible && isAnimationPlaying()) {
            return System.currentTimeMillis() >= animationStartTimeMs;
        }
        return visible;
    }

    public ShaderProgram getShader() {
        return shader;
    }

    public void setShader(ShaderProgram shader) {
        this.shader = shader;
    }

    public boolean isVisibleWithinViewport() {
        if (!isVisible()) return false;

        // Get the bounding rectangle that describes the boundary of our sprite based on position, size, and scale.
        // TODO: Make this better, this is extremely approximate (it should be for speed, but perhaps not THIS bad)
        final Rectangle spriteBounds = new Rectangle(
                getX() - (getWidth() * getScaleX()),
                getY() - (getHeight() * getScaleY()),
                getX() + (getWidth() * getScaleX()),
                getY() + (getHeight() * getScaleY()));

        // Get the bounding rectangle that our screen.  If using a camera you would create this based on the camera's
        // position and viewport width/height instead.
        final Rectangle screenBounds = getEngine().getGameViewport().getCullingArea();

        return spriteBounds.overlaps(screenBounds);
    }


    @SuppressWarnings("RedundantIfStatement")
    private boolean shouldUseHDSprite() {
        if (primarySpriteTypeHD == null) {
            return false;
        }

        if (isAnimated()) {
            return false;
        }

        if (1f / getEngine().getGameZoom() <= 1) {
            return false;
        }

        return true;
    }
}
