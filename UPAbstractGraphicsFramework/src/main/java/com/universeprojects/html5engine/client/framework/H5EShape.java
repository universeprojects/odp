package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ShortArray;

import java.util.List;

public class H5EShape extends H5EGraphicElement{

    public enum Shape{
        CIRCLE, RECTANGLE, POLY, UNDEFINED;
    }

    private Shape shape;
    //circle
    private float radius;

    private List<Vector2> polyshapeVertices;

    private ShapeRenderer renderer;

    private Color color = new Color(1,1,1,0.4f);

    private  final EarClippingTriangulator triangulator = new EarClippingTriangulator();

    public H5EShape(H5ELayer layer){
        super(layer);
        this.shape = Shape.POLY;
        renderer = new ShapeRenderer();
    }
    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        renderer.setProjectionMatrix(batch.getProjectionMatrix());
        renderer.setTransformMatrix(batch.getTransformMatrix());
        batch.end();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        renderer.begin(ShapeRenderer.ShapeType.Filled);
        renderer.scale(100,100,0);
        renderer.setColor(color);
        if(shape == Shape.CIRCLE){
            renderer.circle(getX(),getY(),radius,30);
        }else if(shape == Shape.POLY){
            if(polyshapeVertices.size()>0) {
                float[] vertices = new float[polyshapeVertices.size()*2];
                for(int i = 0;i<polyshapeVertices.size();i++){
                    vertices[i*2]=polyshapeVertices.get(i).x;
                    vertices[i*2+1]=polyshapeVertices.get(i).y;
                }
                ShortArray trianglesVertices = triangulator.computeTriangles(vertices);
                for (int i = 0; i < trianglesVertices.size - 2; i = i + 3)
                {
                    float x1 = vertices[trianglesVertices.get(i) * 2];
                    float y1 = vertices[(trianglesVertices.get(i) * 2) + 1];

                    float x2 = vertices[(trianglesVertices.get(i + 1)) * 2];
                    float y2 = vertices[(trianglesVertices.get(i + 1) * 2) + 1];

                    float x3 = vertices[trianglesVertices.get(i + 2) * 2];
                    float y3 = vertices[(trianglesVertices.get(i + 2) * 2) + 1];

                    renderer.triangle(x1, y1, x2, y2, x3, y3);
                }
            }
        }
        renderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
        batch.begin();
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }

    public void setPolyVertices(List<Vector2> vertices){
        polyshapeVertices = vertices;
    }

    public void setShape(Shape shape) {
        this.shape = shape;
    }


}
