package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.controllers.mappings.Xbox;
import com.universeprojects.common.shared.util.GwtIncompatible;

public class PlatformSpecificH5EngineConstants {
    public static int XBOX_L_STICK = -1;
    public static int XBOX_R_STICK = -1;

    private static Runnable initializer = () -> {
    };

    public static void initialize() {
        initializer.run();
    }

    @GwtIncompatible
    public static void setForNonGwt() {
        initializer = () -> {
            XBOX_L_STICK = Xbox.L_STICK;
            XBOX_R_STICK = Xbox.R_STICK;
        };
    }

    public static void setForGwt() {
        initializer = () -> {
        };
    }
}
