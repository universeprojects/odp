package com.universeprojects.html5engine.client.framework.inputs;

import com.badlogic.gdx.InputMultiplexer;
import com.universeprojects.html5engine.client.framework.H5EEngine;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;


@SuppressWarnings("unused")
public abstract class H5EInputManager {
    protected final Map<String, H5EControlSetup> controlSetupsByName = new TreeMap<>();
    protected final Set<H5EControlSetup> activeControlSetups = new LinkedHashSet<>();
    protected final Map<H5ECommandHandler, List<H5ECommandParams>> commandsToExecute = new LinkedHashMap<>();
    protected final H5EEngine engine;
    protected final InputMultiplexer inputMultiplexer = new InputMultiplexer();

    private boolean enabled = true;
    private Float currentRotation;

    public H5EInputManager(H5EEngine engine) {
        this.engine = engine;
        setEnabled(true);
    }

    public void activateControlSetup(H5EControlSetup controlSetup) {
        boolean success = activeControlSetups.add(controlSetup);
        if(success) {
            inputMultiplexer.addProcessor(controlSetup);
        }
    }

    public void deactivateControlSetup(H5EControlSetup controlSetup) {
        boolean success = activeControlSetups.remove(controlSetup);
        if(success) {
            inputMultiplexer.removeProcessor(controlSetup);
        }
    }

    @SuppressWarnings("unused")
    public void activateControlSetup(String name) {
        activateControlSetup(controlSetupsByName.get(name));
    }

    @SuppressWarnings("unused")
    public void deactivateControlSetup(String name) {
        deactivateControlSetup(controlSetupsByName.get(name));
    }

    public H5EControlSetup getControlSetup(String name, boolean create) {
        H5EControlSetup state = controlSetupsByName.get(name);
        if (state == null && create) {
            state = new H5EControlSetup(this, name);
            controlSetupsByName.put(name, state);
        }
        return state;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        if(this.enabled == enabled) {
            return;
        }
        this.enabled = enabled;
        if (enabled) {
            engine.addInputProcessor(inputMultiplexer);
        } else {
            engine.removeInputProcessor(inputMultiplexer);
        }
    }


    /**
     * Poll all the controls and execute commands
     */
    public void doTick() {
        if (!enabled) {
            return;
        }

        for (H5EControlSetup state : activeControlSetups) {
            state.doTick();
        }

        // execute all commands pending for this tick, and clear for the next tick
        for (Map.Entry<H5ECommandHandler, List<H5ECommandParams>> entry : commandsToExecute.entrySet()) {
            for (H5ECommandParams param : entry.getValue()) {
                entry.getKey().execute(param);
            }
        }
        commandsToExecute.clear();
    }

    protected void addCommandToExecute(H5ECommandHandler cmd, H5ECommandParams params) {
        List<H5ECommandParams> list = commandsToExecute.get(cmd);
        //noinspection Java8MapApi
        if(list == null) {
            list = new ArrayList<>();
            commandsToExecute.put(cmd, list);
        }
        list = commandsToExecute.get(cmd);
        list.add(params);
    }

    public void clearCommandsToExecute() {
        commandsToExecute.clear();
    }
}
