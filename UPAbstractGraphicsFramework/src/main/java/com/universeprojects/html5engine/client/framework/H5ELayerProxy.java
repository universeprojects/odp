package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.Viewport;

public class H5ELayerProxy extends H5ELayer {
    protected final H5ELayer layer;

    public H5ELayerProxy(H5ELayer layer) {
        super(layer.getEngine(), layer.getName() + " Proxy", ((UPViewport) layer.getViewport()));
        this.layer = layer;
        this.layer.setLayerProxy(this);
        // We will handle the events for that layer
        engine.inputMultiplexer.removeProcessor(layer.inputProcessor);
    }

    @Override
    public Group createLevel(int level) {
        return null;
    }

    @Override
    public void onBeforeLevelDraw(Batch batch, int level) {
        layer.onBeforeLevelDraw(batch, level);
    }

    @Override
    public void onAfterLevelDraw(Batch batch, int level) {
        layer.onAfterLevelDraw(batch, level);
    }

    @Override
    public void draw() {
        layer.draw();
    }

    @Override
    public void setLevelEventTransparent(int level, boolean transparent) {
        layer.setLevelEventTransparent(level, transparent);
    }

    @Override
    public int translateLevelName(String name) {
        return layer.translateLevelName(name);
    }

    @Override
    public int getDefaultLevel() {
        return layer.getDefaultLevel();
    }

    @Override
    public void setEventTransparent(boolean value) {
        layer.setEventTransparent(value);
    }

    @Override
    public boolean isEventTransparent() {
        return layer.isEventTransparent();
    }

    @Override
    public boolean receivesInputs() {
        return layer.receivesInputs();
    }

    @Override
    public void show() {
        layer.show();
    }

    @Override
    public void hide() {
        layer.hide();
    }

    @Override
    public boolean toggleVisibility() {
        return layer.toggleVisibility();
    }

    @Override
    public void setVisible(boolean newVisible) {
        layer.setVisible(newVisible);
    }

    @Override
    public boolean isVisible() {
        return layer.isVisible();
    }

    @Override
    public void removeAllElements() {
        layer.removeAllElements();
    }

    @Override
    public H5EEngine getEngine() {
        return layer.getEngine();
    }

    @Override
    public void addActorToTop(Actor el) {
        layer.addActorToTop(el);
    }

    @Override
    public void addActorToTop(Actor el, Integer levelIndex) {
        layer.addActorToTop(el, levelIndex);
    }

    @Override
    public void setLevelShader(int level, ShaderProgram shader) {
        layer.setLevelShader(level, shader);
    }

    @Override
    public void clearLevelShader(int level) {
        layer.clearLevelShader(level);
    }

    @Override
    public ShaderProgram getLevelShader(int level) {
        return layer.getLevelShader(level);
    }

    @Override
    public ShaderProgram getLayerShader() {
        return layer.getLayerShader();
    }

    @Override
    public void setLayerShader(ShaderProgram layerShader) {
        layer.setLayerShader(layerShader);
    }

    @Override
    public void act() {
        layer.act();
    }

    @Override
    public void act(float delta) {
        layer.act(delta);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return layer.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return layer.touchDragged(screenX, screenY, pointer);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return layer.touchUp(screenX, screenY, pointer, button);
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return layer.mouseMoved(screenX, screenY);
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return layer.scrolled(amountX, amountY);
    }

    @Override
    public boolean keyDown(int keyCode) {
        return layer.keyDown(keyCode);
    }

    @Override
    public boolean keyUp(int keyCode) {
        return layer.keyUp(keyCode);
    }

    @Override
    public boolean keyTyped(char character) {
        return layer.keyTyped(character);
    }

    @Override
    public void addTouchFocus(EventListener listener, Actor listenerActor, Actor target, int pointer, int button) {
        layer.addTouchFocus(listener, listenerActor, target, pointer, button);
    }

    @Override
    public void removeTouchFocus(EventListener listener, Actor listenerActor, Actor target, int pointer, int button) {
        layer.removeTouchFocus(listener, listenerActor, target, pointer, button);
    }

    @Override
    public void cancelTouchFocus(Actor actor) {
        layer.cancelTouchFocus(actor);
    }

    @Override
    public void cancelTouchFocus() {
        layer.cancelTouchFocus();
    }

    @Override
    public void cancelTouchFocusExcept(EventListener exceptListener, Actor exceptActor) {
        layer.cancelTouchFocusExcept(exceptListener, exceptActor);
    }

    @Override
    public void addActor(Actor actor) {
        layer.addActor(actor);
    }

    @Override
    public void addAction(Action action) {
        layer.addAction(action);
    }

    @Override
    public Array<Actor> getActors() {
        return layer.getActors();
    }

    @Override
    public boolean addListener(EventListener listener) {
        return layer.addListener(listener);
    }

    @Override
    public boolean removeListener(EventListener listener) {
        return layer.removeListener(listener);
    }

    @Override
    public boolean addCaptureListener(EventListener listener) {
        return layer.addCaptureListener(listener);
    }

    @Override
    public boolean removeCaptureListener(EventListener listener) {
        return layer.removeCaptureListener(listener);
    }

    @Override
    public void clear() {
        layer.clear();
    }

    @Override
    public void unfocusAll() {
        layer.unfocusAll();
    }

    @Override
    public void unfocus(Actor actor) {
        layer.unfocus(actor);
    }

    @Override
    public boolean setKeyboardFocus(Actor actor) {
        return layer.setKeyboardFocus(actor);
    }

    @Override
    public Actor getKeyboardFocus() {
        return layer.getKeyboardFocus();
    }

    @Override
    public boolean setScrollFocus(Actor actor) {
        return layer.setScrollFocus(actor);
    }

    @Override
    public Actor getScrollFocus() {
        return layer.getScrollFocus();
    }

    @Override
    public Batch getBatch() {
        return layer.getBatch();
    }

    @Override
    public Viewport getViewport() {
        return layer.getViewport();
    }

    @Override
    public void setViewport(Viewport viewport) {
        layer.setViewport(viewport);
    }

    @Override
    public float getWidth() {
        return layer.getWidth();
    }

    @Override
    public float getHeight() {
        return layer.getHeight();
    }

    @Override
    public Camera getCamera() {
        return layer.getCamera();
    }

    @Override
    public Group getRoot() {
        if (layer == null)
            return super.getRoot();
        return layer.getRoot();
    }

    @Override
    public void setRoot(Group root) {
        layer.setRoot(root);
    }

    @Override
    public Actor hit(float stageX, float stageY, boolean touchable) {
        return layer.hit(stageX, stageY, touchable);
    }

    @Override
    public Vector2 screenToStageCoordinates(Vector2 screenCoords) {
        return layer.screenToStageCoordinates(screenCoords);
    }

    @Override
    public Vector2 stageToScreenCoordinates(Vector2 stageCoords) {
        return layer.stageToScreenCoordinates(stageCoords);
    }

    @Override
    public Vector2 toScreenCoordinates(Vector2 coords, Matrix4 transformMatrix) {
        return layer.toScreenCoordinates(coords, transformMatrix);
    }

    @Override
    public void calculateScissors(Rectangle localRect, Rectangle scissorRect) {
        layer.calculateScissors(localRect, scissorRect);
    }

    @Override
    public void setActionsRequestRendering(boolean actionsRequestRendering) {
        layer.setActionsRequestRendering(actionsRequestRendering);
    }

    @Override
    public boolean getActionsRequestRendering() {
        return layer.getActionsRequestRendering();
    }

    @Override
    public Color getDebugColor() {
        return layer.getDebugColor();
    }

    @Override
    public void setDebugInvisible(boolean debugInvisible) {
        layer.setDebugInvisible(debugInvisible);
    }

    @Override
    public void setDebugAll(boolean debugAll) {
        layer.setDebugAll(debugAll);
    }

    @Override
    public boolean isDebugAll() {
        return layer.isDebugAll();
    }

    @Override
    public void setDebugUnderMouse(boolean debugUnderMouse) {
        layer.setDebugUnderMouse(debugUnderMouse);
    }

    @Override
    public void setDebugParentUnderMouse(boolean debugParentUnderMouse) {
        layer.setDebugParentUnderMouse(debugParentUnderMouse);
    }

    @Override
    public void setDebugTableUnderMouse(Table.Debug debugTableUnderMouse) {
        layer.setDebugTableUnderMouse(debugTableUnderMouse);
    }

    @Override
    public void setDebugTableUnderMouse(boolean debugTableUnderMouse) {
        layer.setDebugTableUnderMouse(debugTableUnderMouse);
    }

    @Override
    public void dispose() {
        layer.dispose();
    }

    public void wipeCache() {

    }
}
