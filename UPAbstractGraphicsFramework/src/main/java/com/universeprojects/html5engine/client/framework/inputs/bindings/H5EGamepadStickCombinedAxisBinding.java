package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator2Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EGamepadStickCombinedAxisBinding<C extends H5ECommand> extends H5EGamepadBinding<C> {

    public final int axisCode;
    public final int inputParameter;
    private final H5ECommandInputTranslator2Args<Boolean, Float, C> translator;
    private final boolean isX;
    private H5EGamepadStickCombinedAxisBinding<?> otherAxis;

    public H5EGamepadStickCombinedAxisBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, C command, int gamepadIndex, int axisCode,
                                              int inputParameter, boolean isX) {
        super(controlSetup, command, gamepadIndex);
        this.translator = translator;
        this.axisCode = axisCode;
        this.inputParameter = inputParameter;
        this.isX = isX;
    }

    public void setOtherAxis(H5EGamepadStickCombinedAxisBinding<?> otherAxis) {
        this.otherAxis = otherAxis;
    }

    @Override
    public void doTickWhenActive() {
        if (!isX || otherAxis == null) {
            return;
        }
        Vector2 vec = gamePad.getAxesWithDeadzone(axisCode, otherAxis.axisCode);

        fireSelf(vec.x);
        otherAxis.fireSelf(vec.y);
    }

    protected void fireSelf(float strength) {
        H5ECommandParams<C> inp = null;
        if (strength != 0d) {
            inp = translator.generateParams(command, true, strength);
        } else {// if (otherAxis.isActive()) {
            inp = translator.generateParams(command, false, 0f);
        }
        fire(inp);
    }
}