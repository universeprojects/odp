package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.utils.Timer;

import java.util.ArrayList;
import java.util.List;

public abstract class InputExtendedAdapter extends InputAdapter {

    private final List<Integer> pressedKeys;
    private float doublePressInterval;

    public InputExtendedAdapter(float doublePressInterval){
        super();
        pressedKeys = new ArrayList<>();
        this.doublePressInterval = doublePressInterval;
    }

    public abstract void doublePress(int keycode);

    @Override
    public boolean keyDown(int keycode) {
        if(pressedKeys.contains(keycode)){
            doublePress( keycode);
            pressedKeys.remove((Integer) keycode);
            return true;
        }else{
            pressedKeys.add(keycode);
            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    pressedKeys.remove((Integer) keycode);
                }
            },doublePressInterval);
        }
        return super.keyDown(keycode);
    }
}
