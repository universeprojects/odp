package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Log;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;

public class H5EJoystick extends InputAdapter {

    public static final String JOYSTICK_INNER_KEY = "images/joystick-inner.png";
    public static final String JOYSTICK_OUTER_KEY = "images/joystick-outer.png";
    private static final int DETECTION_RADIUS = 90;
    private static final int DETECTION_RADIUS2 = DETECTION_RADIUS * DETECTION_RADIUS;
    @SuppressWarnings("FieldCanBeLocal")
    private final H5EEngine engine;
    private H5ESprite joystickOuter;
    private H5ESprite joystickInner;

    private boolean isActive;
    private int activePointer;
    private H5EJoystickState state;

    public H5EJoystick(H5ELayer layer, float proportionX, float proportionY, float x, float y) {
        Dev.checkNotNull(layer);

        engine = layer.getEngine();

        joystickOuter = new H5ESprite(layer, JOYSTICK_OUTER_KEY);
        joystickOuter.addTo(layer);
        joystickOuter.setOrigin(joystickOuter.getWidth() / 2f, joystickOuter.getHeight() / 2f);
        joystickOuter.setX(x);
        joystickOuter.setY(y);

        joystickInner = new H5ESprite(layer, JOYSTICK_INNER_KEY);
        joystickInner.addTo(layer);
        joystickInner.setOrigin(joystickInner.getWidth() / 2f, joystickInner.getHeight() / 2f);
        joystickInner.setX(x);
        joystickInner.setY(y);

        //joystickInner.setPerPixelDetection(true);
        //joystickInner.setDetectionThreshold(200);

        engine.addInputProcessor(this);
        reset();
    }

    public void hide() {
        joystickOuter.setVisible(false);
        joystickInner.setVisible(false);
    }

    public void show() {
        joystickOuter.setVisible(true);
        joystickInner.setVisible(true);
    }

    private void reset() {
        isActive = false;
        activePointer = -1;
        state = new H5EJoystickState(0, 0);
        joystickInner.setX(joystickOuter.getX());
        joystickInner.setY(joystickOuter.getY());
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (!isActive)
            return false;
        Log.info("Recieving pointerUp with actionId " + pointer);
        if (activePointer != pointer)
            return false; // multi-touch
        Log.info("Same ID - Resetting joystick");
        reset();
        return true;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (isActive)
            return false;

        Vector2 loc = joystickOuter.screenToLocalCoordinates(new Vector2(screenX, screenY));
        loc.add(-joystickOuter.getWidth() / 2, -joystickOuter.getHeight() / 2);

        float distanceFromCenter2 = loc.len2();
        if (distanceFromCenter2 > DETECTION_RADIUS2) {
            // too far
            return false;
        }

        isActive = true;
        Log.info("Activating joystick with actionId " + activePointer);

        touchDragged(screenX, screenY, pointer);
        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (!isActive)
            return false;
        if (activePointer != pointer)
            return false; // multi-touch

        Vector2 loc = joystickOuter.screenToLocalCoordinates(new Vector2(-joystickOuter.getWidth() / 2, -joystickOuter.getHeight() / 2));

        float stateX = loc.x;
        float stateY = loc.y;
//        float distanceFromCenter = loc.len();
		/*if (distanceFromCenter > DETECTION_RANGE) {
			// pointer moved too far. release joystick
			reset();
			return;
		}*/

		/*if (distanceFromCenter > MOTION_RANGE) {
			// make sure that the inner part doesn't come out of the circle
			float proportion = MOTION_RANGE / distanceFromCenter;
			stateX *= proportion;
			stateY *= proportion;
		}*/

        float relX = stateX / DETECTION_RADIUS;
        float relY = stateY / DETECTION_RADIUS;
        if (relX > 1) relX = 1;
        if (relX < -1) relX = -1;
        if (relY > 1) relY = 1;
        if (relY < -1) relY = -1;

        joystickInner.setX(joystickOuter.getX() + stateX);
        joystickInner.setY(joystickOuter.getY() + stateY);
        state = new H5EJoystickState(relX, relY);
        return true;
    }

    public H5EJoystickState getState() {
        return state;
    }

}
