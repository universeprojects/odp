package com.universeprojects.html5engine.client.framework.inputs;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.controllers.mappings.Xbox;
import com.badlogic.gdx.math.Vector2;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.html5engine.client.framework.GdxUtils;
import com.universeprojects.html5engine.client.framework.PlatformSpecificH5EngineConstants;

@SuppressWarnings({"unused", "WeakerAccess"})
public class H5EGamePad {

    public static final int FACE_BUTTON_A = Xbox.A;
    public static final int FACE_BUTTON_B = Xbox.B;
    public static final int FACE_BUTTON_X = Xbox.X;
    public static final int FACE_BUTTON_Y = Xbox.Y;

    public static final int LEFT_BUTTON = Xbox.L_BUMPER;
    public static final int RIGHT_BUTTON = Xbox.R_BUMPER;
    public static final int LEFT_TRIGGER;
    public static final int RIGHT_TRIGGER = 4;
    public static final int SELECT = Xbox.BACK;
    public static final int START = Xbox.START;
    //FIXME: These bindings are missing in the GWT emulation version of the Xbox class
    public static final int LEFT_STICK = PlatformSpecificH5EngineConstants.XBOX_L_STICK;
    public static final int RIGHT_STICK = PlatformSpecificH5EngineConstants.XBOX_R_STICK;
    public static final int DPAD_UP = 12;
    public static final int DPAD_DOWN = 13;
    public static final int DPAD_LEFT = 14;
    public static final int DPAD_RIGHT = 15;



    public static final int AXIS_LEFT_Y = Xbox.L_STICK_VERTICAL_AXIS;
    public static final int AXIS_LEFT_X = Xbox.L_STICK_HORIZONTAL_AXIS;
    public static final int AXIS_RIGHT_Y;
    public static final int AXIS_RIGHT_X;
    static {
        if (GdxUtils.isType(Application.ApplicationType.Android)) {
            AXIS_RIGHT_Y = 3;
            AXIS_RIGHT_X = 2;
            LEFT_TRIGGER = 5;
        } else {
            AXIS_RIGHT_Y = 2;
            AXIS_RIGHT_X = 3;
            LEFT_TRIGGER = 4;
        }
    }

    public static final float DEFAULT_DEAD_ZONE = 0.2f;

    private static boolean initialized = false;

    private int index;
    private Controller controller;
    public float deadZone = DEFAULT_DEAD_ZONE;

    /**
     * Create a new instance of a gamepad.
     *
     * @param index The index of the gamepad (0-3).
     */
    public H5EGamePad(int index) {
        this.index = index;
    }

    public static int getGamepadCount() {
        return Controllers.getControllers().size;
    }

    /**
     * Gets the index of the gamepad.
     *
     * @return The index of the gamepad.
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * Sets the index of the gamepad.
     *
     * @param index The index of the gamepad.
     */
    public void setIndex(int index) {
        this.index = index;
    }

    /**
     * Gets the deadZone of the gamepad.
     *
     * @return The deadZone of the gamepad.
     */
    public float getDeadZone() {
        return this.deadZone;
    }

    /**
     * Sets the deadZone of the gamepad.
     *
     * @param deadZone The deadZone of the gamepad.
     */
    public void setDeadZone(float deadZone) {
        this.deadZone = deadZone;
    }

    /**
     * Polls the gamepad to check for changes. Needs to be called every tick.
     */
    public void poll() {
        this.controller = Controllers.getControllers().get(this.index);
    }

    /**
     * Check whether or not a button is pressed.
     *
     * @param id The button id.
     * @return If the button is pressed true, otherwise false.
     */
    public boolean isDown(int id) {
        return this.controller != null && this.controller.getButton(id);

    }

    public float getTrigger(){
        if(LEFT_TRIGGER==RIGHT_TRIGGER){
            return getAxis(LEFT_TRIGGER);
        }else{
            if(getLeftTrigger()<0.05f && getRightTrigger()<0.05f){
                return 0;
            }else if(getRightTrigger()>0.05f){
                return -getRightTrigger();
            }else return getLeftTrigger();
        }
    }

    /**
     * Check whether or not a button isn't pressed.
     *
     * @param id The button id.
     * @return If the button is pressed false, otherwise true.
     */
    public boolean isUp(int id) {
        return this.controller != null && !this.controller.getButton(id);

    }

    /**
     * Get the float value of one of the axis without the deadZone calculated.
     *
     * @param id The id of the axis.
     * @return The float value of the axis.
     */
    public float getAxis(int id) {
        if (this.controller == null)
            return 0;

        return this.controller.getAxis(id);
    }

    public float getAxisWithDeadzone(int id) {
        if (this.controller == null)
            return 0;
        int xID;
        int yID;
        if(id == AXIS_LEFT_X || id == AXIS_LEFT_Y){
            xID = AXIS_LEFT_X;
            yID = AXIS_LEFT_Y;
        }else{
            xID = AXIS_RIGHT_X;
            yID = AXIS_RIGHT_Y;
        }
        float x = this.controller.getAxis(xID);
        float y = this.controller.getAxis(yID);

        float magnitude2 = x * x + y * y;

        if (magnitude2 < this.deadZone * this.deadZone)
            return 0;
        if (id == AXIS_LEFT_X || id == AXIS_RIGHT_X)
            return x;
        else
            return y;
    }

    public Vector2 getAxesWithDeadzone(int xID, int yID) {
        float x = this.controller.getAxis(xID);
        float y = this.controller.getAxis(yID);

        float magnitude2 = x * x + y * y;

        if (magnitude2 < this.deadZone * this.deadZone)
            return new Vector2(0, 0);
        return new Vector2(x, y);
    }

    /**
     * Gets the name of the gamepad.
     *
     * @return Name of the gamepad.
     */
    public String getName() {
        if (this.controller == null)
            return "";

        return this.controller.getName();
    }

    /**
     * Gets the float value of the left trigger.
     *
     * @return The float value of the left trigger.
     */
    public float getLeftTrigger() {
        return getAxis(LEFT_TRIGGER);
    }

    /**
     * Gets the float value of the right trigger.
     *
     * @return The float value of the right trigger.
     */
    public float getRightTrigger() {
        return getAxis(RIGHT_TRIGGER);
    }

    /**
     * Gets the right X axis with the dead zone calculated.
     *
     * @return The right X axis.
     */
    public float getRightXAxis() {
        return getAxisWithDeadzone(AXIS_RIGHT_X, AXIS_RIGHT_Y, true);
    }

    /**
     * Gets the right Y axis with the dead zone calculated.
     *
     * @return The right Y axis.
     */
    public float getRightYAxis() {
        return getAxisWithDeadzone(AXIS_RIGHT_X, AXIS_RIGHT_Y, false);
    }

    /**
     * Gets the left X axis with the dead zone calculated.
     *
     * @return The left X axis.
     */
    public float getLeftXAxis() {
        return getAxisWithDeadzone(AXIS_LEFT_X, AXIS_LEFT_Y, true);
    }

    /**
     * Gets the left Y axis with the dead zone calculated.
     *
     * @return The left Y axis.
     */
    public float getLeftYAxis() {
        return getAxisWithDeadzone(AXIS_LEFT_X, AXIS_LEFT_Y, false);
    }

    private float getAxisWithDeadzone(int axisX, int axisY, boolean returnX) {
        if (this.controller == null)
            return 0;

        float x = this.controller.getAxis(axisX);
        float y = this.controller.getAxis(axisY);

        float magnitude = UPMath.sqrt(x * x + y * y);

        if (magnitude < this.deadZone)
            return 0;
        if (returnX) {
            return x;
        } else {
            return y;
        }
    }

    public boolean isActive() {
        return this.controller != null;
    }

    public PovDirection getDPad(int povIndex) {
        return this.controller.getPov(povIndex);
    }
}