package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.universeprojects.common.shared.callable.Callable2Args;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingResourceLoader;
import com.universeprojects.html5engine.client.framework.resourceloader.DownloadingSprite;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

/**
 * @author Crokoking
 */
public class H5EIcon extends Image implements GraphicElement {

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public static H5EIcon fromStyle(H5ELayer layer, String styleName, Scaling scaling, int align) {
        Drawable drawable = layer.getEngine().getSkin().getDrawable(styleName);
        return new H5EIcon(layer, drawable, scaling, align);
    }

    public static H5EIcon fromStyle(H5ELayer layer, String styleName) {
        return fromStyle(layer, styleName, Scaling.fit, Align.center);
    }

    public static H5EIcon fromSpriteType(H5ELayer layer, String key, Scaling scaling, int align) {
        H5ESpriteType spriteType = (H5ESpriteType) layer.engine.getResourceManager().getSpriteType(key);
        if (spriteType == null) {
            throw new IllegalStateException("Unable to find sprite-type " + key);
        }
        return new H5EIcon(layer, new TextureRegionDrawable(spriteType.getGraphicData()), scaling, align);
    }

    public static H5EIcon fromSpriteType(H5ELayer layer, String key, Scaling scaling, int align, Callable2Args<DownloadingSprite.DownloadState, H5EIcon> onLoadChanged) {
        H5ESpriteType spriteType = (H5ESpriteType) layer.engine.getResourceManager().getSpriteType(key);
        H5EIcon icon = new H5EIcon(layer, new TextureRegionDrawable(spriteType.getGraphicData()), scaling, align);
        spriteType.subscribeToLoadStateChange((state) -> {
            onLoadChanged.call(state, icon);
        });
        if (spriteType == null) {
            throw new IllegalStateException("Unable to find sprite-type " + key);
        }
        return icon;
    }

    public static H5EIcon fromSpriteType(H5ELayer layer, String key) {
        return fromSpriteType(layer, key, Scaling.fit, Align.center);
    }

    public H5EIcon(H5ELayer layer, H5ESpriteType spriteType) {
        this(layer, new TextureRegionDrawable(spriteType.getGraphicData()), Scaling.fit, Align.center);
    }

    public H5EIcon(H5ELayer layer, String spriteType, int width, int height) {
        this(layer, spriteType, Scaling.fit, Align.center, width, height);
    }

    public H5EIcon(H5ELayer layer, Drawable drawable) {
        this(layer, drawable, Scaling.fit, Align.center);
    }

    public H5EIcon(H5ELayer layer, Drawable drawable, Scaling scaling, int align) {
        super(drawable, scaling, align);
        setStage(layer);
    }

    public H5EIcon(H5ELayer layer, String spriteTypeKey, Scaling scaling, int align, int width, int height) {
        super(spriteTypeKey == null ? null : new TextureRegionDrawable(layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey).getGraphicData()), scaling, align);
        setStage(layer);

        if (spriteTypeKey == null) return;

        this.setWidth(width);
        this.setHeight(height);

        TextureRegion graphicData = layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey).getGraphicData();
        if (graphicData instanceof DownloadingSprite) {
            H5ESpriteType newSpriteType = (H5ESpriteType) layer.getEngine().getResourceManager().getSpriteType(spriteTypeKey);
            newSpriteType.subscribeToLoadStateChange((state) -> {
                this.setDrawable(new TextureRegionDrawable(newSpriteType.getGraphicData()));
                if (state == DownloadingSprite.DownloadState.LOADED && !newSpriteType.getImageKey().equals(DownloadingResourceLoader.UNKNOWN_ICON_KEY)) {
                    this.invalidate();
                    this.setWidth(width);
                    this.setHeight(height);
                }
            });
        }
    }

    @Override
    public void setStage(Stage stage) {
        super.setStage(stage);
    }

    @Override
    public void setDrawable(Drawable drawable) {
        super.setDrawable(drawable);
        H5ELayer.invalidateBufferForActor(this);
    }

    public float getActualScale() {
        if (this.getDrawable() == null) {
            return 1;
        }

        float imageWidth = this.getDrawable().getMinWidth();
        float imageHeight = this.getDrawable().getMinHeight();

        float iconWidth = this.getWidth();
        float iconHeight = this.getHeight();

        if (imageHeight - iconHeight > imageWidth - iconWidth) {
            // The image is overlapping more vertically than horizontally
            float result = (iconHeight / imageHeight);
            return result > 1 ? 1 : result;
        } else {
            // The image is overlapping more horizontally than vertically
            float result = (iconWidth / imageWidth);
            return result > 1 ? 1 : result;
        }
    }
}
