package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.universeprojects.html5engine.shared.abstractFramework.Image;


public class H5EImage extends Image {
    /**
     * The actual javascript image element
     */
    protected TextureRegion texture;

    public TextureRegion getTextureRegion() {
        return texture;
    }

    public void setTexture(TextureRegion texture) {
        this.texture = texture;
    }

    /**
     * The height of the loaded image data. If no image is loaded, the return will be 0.
     *
     * @return The height of the loaded image data. If no image is loaded, the return will be 0.
     */
    @Override
    public int getHeight() {
        return texture.getRegionHeight();
    }

    /**
     * The width of the loaded image data. If no image is loaded, the return will be 0.
     *
     * @return The width of the loaded image data. If no image is loaded, the return will be 0.
     */
    @Override
    public int getWidth() {
        return texture.getRegionWidth();
    }

    public H5EImage(String url) {
        this.url = url;
    }
}
