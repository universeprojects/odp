package com.universeprojects.html5engine.client.framework.inputs;

public interface H5ECommandInputTranslator3Args<A1, A2, A3, C extends H5ECommand> extends H5ECommandInputTranslator {
    H5ECommandParams<C> generateParams(C command, A1 arg1, A2 arg2, A3 arg3);
}
