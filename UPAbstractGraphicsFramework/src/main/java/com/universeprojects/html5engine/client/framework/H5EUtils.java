package com.universeprojects.html5engine.client.framework;

import com.universeprojects.common.shared.math.UPMath;



public class H5EUtils {
    public static Float distance(Float x1, Float y1, Float x2, Float y2) {
        return UPMath.sqrt(UPMath.pow(x2 - x1, 2) + UPMath.pow(y2 - y1, 2));
    }

    public static Float distance(Float x1, Float y1, Float z1, Float x2, Float y2, Float z2) {
        return UPMath.sqrt(UPMath.pow(x2 - x1, 2) + UPMath.pow(y2 - y1, 2) + UPMath.pow(z2 - z1, 2));
    }

    public static float dotProduct(float x1, float y1, float x2, float y2) {
        return (x1 * x2) + (y1 * y2);
    }

    public static String secondsToStandardShortString(long secondsToConvert)
    {
        if (secondsToConvert<0)
            secondsToConvert=secondsToConvert*-1;
        if (secondsToConvert==0)
            return "0";

        Double dSeconds = new Double(secondsToConvert);
        Double dDays = Math.floor(dSeconds/86400);
        dSeconds = dSeconds-(dDays*86400);
        Double dHours = Math.floor(dSeconds/3600);
        dSeconds = dSeconds-(dHours*3600);
        Double dMinutes = Math.floor(dSeconds/60);
        dSeconds = dSeconds-(dMinutes*60);

        if (dDays.intValue()==1)
            return dDays.intValue()+" day";
        else if (dDays.intValue()>1)
            return dDays.intValue()+" days";

        if (dHours.intValue()==1)
            return dHours.intValue()+" hour";
        else if (dHours.intValue()>1)
            return dHours.intValue()+" hours";

        if (dMinutes.intValue()==1)
            return dMinutes.intValue()+" minute";
        else if (dMinutes.intValue()>1)
            return dMinutes.intValue()+" minutes";

        if (dSeconds.intValue()==1)
            return dSeconds.intValue()+" second";
        else if (dSeconds.intValue()>1)
            return dSeconds.intValue()+" seconds";

        return "less than 1 second";
    }

}
