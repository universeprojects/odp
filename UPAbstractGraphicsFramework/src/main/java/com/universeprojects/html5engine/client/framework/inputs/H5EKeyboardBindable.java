package com.universeprojects.html5engine.client.framework.inputs;

public interface H5EKeyboardBindable {
    <C extends H5ECommand> void addKeyboardBinding(C command, H5ECommandInputTranslator1Args<Boolean, C> translator, int... keyCodes);

    void removeKeyboardBinding(int keyCode);
}
