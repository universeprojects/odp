package com.universeprojects.html5engine.client.framework;


import com.badlogic.gdx.graphics.Color;

public abstract class H5EBehaviour {
    private boolean active = true;
    protected H5EGraphicElement element;

    public <T extends H5EGraphicElement> H5EBehaviour(T graphicElement) {
        this.element = graphicElement;
        this.element.addBehaviour(this);
        this.element.getEngine().addBehaviour(this);
    }



    /**
     * This is fired at a consistent tick rate based on the engine's tick rate.
     * @param delta
     */
    public abstract void act(float delta);

    /**
     * This is an ABSTRACT method; it must be overridden by the extending behaviour
     * class. This method specifies the unique name given to this behaviour, a user
     * will be able to retrieve a behaviour from a graphic element based on the name
     * alone. For example: sprite.getBehaviourByName('bouncy') - This method would
     * search through the behaviours in the sprite and look for the one where
     * getName() returns 'bouncy'.
     */
    public abstract String getName();

    public H5EGraphicElement getElement() {
        return element;
    }

    public void setActive(boolean newVal) {
        if (newVal == active) {
            return;
        }
        active = newVal;
    }

    public boolean isActive() {
        return active;
    }

    public void activate() {
        setActive(true);
    }

    public void deactivate() {
        setActive(false);
    }

    public void onDestroy() {
    }

    public float getElementTransparency() {
        Color color = element.getColor();
        return color != null ? color.a : 1f;
    }

    public void setElementTransparency(float value) {
        Color color = element.getColor();
        if(color == null) {
            color = new Color(Color.WHITE);
            element.setColor(color);
        }
        color.a = value;
    }


}
