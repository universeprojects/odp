package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.Gdx;
import com.universeprojects.common.shared.util.Log;

public class GdxRunnableHelper {
    public static void run(Runnable runnable) {
        RunnableWrapper wrapper = new RunnableWrapper(runnable);
        if(Gdx.app != null) {
            Gdx.app.postRunnable(wrapper);
        } else {
            wrapper.run();
        }
    }

    private static class RunnableWrapper implements Runnable {
        private final Runnable wrapped;

        public RunnableWrapper(Runnable wrapped) {
            this.wrapped = wrapped;
        }

        @Override
        public void run() {
            try {
                wrapped.run();
            } catch (Throwable t) {
                Log.error("Error in runnable", t);
            }
        }
    }
}
