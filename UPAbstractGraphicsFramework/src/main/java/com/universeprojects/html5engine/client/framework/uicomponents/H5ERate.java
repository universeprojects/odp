package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ESprite;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Rate component
 */
public class H5ERate extends Table implements GraphicElement {
    private static final String ZERO_IMAGE = "images/GUI/ui2/rate-star1-grey.png";
    private static final String HALF_IMAGE = "images/GUI/ui2/rate-star1-halfyellow.png";
    private static final String ONE_IMAGE = "images/GUI/ui2/rate-star1-yellow.png";
    private static ImageButtonStyle ZERO_STYLE;
    private static ImageButtonStyle HALF_STYLE;
    private static ImageButtonStyle ONE_STYLE;

    private final H5ELabel label;
    private final List<ImageButton> starsImage;
    private float ratingValue = 0;
    public H5ERate(H5ELayer layer, boolean readOnly, String labelText){
        this(layer,readOnly);
        label.setText(labelText);
    }
    public H5ERate(H5ELayer layer, boolean readOnly) {
        starsImage = new ArrayList<>();
        ZERO_STYLE = new ImageButtonStyle(getDrawable(layer, ZERO_IMAGE), null, null, null, null, null);
        HALF_STYLE = new ImageButtonStyle(getDrawable(layer, HALF_IMAGE), null, null, null, null, null);
        ONE_STYLE = new ImageButtonStyle(getDrawable(layer, ONE_IMAGE), null, null, null, null, null);
        label = new H5ELabel(readOnly ? "Rating" : "Rate", layer);
        add(label).left();
        row();
        Table starsTable=new Table();
        for (int i = 0; i < 5; i++) {
            int starIndex = i + 1;
            ImageButton star = new ImageButton(ZERO_STYLE);
            if (!readOnly) {
                star.addListener(new ClickListener() {
                    @Override
                    public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                        super.enter(event, x, y, pointer, fromActor);
                        visualize(starIndex);
                        event.handle();
                    }

                    @Override
                    public boolean mouseMoved(InputEvent event, float x, float y) {
                        super.mouseMoved(event, x, y);
                        visualize(starIndex);
                        event.handle();
                        return true;
                    }

                    @Override
                    public void clicked(InputEvent event, float x, float y) {
                        super.clicked(event, x, y);
                        setValue(starIndex);
                        event.handle();
                    }
                });
            }
            starsImage.add(star);
            starsTable.add(star).left();
        }
        add(starsTable).left().padTop(5);
        if (!readOnly) {
            addListener(new ClickListener() {
                public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                    super.enter(event, x, y, pointer, fromActor);
                    visualize(ratingValue);
                    event.handle();
                }
            });
        }
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public float getValue() {
        return ratingValue;
    }

    public void setValue(float value) {
        ratingValue = value;
        visualize(value);
    }

    private void visualize(float value) {
        for (int i = 0; i < 5; i++) {
            if (value < 0.25f) {
                starsImage.get(i).setStyle(ZERO_STYLE);
            } else if (value < 0.75f) {
                starsImage.get(i).setStyle(HALF_STYLE);
            } else {
                starsImage.get(i).setStyle(ONE_STYLE);
            }
            value--;
        }
    }

    public static TextureRegionDrawable getDrawable(H5ELayer layer, String pathTo) {
        H5ESprite sprite = new H5ESprite(layer, pathTo);
        return new TextureRegionDrawable(sprite.getSpriteType().getTextureRegion());
    }
}