package com.universeprojects.html5engine.client.framework.uicomponents;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Align;
import com.universeprojects.html5engine.client.framework.H5EEngine;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

@SuppressWarnings("unused")
public class H5EProgressBar extends Stack implements GraphicElement {

    protected final ProgressBar progressBar;
    private final H5ELabel progressText;

    public enum DisplayMode {NO_LABEL, LABEL_VALUE, LABEL_VALUE_MAX, LABEL_PERCENTAGE, LABEL_CUSTOM}

    private DisplayMode displayMode = DisplayMode.LABEL_PERCENTAGE;

    public void setStyle(ProgressBar.ProgressBarStyle style) {
        progressBar.setStyle(style);
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public H5EProgressBar(H5ELayer layer) {
        this(layer, "default-horizontal");
    }

    public H5EProgressBar(H5ELayer layer, String styleName){
        this(layer,styleName, false);
    }

    public H5EProgressBar(H5ELayer layer, String styleName, boolean vertical) {
        setStage(layer);
        progressBar = new ProgressBar(0, 100, 1, vertical, layer.getEngine().getSkin(), styleName);
        add(progressBar);
        progressText = new H5ELabel(layer);
        progressText.setFontScale(0.7f);
        progressText.setColor(Color.valueOf("#FFFFFF"));
        progressText.setTouchable(Touchable.disabled);
        progressText.setAlignment(Align.center);
        add(progressText);

        setHeight(25);
        setWidth(400);

        updateProgressText("");
    }

    public boolean setValue(float value) {
        H5ELayer.invalidateBufferForActor(this);
        boolean result = progressBar.setValue(value);
        syncText();
        return result;
    }

    public void setRange(float min, float max) {
        progressBar.setRange(min, max);
        syncText();
    }

    public void setStepSize(float stepSize) {
        progressBar.setStepSize(stepSize);
        syncText();
    }

    public void setDisplayMode(DisplayMode displayMode) {
        this.displayMode = displayMode;
        if (displayMode == DisplayMode.NO_LABEL) {
            progressText.setText("");
            progressText.setVisible(false);
        } else {
            progressText.setVisible(true);
        }
        syncText();
    }

    public float getMinValue() {
        return progressBar.getMinValue();
    }

    public float getMaxValue() {
        return progressBar.getMaxValue();
    }

    public float getStepSize() {
        return progressBar.getStepSize();
    }

    protected void syncText() {

        float value = getValue();
        float maximum = progressBar.getMaxValue();

        switch (displayMode) {
            case NO_LABEL:
                return;
            case LABEL_VALUE:
                updateProgressText(Integer.toString((int)value));
                break;
            case LABEL_VALUE_MAX:
                updateProgressText((int)value + "/" + (int)maximum);
                break;
            case LABEL_PERCENTAGE:
                updateProgressText((int)(100 * value / maximum) + " %");
                break;
            case LABEL_CUSTOM:
                break; // do nothing
            default:
                break;
        }
    }

    public float getValue() {
        return progressBar.getValue();
    }

    public void updateProgressText(String text) {
        progressText.setText(text);
        progressText.setOrigin(progressText.getWidth()/2f, progressText.getHeight()/2f);
    }

    public ProgressBar.ProgressBarStyle getStyle(){
        return progressBar.getStyle();
    }
}
