package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;

public class H5EEaseBehaviour extends H5EBehaviour {

    public enum EaseType {
        IN, OUT, INOUT, NONE
    }

    private Color destinationColor = new Color(0f, 0f, 0f, 1f);
    private float destinationScaleX;
    private float destinationScaleY;
    private float destinationRotation;
    private float destinationX;
    private float destinationY;
    private float startX;
    private float startY;
    private float startRotation;
    private float startScaleX;
    private float startScaleY;
    private Color startColor = new Color(1,1,1,1);
    private float time = 0.03f;
    private float elapsed = 0f;
    private float pace = 0.02f;
    private boolean moving = false;
    private boolean scaling = false;
    private boolean rotating = false;
    private boolean coloring = false;
    private EaseType easeType = EaseType.INOUT;

    public <T extends H5EGraphicElement> H5EEaseBehaviour(T graphicElement) {
        super(graphicElement);
        destinationScaleX = graphicElement.getScaleX();
        destinationScaleY = graphicElement.getScaleY();
        destinationRotation = graphicElement.getRotation();
        destinationX = graphicElement.getX();
        destinationY = graphicElement.getY();
    }

    public void setPosition(float x, float y) {
        destinationX = x;
        destinationY = y;
        moving = true;
        elapsed = 0f;
    }

    public void setRotation(float newRot) {
        destinationRotation = newRot;
        rotating = true;
        elapsed = 0f;
    }

    public void setScale(float scale) {
        setScale(scale, scale);
    }

    public void setScale(float scaleX, float scaleY) {
        destinationScaleX = scaleX;
        destinationScaleY = scaleY;
        scaling = true;
        elapsed = 0f;
    }

    public void setScaleX(float scaleX) {
        destinationScaleX = scaleX;
        scaling = true;
        elapsed = 0f;
    }

    public void setScaleY(float scaleY) {
        destinationScaleY = scaleY;
        scaling = true;
        elapsed = 0f;
    }

    public void setTime(float time) {
        this.time = time;
        elapsed = 0f;
    }
    public void setPace(float pace) {
        this.pace = pace;
    }
    public float getPace() {
        return pace;
    }

    public float getDestinationScaleX() {
        return destinationScaleX;
    }
    public float getDestinationScaleY() {
        return destinationScaleY;
    }
    public float getDestinationRotation() {
        return destinationRotation;
    }
    public float getDestinationX() {
        return destinationX;
    }
    public float getDestinationY() {
        return destinationY;
    }
    public float getTime() {
        return time;
    }
    public EaseType getEaseType() {
        return easeType;
    }
    public void setEaseType(EaseType type) {
        this.easeType = type;
        elapsed = 0f;
    }
    public void setColor(float r, float g, float b, float a) {
        destinationColor.set(r, g, b, a);
        coloring = true;
    }
    public void setColor(Color color) {
        destinationColor = color;
        coloring = true;
    }
    public Color getColor() {
        return destinationColor;
    }

    @Override
    public void act(float delta) {
        if(time == 0 || (!scaling && !moving && !rotating && !coloring)) {
            return;
        }

        if(elapsed == 0f) {
            startX = element.getX();
            startY = element.getY();
            startRotation = element.getRotation();
            startScaleX = element.getScaleX();
            startScaleY = element.getScaleY();
            startColor.set(element.getColor());
        }

        if(elapsed < time) {
            elapsed += pace;
        }

        float timing;
        EaseType internalType = easeType;
        if(easeType == EaseType.INOUT) {
            if(elapsed < time/2) {
                internalType = EaseType.IN;
            }
            else {
                internalType = EaseType.OUT;
            }
        }
        float internalElapsed;
        switch (internalType) {
            case IN:
                timing = (internalElapsed =elapsed/time)* internalElapsed * internalElapsed;
                break;
            case OUT:
                timing = ((internalElapsed =elapsed/time-1)* internalElapsed * internalElapsed + 1);
                break;
            case NONE:
            default:
                timing = elapsed /= time;
                break;
        }

        if(moving) {
            moving = !(timing >= 1);
            if(!moving) {
                element.setX(destinationX);
                element.setY(destinationY);
            }
            if (destinationX != element.getX()) {
                element.setX(startX + timing * (destinationX - startX));
            }
            if (destinationY != element.getY()) {
                element.setY(startY + timing * (destinationY - startY));
            }
        }
        if(element instanceof H5ESprite) {
            H5ESprite sprite = (H5ESprite)element;
            if (scaling) {
                scaling = !(timing >= 1);
                if (!scaling) {
                    sprite.setScaleX(destinationScaleX);
                    sprite.setScaleY(destinationScaleY);
                }
                if (destinationScaleX != sprite.getScaleX()) {
                    sprite.setScaleX(startScaleX + timing * (destinationScaleX - startScaleX));
                }
                if (destinationScaleY != sprite.getScaleY()) {
                    sprite.setScaleY(startScaleY + timing * (destinationScaleY - startScaleY));
                }
            }
            if (rotating) {
                rotating = !(timing >= 1);
                if (!rotating) {
                    sprite.setRotation(destinationRotation);
                }
                if (destinationRotation != sprite.getRotation()) {
                    sprite.setRotation(startRotation + timing * (destinationRotation - startRotation));
                }
            }
            if(coloring) {
                coloring = !(timing >= 1);
                if(!coloring) {
                    sprite.setColor(destinationColor);
                }
                if(!destinationColor.equals(sprite.getColor())) {
                    sprite.setColor(startColor.r + timing * (destinationColor.r - startColor.r),
                            startColor.g + timing * (destinationColor.r - startColor.g),
                            startColor.b + timing * (destinationColor.r - startColor.b),
                            startColor.a + timing * (destinationColor.r - startColor.a));
                }
            }
        }
    }

    @Override
    public String getName() {
        return "ease";
    }
}
