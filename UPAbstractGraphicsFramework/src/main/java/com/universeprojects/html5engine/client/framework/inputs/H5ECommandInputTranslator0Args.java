package com.universeprojects.html5engine.client.framework.inputs;

public interface H5ECommandInputTranslator0Args<C extends H5ECommand> extends H5ECommandInputTranslator {
    H5ECommandParams<C> generateParams(C command);
}
