package com.universeprojects.html5engine.client.framework.inputs;

public class H5ECommandParams3Args<C extends H5ECommand3Args<A1, A2, A3>, A1, A2, A3> extends H5ECommandParams<C> {
    public H5ECommandParams3Args(A1 arg1, A2 arg2, A3 arg3) {
        super(arg1, arg2, arg3);
    }
}
