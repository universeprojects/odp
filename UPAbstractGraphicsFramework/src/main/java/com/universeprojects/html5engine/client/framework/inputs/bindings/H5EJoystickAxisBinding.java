package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator2Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;
import com.universeprojects.html5engine.client.framework.uicomponents.H5EJoystick;

public class H5EJoystickAxisBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final H5EJoystick joystick;
    public final int inputParameter;
    private final boolean isX;
    private H5EJoystickAxisBinding otherAxis;

    private final H5ECommandInputTranslator2Args<Boolean, Float, C> translator;

    public H5EJoystickAxisBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator2Args<Boolean, Float, C> translator, C command, H5EJoystick joystick,
                                  int inputParameter, boolean isX) {
        super(controlSetup, command);
        this.translator = translator;
        this.joystick = joystick;
        this.inputParameter = inputParameter;
        this.isX = isX;
    }

    public void setOtherAxis(H5EJoystickAxisBinding otherAxis) {
        this.otherAxis = otherAxis;
    }

    @Override
    public void doTick() {
        if (!isX || otherAxis == null) {
            return;
        }
        Vector2 vec = joystick.getState().getVector();

        fireSelf(vec.x);

        otherAxis.fireSelf(vec.y);
    }

    protected void fireSelf(float strength) {
        final H5ECommandParams<C> inp;
        if (strength != 0d) {
            inp = translator.generateParams(command, true, strength);
        } else {// if (otherAxis.isActive()) {
            inp = translator.generateParams(command, false, 0f);
        }
        fire(inp);
    }
}
