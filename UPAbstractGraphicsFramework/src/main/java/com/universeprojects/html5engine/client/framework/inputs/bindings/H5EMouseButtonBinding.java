package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.badlogic.gdx.math.Vector2;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator2Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EMouseButtonBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final int buttonID;
    public final int inputParameterX;
    public final int inputParameterY;
    private final H5ECommandInputTranslator2Args<Boolean, Vector2, C> translator;

    public H5EMouseButtonBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator2Args<Boolean, Vector2, C> translator, C command, int buttonID,
                                 int inputParameterX, int inputParameterY) {
        super(controlSetup, command);
        this.translator = translator;
        this.buttonID = buttonID;
        this.inputParameterX = inputParameterX;
        this.inputParameterY = inputParameterY;
    }

    public void fire(boolean on, float screenX, float screenY) {
        H5ECommandParams<C> inp = translator.generateParams(command, on, new Vector2(screenX, screenY));
        fire(inp);
    }
}
