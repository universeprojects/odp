package com.universeprojects.html5engine.client.framework.inputs.bindings;

import com.universeprojects.html5engine.client.framework.inputs.H5ECommand;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandInputTranslator1Args;
import com.universeprojects.html5engine.client.framework.inputs.H5ECommandParams;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlBinding;
import com.universeprojects.html5engine.client.framework.inputs.H5EControlSetup;

public class H5EKeyboardBinding<C extends H5ECommand> extends H5EControlBinding<C> {

    public final int keyCode;
    private final H5ECommandInputTranslator1Args<Boolean, C> translator;
    private boolean wasDown = false;

    public H5EKeyboardBinding(H5EControlSetup controlSetup, H5ECommandInputTranslator1Args<Boolean, C> translator, C command, int keyCode) {
        super(controlSetup, command);
        this.translator = translator;
        this.keyCode = keyCode;
    }

    public void fire(boolean on) {
        if (wasDown != on) {
            H5ECommandParams<C> params = translator.generateParams(command, on);
            fire(params);
            wasDown = on;
        }
    }
}
