package com.universeprojects.html5engine.client.framework.proxy;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.universeprojects.html5engine.client.framework.BlendingMode;
import com.universeprojects.html5engine.client.framework.H5ELayer;
import com.universeprojects.html5engine.client.framework.H5ELayerProxy;

public class UILayerProxy extends H5ELayerProxy {
    private static final int NUMBER_OF_FRAMES_DRAWN_DIRECTLY_TO_SCREEN_AFTER_CACHE_WIPE = -1;

    private boolean showBufferUpdates = false;
    private int redrawInterval = 0;
    private int framesPassedFromCacheWipe = 0;

    private boolean forceRedraw = false;
    private FrameBuffer buffer = null;

    public UILayerProxy(H5ELayer layer) {
        super(layer);
        engine.onResize.registerHandler(this::onResize);
    }

    @Override
    public void draw() {
        if (redrawInterval == 0) {
            layer.draw();
        } else if (framesPassedFromCacheWipe < NUMBER_OF_FRAMES_DRAWN_DIRECTLY_TO_SCREEN_AFTER_CACHE_WIPE) {
            framesPassedFromCacheWipe++;
            layer.draw();
        } else {

            boolean update = forceRedraw || (redrawInterval > 0 && Gdx.graphics.getFrameId() % redrawInterval == 0);
            if (update) {
                renderUi();
            }
            drawBuffer(update);
        }
    }

    private void renderUi() {
        forceRedraw = false;
        Batch batch = getBatch();
        batch.flush();
        buffer.begin();
        try {
            Gdx.gl20.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            Gdx.gl.glClearColor(0f, 0f, 0f, 0f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
            BlendingMode.TO_FRAMEBUFFER.ifNotEqualsAssignTo(batch);

            layer.draw();
            batch.flush();
            BlendingMode.DEFAULT.ifNotEqualsAssignTo(batch);
        } finally {
            buffer.end();
        }
    }

    private void drawBuffer(boolean updated) {
        Batch batch = getBatch();
        int width = Gdx.graphics.getWidth();
        int height = Gdx.graphics.getHeight();

        BlendingMode.FRAMEBUFFER_TO_SCREEN.ifNotEqualsAssignTo(batch);
        if (showBufferUpdates && updated)
            batch.setColor(Color.RED);
        else
            batch.setColor(Color.WHITE);

        float scale = getEngine().getUiScale();

        batch.draw(buffer.getColorBufferTexture(), 0, 0, width /scale, height / scale, 0, 0, width, height, false, true);
        batch.flush();
        BlendingMode.DEFAULT.ifNotEqualsAssignTo(batch);
    }

    /**
     * Set buffer redraw interval for this layer proxy.
     * <p>
     * If set to 0, the caching will be disabled completely.
     * If set to -1, the buffer will not be redrawn unless layer is invalidated
     * If set to >0, the buffer will be regularly redrawn in set interval
     *
     * @param redrawInterval redraw interval in frames
     */
    public void setRedrawInterval(int redrawInterval) {
        int intervalBefore = this.redrawInterval;
        this.redrawInterval = redrawInterval;

        if ((this.redrawInterval == 0) != (intervalBefore == 0)) {
            // If new redraw interval disables/enables caching
            if (this.redrawInterval != 0) {
                createBuffer(powerOf2(Gdx.graphics.getWidth()), powerOf2(Gdx.graphics.getHeight()));
            } else {
                buffer.dispose();
                buffer = null;
            }
        }
    }

    public int getRedrawInterval() {
        return redrawInterval;
    }

    private int powerOf2(int min) {
        int val = 1;
        while (val < min) val *= 2;
        return val;
    }

    private void onResize() {
        if (buffer == null) {
            return;
        }

        wipeCache();

        int bufferWidth = powerOf2(Gdx.graphics.getWidth());
        int bufferHeight = powerOf2(Gdx.graphics.getHeight());

        if (bufferWidth != buffer.getWidth() || bufferHeight != buffer.getHeight()) {
            buffer.dispose();
            buffer = null;
            createBuffer(bufferWidth, bufferHeight);
        }
    }

    private void createBuffer(int width, int height) {
        buffer = new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false);
        forceRedraw = true;
    }

    /**
     * Passthrough boolean value, setting forceRedraw to true
     * if the boolean value is true
     */
    private boolean eventRedrawPassthrough(boolean bool) {
        if (bool) {
            forceRedraw = true;
        }
        return bool;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return eventRedrawPassthrough(super.touchDown(screenX, screenY, pointer, button));
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return eventRedrawPassthrough(super.touchDragged(screenX, screenY, pointer));
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return eventRedrawPassthrough(super.touchUp(screenX, screenY, pointer, button));
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return this.eventRedrawPassthrough(super.mouseMoved(screenX, screenY));
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return eventRedrawPassthrough(super.scrolled(amountX, amountY));
    }

    @Override
    public boolean keyDown(int keyCode) {
        return eventRedrawPassthrough(super.keyDown(keyCode));
    }

    @Override
    public boolean keyUp(int keyCode) {
        return eventRedrawPassthrough(super.keyUp(keyCode));
    }

    @Override
    public boolean keyTyped(char character) {
        return eventRedrawPassthrough(super.keyTyped(character));
    }

    public void setShowBufferUpdates(boolean showBufferUpdates) {
        this.showBufferUpdates = showBufferUpdates;
    }

    public boolean isShowBufferUpdates() {
        return showBufferUpdates;
    }

    @Override
    public void wipeCache() {
        forceRedraw = true;
        framesPassedFromCacheWipe = 0;
    }
}
