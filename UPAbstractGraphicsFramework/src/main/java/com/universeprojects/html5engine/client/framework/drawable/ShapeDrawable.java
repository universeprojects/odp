package com.universeprojects.html5engine.client.framework.drawable;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TransformDrawable;
import com.badlogic.gdx.utils.Disposable;

public abstract class ShapeDrawable<T extends ShapeDrawable<T>> extends BaseDrawable implements Disposable, TransformDrawable {
    public final Color color;
    private ShapeRenderer.ShapeType shapeType = ShapeRenderer.ShapeType.Filled;
    private final ShapeRenderer shapeRenderer = new ShapeRenderer();

    public ShapeDrawable(Color color) {
        this.color = color;
    }

    public ShapeDrawable(Color color, ShapeRenderer.ShapeType shapeType) {
        this.color = color;
        this.shapeType = shapeType;
    }

    @Override
    public void draw(Batch batch, float x, float y, float width, float height) {
        shapeRenderer.setTransformMatrix(batch.getTransformMatrix());
        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.setColor(color);
        batch.end();
        shapeRenderer.begin(shapeType);
        drawShape(shapeRenderer, x, y, width, height);
        shapeRenderer.end();
        batch.begin();
    }

    @Override
    public void draw(Batch batch, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, float rotation) {
        //TODO
        draw(batch, x, y, width * scaleX, height * scaleY);
    }

    protected abstract void drawShape(ShapeRenderer shapeRenderer, float x, float y, float width, float height);

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }

    public ShapeRenderer.ShapeType getShapeType() {
        return shapeType;
    }

    public T setShapeType(ShapeRenderer.ShapeType shapeType) {
        this.shapeType = shapeType;
        //noinspection unchecked
        return (T) this;
    }
}
