package com.universeprojects.html5engine.client.framework.inputs;

@SuppressWarnings("unused")
public class H5ECommand2Args<A1, A2> extends H5ECommand {
    @Override
    public int getNumArgs() {
        return 2;
    }
}
