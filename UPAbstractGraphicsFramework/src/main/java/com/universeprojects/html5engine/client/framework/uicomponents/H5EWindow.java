package com.universeprojects.html5engine.client.framework.uicomponents;

import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;


public interface H5EWindow extends GraphicElement {

    /**
     * When this window is to be destroyed, this method MUST be called in order to detach it
     * from the graphics engine.
     * <p>
     * "Closing" the window is not enough; It will cause a memory leak as the object will remain in memory.
     */
    void destroy();

    /**
     * Opens the window, unless already open
     */
    void open();

    /**
     * Closes the window, unless already closed
     */
    void close();

    /**
     * @return TRUE if the window is open, FALSE if it is closed
     */
    boolean isOpen();

    /**
     * Makes this the active window, unless it is already active
     */
    void activate();

    /**
     * Sets the X-coordinate of the window
     */
    void setX(float newX);

    /**
     * Sets the Y-coordinate of the window
     */
    void setY(float newY);

    /**
     * @return the X-coordinate of the window
     */
    float getX();

    /**
     * @return the Y-coordinate of the window
     */
    float getY();

    /**
     * Applies proportional positioning to the window.
     *
     * @param screenProportionX The proportional position of the window origin on the X-axis in relation to the screen width.
     *                          Must be a fraction between 0 and 1, or NULL to disable this proportion.
     * @param screenProportionY The proportional position of the window origin on the Y-axis in relation to the screen height.
     *                          Must be a fraction between 0 and 1, or NULL to disable this proportion.
     */
    void positionProportionally(Float screenProportionX, Float screenProportionY);

    /**
     * Sets the width of the window
     */
    void setWidth(int width);

    /**
     * Sets the height of the window
     */
    void setHeight(int height);

    /**
     * Sets the origin in the center of this window (width/2, height/2)
     */
    void setOriginCenter();

    /**
     * Sets the window ID (for internal use only, mostly for debugging)
     */
    void setId(String id);

    /**
     * @return The window ID (for internal use only, mostly for debugging)
     */
    String getId();

    /**
     * Sets the window title
     */
    void setTitle(String title);

    /**
     * Shows/hides the "close button" on the top bar
     *
     * @param newVal pass TRUE to show the close button, pass FALSE to hide the close button
     */
    void setCloseButtonEnabled(boolean newVal);

    /**
     * Returns the horizontal content-padding value.
     * This is the space added on the left and on the right of the content pane.
     */
    int getContentPaddingX();

    /**
     * Returns the vertical content-padding value.
     * This is the space added above and below the content pane.
     */
    int getContentPaddingY();

    /**
     * Returns the height of the title bar.
     */
    int getTitleBarHeight();

    /**
     * Returns the width of the entire window
     */
    float getWidth();

    /**
     * Returns the height of the entire window
     */
    float getHeight();
}
