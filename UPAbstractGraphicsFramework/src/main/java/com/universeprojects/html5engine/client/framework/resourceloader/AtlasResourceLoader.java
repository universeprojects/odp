package com.universeprojects.html5engine.client.framework.resourceloader;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.universeprojects.html5engine.client.framework.H5EAudio;
import com.universeprojects.html5engine.client.framework.H5EImage;
import com.universeprojects.html5engine.client.framework.H5EResourceManager;

import java.util.Collection;

public class AtlasResourceLoader implements ResourceLoader {

    public static final String UNKNOWN_ICON_KEY = SimpleResourceLoader.UNKNOWN_ICON_KEY;
    private static final String[] IMAGE_EXTENSIONS = {".9.jpg", ".9.jpeg", ".9.png", ".jpg", ".jpeg", ".png"};
    private final TextureAtlas atlas;
    private H5EResourceManager resourceManager;

    public AtlasResourceLoader(TextureAtlas atlas) {
        this.atlas = atlas;
    }

    @Override
    public void loadResources(Collection<H5EImage> imageFiles, Collection<H5EAudio> audioFiles) {
        for (H5EImage image : imageFiles) {
            String url = image.getOriginalUrl();
            Sprite texture = getSprite(url);
            image.setTexture(texture);
        }
        for (H5EAudio audio : audioFiles) {
            Sound sound = Gdx.audio.newSound(Gdx.files.internal(audio.getURL()));
            audio.setSound(sound);
        }
    }

    protected Sprite getSprite(String url) {
        if (url.startsWith("images/")) {
            url = url.substring("images/".length());
        }
        url = removeExtensions(url);
        return atlas.createSprite(url);
    }

    private String removeExtensions(String url) {
        for(String ext : IMAGE_EXTENSIONS) {
            if(url.endsWith(ext)) {
                return url.substring(0, url.length() - ext.length());
            }
        }
        return url;
    }

    @Override
    public H5EImage loadSingleImage(String imageUrl) {
        Sprite sprite = getSprite(imageUrl);
        if(sprite == null) {
            return null;
        }
        H5EImage image = new H5EImage(imageUrl);
        image.setTexture(sprite);
        return image;
    }

    @Override
    public String getDefaultSpriteKey() {
        return UNKNOWN_ICON_KEY;
    }

    public H5EResourceManager getResourceManager() {
        return resourceManager;
    }

    public void setResourceManager(H5EResourceManager resourceManager) {
        this.resourceManager = resourceManager;
    }
}
