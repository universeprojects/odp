package com.universeprojects.html5engine.client.framework.inputs;

public abstract class H5EControlBinding<C extends H5ECommand> {

    protected final C command;
    protected final H5EControlSetup controlSetup;

    public H5EControlBinding(H5EControlSetup controlSetup, C command) {
        this.controlSetup = controlSetup;
        this.command = command;
    }

    public void fire(H5ECommandParams<C> newInput) {
        if (newInput == null) {
            return;
        }
        controlSetup.fire(command, newInput);
    }

    protected void doTick() {
        // optional
    }

}
