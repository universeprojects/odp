package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.universeprojects.html5engine.shared.abstractFramework.Container;
import com.universeprojects.html5engine.shared.abstractFramework.GraphicElement;

public class H5EScrollablePane extends ScrollPane implements Container {

    private final Table content;

    public <T extends Actor> Cell<T> add(T actor) {
        return content.add(actor);
    }

    public Cell add() {
        return content.add();
    }

    @Override
    public H5ELayer getLayer() {
        return (H5ELayer) getStage();
    }

    @Override
    public H5EEngine getEngine() {
        return getLayer().getEngine();
    }

    public H5EScrollablePane(H5ELayer layer) {
        this(layer, "default");
    }

    public H5EScrollablePane(H5ELayer layer, String styleName) {
        super(new Table(), layer.getEngine().getSkin(), styleName);
        setStage(layer);
        setFadeScrollBars(false);
        addListener(new InputListener() {
            @Override
            public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
                if (pointer == -1) {
                    layer.setScrollFocus(H5EScrollablePane.this);
                }
            }

            @Override
            public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
                if (pointer == -1) {
                    layer.setScrollFocus(null);
                }
            }
        });
        this.content = (Table) getActor();
    }

    public void scrollToPosition(float position) {
        setScrollY(position);
    }

    public float getScrollPosition() {
        return getScrollY();
    }

    public Table getContent() {
        return content;
    }

    public void setContentHeight(int contentHeight) {
        content.setHeight(contentHeight);
    }

    @Deprecated
    public <T extends Actor & GraphicElement> T addChild(T child) {
        content.add(child);
        return child;
    }

    public <T extends Actor & GraphicElement> void removeChild(T child) {
        content.removeActor(child);
    }

}
