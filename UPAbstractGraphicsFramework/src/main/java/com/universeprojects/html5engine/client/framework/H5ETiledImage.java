package com.universeprojects.html5engine.client.framework;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class H5ETiledImage extends Image {

    private float tiledWidth;
    private float tiledHeight;

    public H5ETiledImage(H5ELayer layer, float width, float height, String spriteKey) {
        super(new H5ESprite(layer, spriteKey).getSpriteType().getTextureRegion());
        tiledWidth = width;
        tiledHeight = height;
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        validate();
        Color color = getColor();
        batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
        int drawsX = (int) ((int) tiledWidth / getImageWidth());
        int drawsY = (int) ((int) tiledHeight / getImageHeight());
        Drawable drawable = getDrawable();
        float x = getX();
        float y = getY();
        float scaleX = getScaleX();
        float scaleY = getScaleY();
        float rotation = getRotation();
        float rads = -rotation * (float) Math.PI / 180f;
        TextureRegionDrawable drawable1 = (TextureRegionDrawable) drawable;
        if (drawable instanceof TextureRegionDrawable) {
            drawable1 = (TextureRegionDrawable) drawable;
        }
        for (int i = 0; i < drawsX; i++) {
            for (int j = 0; j < drawsY; j++) {
                drawable1.draw(batch, x + i * getImageWidth() * (float) Math.cos(rads) + j * getImageHeight() * (float) Math.sin(rads), y + j * getImageHeight() * (float) Math.cos(rads) + i * getImageWidth() * (float) Math.sin(rads), getOriginX(), getOriginY(), getImageWidth(), getImageHeight(), scaleX, scaleY, rotation);
            }
        }
    }
}
