package com.universeprojects.html5engine.server;

import com.universeprojects.html5engine.shared.abstractFramework.Image;

/**
 * @author Crokoking
 */
public class ServerImage extends Image {

    public String label;
    protected ServerResourceManager manager;

    public ServerImage(String label, String path, ServerResourceManager manager) {
        this.label = label;
        this.url = path;
        this.manager = manager;
    }

    public boolean isReadyForSaving() {
        return label != null && !label.isEmpty() && url != null && !url.isEmpty();
    }

    @Override
    public int getHeight() {
        throw new IllegalStateException("Cannot get image size on the server");
    }

    @Override
    public int getWidth() {
        throw new IllegalStateException("Cannot get image size on the server");
    }
}
