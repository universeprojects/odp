package com.universeprojects.html5engine.server;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.html5engine.shared.abstractFramework.BaseSpriteType;
import com.universeprojects.html5engine.shared.abstractFramework.Image;

public class ServerSpriteType extends BaseSpriteType {

    public String label;
    public ServerImage image;

    protected ServerResourceManager getResourceManager() {
        return (ServerResourceManager) resourceManager;
    }

    public ServerSpriteType() {
        this(null, null);
    }

    public ServerSpriteType(String key, ServerImage image) {
        this(key, image, 0, 0, 0, 0);
    }

    public ServerSpriteType(String label, ServerImage image, int areaX, int areaY, int areaWidth, int areaHeight) {
        this.label = label;
        this.image = image;
        this.areaX = areaX;
        this.areaY = areaY;
        this.areaWidth = areaWidth;
        this.areaHeight = areaHeight;
    }

    @Override
    public void stabilize() {
        areaX = UPMath.max(areaX, 0);
        areaY = UPMath.max(areaY, 0);
//        if (image != null && image.buffer != null) {
//            areaWidth = UPMath.min(areaWidth, image.getWidth());
//            areaHeight = UPMath.min(areaHeight, image.getHeight());
//        }
    }

    public boolean isReadyForSaving() {
        return label != null && !label.isEmpty() && image != null && image.isReadyForSaving();
    }

    @Override
    public Image getImage() {
        return image;
    }

    @Override
    public void setImage(String imageKey) {
        this.image = getResourceManager().getImage(imageKey);
    }

    @Override
    public String getImageKey() {
        return image.label;
    }

    @Override
    public TextureRegion getGraphicData() {
        // The server doesn't actually use the raw image data at all so we won't bother doing anything here
        return null;
    }
}
