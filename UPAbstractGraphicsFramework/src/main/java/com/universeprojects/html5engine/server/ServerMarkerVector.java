package com.universeprojects.html5engine.server;

import com.universeprojects.html5engine.shared.abstractFramework.MarkerVector;

/**
 * @author Crokoking
 */
public class ServerMarkerVector extends MarkerVector {

    public ServerMarkerVector copy() {
        ServerMarkerVector mv = new ServerMarkerVector();
        mv.label = label;
        mv.x = x;
        mv.y = y;
        mv.setTypeLabel(getTypeLabel());
        mv.deltaX = deltaX;
        mv.deltaY = deltaY;
        return mv;
    }

}
