var engineName = "Untitled";
var engine = null;
var liveViewZoom = 1;
var demoSprite = null;
var demoMapObject = null;
var mapObjectLayer = null;

var overlayLayer = null;
var originSprite = null;

function initialize(canvasContainerId)
{
    //engine.destroy();
    engine = new H5EEngine(canvasContainerId, 400, 400, 60, 60, false, true, true, false);
    mapObjectLayer = new H5EMapObjectLayer(engine, 100, 100, 32);
    engine.addLayer(mapObjectLayer);
    overlayLayer = engine.newLayer();
    engine.getResourceManager().onProgress = function()
    {
        updateGUI();
    };
    engine.getResourceManager().onComplete = function()
    {
        engine.getResourceManager().newSpriteType("libraryeditor_origin");
        engine.getResourceManager().newSpriteType("libraryeditor_footprint");
        originSprite = overlayLayer.newSprite("libraryeditor_origin");
        originSprite.setOriginCenter();
        updateGUI();
    };


    // THIS IS TEMPORARY...
    engine.getResourceManager().loadLibrary("demo.h5l", true);

    engine.getResourceManager().addImage("libraryeditor_origin", "images/origin.png");
    engine.getResourceManager().addImage("libraryeditor_footprint", "images/footprint.png");

    updateGUI();
}

/**
 * This method is used to update the html (specifically the libraryeditor.jsp page)
 * with the latest info. Things like library name changes get updated.
 */
function updateGUI()
{
    $("#titleHeader").html('Resource Library Editor - '+engineName);
    document.title = engineName+' - H5E Library Editor';

    // This object is used in a number of places so I've created a shortcut
    var rm = engine.getResourceManager();


    // Update the show images panel, sprite types images list...
    var newShowImagesPanel = "<h2>Images</h2>";
    var newSpriteTypeImagesList = "";
    // Go through each image in the resource manager and add it to the show images panel...
    var images = rm.getImages();
    var imageKeys = rm.getImageKeys();
    var selectedNewSpriteTypeImageKey = $("#newSpriteType_image").val();
    for(var i = 0; i<images.length; i++)
    {
        newShowImagesPanel += "<div style='float:left;padding:5px; min-width:120px; min-height: 120px;text-align:center'><a  onclick='clickRemoveImage(\""+imageKeys[i]+"\")' class='miniButton' style='text-align:left;float:left'>x</a><br><img src='"+images[i].getJSImage().src+"' style='max-width:100px; max-height:100px;'><br><small>"+imageKeys[i]+"</small></div>";

        var selectedCSSStyle = "";
        if (imageKeys[i] == selectedNewSpriteTypeImageKey)
            selectedCSSStyle = "border: solid #00CC00 2px;";
        newSpriteTypeImagesList += "<div style='float:left;padding:5px; min-width:120px; min-height: 120px; text-align:center'><img src='"+images[i].getJSImage().src+"' style='"+selectedCSSStyle+"max-width:100px; max-height:100px' onclick='clickNewSpriteTypeImageListImage(this,\""+imageKeys[i]+"\")'><br><small>"+imageKeys[i]+"</small></div>";

    }
    $("#showImages").html(newShowImagesPanel);
    $("#newSpriteTypeImagesList").html(newSpriteTypeImagesList);


    // Update the edit sprite type panel...
    var editSpriteTypesList = "<h2>Sprite Types</h2>";
    var editAnimatedSpriteTypesSpriteTypeList = "<h2>Sprite Types</h2>";
    var newMapObjectTypeSpriteTypesList = "";
    var spriteTypes = rm.getSpriteTypes();
    var spriteTypeKeys = rm.getSpriteTypeKeys();
    var selectedSpriteTypeKey = $("#editSpriteType_spriteType").val();
    var selectedAnimatedSpriteTypeSpriteTypeKey = $("#editAnimatedSpriteType_spriteType").val();
    var selectedMapObjectTypeSpriteTypeKey = $("#newMapObjectType_spriteType").val();
    for(var i = 0; i<spriteTypes.length; i++)
    {
        var selectedCSSStyle = "";
        if (spriteTypeKeys[i] == selectedSpriteTypeKey)
        {
            selectedCSSStyle = "border: solid #00CC00 2px;";
            
            // And update the edit controls for the selected sprite type as well...
            $("#editSpriteType_areaX").val(spriteTypes[i].areaX);
            $("#editSpriteType_areaY").val(spriteTypes[i].areaY);
            $("#editSpriteType_areaWidth").val(spriteTypes[i].areaWidth);
            $("#editSpriteType_areaHeight").val(spriteTypes[i].areaHeight);
        }
        editSpriteTypesList += "<div style='float:left;padding:5px; min-width:120px; min-height: 120px'><a  onclick='clickRemoveSpriteType(\""+spriteTypeKeys[i]+"\")' class='miniButton' style='text-align:left'>X</a><br><img src='"+spriteTypes[i].getJSImage().src+"' style='"+selectedCSSStyle+"max-width:100px; max-height:100px' onclick='clickSelectSpriteTypeInEditSpriteTypesPanel(this, \""+spriteTypeKeys[i]+"\")'><br><small>"+spriteTypeKeys[i]+"</small></div>";

        selectedCSSStyle = "";
        if (spriteTypeKeys[i] == selectedAnimatedSpriteTypeSpriteTypeKey)
            selectedCSSStyle = "border: solid #00CC00 2px;";
        editAnimatedSpriteTypesSpriteTypeList += "<p style='margin:3px'><a class='miniButton' onclick='clickEditAnimatedSpriteTypeSpriteTypeListAddSpriteType(\""+spriteTypeKeys[i]+"\")'>+</a>&nbsp;<a onclick='clickSelectSpriteTypeInAnimatedSpriteTypeSpriteTypeList(this,\""+spriteTypeKeys[i]+"\")'>"+spriteTypeKeys[i]+"</a></p>";


        selectedCSSStyle = "";
        if (spriteTypeKeys[i] == selectedMapObjectTypeSpriteTypeKey)
            selectedCSSStyle = "border: solid #00CC00 2px;";
        newMapObjectTypeSpriteTypesList += "<div style='float:left;padding:5px; min-width:120px; min-height: 120px'><img src='"+spriteTypes[i].getJSImage().src+"' style='"+selectedCSSStyle+"max-width:100px; max-height:100px' onclick='clickNewMapObjectType_SpriteType(this, \""+spriteTypeKeys[i]+"\")'><br><small>"+spriteTypeKeys[i]+"</small></div>";
    }
    $("#editSpriteTypesList").html(editSpriteTypesList);
    $("#editAnimatedSpriteTypesSpriteTypeList").html(editAnimatedSpriteTypesSpriteTypeList);
    $("#newMapObjectTypeSpriteTypesList").html(newMapObjectTypeSpriteTypesList);

    // Update the edit map object type panel...
    var editMapObjectTypesList = "<h2>Map Object Types</h2>";
    var mapObjectTypes = rm.getMapObjectTypes();
    var mapObjectTypeKeys = rm.getMapObjectTypeKeys();
    var selectedMapObjectTypeKey = $("#editMapObjectType_mapObjectType").val();
    for(var i = 0; i<mapObjectTypes.length; i++)
    {
        var selectedCSSStyle = "";
        if (mapObjectTypeKeys[i] == selectedMapObjectTypeKey)
        {
            selectedCSSStyle = "border: solid #00CC00 2px;";

            // And update the edit controls for the selected sprite type as well...
            $("#editMapObjectType_adjustX").val(mapObjectTypes[i].adjustX);
            $("#editMapObjectType_adjustY").val(mapObjectTypes[i].adjustY);
        }
        editMapObjectTypesList += "<div style='float:left;padding:5px; min-width:120px; min-height: 120px'><a  onclick='clickRemoveMapObjectType(\""+mapObjectTypeKeys[i]+"\")' class='miniButton' style='text-align:left'>X</a><br><img src='"+mapObjectTypes[i].getSpriteType().getJSImage().src+"' style='"+selectedCSSStyle+"max-width:100px; max-height:100px' onclick='clickSelectMapObjectTypeInEditMapObjectTypesPanel(this, \""+mapObjectTypeKeys[i]+"\")'><br><small>"+mapObjectTypeKeys[i]+"</small></div>";

    }
    $("#editMapObjectTypesList").html(editMapObjectTypesList);


    // Do all animated sprite types...
    var editAnimatedSpriteTypesList = "<h2>Animated Sprite Types</h2>";
    var aniSpriteTypes = rm.getAnimatedSpriteTypes();
    var aniSpriteTypeKeys = rm.getAnimatedSpriteTypeKeys();
    var selectedAnimatedSpriteTypeKey = $("#selectedEditAnimatedSpriteType").val();
    for(var i = 0; i<aniSpriteTypes.length; i++)
    {
        selectedCSSStyle = "";
        if (aniSpriteTypeKeys[i] == selectedAnimatedSpriteTypeKey)
            selectedCSSStyle = "border: solid #00CC00 2px;";

        editAnimatedSpriteTypesList += "<p style='margin:3px;'><a  class='miniButton' onclick='clickDeleteAnimatedSpriteType(\""+aniSpriteTypeKeys[i]+"\")'>x</a>&nbsp;<a  onclick='clickSelectAnimatedSpriteTypeInAnimatedSpriteTypeList(this,\""+aniSpriteTypeKeys[i]+"\")' style='"+selectedCSSStyle+"'>"+aniSpriteTypeKeys[i]+"</a></p>";
    }
    $("#editAnimatedSpriteTypesList").html(editAnimatedSpriteTypesList);

    // Do all animations frames...
    var editAnimatedSpriteTypeFramesList = "";
    var selectedAnimatedSpriteTypeKey = $("#selectedEditAnimatedSpriteType").val();
    if (selectedAnimatedSpriteTypeKey!=null)
    {
        var selectedAnimatedSpriteType = rm.getAnimatedSpriteType(selectedAnimatedSpriteTypeKey);
        if (selectedAnimatedSpriteType!=null)
        {
            for(var i = 0; i<selectedAnimatedSpriteType.frames.length; i++)
            {
                editAnimatedSpriteTypeFramesList += "<p style='margin:3px'><a  class='miniButton' onclick='clickDeleteAnimationFrame(\""+selectedAnimatedSpriteTypeKey+"\","+i+")'>x</a> <a  onclick='viewSpriteType(\""+selectedAnimatedSpriteType.frames[i].spriteTypeKey+"\")'>Frame "+i+"</a>: <a  onclick='clickAdjustFrameDuration(\""+selectedAnimatedSpriteTypeKey+"\","+i+")' title='The duration this frame is shown in, the animation, in milliseconds.'>"+selectedAnimatedSpriteType.frames[i].duration+" ms</a> <a class='miniButton' onclick='clickAdjustFrameX(\""+selectedAnimatedSpriteTypeKey+"\","+i+", -1)'>X-</a><a id='frameAdjustmentX' onclick='clickResetFrameAdjustmentX(\""+selectedAnimatedSpriteTypeKey+"\","+i+")'>"+selectedAnimatedSpriteType.frames[i].adjustedX+"</a><a class='miniButton' onclick='clickAdjustFrameX(\""+selectedAnimatedSpriteTypeKey+"\","+i+", 1)'>X+</a> | <a class='miniButton' onclick='clickAdjustFrameY(\""+selectedAnimatedSpriteTypeKey+"\","+i+", -1)'>Y-</a><a id='frameAdjustmentY' onclick='clickResetFrameAdjustmentY(\""+selectedAnimatedSpriteTypeKey+"\","+i+")'>"+selectedAnimatedSpriteType.frames[i].adjustedY+"</a><a class='miniButton' onclick='clickAdjustFrameY(\""+selectedAnimatedSpriteTypeKey+"\","+i+", 1)'>Y+</a> | <a class='miniButton' onclick='clickAdjustFrameScale(\""+selectedAnimatedSpriteTypeKey+"\","+i+", -1)'>Scale-</a><a id='frameAdjustmentScale' onclick='clickResetFrameAdjustmentScale(\""+selectedAnimatedSpriteTypeKey+"\","+i+")'>"+selectedAnimatedSpriteType.frames[i].adjustedScale+"</a><a class='miniButton' onclick='clickAdjustFrameScale(\""+selectedAnimatedSpriteTypeKey+"\","+i+", 1)'>Scale+</a></p>";
            }
        }
    }
    $("#editAnimatedSpriteTypesFramesList").html(editAnimatedSpriteTypeFramesList);
}

function clickNewLibrary()
{
    if (confirm('This action will lose any unsaved changes made to the current library.\nAre you sure you wish to create a new library?'))
    {
        engineName = prompt("New library name: ", "Untitled");

        engine.clearResourceManager();
        initialize('2dwindow');
        
        alert("This method is not properly implemented. A way to point to an alternative h5l file is necessary. Subsequent loads of the resource library editor will not automatically pull up this library. You will need to re-import every time. Additionally, there is no way to change the source server as of now.");
        
        updateGUI();
    }
}

function clickAddImage()
{
    var name = $("#newImage_name").val();
    var url = $("#newImage_url").val();

    if (url==null || trim(url)=="")
    {
        alert("Name and url cannot be blank. Please ensure the URL is at least filled in.\nIf no name is supplied, the URL will be used instead.");
        return;
    }

    if (trim(name)=="")
        name = url;

    var rm = engine.getResourceManager();
    rm.addImage(name, url);
    rm.loadResources(true);
}

function clickRemoveImage(key)
{
    var rm = engine.getResourceManager();
    rm.removeImage(key);
    updateGUI();
}

function clickNewSpriteTypeImageListImage(imgElem, imageKey)
{
    $("#newSpriteTypeImagesList").find("img").css("border", "none");
    $(imgElem).css("border", "solid #00CC00 2px");
    $("#newSpriteType_image").val(imageKey);
}

function clickAddSpriteType()
{
    var name = $("#newSpriteType_name").val();
    var image = $("#newSpriteType_image").val();

    if (image==null || trim(image)=="")
    {
        alert("Please select an image that you will be using for this sprite type's image data.");
        return;
    }

    if (name==null || trim(name)=="")
        name = image;

    var rm = engine.getResourceManager();
    var spriteType = new H5ESpriteType(engine, image);
    rm.addSpriteType(name, spriteType);
    updateGUI();
}

function clickSelectSpriteTypeInEditSpriteTypesPanel(imgElem, spriteTypeKey)
{
    $("#editSpriteTypesList").find("img").css("border", "none");
    $(imgElem).css("border", "solid #00CC00 2px");
    $("#editSpriteType_spriteType").val(spriteTypeKey);

    var rm = engine.getResourceManager();
    var spriteType = rm.getSpriteType(spriteTypeKey);
    $("#editSpriteType_areaX").val(spriteType.areaX);
    $("#editSpriteType_areaY").val(spriteType.areaY);
    $("#editSpriteType_areaWidth").val(spriteType.areaWidth);
    $("#editSpriteType_areaHeight").val(spriteType.areaHeight);

    viewSpriteType(spriteTypeKey);
}

function selectNextSpriteType(previous)
{
    $("#editSpriteTypesList").find("img").css("border", "none");
//    $(imgElem).css("border", "solid #00CC00 2px");

    var rm = engine.getResourceManager();
    var curIndex = rm.getSpriteTypeKeys().indexOf($("#editSpriteType_spriteType").val());
    if (previous==false)
    {
        if (curIndex>=rm.getSpriteTypes().length-1)
            curIndex=0;
        else
            curIndex+=1;
    }
    else
    {
        if (curIndex<=0)
            curIndex=rm.getSpriteTypes().length-1;
        else
            curIndex-=1;
    }
    var spriteType = rm.getSpriteTypes()[curIndex];
    var spriteTypeKey = rm.getSpriteTypeKeys()[curIndex];
    $("#editSpriteType_spriteType").val(spriteTypeKey);
    $("#editSpriteType_areaX").val(spriteType.areaX);
    $("#editSpriteType_areaY").val(spriteType.areaY);
    $("#editSpriteType_areaWidth").val(spriteType.areaWidth);
    $("#editSpriteType_areaHeight").val(spriteType.areaHeight);

    viewSpriteType(spriteTypeKey);
}

function clickSelectSpriteTypeInAnimatedSpriteTypeSpriteTypeList(hrefElem, spriteTypeKey)
{
    $("#editAnimatedSpriteTypesSpriteTypeList").find("a").css("border", "none");
    $(hrefElem).css("border", "solid #00CC00 2px");

    $("#selectedEditAnimatedSpriteTypeSpriteType").val(spriteTypeKey);

    viewSpriteType(spriteTypeKey);
}

function clickEditSpriteTypesSave()
{
    var spriteTypeKey = $("#editSpriteType_spriteType").val();
    var areaX = parseInt($("#editSpriteType_areaX").val());
    var areaY = parseInt($("#editSpriteType_areaY").val());
    var areaWidth = parseInt($("#editSpriteType_areaWidth").val());
    var areaHeight = parseInt($("#editSpriteType_areaHeight").val());

    var rm = engine.getResourceManager();
    var spriteType = rm.getSpriteType(spriteTypeKey);
    spriteType.areaX = areaX;
    spriteType.areaY = areaY;
    spriteType.areaWidth = areaWidth;
    spriteType.areaHeight = areaHeight;
    
    viewSpriteType(spriteTypeKey);
}

function clickImportLibrary()
{
    var newLib = $("#importLibraryText").val();
    if (newLib==null || trim(newLib)=="")
    {
        alert("No library contents provided. Please paste the contents of the .h5l file you wish to import.");
        return;
    }

    engine.getResourceManager().readLibraryFile(newLib);
    engine.getResourceManager().loadResources(true);
}

function clickLiveViewZoom(zoomIn)
{
    if (zoomIn)
        liveViewZoom *= 1.3;
    else
        liveViewZoom /= 1.3;

    if (demoSprite!=null)
        demoSprite.setScale(liveViewZoom);

    $("#liveViewZoom").html((parseInt(liveViewZoom*100))+"%");
}

function clickLiveViewZoomReset()
{
    liveViewZoom = 1;

    if (demoSprite!=null)
        demoSprite.setScale(liveViewZoom);

    $("#liveViewZoom").html((parseInt(liveViewZoom*100))+"%");
}

function clickAddAnimatedSpriteType()
{
    var name = $("#addAnimatedSpriteType_name").val();
    engine.getResourceManager().newAnimatedSpriteType(name);
    updateGUI();
}

function clickDeleteAnimatedSpriteType(key)
{
    engine.getResourceManager().removeAnimatedSpriteType(key);
    updateGUI();
}


function clickSelectAnimatedSpriteTypeInAnimatedSpriteTypeList(elem, key)
{
    $("#selectedEditAnimatedSpriteType").val(key);

    $("#editAnimatedSpriteTypesList").find("a").css("border", "none");
    $(elem).css("border", "solid #00CC00 2px");
    viewAnimatedSpriteType(key);
    updateGUI();
}


function clickEditAnimatedSpriteTypeSpriteTypeListAddSpriteType(key)
{
    var aniSpriteTypeKey = $("#selectedEditAnimatedSpriteType").val();
    if (aniSpriteTypeKey!=null)
    {
        var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteTypeKey);
        if (aniSpriteType!=null)
        {
            // Now add a new frame...
            if (key!=null)
            {
                var frameSpriteType = engine.getResourceManager().getSpriteType(key);
                if (frameSpriteType!=null)
                {
                    aniSpriteType.newFrame(key);
                    updateGUI();
                }
            }
        }
    }
}

function clickDeleteAnimationFrame(aniSpriteKey, frameIndex)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.removeFrame(frameIndex);
        updateGUI();
    }
}

function clickAdjustFrameDuration(aniSpriteKey, frameIndex)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        var newTime = prompt("Enter a new duration for frame "+frameIndex+" (in milliseconds):", aniSpriteType.frames[frameIndex].duration);
        if (isNaN(parseInt(newTime)))
        {
            alert("An invalid number was given. The duration must be in milliseconds and cannot be a fraction.");
            return;
        }
        aniSpriteType.frames[frameIndex].duration = parseInt(newTime);
        updateGUI();
    }
}

function clickSetAllFrameDurations()
{
    var aniSpriteKey = $("#selectedEditAnimatedSpriteType").val();
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        var newTime = prompt("Enter a new duration for all frames (in milliseconds):", 50);
        var parsedTime = parseInt(newTime);
        if (isNaN(parsedTime))
        {
            alert("An invalid number was given. The duration must be in milliseconds and cannot be a fraction.");
            return;
        }
        for(var i = 0; i<aniSpriteType.frames.length; i++)
            aniSpriteType.frames[i].duration = parsedTime;
        updateGUI();
    }
}

function clickAdjustFrameX(aniSpriteKey, frameIndex, value)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedX += value;
        updateGUI();
    }
}

function clickAdjustFrameY(aniSpriteKey, frameIndex, value)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedY += value;
        updateGUI();
    }
}

function clickAdjustFrameScale(aniSpriteKey, frameIndex, value)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedScale += value;
        updateGUI();
    }
}


function clickResetFrameAdjustmentX(aniSpriteKey, frameIndex)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedX = 0;;
        updateGUI();
    }
}

function clickResetFrameAdjustmentY(aniSpriteKey, frameIndex)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedY = 0;;
        updateGUI();
    }
}

function clickResetFrameAdjustmentScale(aniSpriteKey, frameIndex)
{
    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteKey);
    if (aniSpriteType!=null)
    {
        aniSpriteType.frames[frameIndex].adjustedScale = 0;;
        updateGUI();
    }
}


function clickAddMapObjectType()
{
    var name = $("#newMapObjectType_name").val();
    var spriteType = $("#newMapObjectType_spriteType").val();

    if (name==null || name=="")
    {
        alert("Please give this map object type a name.");
        return;
    }

    if (spriteType==null || spriteType=="")
    {
        alert("Please select a sprite type that will become the map object type's initial graphic.");
        return;
    }

    var rm = engine.getResourceManager();
    var mapObjectType = new H5EMapObjectType(engine, spriteType, 32);
    rm.addMapObjectType(name, mapObjectType);
    updateGUI();
}

function clickNewMapObjectType_SpriteType(imgElem, spriteTypeKey)
{
    $("#newMapObjectTypeSpriteTypesList").find("img").css("border", "none");
    $(imgElem).css("border", "solid #00CC00 2px");
    $("#newMapObjectType_spriteType").val(spriteTypeKey);
}

function clickSelectMapObjectTypeInEditMapObjectTypesPanel(imgElem, mapObjectTypeKey)
{
    $("#editMapObjectTypesList").find("img").css("border", "none");
    $(imgElem).css("border", "solid #00CC00 2px");
    $("#selectedEditMapObjectType").val(mapObjectTypeKey);

    var rm = engine.getResourceManager();
    var mapObjectType = rm.getMapObjectType(mapObjectTypeKey);
    $("#editMapObjectType_adjustX").val(mapObjectType.getAdjustX());
    $("#editMapObjectType_adjustY").val(mapObjectType.getAdjustY());
    $("#editMapObjectType_originX").val(mapObjectType.getOriginX());
    $("#editMapObjectType_originY").val(mapObjectType.getOriginY());

    viewMapObjectType(mapObjectTypeKey);
}


//************************************************************************
//************************************************************************
//************************************************************************
// LIVE VIEW FUNCTIONS

function viewSpriteType(spriteTypeKey)
{
    engine.clearAllLayers();
    demoSprite = engine.layers[0].newSprite(spriteTypeKey);
    demoSprite.setX(200);
    demoSprite.setY(200);
    demoSprite.setScale(liveViewZoom);
    demoSprite.setOriginCenter();


}


function viewAnimatedSpriteType(aniSpriteTypeKey)
{
    engine.clearAllLayers();

    var aniSpriteType = engine.getResourceManager().getAnimatedSpriteType(aniSpriteTypeKey);
    if (aniSpriteType.frames.length>0)
    {
        demoSprite = engine.layers[0].newSprite(aniSpriteType.frames[0].spriteTypeKey);

        demoSprite.setX(200);
        demoSprite.setY(200);
        demoSprite.setScale(liveViewZoom);
        demoSprite.setOriginCenter();
        

        // Now create the animation...
        var ani = new H5EAnimatedSpriteBehaviour(demoSprite, aniSpriteType);
        demoSprite.addBehaviour(ani);
        ani.play();
    }
}

function viewMapObjectType(mapObjectTypeKey)
{
    engine.clearAllLayers();
    engine.addLayer(mapObjectLayer);
    engine.addLayer(overlayLayer);

    overlayLayer.removeAllSprites();

    // Remove any previous map objects from the map object layer...
    if (demoMapObject!=null)
        mapObjectLayer.removeMapObject(demoMapObject);

    var rm = engine.getResourceManager();
    var mapObjectType = rm.getMapObjectType(mapObjectTypeKey);
    demoMapObject = new H5EMapObject(engine, mapObjectLayer, mapObjectType, 5, 5, 32);
    mapObjectLayer.addMapObject(demoMapObject);
    demoMapObject.setScale(liveViewZoom);
    originSprite.setX(5*32+mapObjectType.getOriginX());
    originSprite.setY(5*32+mapObjectType.getOriginY());

    for(var x = demoMapObject.getMapObjectType().getTileAdjustX(); x<demoMapObject.getMapObjectType().getTileAdjustX()+demoMapObject.getMapObjectType().getTileSizeX(); x++)
        for(var y = demoMapObject.getMapObjectType().getTileAdjustY(); y<demoMapObject.getMapObjectType().getTileAdjustY()+demoMapObject.getMapObjectType().getTileSizeY(); y++)
        {
            if (demoMapObject.getMapObjectType().getFootprint()[x-demoMapObject.getMapObjectType().getTileAdjustX()][y-demoMapObject.getMapObjectType().getTileAdjustY()]==true)
            {
                var footprintSprite = overlayLayer.newSprite("libraryeditor_footprint");
                footprintSprite.setX(5*32+x*32);
                footprintSprite.setY(5*32+y*32);
                footprintSprite.setTransparency(0.5);
            }
        }

    overlayLayer.addSprite(originSprite);


    overlayLayer._doClick = function(evt)
    {
        // Get tile clicked...
        var tileX = Math.floor(evt.canvasX/32);
        var tileY = Math.floor(evt.canvasY/32);

        // Transform to local mapobject coords...
        tileX-=demoMapObject.getTileX()+demoMapObject.getMapObjectType().getTileAdjustX();
        tileY-=demoMapObject.getTileY()+demoMapObject.getMapObjectType().getTileAdjustY();

        if (tileX>=0 && tileX<=demoMapObject.getMapObjectType().getTileSizeX()
            && tileY>=0 && tileY<=demoMapObject.getMapObjectType().getTileSizeY())
        {
            demoMapObject.getMapObjectType().getFootprint()[tileX][tileY] = !demoMapObject.getMapObjectType().getFootprint()[tileX][tileY];
        }
        viewMapObjectType(mapObjectTypeKey);
    }
}
