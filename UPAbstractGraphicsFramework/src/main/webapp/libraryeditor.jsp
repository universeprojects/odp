<%-- 
    Document   : libraryeditor
    Created on : Jul 31, 2010, 4:48:09 PM
    Author     : Nikolas
--%>

<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd"> 

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Html5Engine Library Editor</title>
        <link href="standard.css" rel="stylesheet" type="text/css" />
        <link href="libraryeditor.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="js-html5engine/H5EResourceLibrary.js"></script>
        <script type="text/javascript" src="js-html5engine/Hashmap.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EEngine.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EAudio.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EImage.js"></script>
        <script type="text/javascript" src="js-html5engine/H5ELayer.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EResourceManager.js"></script>
        <script type="text/javascript" src="js-html5engine/H5ESprite.js"></script>
        <script type="text/javascript" src="js-html5engine/H5ESpriteType.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EBehaviour.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EBouncyBehaviour.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EGraphicElement.js"></script>
        <script type="text/javascript" src="js-html5engine/H5ETileMap.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EAnimatedSpriteBehaviour.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EAnimatedSpriteType.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EAnimatedSpriteTypeFrame.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EViewport.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EMapObjectType.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EMapObject.js"></script>
        <script type="text/javascript" src="js-html5engine/H5EMapObjectLayer.js"></script>
        <script type="text/javascript">
            <!--
            function selectTab(tabName)
            {
                $('#main').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                $("#"+tabName+"Pane").css({'visibility' : 'inherit', 'position' : 'inherit'});
                $('#controls').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                $("#"+tabName+"Controls").css({'visibility' : 'inherit', 'position' : 'inherit'});
            }
            -->
        </script>
        <script type="text/javascript" src="site-javascript/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="site-javascript/libraryeditor.js"></script>
    </head>
    <body>
        <h1 id="titleHeader">Resource Library Editor</h1>
        <div id="tabs" class="mainpane">
            <a  onclick="selectTab('general')">General</a> |
            <a  onclick="selectTab('images')">Images</a> |
            <a  onclick="selectTab('spriteTypes')">Sprite Types</a> |
            <a  onclick="selectTab('animatedSpriteTypes')">Animated Sprite Types</a> |
            <a  onclick="selectTab('mapObjectTypes')">Map Object Types</a>
        </div>
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td id="controls" style="max-width: 170px; width:170px" valign="top">
                    <script type="text/javascript">
                        function selectGeneralTab(tabName)
                        {
                            $('#generalPane').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                            $("#"+tabName).css({'visibility' : 'inherit', 'position' : 'inherit'});

                            // Special case
                            if (tabName=="exportLibrary")
                                $("#exportLibraryText").val(engine.getResourceManager().writeLibraryFile());
                        }
                    </script>
                    <div class="controls" id="generalControls">
                        <h2>Controls</h2>
                        <a  onclick="selectGeneralTab('newLibrary')" title="Clears the current library and creates a new one">New Library</a><br>
                        <a  onclick="selectGeneralTab('loadLibrary')" title="Loads an existing library from the Html5Engine server under your login">Load Library <small>(from server)</small></a><br>
                        <a  onclick="selectGeneralTab('saveLibrary')" title="Saves the current library to the Html5Engine server under your login">Save Library <small>(to server)</small></a><br>
                        <a  onclick="selectGeneralTab('renameLibrary')" title="Renames the current library">Rename Library</a><br>

                        <a  onclick="selectGeneralTab('exportLibrary')" title="The current library in it's current state will be turned into a .h5l file for you to download">Export Library</a><br>
                        <a  onclick="selectGeneralTab('importLibrary')" title="Allows you to upload a .h5l file to the Html5Engine server for editing">Import Library</a><br>

                    </div>
                    <script type="text/javascript">
                        function selectImageTab(tabName)
                        {
                            $('#imagesPane').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                            $("#"+tabName).css({'visibility' : 'inherit', 'position' : 'inherit'});
                        }
                    </script>
                    <div class="controls" id="imagesControls">
                        <h2><u>Controls</u></h2>
                        <a  title="Adds an image to the library via an url" onclick="selectImageTab('newImage')">Add Image</a><br>
                        <a  title="Displays all images in the current library in the window to the right" onclick="selectImageTab('showImages')">Show Images</a><br>
                    </div>
                    <script type="text/javascript">
                        function selectSpriteTypeTab(tabName)
                        {
                            $('#spriteTypesPane').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                            $("#"+tabName).css({'visibility' : 'inherit', 'position' : 'inherit'});
                        }
                    </script>
                    <div class="controls" id="spriteTypesControls">
                        <h2><u>Controls</u></h2>
                        <a  title="Adds an sprite type to the library" onclick="selectSpriteTypeTab('newSpriteType')">Add Sprite Type</a><br>
                        <a  title="Displays all sprite types, in the current library, in the window to the right and provides options for editing" onclick="selectSpriteTypeTab('editSpriteTypes')">Edit Sprite Types</a><br>
                    </div>
                    <script type="text/javascript">
                        function selectAnimatedSpriteTypeTab(tabName)
                        {
                            $('#animatedSpriteTypesPane').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                            $("#"+tabName).css({'visibility' : 'inherit', 'position' : 'inherit'});
                        }
                    </script>
                    <div class="controls" id="animatedSpriteTypesControls">
                        <h2><u>Controls</u></h2>
                        <a  title="Adds an animated sprite type to the library" onclick="selectAnimatedSpriteTypeTab('newAnimatedSpriteType')">Add Animated Sprite Type</a><br>
                        <a  title="Displays all animated sprite types, in the current library, in the window to the right and provides options for editing" onclick="selectAnimatedSpriteTypeTab('editAnimatedSpriteTypes')">Edit Animated Sprite Types</a><br>
                    </div>
                    <script type="text/javascript">
                        function selectMapObjectTypeTab(tabName)
                        {
                            $('#mapObjectTypesPane').children().css({'visibility' : 'hidden', 'position' : 'absolute'});
                            $("#"+tabName).css({'visibility' : 'inherit', 'position' : 'inherit'});
                        }
                    </script>
                    <div class="controls" id="mapObjectTypesControls">
                        <h2><u>Controls</u></h2>
                        <a  title="Adds a map objectReference type to the library" onclick="selectMapObjectTypeTab('newMapObjectType')">Add Map Object Type</a><br>
                        <a  title="Displays all map objectReference types, in the current library, in the window to the right and provides options for editing" onclick="selectMapObjectTypeTab('editMapObjectTypes')">Edit Map Object Types</a><br>
                    </div>
                </td>
                <td id="main" valign="top" align="left">
                    <div class="mainpane" id="generalPane">
                        <div id="newLibrary">
                            <h2>New Library</h2>
                            <p>Before doing this, please know that any unsaved changes
                                to the current library will be lost.
                            </p>
                            <button onclick="clickNewLibrary()">Create new library</button>
                        </div>
                        <div id="loadLibrary" style="visibility:hidden">
                            <h2>Load Library</h2>
                        </div>
                        <div id="saveLibrary" style="visibility:hidden">
                            <h2>Save Library</h2>

                        </div>
                        <div id="renameLibrary" style="visibility:hidden">
                            <h2>Rename Library</h2>

                        </div>
                        <div id="exportLibrary" style="visibility:hidden">
                            <h2>Export Library</h2>
                            <p>The text below is the contents of the current library
                            in the form of a .H5L file that you can copy
                            and save to your local hard-drive or server for later use.</p>
                            <textarea cols="80" rows="20" id="exportLibraryText"></textarea>
                        </div>
                        <div id="importLibrary" style="visibility:hidden">
                            <h2>Import Library</h2>
                            <p>Paste the contents of the .H5L file you wish to import
                            into the area below. </p>
                            <textarea cols="80" rows="20" id="importLibraryText"></textarea>
                            <input type="button" value="Import Library" onclick="clickImportLibrary()">
                        </div>
                    </div>
                    <div class="mainpane" id="imagesPane">
                        <div id="newImage">
                            <h2>New Image</h2>
                            <p>New Image Name <small>(leave blank if it will be the same as the url)</small></p>
                            <input type="text" id="newImage_name" size="60">
                            <p>New Image URL</p>
                            <input type="text" id="newImage_url" size="60"><br>
                            <input type="button" value="Add" onclick="clickAddImage()">
                        </div>
                        <div id="showImages" style="visibility:hidden">

                        </div>
                    </div>
                    <div class="mainpane" id="spriteTypesPane">
                        <div id="newSpriteType">
                            <h2>New Sprite Type</h2>
                            <p>New Sprite Type Name <small>(leave blank if it will be the same as the image you choose)</small></p>
                            <input type="text" id="newSpriteType_name" size="60">
                            <input type="hidden" id="newSpriteType_image">
                            <input type="button" value="Add" onclick="clickAddSpriteType()">
                            <hr>
                            <h2>Image data</h2>
                            <div id="newSpriteTypeImagesList">
                                
                            </div>
                        </div>
                        <script type="text/javascript">
                            function adjustSpriteTypeAreaValue(areaField, value)
                            {
                                $('#editSpriteType_area'+areaField).val(parseInt($('#editSpriteType_area'+areaField).val())+value);
                                if (areaField=="X")
                                    $('#editSpriteType_areaWidth').val(parseInt($('#editSpriteType_areaWidth').val())-value);
                                if (areaField=="Y")
                                    $('#editSpriteType_areaHeight').val(parseInt($('#editSpriteType_areaHeight').val())-value);
                                document.getElementById('editSpriteType_save').click()
                            }

                        </script>
                        <div id="editSpriteTypes" style="visibility:hidden">
                            <div class="subpanel" style="float:right">
                                <a class="miniButton" onclick="selectNextSpriteType(true)">Previous</a> | <a class="miniButton" onclick="selectNextSpriteType(false)">Next</a>
                            </div>
                            <h2>Edit Sprite Types</h2>
                            <input type="hidden" id="editSpriteType_spriteType">
                            AreaX:&nbsp;<input type="text" id="editSpriteType_areaX" value="" size="8">
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', 100)">+ + +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', 1)">+</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', -1)">-</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('X', -100)">- - -</a>
                            <br>
                            AreaY:&nbsp;<input type="text" id="editSpriteType_areaY" value="" size="8">
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', 100)">+ + +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', 1)">+</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', -1)">-</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Y', -100)">- - -</a>
                            <br>
                            Area&nbsp;Width:&nbsp;<input type="text" id="editSpriteType_areaWidth" value="" size="8">
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', 100)">+ + +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', 1)">+</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', -1)">-</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Width', -100)">- - -</a>
                            <br>
                            Area&nbsp;Height:&nbsp;<input type="text" id="editSpriteType_areaHeight" value="" size="8">
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', 100)">+ + +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', 1)">+</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', -1)">-</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustSpriteTypeAreaValue('Height', -100)">- - -</a>
                            <br>

                            <input type="button" id="editSpriteType_save" value="Save Changes" onclick="clickEditSpriteTypesSave()">
                            <hr>
                            <div id="editSpriteTypesList">
                                
                            </div>
                        </div>
                    </div>
                    <div class="mainpane" id="animatedSpriteTypesPane">
                        <div id="newAnimatedSpriteType">
                            <h2>New Animated Sprite Type</h2>
                            <p>New Animated Sprite Type Name</p>
                            <input type="text" id="addAnimatedSpriteType_name" size="60">
                            <input type="button" value="Add" onclick="clickAddAnimatedSpriteType()">
                        </div>
                        <div id="editAnimatedSpriteTypes" style="visibility:hidden">
                            <h2>Edit Animated Sprite Types</h2>
                            <input type="hidden" id="selectedEditAnimatedSpriteType">
                            <div class="subpanel" style="display: inline-table; min-width: 150px">
                                <h2 style="display:inline-block">Frames</h2>
                                <a class="miniButton" style="display:inline-block;float:right;margin-top:20px" onclick="clickSetAllFrameDurations()" title="Sets all frame durations to a single value">Set All</a>
                                <div id="editAnimatedSpriteTypesFramesList">

                                </div>
                            </div>
                            <br>
                            <hr>
                            <div id="editAnimatedSpriteTypesList" class="subpanel" style="display: inline-table;">

                            </div>
                            <input type="hidden" id="selectedEditAnimatedSpriteTypeSpriteType">
                            <div id="editAnimatedSpriteTypesSpriteTypeList" class="subpanel" style="display: inline-table;">

                            </div>
                        </div>
                    </div>

                    <!-- MAP OBJECT TYPES -->
                    <div class="mainpane" id="mapObjectTypesPane">
                        <div id="newMapObjectType">
                            <h2>New Map Object Type</h2>
                            <p>New Map Object Type Name</p>
                            <input type="text" id="newMapObjectType_name" size="60">
                            <input type="hidden" id="newMapObjectType_spriteType">
                            <input type="button" value="Add" onclick="clickAddMapObjectType()">
                            <hr>
                            <h2>Sprite Types</h2>
                            <div id="newMapObjectTypeSpriteTypesList">

                            </div>
                        </div>
                        <div id="editMapObjectTypes" style="visibility:hidden">
                            <script type="text/javascript">
                                function adjustMapObjectTypeValue(field, value)
                                {
                                    $('#editMapObjectType_'+field).val(parseInt($('#editMapObjectType_'+field).val())+value);
                                    var mapObjectTypeKey = $("#selectedEditMapObjectType").val();
                                    var adjustX = parseInt($("#editMapObjectType_adjustX").val());
                                    var adjustY = parseInt($("#editMapObjectType_adjustY").val());
                                    var originX = parseInt($("#editMapObjectType_originX").val());
                                    var originY = parseInt($("#editMapObjectType_originY").val());

                                    var rm = engine.getResourceManager();
                                    var mapObjectType = rm.getMapObjectType(mapObjectTypeKey);
                                    mapObjectType.setAdjustX(adjustX);
                                    mapObjectType.setAdjustY(adjustY);
                                    mapObjectType.setOriginX(originX);
                                    mapObjectType.setOriginY(originY);

                                    viewMapObjectType(mapObjectTypeKey);
                                }

                            </script>
                            <h2>Edit Map Object Types</h2>
                            <input type="hidden" id="selectedEditMapObjectType">
                            Image&nbsp;Adjust&nbsp;X:&nbsp;<input type="text" id="editMapObjectType_adjustX" value="" size="8">
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', -100)">- - -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', -1)">-</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', 1)">+</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustX', 100)">+ + +</a>
                            <br>
                            Image&nbsp;Adjust&nbsp;Y:&nbsp;<input type="text" id="editMapObjectType_adjustY" value="" size="8">
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', -100)">- - -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', -1)">-</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', 1)">+</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('adjustY', 100)">+ + +</a>
                            <br>
                            Origin&nbsp;Adjust&nbsp;X:&nbsp;<input type="text" id="editMapObjectType_originX" value="" size="8">
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', -100)">- - -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', -1)">-</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', 1)">+</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originX', 100)">+ + +</a>
                            <br>
                            Origin&nbsp;Adjust&nbsp;Y:&nbsp;<input type="text" id="editMapObjectType_originY" value="" size="8">
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', -100)">- - -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', -10)">- -</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', -1)">-</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', 1)">+</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', 10)">+ +</a>
                            <a  class="miniButton" onclick="adjustMapObjectTypeValue('originY', 100)">+ + +</a>
                            <br>

                            <hr>
                            <div id="editMapObjectTypesList" class="subpanel" style="display: inline-table;">

                            </div>
                            <input type="hidden" id="selectedEditMapObjectTypeSpriteType">
                            <div id="editMapObjectTypesSpriteTypeList" class="subpanel" style="display: inline-table;">

                            </div>
                        </div>
                    </div>
                </td>
                <td valign="top" style="min-width: 400px; min-height: 400px">
                    <div class="mainpane" style="float:right;height:450px;min-width:400px;min-height:450px;">
                        <div style="border-bottom: solid 2px #CCCCCC">
                            <h2 style="display:inline-table">Live View</h2>
                            <div style="float:right;display:inline-table;margin-top:20px">
                                <a  class="miniButton" onclick="clickLiveViewZoom(false)">Zoom -</a>
                                <a  id="liveViewZoom" onclick="clickLiveViewZoomReset()">100%</a>
                                <a  class="miniButton" onclick="clickLiveViewZoom(true)">Zoom +</a>
                            </div>
                        </div>
                        <div id="2dwindow">
                            <!--The 2D canvas gets created programmatically here-->
                        </div>
                    </div>
                </td>
            </tr>
        </table>

        <script type="text/javascript">
            selectTab('general');
            selectGeneralTab("newLibrary");
            initialize('2dwindow');
        </script>
    </body>
</html>
