/*
 * $Id: JSONObject.java,v 1.1 2006/04/15 14:10:48 platform Exp $
 * Created on 2006-4-10
 */
package com.universeprojects.json.shared;

import com.universeprojects.common.shared.util.GwtIncompatible;
import com.universeprojects.common.shared.util.Strings;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * A JSON object. Key value pairs are unordered. JSONObject supports java.util.Map interface.
 *
 * @author FangYidong<fangyidong @ yahoo.com.cn>
 */
@SuppressWarnings({"rawtypes", "NonJREEmulationClassesInClientCode"})
public class JSONObject extends LinkedHashMap<String, Object> implements Map<String, Object>, JSONAware, JSONStreamAware {
    private static final long serialVersionUID = -503443796854799292L;

    public JSONObject(Map<String, ?> m) {
        super(m);
    }

    public JSONObject(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
    }

    public JSONObject(int initialCapacity) {
        super(initialCapacity);
    }

    public JSONObject() {
    }

    public JSONObject putFluent(String key, Object value) {
        put(key, value);
        return this;
    }

    public String getString(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof String) {
            return (String) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not String: "+value.getClass());
    }

    public String getString(String key, String defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof String) {
            return (String) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not String: "+value.getClass());
    }

    public String getStringOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof String) {
            return (String) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not String: "+value.getClass());
    }

    public int getInt(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof Number) {
            return ((Number) value).intValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not int: "+value.getClass());
    }

    public int getInt(String key, int defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof Number) {
            return ((Number) value).intValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not int: "+value.getClass());
    }

    public Integer getIntOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof Integer) {
            return (Integer) value;
        } else if(value instanceof Number) {
            return ((Number) value).intValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Integer: "+value.getClass());
    }

    public long getLong(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof Number) {
            return ((Number) value).longValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not long: "+value.getClass());
    }

    public long getLong(String key, long defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof Number) {
            return ((Number) value).longValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not long: "+value.getClass());
    }

    public Long getLongOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof Long) {
            return (Long) value;
        } else if(value instanceof Number) {
            return ((Number) value).longValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Long: "+value.getClass());
    }

    public float getFloat(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof Number) {
            return ((Number) value).floatValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Float: "+value.getClass());
    }

    public float getFloat(String key, float defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof Number) {
            return ((Number) value).floatValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Float: "+value.getClass());
    }

    public Float getFloatOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof Float) {
            return (Float) value;
        } else if(value instanceof Number) {
            return ((Number) value).floatValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Float: "+value.getClass());
    }

    public double getDouble(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof Number) {
            return ((Number) value).doubleValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Double: "+value.getClass());
    }

    public double getDouble(String key, double defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof Number) {
            return ((Number) value).doubleValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Double: "+value.getClass());
    }

    public Double getDoubleOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof Double) {
            return (Double) value;
        } else if(value instanceof Number) {
            return ((Number) value).doubleValue();
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not Double: "+value.getClass());
    }

    public boolean getBoolean(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof Boolean) {
            return (Boolean) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not boolean: "+value.getClass());
    }

    public boolean getBoolean(String key, boolean defaultValue) {
        Object value = get(key);
        if(value == null) {
            return defaultValue;
        } else if(value instanceof Boolean) {
            return (Boolean) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not boolean: "+value.getClass());
    }

    public Boolean getBooleanOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof Boolean) {
            return (Boolean) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not JSONArray: "+value.getClass());
    }

    public JSONArray getJSONArray(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof JSONArray) {
            return (JSONArray) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not JSONArray: "+value.getClass());
    }

    public JSONArray getJSONArrayOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof JSONArray) {
            return (JSONArray) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not JSONObject: "+value.getClass());
    }

    public JSONObject getJSONObject(String key) {
        Object value = get(key);
        if(value == null) {
            throw new IllegalArgumentException("Value for key "+key+" is null");
        } else if(value instanceof JSONObject) {
            return (JSONObject) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not JSONObject: "+value.getClass());
    }

    public JSONObject getJSONObjectOrNull(String key) {
        Object value = get(key);
        if(value == null) {
            return null;
        } else if(value instanceof JSONObject) {
            return (JSONObject) value;
        }
        throw new IllegalArgumentException("Value "+value+" for key "+key+" is not JSONObject: "+value.getClass());
    }

    /**
     * Encode a map into JSON text and write it to out.
     * If this map is also a JSONAware or JSONStreamAware, JSONAware or JSONStreamAware specific behaviours will be ignored at this top level.
     */
    @GwtIncompatible
    public static void writeJSONString(Map map, Writer out, final String indent) throws IOException {
        if (map == null) {
            out.write("null");
            return;
        }
        StringBuilder innerIndent = indent == null ? null : new StringBuilder(indent);
        if (indent != null) {
            for (int i = 0; i < JSONValue.INDENT; i++)
                innerIndent.append(" ");
        }

        boolean first = true;
        Iterator iter = map.entrySet().iterator();

        out.write('{');
        while (iter.hasNext()) {
            if (first)
                first = false;
            else
                out.write(',');
            if (indent != null) {
                out.write("\n");
                out.write(innerIndent.toString());
            }
            Map.Entry entry = (Map.Entry) iter.next();

            out.write('\"');
            out.write(escape(String.valueOf(entry.getKey())));
            out.write('\"');
            out.write(':');
            JSONValue.writeJSONString(entry.getValue(), out, innerIndent == null ? null : innerIndent.toString());
        }
        if (indent != null) {
            out.write("\n");
            out.write(indent);
        }
        out.write('}');
    }

    @GwtIncompatible
    @Override
    public void writeJSONString(Writer out, String indent) throws IOException {
        writeJSONString(this, out, indent);
    }

    /**
     * Convert a map to JSON text. The result is a JSON object.
     * If this map is also a JSONAware, JSONAware specific behaviours will be omitted at this top level.
     *
     * @param map The map to convert
     * @return JSON text, or "null" if map is null.
     * @see JSONValue#toJSONString(Object)
     */
    public static String toJSONString(Map map) {
        if (map == null)
            return "null";

        StringBuffer sb = new StringBuffer();
        boolean first = true;
        Iterator iter = map.entrySet().iterator();

        sb.append('{');
        while (iter.hasNext()) {
            if (first)
                first = false;
            else
                sb.append(',');

            Map.Entry entry = (Map.Entry) iter.next();
            toJSONString(String.valueOf(entry.getKey()), entry.getValue(), sb);
        }
        sb.append('}');
        return sb.toString();
    }

    @Override
    public String toJSONString() {
        return toJSONString(this);
    }

    public static JSONObject exceptionToJSONObject(Throwable ex) {
        JSONObject json = new JSONObject();
        if (ex.getMessage() != null)
            json.put("message", ex.getMessage());
        JSONArray arr = new JSONArray();
        for (StackTraceElement el : ex.getStackTrace()) {
            JSONObject elObj = new JSONObject();
            elObj.put("class", el.getClassName());
            elObj.put("method", el.getMethodName());
            elObj.put("file", el.getFileName());
            elObj.put("line", el.getLineNumber());
            arr.add(elObj);
        }
        if (ex.getCause() != null)
            json.put("cause", exceptionToJSONObject(ex.getCause()));
        json.put("stackTrace", arr);
        return json;
    }

    private static String toJSONString(String key, Object value, StringBuffer sb) {
        sb.append('\"');
        if (key == null)
            sb.append("null");
        else
            JSONValue.escape(key, sb);
        sb.append('\"').append(':');

        sb.append(JSONValue.toJSONString(value));

        return sb.toString();
    }

    @Override
    public String toString() {
        return toJSONString();
    }

    public static String toString(String key, Object value) {
        StringBuffer sb = new StringBuffer();
        toJSONString(key, value, sb);
        return sb.toString();
    }

    /**
     * Escape quotes, \, /, \r, \n, \b, \f, \t and other control characters (U+0000 through U+001F).
     * It's the same as JSONValue.escape() only for compatibility here.
     *
     * @see JSONValue#escape(String)
     */
    public static String escape(String s) {
        return JSONValue.escape(s);
    }

    public void putIfNotEmpty(String key, String value) {
        if(Strings.isEmpty(value)) {
            return;
        }
        put(key, value);
    }

    public void putIfNotNull(String key, Object value) {
        if(value == null) {
            return;
        }
        put(key, value);
    }
}
