package com.universeprojects.common.shared.math;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UPVectorTest {

    @Test
    public void angleTest() {
        UPVector upVectorA = new UPVector(0, 1);
        UPVector upVectorB = new UPVector(-1, 0);
        assertEquals(90, (upVectorA.angleDeg(upVectorB) + 360) % 360, 30);

        upVectorB = new UPVector(0, -1);
        assertEquals(180, (upVectorA.angleDeg(upVectorB) + 360) % 360, 30);

        upVectorB = new UPVector(1, 0);
        assertEquals(270, (upVectorA.angleDeg(upVectorB) + 360) % 360, 30);
    }

    @Test
    public void rotationTest() {
        UPVector upVector = new UPVector(0, 1).rotateDeg(90);
        assertEquals(-1, upVector.x, 0.1f);
        assertEquals(0, upVector.y, 0.1f);

        upVector = new UPVector(0, 1).rotateDeg(-90);
        assertEquals(1, upVector.x, 0.1f);
        assertEquals(0, upVector.y, 0.1f);

        upVector = new UPVector(0, 1).rotateDeg(180);
        assertEquals(0, upVector.x, 0.1f);
        assertEquals(-1, upVector.y, 0.1f);

        upVector = new UPVector(1, 0).rotateDeg(90);
        assertEquals(0, upVector.x, 0.1f);
        assertEquals(1, upVector.y, 0.1f);

        final float oneOverSqrtTwo = (float) (1f/Math.sqrt(2f));

        upVector = new UPVector(0, 1).rotateDeg(45);
        assertEquals(-oneOverSqrtTwo, upVector.x, 0.1f);
        assertEquals(oneOverSqrtTwo, upVector.y, 0.1f);

        upVector = new UPVector(0, -1).rotateDeg(45);
        assertEquals(oneOverSqrtTwo, upVector.x, 0.1f);
        assertEquals(-oneOverSqrtTwo, upVector.y, 0.1f);
    }
}