package com.universeprojects.common.shared.util;

import java.util.LinkedHashMap;
import java.util.Map;


public class GWTDebug {

	
	public static void log(String message) {
		Log.warn(message);
	}
	
	public static void log(String message, Exception e) {
		Log.warn(message, e);
	}
	

	
	
    /** This is a key/value map of debug values. They are included in the FPS counter line when debugMode is true. This map is cleared every tick. */
    @SuppressWarnings("rawtypes")
	public static Map debugValuesPerTick = new LinkedHashMap<String, Object>();
    /**
     * The "Debug Values Per Tick" mechanism is a special map that resets every tick in the H5EEngine. It is intended as
     * a way of counting operations (or reporting an operation) that took place in the last tick before a frame draw.
     * As of right now, the H5EEngine will output all the values in this map as part of the FPS line when the engine's
     * debugMode is true.
     * 
     * This version of the map will set the given key's value to the given value.
     * @param key
     * @param value
     */
    @SuppressWarnings("unchecked")
	public static void setDebugValues_PerTick(String key, Object value)
    {
    	debugValuesPerTick.put(key, value);
    }
    
    /**
     * The "Debug Values Per Tick" mechanism is a special map that resets every tick in the H5EEngine. It is intended as
     * a way of counting operations (or reporting an operation) that took place in the last tick before a frame draw.
     * As of right now, the H5EEngine will output all the values in this map as part of the FPS line when the engine's
     * debugMode is true.
     * 
     * This version of the map will get the given key's value and ADD to it the given value. It assumes that the key's 
     * value is an integer and will throw a ClassCastException if it is not.
     * @param key
     * @param amount
     */
    @SuppressWarnings("unchecked")
	public static void addToDebugValues_PerTick(String key, Integer amount)
    {
    	Integer currentVal = (Integer)debugValuesPerTick.get(key);
    	if (currentVal == null) currentVal = 0;
    	currentVal+=amount;
    	debugValuesPerTick.put(key, currentVal);
    }
	
    
    
}
