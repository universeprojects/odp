package com.universeprojects.common.shared.util;

public class TextBuilder {

    private final StringBuilder sb = new StringBuilder();

    public TextBuilder() { }

    public TextBuilder appendLine() {
        return appendLine("");
    }

    public TextBuilder appendLine(String line) {
        sb.append(line).append("\n");
        return this;
    }


    @Override
    public String toString() {
        return sb.toString();
    }

}
