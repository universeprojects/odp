package com.universeprojects.common.shared.profiler;

import com.universeprojects.common.shared.log.Logger;
import com.universeprojects.common.shared.math.UPMath;
import com.universeprojects.common.shared.util.Log;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class ProfilerBase implements Profiler {
    private final Logger logger;

    protected abstract String format(long number);

    protected abstract long getTimeMicro();

    private class ProfilerNode implements ProfilerAutoCloseable {
        final String name;
        final ProfilerNode parent;
        int executions = 0;
        long time = -1;
        long startTime = -1;
        long maxTime = 0;
        int ticks = 0;
        Map<String, ProfilerNode> children = new LinkedHashMap<>();
        Map<String, Integer> counters = new LinkedHashMap<>();

        public ProfilerNode(ProfilerNode parent, String name) {
            this.name = name;
            this.parent = parent;
        }

        public void start() {
            startTime = getTimeMicro();
            executions++;
        }

        public void end() {
            long delta = getTimeMicro() - startTime;
            maxTime = UPMath.max(delta, maxTime);
            time += delta;
            startTime = -1;
        }

        public void inc(String counterKey) {
            Integer count = counters.get(counterKey);
            if (count == null)
                counters.put(counterKey, 1);
            else
                counters.put(counterKey, count + 1);
        }

        public void observeTickStart() {
            ticks++;
            inc("ticks");
        }

        @Override
        public void close() {
            end();
        }
    }


    boolean enabled = false;
    private ProfilerNode root = null;
    private ProfilerNode currentNode = null;


    public ProfilerBase(Logger logger) {
        this.logger = logger;
        clearAll();
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#isEnabled()
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#enable()
     */
    @Override
    public void enable() {
        enabled = true;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#disable()
     */
    @Override
    public void disable() {
        enabled = false;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#start(java.lang.String)
     */
    @Override
    public ProfilerAutoCloseable start(String name) {
        if (!enabled)
            return null;

        ProfilerNode node = currentNode.children.get(name);
        if(node == null) {
            node = new ProfilerNode(currentNode, name);
            currentNode.children.put(name, node);
        }
        currentNode = node;

        currentNode.start();
        return currentNode;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#end(java.lang.String)
     */
    @Override
    public void end(String name) {
        if (!enabled)
            return;

        if (!currentNode.name.equals(name)) {
//            Log.warn("Attempted to end a perf test node out of order! '" + name + "' was given but we were expecting to end '" + currentNode.name + "'.");
            return;
        }

        currentNode.end();
        currentNode = currentNode.parent;
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#incCounter(java.lang.String)
     */
    @Override
    public void incCounter(String counterKey) {
        if (!enabled)
            return;

        currentNode.inc(counterKey);
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#clearAll()
     */
    @Override
    public void clearAll() {
        root = new ProfilerNode(null, "root");
        root.start();
        currentNode = root;
    }

    @Override
    public void observeTickStart() {
        root.observeTickStart();
    }

    @Override
    public void ensureRoot() {
        if(currentNode == root) {
            return;
        }
        Log.warn("Profiler not at root level: "+currentNode.name);
        clearAll();
    }

    /**
     * (non-Javadoc)
     *
     * @see com.universeprojects.common.shared.profiler.Profiler#printAndReset()
     */
    @Override
    public void printAndReset() {
        if (!enabled)
            return;
        root.end();
        logger.info("");
        print(root, root.ticks, 0);
        clearAll();
    }

    private void print(ProfilerNode node, int ticks, int level) {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < level; i++)
            line.append("-");

        String percentage = "";
        if (node.parent != null && node.parent.time > 0) {
            percentage = (100 * node.time) / node.parent.time + "% time, ";
        }
        StringBuilder countersStr = new StringBuilder();
        if (node.counters.size() > 0) {
            countersStr = new StringBuilder("(");
            for (Iterator<String> it = node.counters.keySet().iterator(); it.hasNext(); ) {
                String key = it.next();
                countersStr.append(key).append("=").append(node.counters.get(key));
                if (it.hasNext()) countersStr.append(", ");
            }
            countersStr.append(")");
        }
        long msEx = 0;
        if (node.executions > 0) {
            msEx = UPMath.round((float) node.time / (float) node.executions);
        }
        float exTick = 0;
        long msTick = 0;
        if(ticks > 0) {
            msTick = UPMath.round((float) node.time / (float) ticks);
            exTick = UPMath.round((float) node.executions / (float) ticks);
        }
        logger.info(line + " " + node.name + " = " + format(node.time) + "ms total, " + format(msEx) + "ms/ex, "
                + format(msTick) + "ms/tick, " + format(node.maxTime) + "ms max, " + node.executions + "ex, " + exTick + "ex/tick, " + percentage + countersStr);

        for (ProfilerNode childNode : node.children.values()) {
            print(childNode,  ticks,level + 1);
        }
    }
}
