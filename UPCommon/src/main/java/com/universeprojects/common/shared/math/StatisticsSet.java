package com.universeprojects.common.shared.math;

public class StatisticsSet {

	private double sum;
	private double sqsum;
	private int num;
	private double min;
	private double max;
	private String info;

	public synchronized void add(double amount) {
		sum += amount;
		sqsum += amount*amount;
		num++;
		if(amount < min)
			min = amount;
		if(amount > max)
			max = amount;
	}

	public synchronized void reset() {
		sum = 0;
		sqsum = 0;
		num = 0;
		min = 0;
		max = 0;
		info = null;
	}

	public synchronized double mean() {
		return sum/num;
	}

	public synchronized double stddev() {
		if(num <= 1)
			return 0;
		double sqrtIn = 1./(num-1.)*(sqsum-sum*sum/num);
		if(sqrtIn < 0) return 0;
		return UPMath.sqrt(sqrtIn);
	}

	public synchronized double totalSum() {
		return sum;
	}

	public synchronized int totalNumber() {
		return num;
	}

	public synchronized double max() {
		return max;
	}

	public synchronized double min() {
		return min;
	}

	public synchronized void setInfo(String info) {
		this.info = info;
	}

	public synchronized String getInfo() {
		return info;
	}

}
