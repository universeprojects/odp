package com.universeprojects.common.shared.util;

import com.universeprojects.common.shared.callable.ReturningCallable1Args;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CollectionUtils {
    public static <T> List<List<T>> collate(Collection<T> collection, int size) {
        return collate(collection, size, true);
    }

    public static <T> List<List<T>> collate(Collection<T> collection, int size, boolean keepRemainder) {
        return collate(collection, size, size, keepRemainder);
    }

    public static <T> List<List<T>> collate(Collection<T> collection, int size, int step, boolean keepRemainder) {
        List<T> selfList = collection instanceof List ? (List<T>)collection : new ArrayList<>(collection);
        List<List<T>> answer = new ArrayList<>();
        if (size <= 0) {
            answer.add(selfList);
        } else {
            for (int pos = 0; pos < selfList.size() && pos > -1; pos += step) {
                if (!keepRemainder && pos > selfList.size() - size) {
                    break ;
                }
                List<T> element = new ArrayList<>() ;
                for (int offs = pos; offs < pos + size && offs < selfList.size(); offs++) {
                    element.add(selfList.get(offs));
                }
                answer.add( element ) ;
            }
        }
        return answer ;
    }

    public static <T, R> List<R> map(Collection<T> list, ReturningCallable1Args<R, T> function) {
        if(function == null || list == null || list.isEmpty()) {
            return Collections.emptyList();
        }
        List<R> outputList = new ArrayList<>(list.size());
        for (T element : list) {
            final R newElement = function.call(element);
            outputList.add(newElement);
        }
        return outputList;
    }

    public static <T, R> Map<T, R> mapAsMapKey(Collection<T> list, ReturningCallable1Args<R, T> function) {
        if(function == null || list == null || list.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<T, R> outputMap = new HashMap<>(list.size());
        for (T key : list) {
            final R value = function.call(key);
            if(value != null) {
                outputMap.put(key, value);
            }
        }
        return outputMap;
    }

    public static <T, R> Map<R, T> mapAsMapValue(Collection<T> list, ReturningCallable1Args<R, T> function) {
        if(function == null || list == null || list.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<R, T> outputMap = new HashMap<>(list.size());
        for (T value : list) {
            final R key = function.call(value);
            outputMap.put(key, value);
        }
        return outputMap;
    }

    public static <R, T> Map<R, List<T>> mapAsMapValueList(Collection<T> list, ReturningCallable1Args<R, T> function) {
        if(function == null || list == null || list.isEmpty()) {
            return Collections.emptyMap();
        }
        Map<R, List<T>> outputMap = new LinkedHashMap<>();
        for (T value : list) {
            final R key = function.call(value);
            List<T> mapList = outputMap.get(key);
            if(mapList == null) {
                mapList = new ArrayList<>();
                outputMap.put(key, mapList);
            }
            mapList.add(value);
        }
        return outputMap;
    }
}
