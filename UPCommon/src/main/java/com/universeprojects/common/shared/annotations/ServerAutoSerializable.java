package com.universeprojects.common.shared.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
public @interface ServerAutoSerializable {
    String[] value();
    SerializationType serializationType() default SerializationType.ARRAY;
}
