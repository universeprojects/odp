package com.universeprojects.common.shared.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.util.LinkedHashMap;

@Target({ElementType.FIELD, ElementType.PARAMETER})
public @interface SerializableMap {
    Class keyClass();

    Class valueClass();

    Class mapClass() default LinkedHashMap.class;
}
