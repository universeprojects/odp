package com.universeprojects.common.shared.util;

/**
 * (FOR DEV PURPOSES)
 *
 * This class is useful for timing the execution of various code blocks.
 * Technically, it's a very basic profiler.
 *
 */
public class Timer {

    private final String name;
    private Long startMs;
    private Long stopMs;

    public Timer(String name) {
        this(name, true);
    }

    public Timer(String name, boolean printLog) {
        if (Strings.isEmpty(name)) {
            throw new IllegalArgumentException("name must be provided");
        }
        this.name = name;
        start(printLog);
    }

    public void reset() {
        startMs = null;
        stopMs = null;
    }

    public void start() {
        start(true);
    }

    public void stop() {
        stop(true);
    }

    public void start(boolean printLog) {
        if (startMs != null) {
            throw new IllegalStateException("Timer already started");
        }
        startMs = System.currentTimeMillis();
        if (printLog) {
            log("started");
        }
    }

    public void stop(boolean printLog) {
        if (startMs == null) {
            throw new IllegalStateException("Timer not started");
        }
        if (stopMs != null) {
            throw new IllegalStateException("Timer already stopped");
        }
        stopMs = System.currentTimeMillis();
        if (printLog) {
            log("recorded a duration of " + TimeUtil.millisToReadableStr(stopMs - startMs));
        }
    }

    public long getDuration() {
        if (startMs == null) {
            return 0L;
        }
        if (stopMs == null ) {
            return System.currentTimeMillis() - startMs;
        }
        return stopMs - startMs;
    }

    private void log(String msg) {
        System.out.println("Timer " + Strings.inQuotes(name) + " " + msg);
    }

}
