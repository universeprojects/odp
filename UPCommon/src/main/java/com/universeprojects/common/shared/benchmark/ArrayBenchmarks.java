package com.universeprojects.common.shared.benchmark;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.universeprojects.common.shared.util.Log;

public class ArrayBenchmarks {

	private final int dataSize;

	private Random r = new Random();

	public ArrayBenchmarks(int dataSize) {
		this.dataSize = dataSize;
		log("Random index range: 0.." + (dataSize-1));
	}

	private void log(String message) {
		Log.info(message);
	}

	private int randomIndex() {
		return r.nextInt(dataSize);
	}

	private String randomString() {
		StringBuilder sb = new StringBuilder();
		int numChars = r.nextInt(16) + 5;
		for (int i = 0; i < numChars; i++) {
			char ch = (char)(r.nextInt(26) + 'a');
			sb.append(ch);
		}
		return sb.toString();
	}

	public void doArray() {
		String[] array = new String[dataSize];

		long start_write = System.currentTimeMillis();
		int count_write = 0;
		while (System.currentTimeMillis() - start_write < 1000) {
			array[randomIndex()] = randomString();
			count_write++;
		}

		log("Array write: " + count_write + " per second");

		long start_read = System.currentTimeMillis();
		long count_read = 0;
		while (System.currentTimeMillis() - start_read < 1000) {
			String str = array[randomIndex()];
			count_read++;
		}
		log("Array read: " + count_read + " per second");
	}

	public void doArrayList() {
		List<String> list = new ArrayList<String>(dataSize);
		for (int i = 0; i < dataSize; i++) {
			list.add("");
		}

		long start_set = System.currentTimeMillis();
		int count_set = 0;
		while (System.currentTimeMillis() - start_set < 1000) {
			list.set(randomIndex(), randomString());
			count_set++;
		}

		log("ArrayList.set(): " + count_set + " per second");

		long start_get = System.currentTimeMillis();
		long count_get = 0;
		while (System.currentTimeMillis() - start_get < 1000) {
			list.get(randomIndex());
			count_get++;
		}
		log("ArrayList.get(): " + count_get + " per second");
	}

	public void doHashMap() {
		Map<Integer, String> map = new HashMap<Integer, String>();

		long start_put = System.currentTimeMillis();
		int count_put = 0;
		while (System.currentTimeMillis() - start_put < 1000) {
			map.put(randomIndex(), randomString());
			count_put++;
		}
		log("HashMap.put(): " + count_put + " per second");

		long start_get = System.currentTimeMillis();
		int count_get = 0;
		while (System.currentTimeMillis() - start_get < 1000) {
			map.get(randomIndex());
			count_get++;
		}
		log("HashMap.get(): " + count_get + " per second");
	}

	public void run() {
		doArray();
		doArrayList();
		doHashMap();
	}

	public static void main(String[] args) {
		new ArrayBenchmarks(100).run();
		new ArrayBenchmarks(1000).run();
		new ArrayBenchmarks(10000).run();
	}

}
