package com.universeprojects.common.shared.reflection;

public abstract class InstantiationHelper {
    public static InstantiationHelper INSTANCE;

    public abstract <T> T instantiate(String className, Class<T> targetClass) throws ReflectionException;
}
