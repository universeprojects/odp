package com.universeprojects.common.shared.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
public @interface EditorConfig {
    boolean showInEditor() default true;
    String editorFieldType() default "";
    String editorFieldDefault() default "";
}
