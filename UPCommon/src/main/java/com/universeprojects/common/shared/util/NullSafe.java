package com.universeprojects.common.shared.util;

/**
 * The purpose of this class is to help reduce boilerplate code
 * by providing null-safe alternatives to frequently-used operations
 */
public class NullSafe {

    /**
     * A null-safe a.equals(b) operation:
     * First performs null checks, then calls the standard equals() method.
     *
     * @param a The first object
     * @param b The second object
     * @return  - TRUE if both object references are NULL
     *          - FALSE if just one of the objects is NULL
     *          otherwise, the result of a.equals(b)
     */
    public static boolean equals(Object a, Object b) {
        if (a == null && b == null) {
            return true;
        }
        if (a == null ^ b == null) {
            return false;
        }
        return a.equals(b);
    }

}
