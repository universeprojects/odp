package com.universeprojects.common.shared.util;

import java.util.ArrayList;
import java.util.List;

public class StatsCounter {
    private List<Counter> counters = new ArrayList<>();
    public Counter addCounter(String name) {
        Counter counter = new Counter(name);
        counters.add(counter);
        return counter;
    }

    public synchronized void reset() {
        for(Counter counter : counters) {
            //noinspection SynchronizationOnLocalVariableOrMethodParameter
            synchronized (counter) {
                counter.value = 0;
            }
        }
    }

    public String report() {
        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for(Counter counter : counters) {
            if(!first) {
                sb.append(", ");
            }
            sb.append(counter.name);
            sb.append("=");
            sb.append(counter.value);
            first = false;
        }
        return sb.toString();
    }

    public static class Counter {
        public final String name;
        private int value = 0;

        private Counter(String name) {
            this.name = name;
        }

        public synchronized void inc() {
            value++;
        }
    }
}
