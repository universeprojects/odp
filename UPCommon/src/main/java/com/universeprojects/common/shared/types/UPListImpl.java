package com.universeprojects.common.shared.types;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings({"serial", "GwtInconsistentSerializableClass"})
public class UPListImpl<E> extends ArrayList<E> implements UPList<E> {
    protected final Class<E> elementClass;

    public UPListImpl(Class<E> elementClass) {
        if (elementClass == null)
            throw new RuntimeException("List-Element-class cannot be null");
        this.elementClass = elementClass;
    }

    public UPListImpl(Collection<E> coll, Class<E> elementClass) {
        super(coll);
        this.elementClass = elementClass;
    }

    public UPListImpl(UPList<E> listToCopy) {
        this(listToCopy, listToCopy.getElementClass());
    }

    @Override
    public Class<E> getElementClass() {
        return elementClass;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result
            + ((elementClass == null) ? 0 : elementClass.hashCode());
        return result;
    }

    @SuppressWarnings("EqualsBetweenInconvertibleTypes")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        UPListImpl<?> other = (UPListImpl<?>) obj;

        return elementClass == other.elementClass;
    }


}
