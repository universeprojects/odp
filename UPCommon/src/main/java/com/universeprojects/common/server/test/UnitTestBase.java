package com.universeprojects.common.server.test;

import com.universeprojects.common.shared.log.LoggerFactory;
import com.universeprojects.common.shared.log.SysOutLogProcessor;
import com.universeprojects.common.shared.reflection.SimpleInstantiationHelper;
import com.universeprojects.common.shared.util.Dev;
import com.universeprojects.common.shared.util.Environment;
import com.universeprojects.common.shared.util.Strings;
import org.junit.Assert;
import org.junit.Rule;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnitTestBase {

    static {
//        Log.debugToSystemOut = true;
        LoggerFactory.getInstance().setProcessor(new SysOutLogProcessor());
        Environment.current = Environment.TEST;
        SimpleInstantiationHelper.initialize();
    }

    @Rule
    public RepeatRule repeatRule = new RepeatRule();

    protected void failIfNoExceptionThrown() {
        Assert.fail("Expected exception to be thrown");
    }

    @SuppressWarnings("unused")
    protected void failIfExceptionWasThrown(Exception ex) {
        Assert.fail("Did not expect exception to be thrown: "
                + ex.getClass().getName());
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    protected void assertExceptionType(Exception ex,
                                       Class<? extends Exception> desiredType) {
        Dev.checkNotNull(ex);
        Dev.checkNotNull(desiredType);

        Class<? extends Exception> actualType = ex.getClass();
        if (!actualType.equals(desiredType)) {
            throw new RuntimeException("Expected exception of type "
                    + desiredType.getName() + ", actual exception is of type "
                    + actualType.getName());
        }

    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    protected void assertExceptionMessageContains(Exception ex,
                                                  String... messageParts) {
        Dev.checkNotNull(ex);
        Dev.checkNotNull(messageParts);

        String message = ex.getMessage();
        for (String messagePart : messageParts) {
            if (!message.contains(messagePart)) {
                throw new RuntimeException("Message "
                        + Strings.inSingleQuotes(message)
                        + " does not contain message part "
                        + Strings.inSingleQuotes(messagePart));
            }
        }
    }



    protected static <T> List<T> emptyList(Class<T> elementType) {
        return Collections.emptyList();
    }

    protected static <K, V> Map<K, V> emptyMap(Class<K> keyType, Class<V> valueType) {
        return Collections.emptyMap();
    }

    @SafeVarargs
    protected static <T> List<T> list(T... elements) {
        return Arrays.asList(elements);
    }

    protected static <K, V> Map<K, V> map(K[] keys, V[] values) {
        Map<K, V> map = new HashMap<>();
        if (keys == null || values == null) {
            throw new IllegalArgumentException("Input arrays can't be null");
        }
        if (keys.length != values.length) {
            throw new IllegalArgumentException("Input arrays must have the same length");
        }
        for (int i=0; i<keys.length; i++) {
            map.put(keys[i], values[i]);
        }
        return map;
    }

    @SafeVarargs
    protected static <T> T[] array(T... values) {
        return values;
    }


}
