package com.universeprojects.common.server;

import com.universeprojects.common.shared.config.ConfigurationBase;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SuppressWarnings("unused")
public abstract class ServerConfigurationBase extends ConfigurationBase {

    /**
     * Stores the configuration properties that were loaded from an input stream, or set explicitly
     */
    private final Properties propertyStore = new Properties();


    protected ServerConfigurationBase() {

    }

    /**
     * Loads configuration properties from a provided InputStream.
     * When called multiple times, new values will override previously-loaded values for properties of the same name.
     */
    protected void loadProperties(InputStream inStream) throws IOException {
        propertyStore.load(inStream);
    }

    /**
     * Sets a single property in the underlying properties store.
     * Useful for establishing default configurations
     *
     * @param propertyName The name of the property to set
     * @param propertyValue The value to assign to the property
     */
    protected void writeProperty(String propertyName, String propertyValue) {
        propertyStore.setProperty(propertyName, propertyValue);
    }

    /**
     * Retrieves a configuration property value in the following order:
     * <br/>
     * <br/>
     * 1. From a system property variable with the specified name
     * <br/>
     * 2. From an environment variable with the specified name
     * <br/>
     * 3. From the loaded properties, by the specified name
     * </br>
     *
     * @param propertyName The name of the property to retrieve
     * @return The String value of the property, NULL if not found
     */
    protected String readProperty(String propertyName, String defaultValue) {

        // 1. See if the property is set at system level
        String value = System.getProperty(propertyName);
        if (value != null) {
            return value;
        }

        // 2. See if the property is set as an environment variable
        value = System.getenv(propertyName);
        if (value != null) {
            return value;
        }

        // 3. See if the property is set in the properties store
        value = (String)propertyStore.get(propertyName);
        if (value != null) {
            return value;
        }

        // 4. Finally, resort to the default value (which could be NULL)
        return defaultValue;
    }

}
