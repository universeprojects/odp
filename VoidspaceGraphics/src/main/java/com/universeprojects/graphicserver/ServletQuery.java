package com.universeprojects.graphicserver;

import com.universeprojects.common.shared.util.Strings;
import com.universeprojects.json.shared.JSONArray;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ServletQuery extends HttpServlet {
    private static final long serialVersionUID = 8715060428325324298L;

    private static final String[] imageFileTypes = new String[]{".png", ".jpg", ".gif"};
    private static final String[] audioFileTypes = new String[]{".mp3", ".ogg", ".wav"};
    private String baseDir;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        baseDir = config.getInitParameter("baseDir");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String type = req.getParameter("type");

        if ("imageSearch".equals(type)) {
            String filename = req.getParameter("term");
            String callback = req.getParameter("callback");

            List<String> images = findFiles(filename, "images", imageFileTypes);

            JSONArray json = new JSONArray();
            json.addAll(images);

            resp.setContentType("application/json");
            PrintWriter out = resp.getWriter();
            out.print(callback + "(" + json.toJSONString() + ")");
            out.flush();
            out.close();
        } else if ("audioSearch".equals(type)) {
            String filename = req.getParameter("term");
            String callback = req.getParameter("callback");

            List<String> images = findFiles(filename, "audio", audioFileTypes);

            JSONArray json = new JSONArray();
            json.addAll(images);

            resp.setContentType("application/json");
            PrintWriter out = resp.getWriter();
            out.print(callback + "(" + json.toJSONString() + ")");
            out.flush();
            out.close();
        } else if ("musicSearch".equals(type)) {
            String filename = req.getParameter("term");
            String callback = req.getParameter("callback");

            List<String> images = findFiles(filename, "music", audioFileTypes);

            JSONArray json = new JSONArray();
            json.addAll(images);

            resp.setContentType("application/json");
            PrintWriter out = resp.getWriter();
            out.print(callback + "(" + json.toJSONString() + ")");
            out.flush();
            out.close();
        }
    }

    List<String> findFiles(String filenameContains, String baseFolder, String[] fileTypes) {
        if (filenameContains != null) {
            filenameContains = filenameContains.toLowerCase();
        }

        ArrayList<String> result = new ArrayList<>();
        if (baseFolder == null) {
            return result;
        }

        String baseFolderForAppengine = baseFolder;
        if (Strings.isNotEmpty(baseDir)) {
            if (baseFolderForAppengine.startsWith("/")) {
                baseFolderForAppengine = baseDir + baseFolderForAppengine;
            } else {
                baseFolderForAppengine = baseDir + "/" + baseFolderForAppengine;
            }
        } else {
            if (baseFolderForAppengine.startsWith("/")) {
                baseFolderForAppengine = baseFolderForAppengine.substring(1);
            }
        }

        File base = new File(baseFolderForAppengine);
        Collection<File> files = FileUtils.listFiles(base, TrueFileFilter.INSTANCE, DirectoryFileFilter.DIRECTORY);
        List<String> completeList = GoogleCloudStorageService.getFileList("resources.universeprojects.com", baseFolder);
        files.stream().forEach(file -> completeList.add(file.getPath()));
        for (String fname : completeList) {


            boolean goodExtension = false;
            for (String ext : fileTypes) {
                if (fname.toLowerCase().endsWith(ext)) {
                    goodExtension = true;
                    break;
                }
            }

            if (!goodExtension) {
                continue;
            }


            boolean containsAll = true;
            if (filenameContains != null) {
                String[] keywords = filenameContains.split(" ");
                containsAll = containsAll(fname, keywords);
            }

            if (filenameContains == null || containsAll) {
                fname = fname.replace('\\', '/');
                if (Strings.isNotEmpty(baseDir) && fname.startsWith("https://") == false) {
                    fname = fname.substring(baseDir.length() + 1);
                }
                result.add(fname);
            }
        }
        return result;
    }

    private boolean containsAll(String source, String[] keywords) {
        if (source == null) {
            return false;
        }
        if (keywords == null || keywords.length == 0) {
            return true;
        }

        for (String keyword : keywords) {
            if (!source.contains(keyword)) {
                return false;
            }
        }

        return true;
    }

}
