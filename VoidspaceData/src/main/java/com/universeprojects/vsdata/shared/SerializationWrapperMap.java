package com.universeprojects.vsdata.shared;

import com.universeprojects.json.shared.serialization.SerializationWrapper;

import java.util.HashMap;
import java.util.Map;

public class SerializationWrapperMap<O> extends HashMap<String, O> {

    public SerializationWrapperMap() {
    }

    public SerializationWrapperMap(Map<? extends String, ? extends O> m) {
        super(m);
    }

    public Map<String, SerializationWrapper<O>> toWrappedMap() {
        return toSerializationWrapper(this);
    }

    public void loadFromWrappedMap(Map<String, SerializationWrapper<O>> wrapped) {
        fromSerializationWrapper(wrapped, this);
    }

    public static <O> Map<String, SerializationWrapper<O>> toSerializationWrapper (Map<String, O> objectMap) {
        return toSerializationWrapper(objectMap, new HashMap<>());
    }

    public static <O> Map<String, SerializationWrapper<O>> toSerializationWrapper (Map<String, O> objectMap, Map<String, SerializationWrapper<O>> target) {
        for (Map.Entry<String, O> entry : objectMap.entrySet()) {
            target.put(entry.getKey(), new SerializationWrapper<>(entry.getValue()));
        }
        return target;
    }

    public static <O> Map<String, O> fromSerializationWrapper (Map<String, SerializationWrapper<O>> wrappedObjectMap) {
        return fromSerializationWrapper(wrappedObjectMap, new HashMap<>());
    }

    public static <O> Map<String, O> fromSerializationWrapper (Map<String, SerializationWrapper<O>> wrappedObjectMap, Map<String, O> target) {
        for (Map.Entry<String, SerializationWrapper<O>> entry : wrappedObjectMap.entrySet()) {
            target.put(entry.getKey(), entry.getValue().getValue());
        }
        return target;
    }
}
