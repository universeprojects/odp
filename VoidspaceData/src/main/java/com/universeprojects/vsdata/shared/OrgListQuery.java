package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;

@AutoSerializable(value = {"query", "membershipType"}, serializationType = SerializationType.MAP)
public class OrgListQuery {
    public String query;
    public OrgQueryMembershipType membershipType;

    public OrgListQuery(String query, OrgQueryMembershipType membershipType) {
        this.query = query;
        this.membershipType = membershipType;
    }

    public OrgListQuery() {
    }
}
