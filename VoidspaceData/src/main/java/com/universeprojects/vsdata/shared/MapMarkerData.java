package com.universeprojects.vsdata.shared;


import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"id","name","icon", "description"})
public class MapMarkerData {
    public String id;
    public String name;
    public String icon;
    public String description;

    public MapMarkerData() {
    }

    public MapMarkerData(String id, String name, String icon) {
        this(id, name, icon, "");
    }

    public MapMarkerData(String id, String name, String icon, String description) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.description = description;
    }
}
