package com.universeprojects.vsdata.shared;

public enum SlotLockState {
    OPEN(true, true), LOCKED(false, false), SET_ONLY(true, false), INVISIBLE(false, false);

    public final boolean allowConnect;
    public final boolean allowDisconnect;

    SlotLockState(boolean allowConnect, boolean allowDisconnect) {
        this.allowConnect = allowConnect;
        this.allowDisconnect = allowDisconnect;
    }
}
