package com.universeprojects.vsdata.shared;

import com.universeprojects.json.shared.serialization.AutoSerializer;

public interface VoidspaceDataAutoSerializer extends AutoSerializer {
}
