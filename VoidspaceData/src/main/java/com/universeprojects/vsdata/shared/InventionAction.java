package com.universeprojects.vsdata.shared;

public enum InventionAction {
    EXPERIMENT, PROTOTYPE_ITEM, PROTOTYPE_BUILDING, CONSTRUCT_ITEM, CONSTRUCT_BUILDING, UPGRADE_SKILL
}
