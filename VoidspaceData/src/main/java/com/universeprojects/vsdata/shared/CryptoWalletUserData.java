package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;

@AutoSerializable({"walletName", "currency", "address", "balanceDbl", "balanceStr", "verified", "verifierAmount"})
public class CryptoWalletUserData {
    public String walletName;
    public CryptoCurrency currency;
    public String address;
    public double balanceDbl;
    public String balanceStr;
    public boolean verified;
    public long verifierAmount;
}
