package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.List;
import java.util.ArrayList;

@AutoSerializable({"id", "name", "listOfSubcategories"})
public class CategoryData {
    public String id;
    public String name;
    @SerializableList(elementClass = CategoryData.class)
    public List<CategoryData> listOfSubcategories;

    public CategoryData() {
    }

    public CategoryData(String id, String name, List<CategoryData> list) {
        this.id = id;
        this.name = name;
        this.listOfSubcategories = list;
    }
}