package com.universeprojects.vsdata.shared;

public class CryptoTransactionOutput {
    public double value;
    public String valueText;
    public String[] addresses;
    public String spentBy;


    public CryptoTransactionOutput(double value,String valueText, String[] addresses, String spentBy) {
        this.value = value;
        this.valueText=valueText;
        this.addresses = addresses;
        this.spentBy = spentBy;
    }

    public CryptoTransactionOutput() {
    }

}
