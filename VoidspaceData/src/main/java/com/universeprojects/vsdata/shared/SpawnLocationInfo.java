package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable({"claimedByCurrentChar", "numSlots", "numUsedSlots", "organisations", "characters"})
public class SpawnLocationInfo {
    public boolean claimedByCurrentChar;
    public Integer numSlots;
    public int numUsedSlots;
    public OrgListData organisations;
    @SerializableList(elementClass = CharacterData.class)
    public final List<CharacterData> characters = new ArrayList<>();
}
