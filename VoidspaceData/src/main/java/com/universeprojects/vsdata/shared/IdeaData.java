package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.Idea;
import com.universeprojects.vsdata.shared.objects.UPGameObject;

import java.util.Date;

@AutoSerializable(value = {"id", "name", "description", "icon", "baseTimeToRunSecs", "skillType", "parentCategoryId", "gameObject", "designOnly", "createdAt"}, serializationType = SerializationType.MAP)
public final class IdeaData implements Idea {
    public String name;
    public String description;
    public String icon;
    public Long baseTimeToRunSecs;
    public Long id;
    public VoidspaceSkillType skillType;
    public Long parentCategoryId;
    public UPGameObject gameObject;
    public Boolean designOnly;
    public Boolean classUnrelated;
    public Long createdAt;

    @Override
    public String toString() {
        return "IdeaData{" +
                "name='" + name + '\'' +
                ", descr='" + description + '\'' +
                ", icon='" + icon + '\'' +
                ", secs=" + baseTimeToRunSecs +
                '}';
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Long getBaseTimeToRunSecs() {
        return baseTimeToRunSecs;
    }

    @Override
    public VoidspaceSkillType getSkillType() {
        return skillType;
    }

    @Override
    public Long getParentCategoryId() {
        return parentCategoryId;
    }

    @Override
    public Boolean getClassUnrelated() {
        return classUnrelated;
    }

    @Override
    public Date getCreatedDate() {
        return new Date(createdAt);
    }

    public static IdeaData fromIdea(Idea idea) {
        if (idea instanceof IdeaData) return (IdeaData) idea;
        IdeaData ideaData = new IdeaData();
        ideaData.name = idea.getName();
        ideaData.createdAt = idea.getCreatedDate().getTime();
        ideaData.description = idea.getDescription();
        ideaData.icon = idea.getIcon();
        ideaData.baseTimeToRunSecs = idea.getBaseTimeToRunSecs();
        ideaData.id = idea.getId();
        if(idea.getSkillType() != null) {
            ideaData.skillType = VoidspaceSkillType.valueOf(idea.getSkillType().name());
        } else {
            ideaData.skillType = VoidspaceSkillType.Item;
        }
        ideaData.parentCategoryId = idea.getParentCategoryId();
        ideaData.classUnrelated = idea.getClassUnrelated();
        return ideaData;
    }
}
