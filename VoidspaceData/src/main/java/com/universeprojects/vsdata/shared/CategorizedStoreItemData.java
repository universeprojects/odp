package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.List;
import java.util.ArrayList;

@AutoSerializable({"id", "name", "description", "parentCategoryId", "sellItemId", "price", "sellerNickname", "sellAmount"})
public class CategorizedStoreItemData extends CategoryItemData {
    public String sellItemId;
    public CurrencyData price;
    public String sellerNickname;
    public int sellAmount;

    public CategorizedStoreItemData(String id, String sellItemId, String sellItemName, CurrencyData price, String sellerNickname, String categoryId) {
        this(id, sellItemId, sellItemName, price, sellerNickname, categoryId, 1);
    }

    public CategorizedStoreItemData(String id, String sellItemId, String sellItemName, CurrencyData price, String sellerNickname, String categoryId, int sellAmount) {
        super(id, "", sellItemName, "", categoryId);
        this.sellItemId = sellItemId;
        this.sellerNickname = sellerNickname;
        this.price = price;
        this.sellAmount = sellAmount;
    }

    public CategorizedStoreItemData() {
        super();
    }
}