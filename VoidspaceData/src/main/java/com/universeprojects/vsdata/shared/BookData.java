package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable({"id", "name", "authors", "editableMode", "editable", "listOfChapters", "listOfIdeas", "listOfMarkers"})
public class BookData {
    public Long id;
    public String name;
    @SerializableList(elementClass = String.class)
    public List<String> authors = new ArrayList<>();
    public BookEditableMode editableMode;
    public boolean editable;
    @SerializableList(elementClass = BookChapterData.class)
    public final List<BookChapterData> listOfChapters;
    @SerializableList(elementClass = IdeaData.class)
    public List<IdeaData> listOfIdeas;
    @SerializableList(elementClass = MapMarkerData.class)
    public List<MapMarkerData> listOfMarkers;


    public BookData() {
        this.listOfChapters = new ArrayList<>();
    }


    public BookData(Long id, String name, BookEditableMode editableMode, boolean editable, List<BookChapterData> listOfChapters) {
        this.id = id;
        this.name = name;
        this.editableMode = editableMode;
        this.editable = editable;
        this.listOfChapters = listOfChapters;
    }
}