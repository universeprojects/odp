package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.DistributedFieldConfiguration;
import com.universeprojects.gefcommon.shared.elements.UserFieldDistributionConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AutoSerializable(value = {"name", "totalPoints", "fieldConfigurations"}, serializationType = SerializationType.MAP)
public class UserFieldDistributionConfigurationData implements UserFieldDistributionConfiguration {
    public String name;
    public Double totalPoints;
    @SerializableList(elementClass = DistributedFieldConfigurationData.class)
    public List<DistributedFieldConfigurationData> fieldConfigurations;

    public double getTotalPointsPossible() {
        if(fieldConfigurations == null) {
            return 0;
        }
        float points = 0;
        for (DistributedFieldConfigurationData config : fieldConfigurations) {
            points += config.getMaxPoints();
        }
        return points;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Double getTotalPoints() {
        return totalPoints;
    }

    @Override
    public List<DistributedFieldConfigurationData> getFieldConfigurations() {
        return fieldConfigurations;
    }

    public List<DistributedFieldConfigurationData> getFieldConfigurationsSafe() {
        if(fieldConfigurations == null) {
            return Collections.emptyList();
        }
        return fieldConfigurations;
    }

    public static UserFieldDistributionConfigurationData fromConfig(UserFieldDistributionConfiguration config) {
        UserFieldDistributionConfigurationData configData = new UserFieldDistributionConfigurationData();
        configData.name = config.getName();
        configData.totalPoints = config.getTotalPoints();
        if(config.getFieldConfigurations() != null) {
            configData.fieldConfigurations = new ArrayList<>();
            for (DistributedFieldConfiguration distributedConfig : config.getFieldConfigurations()) {
                DistributedFieldConfigurationData distributedConfigData = DistributedFieldConfigurationData.fromConfig(distributedConfig);
                configData.fieldConfigurations.add(distributedConfigData);
            }
        }
        return configData;
    }
}
