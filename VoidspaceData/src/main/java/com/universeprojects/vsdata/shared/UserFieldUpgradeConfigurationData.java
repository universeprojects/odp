package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.UserFieldUpgradeConfiguration;

@AutoSerializable(value = {"fieldName","name", "rarityValue", "rarity"}, serializationType = SerializationType.MAP)
public class UserFieldUpgradeConfigurationData implements UserFieldUpgradeConfiguration {
    public String fieldName;
    public String name;
    public Double rarityValue;
    public String rarity;

    @Override
    public String getFieldName(){
        return fieldName;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Double getRarityValue() {
        return rarityValue;
    }

    @Override
    public String getRarity() {
        return rarity;
    }

    public static UserFieldUpgradeConfigurationData fromConfig(UserFieldUpgradeConfiguration configuration){
        UserFieldUpgradeConfigurationData data = new UserFieldUpgradeConfigurationData();
        data.fieldName = configuration.getFieldName();
        data.name = configuration.getName();
        data.rarityValue = configuration.getRarityValue();
        data.rarity = configuration.getRarity();
        return data;
    }
}
