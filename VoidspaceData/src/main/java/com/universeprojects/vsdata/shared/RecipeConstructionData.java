package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializableMap;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeConstruction;
import com.universeprojects.gefcommon.shared.elements.RecipeConstructionSlot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@AutoSerializable(value = {"slotData", "userFieldValues", "userFieldDistributions", "improvementSkillId", "fieldsToUpgrade"}, serializationType = SerializationType.MAP)
public final class RecipeConstructionData implements RecipeConstruction {
    @SerializableList(elementClass = RecipeConstructionSlotData.class)
    public List<RecipeConstructionSlotData> slotData;
    @SerializableMap(keyClass = String.class, valueClass = String.class)
    public Map<String, String> userFieldValues;
    public Long improvementSkillId;
    @SerializableMap(keyClass = String.class, valueClass = Double.class)
    public Map<String, Double> userFieldDistributions;
    @SerializableList(elementClass = String.class)
    public List<String> fieldsToUpgrade;

    @Override
    public List<RecipeConstructionSlotData> getSlotData() {
        if(slotData == null) {
            return Collections.emptyList();
        }
        return slotData;
    }

    @Override
    public Map<String, String> getUserFieldValues() {
        if(userFieldValues == null) {
            return Collections.emptyMap();
        }
        return userFieldValues;
    }

    @Override
    public Long getImprovementSkillId() {
        return improvementSkillId;
    }

    @Override
    public Map<String, Double> getUserFieldDistributions() {
        if(userFieldDistributions == null) {
            return Collections.emptyMap();
        }
        return userFieldDistributions;
    }

    @Override
    public List<String> getFieldsToUpgrade() {
        if(fieldsToUpgrade==null){
            return Collections.emptyList();
        }
        return fieldsToUpgrade;
    }

    public void addSlot(RecipeConstructionSlotData slot) {
        if (this.slotData == null) {
            this.slotData = new ArrayList<>();
        }
        this.slotData.add(slot);
    }

    public void addUserFieldValue(String targetFieldName, String value) {
        if(this.userFieldValues == null) {
            this.userFieldValues = new LinkedHashMap<>();
        }
        this.userFieldValues.put(targetFieldName, value);
    }

    public void addUserFieldDistribution(String targetFieldName, Double value) {
        if(this.userFieldDistributions == null) {
            this.userFieldDistributions = new LinkedHashMap<>();
        }
        this.userFieldDistributions.put(targetFieldName, value);
   }

   public void addFieldToUpgrade(String targetFieldName){
       if(this.fieldsToUpgrade == null) {
           this.fieldsToUpgrade = new LinkedList<>();
       }
       this.fieldsToUpgrade.add(targetFieldName);
   }

    public RecipeConstructionData copy() {
        RecipeConstructionData constructionDataCopy = new RecipeConstructionData();
        constructionDataCopy.improvementSkillId = improvementSkillId;
        if (slotData != null) {
            constructionDataCopy.slotData = new ArrayList<>();
            for (RecipeConstructionSlotData slot : slotData) {
                RecipeConstructionSlotData slotDataCopy = slot.copy();
                constructionDataCopy.slotData.add(slotDataCopy);
            }
        }
        if(userFieldValues != null) {
            constructionDataCopy.userFieldValues = new LinkedHashMap<>(userFieldValues);
        }
        if(userFieldDistributions != null) {
            constructionDataCopy.userFieldDistributions = new LinkedHashMap<>(userFieldDistributions);
        }
        if(fieldsToUpgrade!=null){
            constructionDataCopy.fieldsToUpgrade = new LinkedList<>(fieldsToUpgrade);
        }
        return constructionDataCopy;
    }

    public static RecipeConstructionData fromRecipeConstruction(RecipeConstruction construction) {
        if (construction instanceof RecipeConstructionData) return (RecipeConstructionData) construction;
        RecipeConstructionData constructionData = new RecipeConstructionData();
        constructionData.improvementSkillId = construction.getImprovementSkillId();
        if (construction.getSlotData() != null) {
            constructionData.slotData = new ArrayList<>();
            for (RecipeConstructionSlot slot : construction.getSlotData()) {
                RecipeConstructionSlotData slotData = RecipeConstructionSlotData.fromRecipeConstructionSlot(slot);
                constructionData.slotData.add(slotData);
            }
        }
        if(construction.getUserFieldValues() != null) {
            constructionData.userFieldValues = new LinkedHashMap<>(construction.getUserFieldValues());
        }
        if(construction.getUserFieldDistributions() != null) {
            constructionData.userFieldDistributions = new LinkedHashMap<>(construction.getUserFieldDistributions());
        }
        return constructionData;
    }
}
