package com.universeprojects.vsdata.shared.objects;

import com.universeprojects.common.shared.callable.Callable1Args;
import com.universeprojects.common.shared.math.UPVector;
import com.universeprojects.gefcommon.shared.elements.GameObject;
import com.universeprojects.json.shared.serialization.SerializedDataList;
import com.universeprojects.json.shared.serialization.SerializedDataMap;
import com.universeprojects.vsdata.shared.SnapshotGameObject;

import java.util.*;

@SuppressWarnings({"ArraysAsListWithZeroOrOneArgument", "unused"})
public final class UPGameObject implements SnapshotGameObject<String> {

    public static final List<String> coreAspectClasses = Arrays.asList("UPPhysicalCoreAspect");
    public static final List<String> quantityAspectClasses = Arrays.asList("SimpleStackableAspect");
    public static final List<String> durabilityAspectClasses = Arrays.asList("DamageableAspect");
    public static final List<String> characterAspectClasses = Arrays.asList("CharacterAspect");

    private Map<String, Object> additionalProperties = null;

    private final Map<String, UPGameAspect> aspectMap;
    private final Map<String, UPGameAspect> aspectMap_view;
    private Set<String> addedAspectNames;
    private String uid;
    private UPGameAspect coreAspect;
    private UPGameAspect quantityAspect;
    private UPGameAspect durabilityAspect;
    private UPGameAspect characterAspect;

    private boolean generatedUid = false;
    private boolean destroyed = false;
    private String creatorUid;
    private Long creationTick;
    private SerializedDataMap<String, SerializedDataList> eventSubscriptions;
    private SerializedDataList runningEvents;
    private Set<String> tags;
    private UPVector actualLocation;

    public UPGameObject(String uid, Collection<UPGameAspect> aspects) {
        this(uid, toAspectMap(aspects));
    }

    public static UPGameObject fromGameObjectWithoutLinks(com.universeprojects.gefcommon.shared.elements.GameObject<String> source) {
        return fromGameObject(source, false);
    }

    public static UPGameObject fromGameObjectWithLinks(com.universeprojects.gefcommon.shared.elements.GameObject<String> source) {
        return fromGameObject(source, true);
    }

    public static UPGameObject fromGameObject(com.universeprojects.gefcommon.shared.elements.GameObject<String> source, boolean copyLinks) {
        if (source == null) return null;

        if (source instanceof UPGameObject) return (UPGameObject) source;

        return copyFromGameObject(source, copyLinks);
    }

    public static UPGameObject copyFromGameObject(com.universeprojects.gefcommon.shared.elements.GameObject<String> source, boolean copyLinks) {
        Set<UPGameAspect> gameAspects = new LinkedHashSet<>();
        for (String aspectName : source.getAspectNames()) {
            UPGameAspect gameAspect = UPGameAspect.copyFromGameAspect(source.getAspect(aspectName), copyLinks);

            gameAspects.add(gameAspect);
        }

        return new UPGameObject(source.getKey(), gameAspects);
    }

    public static UPGameObject fromSnapshotGameObject(SnapshotGameObject<String> gameObject) {
        if (gameObject instanceof UPGameObject) return (UPGameObject) gameObject;
        UPGameObject upGameObject = fromGameObjectWithoutLinks(gameObject);
        upGameObject.setDestroyed(gameObject.isDestroyed());
        upGameObject.setCreatorUid(gameObject.getCreatorUid());
        upGameObject.setCreationTick(gameObject.getCreationTick());
        upGameObject.setEventSubscriptions(gameObject.getEventSubscriptions());
        upGameObject.setRunningEvents(gameObject.getRunningEvents());
        upGameObject.setActualLocation(gameObject.getActualLocation());
        upGameObject.setTags(gameObject.getTags());
        return upGameObject;
    }

    public static UPGameObject fromSnapshotGameObjectWithLinks(SnapshotGameObject<String> gameObject) {
        if (gameObject instanceof UPGameObject) return (UPGameObject) gameObject;
        UPGameObject upGameObject = fromGameObjectWithLinks(gameObject);
        upGameObject.setDestroyed(gameObject.isDestroyed());
        upGameObject.setCreatorUid(gameObject.getCreatorUid());
        upGameObject.setCreationTick(gameObject.getCreationTick());
        upGameObject.setEventSubscriptions(gameObject.getEventSubscriptions());
        upGameObject.setRunningEvents(gameObject.getRunningEvents());
        upGameObject.setActualLocation(gameObject.getActualLocation());
        upGameObject.setTags(gameObject.getTags());
        return upGameObject;
    }

    public static UPGameObject asDestroyed(String uid) {
        return new UPGameObject(uid);
    }

    private UPGameObject(String uid) {
        this.uid = uid;
        this.aspectMap = Collections.emptyMap();
        this.aspectMap_view = Collections.emptyMap();
        this.coreAspect = null;
        this.quantityAspect = null;
        this.durabilityAspect = null;
        this.characterAspect = null;
        this.destroyed = true;
    }

    public UPGameObject(String uid, Map<String, UPGameAspect> aspectMap) {
        this(uid, false, aspectMap);
    }

    public UPGameObject(String uid, boolean generatedUid, Map<String, UPGameAspect> aspectMap) {
        this.uid = uid;
        this.generatedUid = generatedUid;
        this.aspectMap = new HashMap<>(aspectMap);
        this.aspectMap_view = Collections.unmodifiableMap(this.aspectMap);
        configureAspects();
    }

    @Override
    public UPGameAspect addAspect(String aspectName) {
        if(aspectMap.containsKey(aspectName)) {
            return aspectMap.get(aspectName);
        }
        UPGameAspect aspect = new UPGameAspect(aspectName);
        aspectMap.put(aspectName, aspect);
        if(addedAspectNames == null) {
            addedAspectNames = new HashSet<>();
        }
        addedAspectNames.add(aspectName);
        configureAspects();
        return aspect;
    }

    @Override
    public void removeAspect(String aspectName) {
        throw new UnsupportedOperationException("Unable to remove aspects from live objects in Voidspace.");
    }

    public void setAddedAspectNames(Set<String> set) {
        this.addedAspectNames = set;
    }

    public Set<String> getAddedAspectNames() {
        if(addedAspectNames == null) {
            return Collections.emptySet();
        }
        return addedAspectNames;
    }

    private void configureAspects() {
        this.coreAspect = findMatchingAspect(aspectMap, coreAspectClasses, true);
        this.quantityAspect = findMatchingAspect(aspectMap, quantityAspectClasses, false);
        this.durabilityAspect = findMatchingAspect(aspectMap, durabilityAspectClasses, false);
        this.characterAspect = findMatchingAspect(aspectMap, characterAspectClasses, false);
        for (UPGameAspect gameAspect : aspectMap.values()) {
            gameAspect.gameObject = this;
        }
    }

    public void setUid(String uid, boolean generatedUid) {
        this.uid = uid;
        this.generatedUid = generatedUid;
    }

    public UPGameObject copy() {
        return UPGameObject.copyFromGameObject(this, true);
    }

    public UPGameAspect getCoreAspect() {
        return coreAspect;
    }

    private static UPGameAspect findMatchingAspect(Map<String, UPGameAspect> aspectMap, List<String> classes, boolean coreAspect) {
        UPGameAspect foundAspect = null;
        for (String name : classes) {
            UPGameAspect aspect = aspectMap.get(name);
            if (aspect != null) {
                if (foundAspect != null) {
                    //noinspection ObjectToString
                    throw new IllegalArgumentException("More than one core-aspect was supplied: " + foundAspect + ", " + aspect);
                }
                foundAspect = aspect;
                if (!coreAspect) break;
            }
        }
        if (foundAspect == null && coreAspect) {
            throw new IllegalArgumentException("No core-aspect was supplied");
        }
        return foundAspect;
    }

    public boolean hasTag(String tag) {
        return tags != null && tags.contains(tag);
    }

    public Long getQuantityOrNull() {
        if (quantityAspect != null) {
            return (Long) quantityAspect.getProperty("FIELD_Quantity");
        }
        return null;
    }

    public Collection<UPGameAspect> getAspects() {
        return aspectMap_view.values();
    }

    @Override
    public String getSchemaKind() {
        return "GameObject";
    }

    @Override
    public String getKey() {
        if(generatedUid) {
            String name = getName();
            if(name != null) {
                return name.replaceAll("\\s", "")+"."+uid;
            } else {
                return "CMDGEN."+uid;
            }
        }
        return uid;
    }

    public String getUid() {
        return uid;
    }

    @Override
    public String getName() {
        if(characterAspect != null) {
            String name = (String) characterAspect.getProperty("FIELD_Name");
            if(name != null) return name;
        }
        String name = (String) coreAspect.getProperty("FIELD_Name");
        if(name != null) return name;
        return uid;
    }

    @Override
    public Long getQuantity() {
        final Long quantity = getQuantityOrNull();
        if(quantity == null) {
            return 1L;
        } else {
            return quantity;
        }
    }

    @Override
    public void setQuantity(Long value) {
        if (quantityAspect != null) {
            quantityAspect.setProperty("FIELD_Quantity", value);
        }
    }

    @Override
    public Double getMass() {
        if (coreAspect != null) {
            return (Double) coreAspect.getProperty("FIELD_SelfMass");
        }
        return null;
    }

    @Override
    public void setMass(Double value) {
        if (coreAspect != null) {
            coreAspect.setProperty("FIELD_SelfMass", value);
        }
    }


    @Override
    public Double getVolume() {
        if (coreAspect != null) {
            return (Double) coreAspect.getProperty("FIELD_Volume");
        }
        return null;
    }

    @Override
    public void setVolume(Double value) {
        if (coreAspect != null) {
            coreAspect.setProperty("FIELD_Volume", value);
        }
    }


    @Override
    public Long getDurability() {
        if (durabilityAspect != null && durabilityAspect.getProperty("FIELD_Health") != null) {
            return ((Number) durabilityAspect.getProperty("FIELD_Health")).longValue();
        }
        return null;
    }

    @Override
    public void setDurability(Long value) {
        if (durabilityAspect != null) {
            durabilityAspect.setProperty("FIELD_Health", value.floatValue());
        }
    }

    @Override
    public Object getProperty(String fieldName) {
        if(fieldName.startsWith("FIELD_")) {
            return coreAspect.getProperty(fieldName);
        } else {
            if(additionalProperties == null) {
                return null;
            } else {
                return additionalProperties.get(fieldName);
            }
        }
    }

    @Override
    public void setProperty(String fieldName, Object value) {
        if(fieldName.startsWith("FIELD_")) {
            coreAspect.setProperty(fieldName, value);
        } else {
            if(additionalProperties == null) {
                additionalProperties = new HashMap<>();
            }
            additionalProperties.put(fieldName, value);
        }
    }

    @Override
    public Collection<String> getPropertyNames() {
        return coreAspect.getPropertyNames();
    }

    @Override
    public Collection<String> getAspectNames() {
        return aspectMap_view.keySet();
    }

    @Override
    public UPGameAspect getAspect(String aspectName) {
        return aspectMap.get(aspectName);
    }

    @Override
    public boolean hasAspect(String aspectName) {
        return aspectMap.containsKey(aspectName);
    }

    @Override
    public SerializedDataList getRunningEvents() {
        return runningEvents;
    }

    public void setRunningEvents(SerializedDataList runningEvents) {
        this.runningEvents = runningEvents;
    }

    @Override
    public SerializedDataMap<String, SerializedDataList> getEventSubscriptions() {
        return eventSubscriptions;
    }

    public void setEventSubscriptions(SerializedDataMap<String, SerializedDataList> eventSubscriptions) {
        this.eventSubscriptions = eventSubscriptions;
    }

    @Override
    public Long getCreationTick() {
        return creationTick;
    }

    public void setCreationTick(Long creationTick) {
        this.creationTick = creationTick;
    }

    @Override
    public String getCreatorUid() {
        return creatorUid;
    }

    public void setCreatorUid(String creatorUid) {
        this.creatorUid = creatorUid;
    }

    @Override
    public boolean isDestroyed() {
        return destroyed;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    @Override
    public String getItemClass() {
        return String.valueOf(coreAspect.getProperty("FIELD_Class"));
    }

    public void setItemClass(String itemClass) {
        coreAspect.setProperty("FIELD_Class", itemClass);
    }

    @Override
    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public void addTag(String tag) {
        if(tags == null) {
            tags = new HashSet<>();
        }
        tags.add(tag);
    }

    public void applyRecursivelyToObjects(Callable1Args<UPGameObject> callable) {
        callable.call(this);
        for (UPGameAspect gameAspect : aspectMap.values()) {
            for (GameObject<String> gameObject : gameAspect.getSubObjectsSingle().values()) {
                ((UPGameObject)gameObject).applyRecursivelyToObjects(callable);
            }
            for (List<GameObject<String>> list : gameAspect.getSubObjectsList().values()) {
                for (GameObject<String> gameObject : list) {
                    ((UPGameObject)gameObject).applyRecursivelyToObjects(callable);
                }
            }
            for (Map<?, GameObject<String>> map : gameAspect.getSubObjectsMap().values()) {
                for (GameObject<String> gameObject : map.values()) {
                    ((UPGameObject)gameObject).applyRecursivelyToObjects(callable);
                }
            }
        }
    }

    public void removeTag(String tag) {
        if(tags == null) {
            return;
        }
        tags.remove(tag);
    }

    @Override
    public UPVector getActualLocation() {
        return actualLocation;
    }

    public void setActualLocation(UPVector actualLocation) {
        this.actualLocation = actualLocation;
    }

    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public void setAdditionalProperties(Map<String, Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    private static Map<String, UPGameAspect> toAspectMap(Collection<UPGameAspect> collection) {
        Map<String, UPGameAspect> map = new TreeMap<>();
        for (UPGameAspect aspect : collection) {
            map.put(aspect.aspectName, aspect);
        }
        return map;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UPGameObject that = (UPGameObject) o;
        return Objects.equals(uid, that.uid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid);
    }

    @Override
    public String toString() {
        return "UPGameObject{" +
            "uid='" + uid + '\'' +
            ", destroyed=" + destroyed +
            ", aspects=" + aspectMap.keySet() +
            '}';
    }
}
