package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.json.shared.JSONObject;

@AutoSerializable(value = {"spawnType", "name", "description", "iconKey", "spawnParams"}, serializationType = SerializationType.MAP)
public class SpawnOptionData{
    public String spawnType;
    public String name;
    public String description;
    public String iconKey;
    public JSONObject spawnParams;

    public SpawnOptionData(){
    }

    public SpawnOptionData(String spawnType, String name, String description, String iconKey) {
        this(spawnType, name, description, iconKey, null);
    }

    public SpawnOptionData(String spawnType, String name, String description, String iconKey, JSONObject spawnParams){
        this.spawnType =spawnType;
        this.name=name;
        this.description=description;
        this.iconKey = iconKey;
        this.spawnParams = spawnParams;
    }

    @Override
    public String toString() {
        return "SpawnOptionData{" +
            "spawnType='" + spawnType + '\'' +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", iconKey='" + iconKey + '\'' +
            ", spawnParams=" + spawnParams +
            '}';
    }
}