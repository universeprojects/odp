package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.Recipe;
import com.universeprojects.gefcommon.shared.elements.RecipeCategory;
import com.universeprojects.gefcommon.shared.elements.UserFieldDistributionConfiguration;
import com.universeprojects.gefcommon.shared.elements.UserFieldUpgradeConfiguration;
import com.universeprojects.gefcommon.shared.elements.UserFieldValueConfiguration;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@AutoSerializable(value = {"name", "description", "categories", "userFieldValueConfigurations", "userFieldDistributionConfigurations", "userFieldUpgradeConfigurations"}, serializationType = SerializationType.MAP)
public final class RecipeData implements Recipe {
    public String name;
    public String description;
    @SerializableList(elementClass = RecipeCategoryData.class)
    public List<RecipeCategoryData> categories;
    @SerializableList(elementClass = UserFieldValueConfigurationData.class)
    public List<UserFieldValueConfigurationData> userFieldValueConfigurations;
    @SerializableList(elementClass = UserFieldDistributionConfigurationData.class)
    public List<UserFieldDistributionConfigurationData> userFieldDistributionConfigurations;
    @SerializableList(elementClass = UserFieldUpgradeConfigurationData.class)
    public List<UserFieldUpgradeConfigurationData> userFieldUpgradeConfigurations;


    public boolean needsConfiguration() {
        return getUserFieldDistributionConfigurationsSafe().size() > 0 || getUserFieldValueConfigurationsSafe().size() > 0 || getUserFieldUpgradeConfigurationsSafe().size()>0;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public List<RecipeCategoryData> getCategories() {
        return categories;
    }

    public List<RecipeCategoryData> getCategoriesSafe() {
        if(categories == null) {
            return Collections.emptyList();
        }
        return categories;
    }

    @Override
    public List<UserFieldDistributionConfigurationData> getUserFieldDistributionConfigurations() {
        return userFieldDistributionConfigurations;
    }

    @Override
    public List<? extends UserFieldUpgradeConfiguration> getUserFieldUpgradeConfigurations() {
        return userFieldUpgradeConfigurations;
    }

    public List<UserFieldDistributionConfigurationData> getUserFieldDistributionConfigurationsSafe() {
        if(userFieldDistributionConfigurations == null) {
            return Collections.emptyList();
        }
        return userFieldDistributionConfigurations;
    }

    public List<UserFieldUpgradeConfigurationData> getUserFieldUpgradeConfigurationsSafe() {
        if(userFieldUpgradeConfigurations == null) {
            return Collections.emptyList();
        }
        return userFieldUpgradeConfigurations;
    }

    @Override
    public List<UserFieldValueConfigurationData> getUserFieldValueConfigurations() {
        return userFieldValueConfigurations;
    }

    public List<UserFieldValueConfigurationData> getUserFieldValueConfigurationsSafe() {
        if(userFieldValueConfigurations == null) {
            return Collections.emptyList();
        }
        return userFieldValueConfigurations;
    }


    public static RecipeData fromRecipe(Recipe recipe) {
        if (recipe instanceof RecipeData) return (RecipeData) recipe;
        RecipeData recipeData = new RecipeData();
        recipeData.name = recipe.getName();
        recipeData.description = recipe.getDescription();
        if (recipe.getCategories() != null) {
            recipeData.categories = new ArrayList<>();
            for (RecipeCategory category : recipe.getCategories()) {
                RecipeCategoryData categoryData = RecipeCategoryData.fromRecipeCategory(category);
                recipeData.categories.add(categoryData);
            }
        }
        if(recipe.getUserFieldValueConfigurations() != null) {
            recipeData.userFieldValueConfigurations = new ArrayList<>();
            for (UserFieldValueConfiguration config : recipe.getUserFieldValueConfigurations()) {
                UserFieldValueConfigurationData configData = UserFieldValueConfigurationData.fromConfig(config);
                recipeData.userFieldValueConfigurations.add(configData);
            }
        }
        if(recipe.getUserFieldDistributionConfigurations() != null) {
            recipeData.userFieldDistributionConfigurations = new ArrayList<>();
            for (UserFieldDistributionConfiguration config : recipe.getUserFieldDistributionConfigurations()) {
                UserFieldDistributionConfigurationData configData = UserFieldDistributionConfigurationData.fromConfig(config);
                recipeData.userFieldDistributionConfigurations.add(configData);
            }

        }
        if(recipe.getUserFieldUpgradeConfigurations()!=null){
            recipeData.userFieldUpgradeConfigurations = new ArrayList<>();
            for (UserFieldUpgradeConfiguration config : recipe.getUserFieldUpgradeConfigurations()) {
                UserFieldUpgradeConfigurationData configData = UserFieldUpgradeConfigurationData.fromConfig(config);
                recipeData.userFieldUpgradeConfigurations.add(configData);
            }
        }
        return recipeData;
    }
}
