package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

@AutoSerializable({"name", "icon", "amount"})
public class CurrencyData {
    public String name;
    public String icon;
    public long amount;

    public CurrencyData() {
    }

    public CurrencyData(String name, String icon, long amount) {
        this.name = name;
        this.icon = icon;
        this.amount = amount;
    }
}