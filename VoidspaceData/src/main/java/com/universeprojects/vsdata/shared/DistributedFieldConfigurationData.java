package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.DistributedFieldConfiguration;

@AutoSerializable(value = {"fieldName", "displayName", "description", "icon",
    "maxPoints", "baseMinValue", "baseMaxValue", "multiplierMinValue", "multiplierMaxValue"}, serializationType = SerializationType.MAP)
public class DistributedFieldConfigurationData implements DistributedFieldConfiguration {
    public String fieldName;
    public String displayName;
    public String description;
    public String icon;
    public Double maxPoints;
    public Double baseMinValue;
    public Double baseMaxValue;
    public Double multiplierMinValue;
    public Double multiplierMaxValue;


    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Double getMaxPoints() {
        return maxPoints;
    }

    @Override
    public Double getBaseMinValue() {
        return baseMinValue;
    }

    @Override
    public Double getBaseMaxValue() {
        return baseMaxValue;
    }

    @Override
    public Double getMultiplierMinValue() {
        return multiplierMinValue;
    }

    @Override
    public Double getMultiplierMaxValue() {
        return multiplierMaxValue;
    }

    public static DistributedFieldConfigurationData fromConfig(DistributedFieldConfiguration config) {
        DistributedFieldConfigurationData configData = new DistributedFieldConfigurationData();
        configData.fieldName = config.getFieldName();
        configData.displayName = config.getDisplayName();
        configData.description = config.getDescription();
        configData.icon = config.getIcon();
        configData.maxPoints = config.getMaxPoints();
        configData.baseMinValue = config.getBaseMinValue();
        configData.baseMaxValue = config.getBaseMaxValue();
        configData.multiplierMinValue = config.getMultiplierMinValue();
        configData.multiplierMaxValue = config.getMultiplierMaxValue();
        return configData;
    }
}
