package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.json.shared.JSONObject;

@AutoSerializable({"text", "metadata"})
public class ProblemData {
    public String text;
    public JSONObject metadata;

    public ProblemData(){ }

    public ProblemData(String text) {
        this.text=text;
    }
}

