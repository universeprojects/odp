package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable(value = {"items"}, serializationType = SerializationType.MAP)
public class OrgMemberData {

    @SerializableList(elementClass = OrgMemberDataItem.class)
    public List<OrgMemberDataItem> items = new ArrayList<>();

    public OrgMemberData() {
    }

    public static OrgMemberData createNew() {
        return new OrgMemberData();
    }

    public OrgMemberData add(OrgMemberData other) {
        for (OrgMemberDataItem item : other.items) {
            add(item.name, item.memberID, item.isAdmin, item.isMod, item.position, item.joinDate);
        }
        return this;
    }

    public OrgMemberData add(String name, long memberID, boolean isAdmin, boolean isMod, String position, String joinDate) {
        int index = items.size();
        items.add(new OrgMemberDataItem(index, name, memberID, isAdmin, isMod, position, joinDate));
        return this;
    }

    public OrgMemberDataItem get(int index) {
        return items.get(index);
    }

    public int size() {
        return items.size();
    }

    @AutoSerializable({"index", "name", "memberID", "isAdmin", "isMod", "position", "joinDate"})
    public static class OrgMemberDataItem {
        public int index;
        public String name;
        public long memberID;
        public boolean isAdmin;
        public boolean isMod;
        public String position;
        public String joinDate;

        public OrgMemberDataItem(int index, String name, long memberID, boolean isAdmin, boolean isMod, String position, String joinDate) {
            this.index = index;
            this.name = name;
            this.memberID = memberID;
            this.isAdmin = isAdmin;
            this.isMod = isMod;
            this.position = position;
            this.joinDate = joinDate;
        }

        public OrgMemberDataItem() {
        }
    }
}
