package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;

import java.util.List;

@AutoSerializable({"id", "name","markers"})
public class MapMarkerGroupData {
    public String id;
    public String name;
    @SerializableList(elementClass = MapMarkerData.class)
    public List<MapMarkerData> markers;

    public MapMarkerGroupData() {
    }

    public MapMarkerGroupData(String id, String name, List<MapMarkerData> markers) {
        this.id = id;
        this.name = name;
        this.markers = markers;
    }
}
