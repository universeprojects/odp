package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.GameObject;
import com.universeprojects.gefcommon.shared.elements.RecipeConstructionSlot;

@AutoSerializable(value = {"slotId", "objectUid", "object"}, serializationType = SerializationType.MAP)
public final class RecipeConstructionSlotData implements RecipeConstructionSlot {
    public String slotId;
    public String objectUid;
    public GameObject<String> object;

    @Override
    public String getSlotId() {
        return slotId;
    }

    @Override
    public GameObject<String> getObject() {
        return object;
    }

    public static RecipeConstructionSlotData fromRecipeConstructionSlot(RecipeConstructionSlot slot) {
        if (slot instanceof RecipeConstructionSlotData) return (RecipeConstructionSlotData) slot;
        RecipeConstructionSlotData slotData = new RecipeConstructionSlotData();
        slotData.slotId = slot.getSlotId();
        //noinspection unchecked
        slotData.object = slot.getObject();
        return slotData;
    }

    public RecipeConstructionSlotData copy() {
        RecipeConstructionSlotData slotDataCopy = new RecipeConstructionSlotData();
        slotDataCopy.slotId = slotId;
        slotDataCopy.object = object;
        slotDataCopy.objectUid = objectUid;
        return slotDataCopy;
    }
}
