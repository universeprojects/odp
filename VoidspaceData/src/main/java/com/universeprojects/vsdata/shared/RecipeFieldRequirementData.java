package com.universeprojects.vsdata.shared;

import com.universeprojects.common.shared.annotations.AutoSerializable;
import com.universeprojects.common.shared.annotations.SerializableList;
import com.universeprojects.common.shared.annotations.SerializationType;
import com.universeprojects.gefcommon.shared.elements.RecipeFieldRequirement;
import com.universeprojects.gefcommon.shared.elements.RecipeFieldRequirementOption;

import java.util.ArrayList;
import java.util.List;

@AutoSerializable(value = {"fieldOptions"}, serializationType = SerializationType.MAP)
public final class RecipeFieldRequirementData implements RecipeFieldRequirement {

    @SerializableList(elementClass = RecipeFieldRequirementOptionData.class)
    public List<RecipeFieldRequirementOptionData> fieldOptions;

    @Override
    public List<RecipeFieldRequirementOptionData> getFieldOptions() {
        return fieldOptions;
    }

    public static RecipeFieldRequirementData fromRecipeFieldRequirement(RecipeFieldRequirement requirement) {
        if (requirement instanceof RecipeFieldRequirementData) return (RecipeFieldRequirementData) requirement;
        RecipeFieldRequirementData fieldRequirementData = new RecipeFieldRequirementData();
        fieldRequirementData.fieldOptions = new ArrayList<>();
        for (RecipeFieldRequirementOption option : requirement.getFieldOptions()) {
            RecipeFieldRequirementOptionData optionData = RecipeFieldRequirementOptionData.fromRecipeFieldRequirementOption(option);
            fieldRequirementData.fieldOptions.add(optionData);
        }
        return fieldRequirementData;
    }
}
