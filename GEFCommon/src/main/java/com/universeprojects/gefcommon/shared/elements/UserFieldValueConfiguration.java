package com.universeprojects.gefcommon.shared.elements;

public interface UserFieldValueConfiguration {
    String getDescription();
    UserFieldValueConfigurationType getType();
    String getTypeConfiguration();
    String getTargetFieldName();
    Integer getMinLength();
    Integer getMaxLength();
    Boolean isRequired();
    String getFormattingErrorMessage();
}
