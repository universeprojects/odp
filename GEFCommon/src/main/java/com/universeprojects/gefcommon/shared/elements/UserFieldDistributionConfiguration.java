package com.universeprojects.gefcommon.shared.elements;

import java.util.List;

public interface UserFieldDistributionConfiguration {
    String getName();
    Double getTotalPoints();
    List<? extends DistributedFieldConfiguration> getFieldConfigurations();
}
